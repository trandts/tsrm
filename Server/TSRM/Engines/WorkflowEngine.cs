﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using TSRM.Models;
using Emailing;
using System.Configuration;
using TSRM.Signalr;
using TSRM.Extentions;

namespace TSRM.Engines
{
    public class WorkflowEngineStateInstanceResult
    {
        public List<StateInstance> NewInstances { get; set; }
        public List<int> NewActivatedInstancesId { get; set; }
    }
    public class stateInfo
    {
        public State newState { get; set; }
        public int oldStateId { get; set; }
    }
    public class WorkflowEngine
    {
        public static
            WorkflowEngineStateInstanceResult CreateStateInstance(
             string context, int StateId, int parentActualStateInstanceId, int User, bool IsInstanceActive)
        {
            var db = new TSRMDBContext();
            var s = db.States.Find(StateId);
            var states = new List<State>();
            var user = db.User.Find(User);
            var parentSI = db.StateInstances.Find(parentActualStateInstanceId);

            StateInstance si = db.StateInstances.Add(new StateInstance
            {
                Context = context,
                Requestor = user,
                ParentInstanceId = parentActualStateInstanceId,
                State = s,
                Status = "Active",
                CreatedBy = User,
                CreatedOn = DateTime.Now,
                internalContext_ParentStateInstance = (parentSI?.internalContext_ParentStateInstance ?? "") + (parentActualStateInstanceId != 0 ? ("/" + parentActualStateInstanceId) : ""),
                internalContext_ParentStateInstanceContext = (parentSI?.internalContext_ParentStateInstanceContext ?? "") + (parentActualStateInstanceId != 0 ? ("/" + context) : "")
            });
            db.SaveChanges();
            db.StateInstances.Attach(si);
            var ListStateInstances = new List<StateInstance>() { si };
            var Ids = IsInstanceActive ? new List<int> { si.Id } : new List<int>();
            var childStates = db.States
                .Where(e => e.IsDeleted == false && e.ParentStateId == StateId && e.IsMultiInstance == false
                && e.StateType.Id == si.State.StateType.Id && e.ActiveTo == null)
              .ToList();
            foreach (var item in childStates)
            {
                var IsChildInstanceActive = false;
                if (IsInstanceActive && (s.Gate.Name == "AND" || (s.Gate.Name == "XOR" && item.IsStart)))
                {
                    IsChildInstanceActive = IsInstanceActive;
                }
                var childResult = CreateStateInstance(context, item.Id, si.Id, User, IsChildInstanceActive);
                ListStateInstances.AddRange(childResult.NewInstances);
                Ids.AddRange(childResult.NewActivatedInstancesId);
            }
            return new WorkflowEngineStateInstanceResult { NewInstances = ListStateInstances, NewActivatedInstancesId = Ids };
        }
        public static int CreateWorkflowInstance(
            int User, string context, int StateId, int parentStateInstanceId, string Event, bool IsActive)
        {

            var db = new TSRMDBContext();
            var state = db.States.Find(StateId);
            //var ss = wf_ss.FirstOrDefault(e => e.Id == actualStateId);

            if (db.Transitions.Any(f => f.Event.EventName == Event && f.FromState.Id == StateId) || Event == null || state.ParentStateId == 0)
            {
                var siIDs = new List<int>();
                var siR = CreateStateInstance(context, state.Id, parentStateInstanceId, User, IsActive);
                foreach (var item in siR.NewActivatedInstancesId)
                {
                    siIDs.Add(item);
                }

                var user = db.User.Find(User);
                var IsAnd = state.Gate.Name == "AND";
                var wf_ss = GetStartStates(StateId);
                user = db.User.Find(User);
                var actualStateInstance = siR.NewInstances.Find(e => e.State.Id == StateId).Id;
                if (siIDs.Count() > 0 && IsActive)
                {
                    var h = db.Histories.Add(new History { User = user, CreatedOn = DateTime.Now });
                    db.SaveChanges();
                    db.Histories.Attach(h);
                    var activeStateInstances = db.StateInstances.Where(e => siIDs.Contains(e.Id)).ToList();
                    for (int i = 0; i < activeStateInstances.Count(); i++)
                    {
                        var asi = activeStateInstances[i];
                        asi = db.StateInstances.Find(asi.Id);
                        if (asi.State.EnterNonTriggerActivity != null)
                        {
                            if (!ExecuteNonTriggerActivities(asi.State.EnterNonTriggerActivity, asi.Id, Event, User))
                            {
                                return 0;
                            }
                        }
                        var CtxId = 0;
                        int.TryParse(asi.Context, out CtxId);
                        string ctxName = null;
                        if (CtxId != 0)
                        {
                            switch (asi.State?.Case?.Name)
                            {
                                case "System":
                                    break;
                                case "Organization":
                                    ctxName = db.Organizations.Find(CtxId).Name;
                                    break;
                                case "Project":
                                    ctxName = db.Projects.Find(CtxId).Name;
                                    break;
                                case "Task":
                                    ctxName = db.Tasks.Find(CtxId).Name;
                                    break;
                                default:
                                    break;
                            }
                        }
                        var _task = asi.State.TaskConfiguration == null ? null : new Task { Name = asi.State.TaskConfiguration.TaskName, ToDisplay = true, StateInstanceId = asi.Id, Case = asi.State.Case.Name, CaseContextId = CtxId, CaseContext = ctxName, IsCompleted = false, ModifiedBy = User, ModifiedOn = DateTime.Now, CreatedBy = User, CreatedOn = DateTime.Now };
                        db.HistoryStateInstances.Add(new HistoryStateInstance
                        {
                            CurrentStateInstance = asi,
                            History = h,
                            Task = _task,
                            ModifiedBy = User,
                            ModifiedOn = DateTime.Now,
                            CreatedBy = User,
                            CreatedOn = DateTime.Now
                        });
                        db.SaveChanges();
                        EmailTaskCreated(_task?.Id, asi.Id, User);
                    }

                    if (state.ParentStateId == 0 || Event == null)
                    {

                    }
                    else
                    {

                        EventWorkflowInstance(Event, actualStateInstance, User, context);//, StateId
                        db.SaveChanges();
                    }
                }

                return actualStateInstance;
            }
            else
            {
                return 0;
            }
        }
        public static int EventWorkflowInstance(string Event, int StateInstance, int User, string context)
        {
            var db = new TSRMDBContext();
            var si = db.StateInstances.Find(StateInstance);
            var ParentInstance = db.StateInstances.Find(si.ParentInstanceId);
            int StateId=si?.State?.Id??0;
            var transition = db.Transitions.Where(e => e.Event.EventName == Event && e.FromState.Id == StateId
            && e.ToState.ActiveFrom < si.CreatedOn && (e.ToState.ActiveTo == null || e.ToState.ActiveTo > si.CreatedOn)).FirstOrDefault();
            if (transition == null || !db.HistoryStateInstances.Any(hsi => hsi.CurrentStateInstance.Id == StateInstance && hsi.DisableHistoryId == 0))
            {
                return 0;
            }
            var ToStateId = transition.ToState.Id;

            StateInstance ToStateInstance = null;
            WorkflowEngineStateInstanceResult siR = null;
            if (transition.ToState.IsMultiInstance)
            {
                if (db.StateInstances.Any(e => e.Context == context && e.State.Id == ToStateId && e.ParentInstanceId == si.ParentInstanceId))
                {
                    return 0;
                }
                siR = CreateStateInstance(context, ToStateId, si.ParentInstanceId, User, true);
                ToStateInstance = db.StateInstances.Find(siR.NewInstances.Find(e => e.State.Id == ToStateId).Id);
            }
            else
            {
                ToStateInstance = db.StateInstances.FirstOrDefault(e => e.ParentInstanceId == si.ParentInstanceId && e.Context == si.Context && e.State.Id == ToStateId);
            }
            if (ToStateInstance == null)
            {
                siR = CreateStateInstance(context, ToStateId, si.ParentInstanceId, User, true);
                ToStateInstance = db.StateInstances.Find(siR.NewInstances.Find(e => e.State.Id == ToStateId).Id);
            }
            db = new TSRMDBContext();
            si = db.StateInstances.Find(StateInstance);
            transition = db.Transitions.FirstOrDefault(e => e.Id == transition.Id);
            var s = db.States.Find(StateId);
            //filter Entry/Exit States
            var ExitStateInstances = transition.ToState.IsMultiInstance ? new List<StateInstance> { } : new List<StateInstance> { si };

            for (int i = 0; i < ExitStateInstances.Count(); i++)
            {
                var asi = ExitStateInstances[i];
                asi = db.StateInstances.Find(asi.Id);
                if (db.HistoryStateInstances.Any(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == asi.Id && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id))
                {
                    ExitStateInstances.AddRange(db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == asi.Id && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id).Select(e => e.CurrentStateInstance).ToList());
                }
            }
            //.Select(e=>new StateInstance {  Id=e})
            var EntryStateInstances = ((siR?.NewActivatedInstancesId?.Count() ?? 0) > 0) ?
            db.StateInstances.Where(e => siR.NewActivatedInstancesId.Contains(e.Id)).ToList()
                : new List<StateInstance> { ToStateInstance };
            for (int i = 0; i < EntryStateInstances.Count(); i++)
            {
                var asi = EntryStateInstances[i];
                asi = db.StateInstances.Find(asi.Id);
                if (!db.HistoryStateInstances.Any(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == asi.Id && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id))
                {
                    var ch = db.StateInstances.Where(e => e.ParentInstanceId == asi.Id && (e.State.IsStart || asi.State.Gate.Name == "AND") && e.State.StateType.Id == s.StateType.Id).ToList();
                    ch = ch.Where(c => !EntryStateInstances.Any(e => e.Id == c.Id)).ToList();
                    EntryStateInstances.AddRange(ch);
                }
            }
            //Trigger Activities
            if (transition.OnTransitionActivity != null)
            {
                if (!ExecuteNonTriggerActivities(transition.OnTransitionActivity, StateInstance, Event, User))
                {
                    return 0;
                }
            }

            foreach (var ExitStateInstance in ExitStateInstances)
            {
                if (ExitStateInstance.State.ExitNonTriggerActivity != null)
                {
                    if (!ExecuteNonTriggerActivities(ExitStateInstance.State.ExitNonTriggerActivity, ExitStateInstance.Id, Event, User))
                    {
                        return 0;
                    }
                }
            }
            foreach (var EntryStateInstance in EntryStateInstances)
            {
                if (EntryStateInstance.State.EnterNonTriggerActivity != null)
                {
                    if (!ExecuteNonTriggerActivities(EntryStateInstance.State.EnterNonTriggerActivity, EntryStateInstance.Id, Event, User))
                    {
                        return 0;
                    }
                }
            }
            db.Transitions.Attach(transition);
            var user = db.User.Find(User);
            //History Update
            var h = db.Histories.Add(new History { Transition = transition, User = user, CreatedOn = DateTime.Now, CreatedBy = User, ModifiedBy = User, ModifiedOn = DateTime.Now });
            db.SaveChanges();
            db.Histories.Attach(h);
            if (!transition.ToState.IsMultiInstance)
            {
                foreach (var HistoryStateInstance in db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.Id == StateInstance && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id).ToList())
                {

                    db.HistoryStateInstances.Attach(HistoryStateInstance);
                    HistoryStateInstance.DisableHistoryId = h.Id;
                    HistoryStateInstance.ModifiedBy = User;
                    HistoryStateInstance.ModifiedOn = DateTime.Now;
                    db.SaveChanges();
                    if (HistoryStateInstance.Task != null)
                    {
                        var _task = db.Tasks.Find(HistoryStateInstance.Task.Id);
                        _task.IsCompleted = true;
                        _task.ModifiedBy = User;
                        _task.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                        EmailTaskCompleted(_task?.Id, HistoryStateInstance.CurrentStateInstance.Id, User);
                    }
                    var ChildHistories = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == HistoryStateInstance.CurrentStateInstance.Id
                    && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id).ToList();
                    for (int i = 0; i < ChildHistories.Count(); i++)
                    {
                        var chHist = ChildHistories[i];
                        if (chHist.Task != null)
                        {
                            var t = db.Tasks.Find(chHist.Task.Id);
                            t.IsClosed = true;
                            db.SaveChanges();
                        }
                        ChildHistories.AddRange(db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id && e.CurrentStateInstance.ParentInstanceId == chHist.CurrentStateInstance.Id).ToList());
                    }
                }
            }


            foreach (var EntryStateInstance in EntryStateInstances)
            {
                var CtxId = 0;
                int.TryParse(EntryStateInstance.Context, out CtxId);
                string ctxName = null;
                if (CtxId != 0)
                {
                    switch (EntryStateInstance.State?.Case?.Name)
                    {
                        case "System":
                            break;
                        case "Organization":
                            ctxName = db.Organizations.Find(CtxId).Name;
                            break;
                        case "Project":
                            ctxName = db.Projects.Find(CtxId).Name;
                            break;
                        case "Task":
                            ctxName = db.Tasks.Find(CtxId).Name;
                            break;
                        case "Authentication":
                            ctxName = db.AuthenticationSessions.Find(CtxId).GUID;
                            break;
                        case "TaskTagCategory":
                            var a = db.TaskTagCategories.Find(CtxId);
                            ctxName = a.Task.Name + " (" + a.TagCategory.CategoryName + ")";
                            break;

                        case "ContentTagCategory":
                            ctxName = "";
                            break;
                        default:
                            break;
                    }
                }
                var _task = EntryStateInstance.State.TaskConfiguration == null ? null : new Task { Case = EntryStateInstance.State.Case.Name, CaseContext = ctxName, CaseContextId = CtxId, Name = EntryStateInstance.State.TaskConfiguration.TaskName, ToDisplay = true, StateInstanceId = EntryStateInstance.Id, IsCompleted = false, ModifiedBy = User, ModifiedOn = DateTime.Now, CreatedBy = User, CreatedOn = DateTime.Now };
                var entryInstance = db.StateInstances.Find(EntryStateInstance.Id);
                db.HistoryStateInstances.Add(new HistoryStateInstance
                {
                    History = h,
                    CurrentStateInstance = entryInstance,
                    Task = _task,
                    ModifiedBy = User,
                    ModifiedOn = DateTime.Now,
                    CreatedBy = User,
                    CreatedOn = DateTime.Now
                });
                db.SaveChanges();
                EmailTaskCreated(_task?.Id,  EntryStateInstance.Id, User);
            }
            //State instance is to close
            if (transition.ToState.IsEnd)
            {
                si = db.StateInstances.Find(StateInstance);
                si.Status = "Completed";
                db.SaveChanges();
                var CloseStateInstanceTasks = db.HistoryStateInstances
                    .Where(e => e.CurrentStateInstance.ParentInstanceId == si.Id
                    && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id
                    && e.DisableHistoryId == 0).ToList();
                for (int i = 0; i < CloseStateInstanceTasks.Count(); i++)
                {
                    var cit = CloseStateInstanceTasks[i];
                    CloseStateInstanceTasks.AddRange(db.HistoryStateInstances
                    .Where(e => e.CurrentStateInstance.ParentInstanceId == cit.CurrentStateInstance.Id
                    && e.CurrentStateInstance.State.StateType.Id == s.StateType.Id && e.DisableHistoryId == 0).ToList());
                }
                var _tasks = CloseStateInstanceTasks.Where(e => e.Task != null).Select(e => e.Task).ToList();
                foreach (var task in _tasks)
                {
                    var t = db.Tasks.Find(task.Id);
                    t.IsClosed = true;
                    db.SaveChanges();
                }
            }
            var Users = db.CaseTeamMembers.ToList().Where(e => EntryStateInstances.Any(est => est.Context == e.Case_ContextId.ToString() && est?.State?.Case?.Id == e?.Case?.Id)).Select(e => e.User?.Id ?? 0).Distinct().ToList();
            MainHub.ReloadPage_OnStateChange(EntryStateInstances.Select(e => e.Id).Concat(ExitStateInstances.Select(e => e.Id)).Distinct().ToList(), Users);
            return ToStateInstance.Id;

        }
        public static StateInstance GetCurrentStatesWithTriggerActivities(int StateInstance, int User)
        {
            var db = new TSRMDBContext();
            var SI = db.StateInstances.Find(StateInstance);
            if (SI==null)
            {
                return null;
            }
            var C_Id = 0;
            int.TryParse(SI.Context, out C_Id);
            var caseType = SI.State?.Case?.Id ?? 0;
            var _teamList = db.CaseTeamMembers.Where(e => e.Case_ContextId == C_Id && e.Case.Id == caseType && e.User.Id == User
              && e.IsDeleted == false).Select(e => e.Team.Id).ToList();
            var IsNote = db.States.Find(SI.State.ParentStateId).Name == "Notes Detail Management" || SI.State.Name == "Ready To Create Note";
            Guid _guid;
            if (!db.StateRoles.Any(e => e.State.Id == SI.State.Id && _teamList.Contains(e.Team.Id)) && User != 0 && !IsNote && !Guid.TryParse(SI.Context, out _guid) && db.StateRoles.Any(e => e.State.Id == SI.State.Id))
            {
                return null;
            }
            return db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && (e.CurrentStateInstance.Id == StateInstance) && !e.CurrentStateInstance.State.IsEnd
           //&&  e.CurrentStateInstance.State.TriggerAcitvity != null
           )
                .Select(e => e.CurrentStateInstance).FirstOrDefault();


        }
        public static List<State> GetStartStates(int StateId)
        {
            var db = new TSRMDBContext();
            var state = db.States.Find(StateId);
            var states = db.States.Where(e => (e.IsStart == true || state.Gate.Name == "AND") && e.ParentStateId == StateId).ToList();
            states = states.Where(e =>
            //e.TriggerAcitvity != null ||
            e.IsMultiInstance == true).ToList();
            var ChildStates = states.Where(e => e.IsMultiInstance).SelectMany(e => GetStartStates(e.Id));
            foreach (var ChildState in ChildStates)
            {
                states.Add(ChildState);
            }
            return states;
        }

        public static void CreateNewVersionOfState(int StateId, int User)
        {
            var db = new TSRMDBContext();
            var oldstate = db.States.Find(StateId);
            oldstate.ActiveTo = DateTime.Now;
            oldstate.ModifiedBy = User;
            oldstate.ModifiedOn = DateTime.Now;
            var newState = db.States.Add(new State
            {
                Case = db.Cases.SafeAttach(oldstate.Case),
                CreatedBy = User,
                CreatedOn = DateTime.Now,
                EnterNonTriggerActivity =db.NonTriggerActivities.SafeAttach(oldstate.EnterNonTriggerActivity),
                ExitNonTriggerActivity =  db.NonTriggerActivities.SafeAttach(oldstate.ExitNonTriggerActivity),
                Gate = db.StateOrthogonalityGates.SafeAttach(oldstate.Gate),
                IsDisabled = oldstate.IsDisabled,
                IsEnd = oldstate.IsEnd,
                IsMultiInstance = oldstate.IsMultiInstance,
                IsStart = oldstate.IsStart,
                Name = oldstate.Name + " (Cloned)",
                StateType = db.StateTypes.SafeAttach(oldstate.StateType),
                TaskConfiguration = db.TaskConfigurations.SafeAttach(oldstate.TaskConfiguration),
                TriggerAcitvity =db.TriggerAcitvities.SafeAttach(oldstate.TriggerAcitvity),
                ParentStateId = oldstate.ParentStateId,
                Region = oldstate.Region,
                ActiveFrom = DateTime.Now
            });
            db.SaveChanges();
            db.States.SafeAttach(newState);
            db.StateRoles.AddRange(db.StateRoles.Where(e => e.State.Id == StateId && e.IsDeleted == false).ToList().Select(e => new StateRole
            {
                CreatedBy = User,
                CreatedOn = DateTime.Now,
                State = newState,
                Team = db.Teams.SafeAttach(e.Team),
                PermissionType = db.PermissionTypes.SafeAttach(e.PermissionType)
            }).ToList());
            foreach (var item in db.UsmBsmBridge.Where(e => e.BSM.Id == StateId).ToList())
            {
                db.UsmBsmBridge.Add(new UsmBsmBridge
                {
                    BSM = newState,
                    CreatedBy = User,
                    CreatedOn = DateTime.Now,
                    Url = item.Url,
                    USM = db.States.SafeAttach(item.USM)
                });
            }
            var newTransitions = db.Transitions.Where(e=>e.FromState.Id== StateId&& e.IsDeleted==false && e.ToState.ActiveTo==null).ToList().Select(t=>
            new Transition
            {
                FromState = newState,
                CreatedBy = User,
                CreatedOn = DateTime.Now,
                ToState = db.States.SafeAttach(t.ToState),
                IsDeleted = t.IsDeleted,
                Name = t.Name,
                OnTransitionActivity =  db.NonTriggerActivities.SafeAttach(t.OnTransitionActivity),
                Event =db.Events.SafeAttach(t.Event)
            }).ToList();
            newTransitions.AddRange(db.Transitions.Where(e => e.ToState.Id == StateId && e.IsDeleted == false && e.FromState.ActiveTo == null).ToList().Select(t=>
                 new Transition
                 {
                     ToState = newState,
                     CreatedBy = User,
                     CreatedOn = DateTime.Now,
                     FromState = db.States.SafeAttach(t.FromState),
                     IsDeleted = t.IsDeleted,
                     Name = t.Name,
                     OnTransitionActivity =  db.NonTriggerActivities.SafeAttach(t.OnTransitionActivity),
                     Event = db.Events.SafeAttach(t.Event)
                 }
                ));
            db.Transitions.AddRange(newTransitions);
            db.SaveChanges();
            CreateNewVersionChildren(StateId, newState.Id, User);
        }
        private static void CreateNewVersionChildren(int oldParentStateId,int newParentStateId, int User)
        {
            var db = new TSRMDBContext();
            var oldStates=db.States.Where(e => e.IsDeleted == false && e.ParentStateId == oldParentStateId && e.ActiveTo == null).ToList();
            var newStates = new List<stateInfo>();
            foreach (var oldstate in oldStates)
            {
                var SInfo = new stateInfo
                {
                    newState = db.States.Add(new State
                    {
                        Case = db.Cases.SafeAttach(oldstate.Case),
                        CreatedBy = User,
                        CreatedOn = DateTime.Now,
                        EnterNonTriggerActivity = db.NonTriggerActivities.SafeAttach(oldstate.EnterNonTriggerActivity),
                        ExitNonTriggerActivity = db.NonTriggerActivities.SafeAttach(oldstate.ExitNonTriggerActivity),
                        Gate = db.StateOrthogonalityGates.SafeAttach(oldstate.Gate),
                        IsDisabled = oldstate.IsDisabled,
                        IsEnd = oldstate.IsEnd,
                        IsMultiInstance = oldstate.IsMultiInstance,
                        IsStart = oldstate.IsStart,
                        Name = oldstate.Name,
                        StateType = db.StateTypes.SafeAttach(oldstate.StateType),
                        TaskConfiguration = db.TaskConfigurations.SafeAttach(oldstate.TaskConfiguration),
                        TriggerAcitvity = db.TriggerAcitvities.SafeAttach(oldstate.TriggerAcitvity),
                        ParentStateId = newParentStateId,
                        Region = oldstate.Region,
                        ActiveFrom = DateTime.Now
                    }),
                    oldStateId = oldstate.Id
                };
                newStates.Add(SInfo);
                foreach (var item in db.UsmBsmBridge.Where(e => SInfo.oldStateId==e.BSM.Id).ToList())
                {
                    db.UsmBsmBridge.Add(new UsmBsmBridge
                    {
                        BSM = SInfo.newState,
                        CreatedBy = User,
                        CreatedOn = DateTime.Now,
                        Url = item.Url,
                        USM = db.States.SafeAttach(item.USM)
                    });
                }
               db.StateRoles.AddRange(db.StateRoles.Where(e => e.State.Id == SInfo.oldStateId && e.IsDeleted == false).ToList().Select(e => new StateRole
                {
                    CreatedBy = User,
                    CreatedOn = DateTime.Now,
                    State = SInfo.newState,
                    Team = db.Teams.SafeAttach(e.Team),
                    PermissionType = db.PermissionTypes.SafeAttach(e.PermissionType)
                }).ToList());
            }
            db.States.AddRange(newStates.Select(e=>e.newState));
            db.SaveChanges();
            var allOldStateIds = newStates.Select(e => e.oldStateId).ToList();
            var _oldTransitions=db.Transitions.Where(t =>  t.IsDeleted == false && ((allOldStateIds.Contains(t.FromState.Id) && t.FromState.ActiveTo == null) || (allOldStateIds.Contains(t.ToState.Id) && t.ToState.ActiveTo == null))).ToList();
            foreach (var _oldTransition in _oldTransitions)
            {
                db.Transitions.Add(new Transition
                {
                    ToState = db.States.SafeAttach(newStates.Find(s => s.oldStateId == _oldTransition.ToState.Id)?.newState),
                    FromState = db.States.SafeAttach(newStates.Find(s => s.oldStateId == _oldTransition.FromState.Id)?.newState),
                    CreatedBy = User,
                    CreatedOn = DateTime.Now,
                    IsDeleted = _oldTransition.IsDeleted,
                    Name = _oldTransition.Name,
                    OnTransitionActivity = db.NonTriggerActivities.SafeAttach(_oldTransition.OnTransitionActivity),
                    Event = db.Events.SafeAttach(_oldTransition.Event)
                });
            }
            db.SaveChanges();
            newStates.ForEach(e => CreateNewVersionChildren(e.oldStateId, e.newState.Id, User));
         }

        public static void EmailTaskCreated(int? TaskId, int SI_Id, int user)
        {
            SendNotification(TaskId, "A Task Created:", "A Task Created:", SI_Id, user);
        }
        private static void SendNotification(int? TaskId, string Subject, string body, int SI_Id, int user)
        {

            if (TaskId != null)
            {
                var db = new TSRMDBContext();
                var _task = db.Tasks.Find(TaskId ?? 0);
                var stateId = db.StateInstances.Find(_task.StateInstanceId).State.Id;
                var Teams = db.StateRoles.Where(e => !e.IsDeleted && e.State.Id == stateId).Select(e => e.Team.Id).ToList();
                var Users = db.CaseTeamMembers.Where(e => e.User.Id != user && Teams.Contains(e.Team.Id) && e.Case.Name == _task.Case
                  && e.Case_ContextId == _task.CaseContextId).Select(e => e.User.Email).Distinct().ToList();
#if DEBUG
                Users = new List<string> { "faheem.khan@trandts.com" };
#endif
                var Username = db.User.Find(user)?.Username??"UnKnown";
                new EmailSender().EmailAllText(
                   Users.Select(e =>
                      new EmailMessage
                      {
                          Body = Subject + _task.Name+"\n"+ _task.Case+":"+ _task.CaseContext+"\nBy User:"+ Username,
                          FromName = "System",
                          Receiver = e,
                          Subject = body + _task.Name
                      }
                    ).ToList()
                    , new System.Net.Mail.Attachment[0],
                    new EmailConfiguration
                    {
                        Email = ConfigurationManager.AppSettings["Email"],
                        EnableSsl = bool.Parse(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = int.Parse(ConfigurationManager.AppSettings["Port"]),
                        Password = ConfigurationManager.AppSettings["Password"],
                        UseDefaultCredentials = bool.Parse(ConfigurationManager.AppSettings["UseDefaultCredentials"])
                    });
            }
        }
        public static void EmailTaskCompleted(int? TaskId, int SI_Id, int user)
        {
            SendNotification(TaskId, "A Task Completed:", "A Task Completed:", SI_Id, user);
        }

        private static bool ExecuteNonTriggerActivities(NonTriggerActivity model, int StateInstance, string Event, int UserId)
        {
            bool Check = false;
            if (model.ActivityType.IsAutomated)
            {
                var ac = new AutomatedActivity();
                var type = ac.GetType();
                MethodInfo methodInfo = type.GetMethod(model.MethodName);
                if (methodInfo != null)
                {
                    object result = null;
                    ParameterInfo[] parameters = methodInfo.GetParameters();
                    if (parameters.Length == 0)
                    {
                        result = methodInfo.Invoke(ac, null);
                        Check = true;
                    }
                    else
                    {
                        object[] parametersArray = new object[] { StateInstance, Event, UserId };
                        result = methodInfo.Invoke(ac, parametersArray);
                        try
                        {
                            Check = (bool)result;
                        }
                        catch (Exception)
                        { }
                    }
                }
            }
            else
            {
                throw new Exception("No Manual Implementation Found");
            }
            return Check;
        }
    }
}