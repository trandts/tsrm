﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TSRM.Models;

namespace TSRM.Signalr
{
    /*
    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> _connections =
            new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(T key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }
    */
    public struct MapperModel
    {
        public List<int> StateIds { get; set; }
        public string ConnectionId { get; set; }


        //public override bool Equals(object ob)
        //{
        //    if (ob is Complex)
        //    {
        //        Complex c = (Complex)ob;
        //        return m_Re == c.m_Re && m_Im == c.m_Im;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public override int GetHashCode()
        {
            return ConnectionId.GetHashCode()^ StateIds.Distinct().Aggregate(0, (x, y) => x.GetHashCode() ^ y.GetHashCode());
        }
    }
    public class ConnectionMapping
    {
       
        public int Count
        {
            get
            {
                return new TSRMDBContext().RealtimeConnections.Count();
            }
        }

        public void Add(int key, MapperModel connectionData)
        {
            TSRMDBContext db = new TSRMDBContext();
            var userConnection = db.RealtimeConnections.FirstOrDefault(f => f.ConnectionID== connectionData.ConnectionId );
            
                if (userConnection==null)
                {
                userConnection = db.RealtimeConnections.Add(new RealtimeConnection { User = key, ConnectionID= connectionData.ConnectionId});
                db.SaveChanges();
                }
            db.RealtimeConnections.Attach(userConnection);
            userConnection.User = key;
            db.RealtimeConnectionData.AddRange(connectionData.StateIds.Select(s => new RealtimeConnectionData
            {
                RealtimeConnection = userConnection,
                StateInstance = s
            }));
            db.SaveChanges();
        }

        public IEnumerable<MapperModel> GetConnections(int key)
        {
            TSRMDBContext db = new TSRMDBContext();
            return  db.RealtimeConnections.Where(e => e.User == key ).Include(e => e.Data).ToList().Select(e => new MapperModel
            {
                ConnectionId = e.ConnectionID,
                StateIds = e.Data.Select(d => d.StateInstance).ToList()
            }).ToList();
        }

        public void Remove(int key, MapperModel connectionId)
        {
            TSRMDBContext db = new TSRMDBContext();
            db.RealtimeConnectionData.RemoveRange(db.RealtimeConnectionData.Where(e=>e.RealtimeConnection.User==key && e.RealtimeConnection.ConnectionID== connectionId.ConnectionId ));
            db.RealtimeConnections.RemoveRange(db.RealtimeConnections.Where(e => e.User == key && e.ConnectionID == connectionId.ConnectionId ));
            db.SaveChanges();
        }

        public IEnumerable<string> GetConnections(List<int> StatesIds, List<int> Users)
        {
            TSRMDBContext db = new TSRMDBContext();
            return db.RealtimeConnections.Where(e =>   Users.Contains(e.User) ||e.Data.Any(d=> StatesIds.Contains(d.StateInstance))).ToList().Select(e => e.ConnectionID).ToList();
        }
        public IEnumerable<string> GetConnectionsWithoutUsers(List<int> StatesIds, List<int> Users)
        {
            TSRMDBContext db = new TSRMDBContext();
            return db.RealtimeConnections.Where(e => (!Users.Contains(e.User)) && e.Data.Any(d => StatesIds.Contains(d.StateInstance))).ToList().Select(e => e.ConnectionID).ToList();
        }
    }
}