﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace TSRM.Signalr
{
   
    public class MainHub : Hub
    {
        //private readonly static ConnectionMapping<MapperModel> _connections =
         //     new ConnectionMapping<MapperModel>();
        private readonly static ConnectionMapping _connections =
           new ConnectionMapping();
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MainHub>();

        public override Task OnConnected()
        {
            int User = 0;
            int.TryParse(Context.QueryString["User"],out User);
            var States= (Context.QueryString["StateInstances[]"]??"").Split(',').Where(e => !string.IsNullOrWhiteSpace(e)).Select(e=>int.Parse(e)).ToList();
            _connections.Add(User,new MapperModel { StateIds= States, ConnectionId= Context.ConnectionId });
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            int User = 0;
            int.TryParse(Context.QueryString["User"], out User);
            var States = (Context.QueryString["StateInstances[]"] ?? "").Split(',').Where(e => !string.IsNullOrWhiteSpace(e)).Select(e => int.Parse(e)).ToList();

            _connections.Remove(User, new MapperModel { StateIds = States, ConnectionId = Context.ConnectionId });

            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            int User = 0;
            int.TryParse(Context.QueryString["User"], out User);
            var States = (Context.QueryString["StateInstances[]"] ?? "").Split(',').Where(e => !string.IsNullOrWhiteSpace(e)).Select(e => int.Parse(e)).ToList();

            if (!_connections.GetConnections(User).Contains(new MapperModel { StateIds = States, ConnectionId = Context.ConnectionId }))
            {
                _connections.Add(User, new MapperModel { StateIds = States, ConnectionId = Context.ConnectionId });
            }

            return base.OnReconnected();
        }


        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.PageReload(name, message);
        }
        public static void ReloadPage_OnStateChange(List<int> arr, List<int> Users)
        {
            // Call the broadcastMessage method to update clients.
            
            var connections = _connections.GetConnections(arr, Users).ToList();
            hubContext.Clients.Clients(connections).PageReload();
        }
        public static void CommentAdded(int StateInstanceId, List<int> Users, string ByUserName,string Comments)
        {
            // Call the broadcastMessage method to update clients.

            var connections = _connections.GetConnections(new List<int> { StateInstanceId }, Users).ToList();
            hubContext.Clients.Clients(connections).CommentAdded(StateInstanceId, ByUserName, Comments);
        }
        public static void TagAdded(int StateInstanceId, List<int> Users, string ByUserName, List<string> TaskTags)
        {
            // Call the broadcastMessage method to update clients.

            var connections = _connections.GetConnections(new List<int> { StateInstanceId }, Users).ToList();
            hubContext.Clients.Clients(connections).TagAdded(StateInstanceId, ByUserName, TaskTags);
        }
        public static void TagAdded(List<int> StateInstanceIds, List<int> Users, string ByUserName, List<string> TaskTags)
        {
            // Call the broadcastMessage method to update clients.

            var connections = _connections.GetConnections(StateInstanceIds, Users).ToList();
            foreach (var item in StateInstanceIds)
            {
                hubContext.Clients.Clients(connections).TagAdded(item, ByUserName, TaskTags);
            }
        }
    }
}