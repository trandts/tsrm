﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class LogoutServiceController : Controller
    {
        // GET: LoginService
        [System.Web.Mvc.HttpGet]
        public JsonResult Index()
        {
            //Session.Abandon();
            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            var db = new TSRMDBContext();
            var d = db.AuthenticationSessions.FirstOrDefault(a => a.IsOpen && a.Session == Session.SessionID);
            if(d!=null)
            {
           var oldAuth = db.AuthenticationSessions.Attach(d);
            oldAuth.IsOpen = false;
            db.SaveChanges();
            Session["User"] = null;
            Session["AuthenticationSessionUID"] = null;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false,JsonRequestBehavior.AllowGet);
        }
    }
}