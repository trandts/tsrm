﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class LoginServiceController : Controller
    {
        // GET: LoginService
        [System.Web.Mvc.HttpPost]
        public JsonResult Index([FromBody]User model)
        {
            var db = new TSRMDBContext();
            var User = db.User.FirstOrDefault(e => e.Username == model.Username && e.Password == model.Password) ?? new Models.User();
            AuthenticationSession oldAuth = null;
            if (User.Id>0)
            {
                oldAuth= db.AuthenticationSessions.Attach(db.AuthenticationSessions.FirstOrDefault(a => a.IsOpen && a.Session == Session.SessionID));
                oldAuth.User = User.Id;
                db.SaveChanges();
                var f = db.Cases.Attach(db.Cases.FirstOrDefault(e => e.Name == "Authentication"));
                db.CaseTeamMembers.Add(
                    new CaseTeamMember
                    {
                        Case = f,
                        Case_ContextId = oldAuth.Id,
                        User = db.User.Attach(User),
                        Team = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Authenticated User"))
                    });
                db.SaveChanges();
                Session["User"] = User;
            }
            return Json(new {  Id=User?.Id,  Username=User?.Username,  Email =User?.Email , AuthSession= oldAuth?.Id??0 });
        }
    }
}