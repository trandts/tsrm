﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class CurrentUIController : Controller
    {
        // GET: CurrentUI
        public JsonResult Index(string Refferer = null)
        {
            TSRMDBContext db = new TSRMDBContext();
            var user=Session["User"] as User ?? new Models.User();
            var UrlRefferer = Refferer ?? Request?.UrlReferrer?.AbsolutePath?.Replace("/tsrm", "");
            var model = user.Id == 0 ? "/Login" : null;
            model=model??(db.UsmBsmBridge.Any(e=>e.Url==UrlRefferer)?UrlRefferer:"/Tasks");
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}