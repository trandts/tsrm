﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class LoginCheckController : Controller
    {
        // GET: LoginCheck
        [System.Web.Mvc.HttpGet]
        public JsonResult Index()
        {
            var User = Session["User"] as User ?? new Models.User();
            return Json(User, JsonRequestBehavior.AllowGet);

        }
    }
}