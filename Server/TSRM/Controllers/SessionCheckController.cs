﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class SessionCheck
    {
        public bool CreateNew { get; set; }
        public int AuthSession { get; set; }
    }
    public class SessionCheckController : Controller
    {
        // GET: SessionCheck
        public JsonResult Index()
        {
            var db = new TSRMDBContext();
            var AuthSession=db.AuthenticationSessions.Where(e => e.IsOpen && e.Session == Session.SessionID).FirstOrDefault();
            SessionCheck result = null;
            if(AuthSession!=null)
            {
                if (Session["User"] == null)
                {
                    Session["AuthenticationSessionUID"] = AuthSession.GUID;
                    Session["User"] = db.User.Find(AuthSession.User)??new Models.User();
                }
                result = new SessionCheck { CreateNew = false, AuthSession = AuthSession.Id };
            }
            else
            {
                 AuthSession = new AuthenticationSession
                {
                    GUID = Guid.NewGuid().ToString(),
                    IsOpen = true,
                    CreatedOn = DateTime.Now,
                    Session = Session.SessionID
                };
                Session["AuthenticationSessionUID"] = AuthSession.GUID;
                db.AuthenticationSessions.Add(AuthSession);
                db.SaveChanges();
                result = new SessionCheck { CreateNew = true, AuthSession = AuthSession.Id };
            }
            return Json(result, JsonRequestBehavior.AllowGet );
        }
    }
}