﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //var User = (Session["User"] as User)??new Models.User();
            //if (!this.Request.Url.AbsolutePath.ToString().ToLowerInvariant().Contains("/login") && User.Id==0)
            //{
            //  return   Redirect("~/login");
            //}
            //     var SIs = WorkflowEngine.CreateWorkflowInstance(1, null, 1, 0, null);
            //return View();
            return File(Server.MapPath("~/Content/index.html") , "text /html");
        }
       
      

        public ActionResult Test()
        {
          
            /*
            ViewBag.Title = "Home Page";
            var db = new TSRMDBContext();
            var AT = new ActivityType { Name = "UI Element", IsAutomated = true };
            var user = new User { Username = "faheem", Password = "123" };
            var R1 = new Role { Name = "Role 1" };
            var PT = new PermissionType { Name = "Edit" };


            db.ActivityTypes.Add(AT);
            db.RoleUsers.Add(new RoleUser { Role = R1, User = user });
            db.ProjectTeamMembers.Add(new ProjectTeamMembers { Team = new Team { Name = "Team 1" }, User = user, Project = new Project { Name = "Project 1" } });
            db.PermissionTypes.Add(PT);

            db.SaveChanges();
            var actA = db.TriggerAcitvities.Add(new TriggerAcitvity
            {
                ActivityType = AT,
                UIElement = "UI A"
            });
            var actB = db.TriggerAcitvities.Add(new TriggerAcitvity
            {
                ActivityType = AT,
                UIElement = "UI B"
            });
            db.SaveChanges();

            db.TriggerAcitvities.Attach(actA);
            db.TriggerAcitvities.Attach(actB);

            var wf = db.Workflows.Add(new Workflow
            {
                Name = "Workflow 1",
                WorkflowType = new WorkflowType
                {
                    Name = "Organizational"
                }
            });
            db.SaveChanges();
            db.Workflows.Attach(wf);

            var state1 = new State { Workflow = wf, Name = "A", IsEnd = false, TriggerAcitvity = actA };
            db.States.Add(state1);
            db.SaveChanges();
            db.States.Attach(state1);
            var state2 = new State { Workflow = wf, Name = "B", IsEnd = false };
            db.States.Add(state2);
            db.SaveChanges();
            db.States.Attach(state2);
            var state3 = new State { Workflow = wf, Name = "C", IsEnd = true };
            db.States.Add(state3);
            db.SaveChanges();
            db.States.Attach(state3);
           // wf.StartStateId = state1.Id;
            db.ActivityTypes.Attach(AT);
            db.User.Attach(user);
            db.Roles.Attach(R1);
            db.PermissionTypes.Attach(PT);

            db.StateRoles.AddRange(new List<StateRole> {
                new StateRole { PermissionType=PT,
                 State=state1,
                 Role=R1,
                },
                 new StateRole { PermissionType=PT,
                 State=state2,
                 Role=R1,
                },
                  new StateRole { PermissionType=PT,
                 State=state3,
                 Role=R1,
                }
            });
            db.SaveChanges();



            db.Transitions.AddRange(
                new List<Transition>
                {
                    new Transition
                               {
                        Name = "T1 A-to-B",
                        FromState = state1,
                        ToState = state2,
                        Event = new Event { EventName = "OnB", TriggerAcitvity =actA },
                      Workflow = wf
                    },new Transition
                               {
                        Name = "T2 B-to-A",
                        FromState = state2,
                        ToState = state1,
                        Event = new Event { EventName = "OnA", TriggerAcitvity =actB },
                   Workflow = wf
                    },new Transition
                               {
                        Name = "T2 B-to-C",
                        FromState = state2,
                        ToState = state3,
                        Event = new Event { EventName = "OnC", TriggerAcitvity =actB },
                   Workflow = wf
                    }
                });

            db.SaveChanges();
            */
            return View();
        }
    }
}
