﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class SessionData
    {
        public int USM { get; set; }
        public string USM_Name { get; set; }
        public int BSM { get; set; }
        public string BSMI { get; set; }
        public string Url { get; set; }
        public string Context { get; set; }
        public int UserId { get; set; }
        public List<SessionData> ChildSMs { get; set; } = new List<SessionData> { };
        public SessionData Load(TSRMDBContext db)
        {
            var list = new List<SessionData>();
           var instances= db.HistoryStateInstances.Where(e =>
            e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == USM
            ).Select(e => e.CurrentStateInstance).ToList();
            var instanceList = instances.Select(i => i.State.Id).ToList();
            foreach (var item in instanceList)
            {
                var bridge = db.UsmBsmBridge.Where(e => item==e.USM.Id).FirstOrDefault();
                if(bridge!=null)
                {
                    var BSM_I = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.Id == bridge.BSM.Id
                   //&& e.CurrentStateInstance.Context==Session.SessionID
                   ).Select(e => e.CurrentStateInstance).FirstOrDefault();
                    if (BSM_I != null)
                    {
                        list.Add(new SessionData
                        {
                            USM = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.Id == bridge.USM.Id
                  && e.CurrentStateInstance.Context == Context
                                 )
                                         .Select(e => e.CurrentStateInstance.Id).FirstOrDefault(),
                            USM_Name = bridge?.USM?.Name//db.StateInstances.Find(Usm_Id)?.State?.Name
                                        ,
                            BSM = BSM_I.Id
                                        ,
                            BSMI = BSM_I.Context
                                        ,
                            Url = bridge?.Url
                                        ,
                            Context = Context
                                     ,
                            UserId = UserId
                        }.Load(db));
                    }
                }
                
            }
           
            ChildSMs.AddRange(list);
            return this;
        }
    }
    public class GetUSMController : Controller
    {
        // GET: Session
        public JsonResult Index(string Refferer=null)
        {
            TSRMDBContext db = new TSRMDBContext();
            var UrlRefferer = Refferer??Request?.UrlReferrer?.AbsolutePath?.Replace("/tsrm", "");
            if (Session["USM"] == null)
            {
                var rootInstanceId = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.ParentStateId == 0)
                    .Select(e => e.CurrentStateInstance.Id).FirstOrDefault();
                var userId = db.User.Select(e => e.Id).FirstOrDefault();
                var rsm = db.States.Where(e => e.ParentStateId == 0).Select(e => e.Id).FirstOrDefault();
                if (rootInstanceId == 0)
                {
                    rootInstanceId =WorkflowEngine.CreateWorkflowInstance(userId, null, rsm,0, null, true);
                }
               
                var usm = db.States.Where(e => e.StateType.ShortName == "USM" && e.ParentStateId == rsm).Select(e => e.Id).FirstOrDefault();

                var EarlierUSM=db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == rootInstanceId
                      && e.CurrentStateInstance.State.Id == usm && e.CurrentStateInstance.Context==Session.SessionID)
                      .Select(e => e.CurrentStateInstance.Id).FirstOrDefault();

                Session["USM"] = EarlierUSM>0? EarlierUSM:WorkflowEngine.CreateWorkflowInstance(userId, Session.SessionID, usm, rootInstanceId, null, true);
            }
            int Usm_Id = (Session["USM"] as int? )??0;
            var bridge=db.UsmBsmBridge.Where(e => e.Url == UrlRefferer).FirstOrDefault();
            var bridge_bsm_ID=bridge?.BSM?.Id ?? 0;
            var bridge_usm_ID = bridge?.USM?.Id ?? 0;
            var BSM_I = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.Id == bridge_bsm_ID
            //&& e.CurrentStateInstance.Context==Session.SessionID
            )
                    .Select(e => e.CurrentStateInstance).FirstOrDefault();
            var model = new SessionData
            {
                USM = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.Id == bridge_usm_ID
            && e.CurrentStateInstance.Context == Session.SessionID
            )
                    .Select(e => e.CurrentStateInstance.Id).FirstOrDefault()// Usm_Id 
                ,
                USM_Name = bridge?.USM?.Name//db.StateInstances.Find(Usm_Id)?.State?.Name
                ,
                BSM = BSM_I?.Id ?? 0
                ,
                BSMI = BSM_I?.Context
                ,
                Url = bridge?.Url
                ,
                Context = Session.SessionID
             ,
                UserId = (Session["User"] as User ?? new User()).Id
            }.Load(db);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}