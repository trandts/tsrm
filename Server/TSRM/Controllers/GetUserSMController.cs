﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.Controllers
{
    public class TempSIData
    {
        public int SI_Id { get; set; }
        public int S_Id { get; set; }
    }
    public class GetUserSMSessionData
    {
        public int USM { get; set; }
        public string USM_Name { get; set; }
        public string Region { get; set; }
        public int BSM { get; set; }
        public string BSMI { get; set; }
        public string Url { get; set; }
        public string Context { get; set; }
        public string USMContext { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }


        public bool IsTask { get;  set; }
        public string TaskName { get; set; }
        public string TaskContext { get; set; }
        public string TaskContextType { get; set; }


        public string TaskProject { get; set; }

        public List<Tag> ContextTags { get; set; } 

        public List<string> VisibilityUsers { get; set; } = new List<string>();
        public List<string> AssignedUsers { get; set; } = new List<string>();
        public List<string> NotesTags { get; set; } = new List<string>();
        public DateTime? LastTransitionOn { get; set; }
        public string LastUser { get; set; }

        public List<GetUserSMSessionData> ChildSMs { get; set; } = new List<GetUserSMSessionData> { };
        

        public GetUserSMSessionData Load(TSRMDBContext db,string Targetted_SM,string AuthId)
        {

            var list = new List<GetUserSMSessionData>();
            var instances = db.HistoryStateInstances.Where(e =>
              e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == BSM
              && e.CurrentStateInstance.State.StateType.ShortName== Targetted_SM && e.CurrentStateInstance.State.IsEnd==false
             ).Select(e => e.CurrentStateInstance).ToList();
            var instanceList = instances.Select(i => new TempSIData { S_Id = i.State.Id, SI_Id = i.Id }).ToList();
            foreach (var item in instanceList)
            {
                var bridge = db.UsmBsmBridge.Where(e => item.S_Id == e.BSM.Id).FirstOrDefault();
                var bsm_Id = bridge?.BSM?.Id ?? 0;
                var usm_s_Id = bridge?.USM?.Id ?? 0;
                var HSI = db.HistoryStateInstances.Where(e =>
                             e.DisableHistoryId == 0 &&
                             e.CurrentStateInstance.State.Id == usm_s_Id
                             && e.CurrentStateInstance.Context == USMContext
                    ).FirstOrDefault();
                var USM_SI = HSI?.CurrentStateInstance;
                var BSM_I = db.StateInstances.Where(e => e.Id == item.SI_Id).FirstOrDefault();
                var USM_Id = db.HistoryStateInstances.Where(e =>
                           e.DisableHistoryId == 0 &&
                           e.CurrentStateInstance.State.Id == usm_s_Id
                           && e.CurrentStateInstance.Context == USMContext
                    ).Select(e => e.CurrentStateInstance).FirstOrDefault();
                int context_Parsed;
                int.TryParse(BSM_I.Context, out context_Parsed);
                var _case_Id = BSM_I?.State?.Case?.Id??0;
               var _teamList= db.CaseTeamMembers.Where(e => e.User.Id == UserId && e.Case_ContextId == context_Parsed
                && e.Case.Id == _case_Id && !e.IsDeleted).Select(e => e.Team.Id).Distinct().ToList();
                //Guid _guid;|| !Guid.TryParse(BSM_I.Context, out _guid)
                //Complex Condition
                var IsNoUserOrTeamAuthorizedForState = (!db.StateRoles.Any(e => e.State.Id == bsm_Id) || !db.CaseTeamMembers.Any(e => e.Case.Id == BSM_I.State.Case.Id && !e.IsDeleted));
                var IsUserAuthorizedForState = db.StateRoles.Any(e => e.State.Id == bsm_Id && _teamList.Contains(e.Team.Id));
                var IsLoginState = BSM_I.State.Name == "Ready to Authenticate";
                var IsEdgeCasesAvailable = (_case_Id != 0);// || (_case_Id == 0 && UserId != 0)
                //Complex Condition Expressions
                var RoleLessStateOrEdgeCase = (IsNoUserOrTeamAuthorizedForState && !IsLoginState && IsEdgeCasesAvailable);
                var CurrentUserLoginForm = (IsLoginState && BSM_I.Context == AuthId);
                if (IsUserAuthorizedForState  || CurrentUserLoginForm || RoleLessStateOrEdgeCase)
                {
                    var _task=db.Tasks.Where(e => e.StateInstanceId == item.SI_Id &&
                    e.ToDisplay == true && e.IsCompleted == false && e.IsClosed == false && e.IsDeleted == false).FirstOrDefault()??new Task();
                    var _allTeams=db.StateRoles.Where(e => e.State.Id == bsm_Id).Select(e => e.Team.Id).ToList();
                    var ctx= BSM_I?.Context;
                    var ctxT = BSM_I?.State?.Case?.Id;
                    var ctxNoteT = db.Cases.FirstOrDefault(f=>f.Name== "Note")?.Id??0;

                    
                    list.Add(new GetUserSMSessionData
                    {
                        USM = USM_SI?.Id ?? 0,
                        USM_Name = bridge?.USM?.Name,
                        BSM = item.SI_Id,
                        BSMI = BSM_I?.State?.Name,
                        Url = bridge?.Url,
                        Context = BSM_I?.Context,
                        USMContext = USMContext,
                        UserId = UserId,
                        UserName = UserName,
                        Region = bridge?.USM?.Region,

                        IsTask = (BSM_I?.State?.TaskConfiguration?.Id ?? 0) > 0,
                        TaskName = _task?.Name,
                        TaskContext = _task?.CaseContext,
                        TaskProject="Task" == _task?.Case ? db.Tasks.Find( _task?.CaseContextId)?.Project?.Name : null,
                        ContextTags = db.ContentTags.Where(ct =>
                            ct.ContentTagCategory.ContentContext.Context == ctx &&
                            ct.ContentTagCategory.ContentContext.ContextType == ctxT &&
                            ct.IsDeleted==false
                          ).Select(ct => ct.Tag).ToList(),
                        TaskContextType = _task?.Case,
                        NotesTags=db.ContentTags.Where(e =>
                    e.IsDeleted == false &&
                    db.Notes.Where(n => n.task.Id == _task.Id).Select(n => n.Id.ToString()).Contains(e.ContentTagCategory.ContentContext.Context)
                        && e.ContentTagCategory.ContentContext.ContextType == ctxNoteT).Select(e => e.Tag.Name).ToList(),
                        AssignedUsers = db.CaseTeamMembers.Where(e => e.Case.Id == BSM_I.State.Case.Id && e.Case_ContextId == _task.CaseContextId && !e.IsDeleted && e.Team.Name== "Project Task Assigned To").Select(e => e.User.Username).Distinct().ToList(),
                        VisibilityUsers = db.CaseTeamMembers.Where(e => e.Case.Id == BSM_I.State.Case.Id && e.Case_ContextId == _task.CaseContextId && !e.IsDeleted && _allTeams.Contains(e.Team.Id)).Select(e => e.User.Username).Distinct().ToList(),
                        LastUser=db.User.Find(_task?.CreatedBy??0)?.Username,
                        LastTransitionOn= _task?.CreatedOn
                    }.Load(db, Targetted_SM, AuthId));

                }else
                {

                }
            }

            ChildSMs.AddRange(list);
            return this;
        }

        public void LoadRegionalSMs(List<GetUserSMSessionData> flattened)
        {
            ChildSMs = flattened.Where(e => e.Region==USM_Name).ToList();
            flattened.RemoveAll(e => e.Region == USM_Name);
            foreach (var item in ChildSMs)
            {
                item.LoadRegionalSMs(flattened);
            }
        }
    }
    public class GetUserSMController : Controller
    {
        // GET: Session
        public string Targetted_SM = "BSM";
        public JsonResult Index(int UserId=0)
        {
            var search = (Request.Params["Search"]??"").ToLowerInvariant().Split(',').Where(e=>!string.IsNullOrEmpty( e)).ToList();
            string UserName = null;
            TSRMDBContext db = new TSRMDBContext();
            if (UserId==0)
            {
                UserId = ((Session["User"] as User) ?? new Models.User()).Id;
                UserName= ((Session["User"] as User) ?? new Models.User()).Username;
            }
            else
            {
                UserName=db.User.Find(UserId).Username;
            }
            var rootInstanceId = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.ParentStateId == 0)
                    .Select(e => e.CurrentStateInstance.Id).FirstOrDefault();
            
            if (Session["USM"] == null)
            {
                var userId = db.User.Where(e => e.Username == "System").Select(e => e.Id).FirstOrDefault();
                var rsm = db.States.Where(e => e.ParentStateId == 0).Select(e => e.Id).FirstOrDefault();
                if (rootInstanceId == 0)
                {
                    rootInstanceId = WorkflowEngine.CreateWorkflowInstance(userId, null, rsm, 0, null, true);
                }
                var usm = db.States.Where(e => e.StateType.ShortName == "USM" && e.ParentStateId == rsm).Select(e => e.Id).FirstOrDefault();
                var EarlierUSM = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == rootInstanceId
                    && e.CurrentStateInstance.State.Id == usm && e.CurrentStateInstance.Context == Session.SessionID)
                  .Select(e => e.CurrentStateInstance.Id).FirstOrDefault();
                Session["USM"] = EarlierUSM > 0 ? EarlierUSM : WorkflowEngine.CreateWorkflowInstance(userId, Session.SessionID, usm, rootInstanceId, null, true);
            }
            var BSM = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.ParentInstanceId == rootInstanceId
             && e.CurrentStateInstance.State.StateType.ShortName == Targetted_SM).Select(e => e.CurrentStateInstance).ToList();
            var model = new List<GetUserSMSessionData>();
            var AuthId=db.AuthenticationSessions.Where(e => e.IsOpen && e.Session == Session.SessionID)
                .Select(e => e.Id).FirstOrDefault().ToString();
            foreach (var e in BSM)
            {
                var bridge = db.UsmBsmBridge.Where(u => u.BSM.Id == e.State.Id).FirstOrDefault();
                var BSM_case=(e?.State?.Case?.Id ?? 0);
                model.Add(new GetUserSMSessionData { BSM = e.Id, BSMI = e.State.Name, Context = e.Context, Url = bridge?.Url, USM = bridge?.USM?.Id ?? 0, USM_Name = bridge?.USM?.Name, Region=bridge?.USM?.Region,  UserId = UserId, USMContext = Session.SessionID,
                    IsTask = (e?.State?.TaskConfiguration?.Id ?? 0) > 0,
                     UserName=UserName,
                    ContextTags=db.ContentTags.Where(ct=>
                          ct.ContentTagCategory.ContentContext.Context==e.Context &&
                          ct.ContentTagCategory.ContentContext.ContextType== BSM_case
                          &&
                            ct.IsDeleted == false).Select(ct=>ct.Tag).ToList()
                }.Load(db, Targetted_SM, AuthId));
            }
            List<GetUserSMSessionData> Flattened = new List<GetUserSMSessionData>();
            Flattened.AddRange(model);
            for (int i = 0; i < Flattened.Count(); i++)
            {
                Flattened.AddRange(Flattened[i].ChildSMs);
            }
            Flattened=Flattened.OrderByDescending(e => e.LastTransitionOn).ToList();
            Flattened.RemoveAll(e => e.USM_Name == null);
            if (search.Count() > 0)
            {
               var Sis= db.Notes.Where(n => search.Any(s=>n.Name.Contains(s))).Select(e => e.task.StateInstanceId).ToList();
               
                Flattened.RemoveAll(e =>e.IsTask==true &&
                (!search.Any(s=>e.BSMI.ToLowerInvariant().Contains(s)) &&
                (search.Any(s => (e.TaskContextType + ":"+e.TaskContext).ToLowerInvariant()!= s) &&
               search.Any(s => !e.AssignedUsers.Select(a => ("assigned:" + a).ToLowerInvariant()).Contains(s)) &&
               search.Any(s => !e.VisibilityUsers.Select(a => ("visibility:" + a).ToLowerInvariant()).Contains(s)) &&
                !Sis.Contains(e.BSM) && 
                !(e.LastTransitionOn.Value.Date==DateTime.Now.Date && search.Any(s => s =="today"))
               && !search.Any(s => e.ContextTags.Any(ct=>"#"+ct.Name.ToLower()==s))
                && !search.Any(s => e.NotesTags.Any(ct => "#" + ct.ToLower() == s))
                ) 
                ));
            }
            var states = Flattened.Where(e => e.IsTask).Select(e => e.BSM).ToList();
            var Arrange = Flattened.FirstOrDefault(f=>f.Region==null);
            Arrange.LoadRegionalSMs(Flattened);
            
            return Json(new { Data = Arrange, States = states }, JsonRequestBehavior.AllowGet);
        }
      


    }
}