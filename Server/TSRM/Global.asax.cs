﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TSRM.Initializer;
using TSRM.Models;

namespace TSRM
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            HostingEnvironment.QueueBackgroundWorkItem(ct => abc(ct));
            DatabaseInitializer.GetInstance().Initialize();
        }
       private void abc(CancellationToken CT)
        {
            while (!CT.IsCancellationRequested)
            {
                try
                {
                    var db = new TSRMDBContext();
                    var AllActiveSIs = db.HistoryStateInstances
                         .Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.State.StateType.ShortName == "BSM")
                         .ToList().Select(e => new HS { History = e.History.Id, StateId = e.CurrentStateInstance.State.Id, Context = e.CurrentStateInstance.Context });
                    var gKeys = AllActiveSIs.GroupBy(e => new { e.Context, e.StateId }).Where(group => group.Count() > 1)
                             .Select(group => group.Key).ToList();
                    if (gKeys.Count() > 0)
                    {
                        var History = AllActiveSIs.Where(e => gKeys.Any(g => g.StateId == e.StateId && g.Context == e.Context)).Select(e => e.History).Distinct().ToList().FirstOrDefault();
                        var HInstances = db.HistoryStateInstances.Where(e => e.History.Id == History).Include(e => e.CurrentStateInstance).ToList();
                        var Sis = HInstances.Select(e => e.CurrentStateInstance).Distinct();
                        var SiIds = Sis.Select(e => e.Id).ToList();
                        db.Tasks.RemoveRange(db.Tasks.Where(e => SiIds.Contains(e.StateInstanceId)));
                        db.SaveChanges();
                        db.StateInstances.RemoveRange(Sis);
                        db.SaveChanges();
                        db.HistoryStateInstances.RemoveRange(HInstances);
                        db.SaveChanges();
                        db.Histories.Remove(db.Histories.Find(History));
                        db.SaveChanges();
                    }
                    else
                    {
                        System.Threading.Tasks.Task.Delay(5000);
                    }
                }
                catch (Exception)
                {

                }
            }
        }
    }

    public class HS
    {
        public string Context { get; set; }
        public int History { get; set; }
        public int StateId { get;  set; }
    }
}
