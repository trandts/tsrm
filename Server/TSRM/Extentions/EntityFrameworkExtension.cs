﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TSRM.Extentions
{
    public static class EntityFrameworkExtension
    {
        //public static bool SafeAttach<TContext, TEntity>(this TContext context, TEntity entity)
        // where TContext : DbContext where TEntity : class
        //{
        //    if (context.Set<TEntity>().Local.Any(e => e == entity)||  entity == null)
        //    {
        //        return true;
        //    }
        //    context.Set<TEntity>().Attach(entity);
        //    return true;
        //}
        public static TEntity SafeAttach<TEntity>(this DbSet<TEntity> set, TEntity entity)
         where TEntity : class
        {
            if (set.Local.Any(e => e == entity) )
            {
                return set.Attach(entity);
            }
            return entity;
        }
    }
}