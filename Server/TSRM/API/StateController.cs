﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API
{
    public class StateModel
    {
        public string Activity { get; set; }
        public List<string> Events { get; set; } = new List<string>();
        public string Name { get; set; }
        public int ActivityId { get; set; }
        public int ParentStateId { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public int Gate { get; set; }
        public string TaskConfiguration { get; set; }
        public bool IsMultiInstance { get; set; }
        public List<int> Team { get; set; } = new List<int>();
    }
    public class StateController : ApiController
    {
        public IEnumerable<State> Get()
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return db.States.Include(e => e.Case)
                .Include(e => e.Gate)
                .Include(e => e.TaskConfiguration)
                //.Include(e => e.TriggerAcitvity)
                .Where(e => !e.IsDeleted).OrderByDescending(E => E.Id).ToList();
        }
        //GET api/<controller>
        public void Post([FromBody]StateModel value)
        {
            var db = new TSRMDBContext();
            TriggerAcitvity triggerActivity=null;
            
            if(value.ActivityId==0 && !string.IsNullOrWhiteSpace( value.Activity))
            {
                var act = db.ActivityTypes.Attach(db.ActivityTypes.FirstOrDefault(f => f.Name == "UI Element"));
                triggerActivity =  db.TriggerAcitvities.Add(new TriggerAcitvity {  ActivityType=act, UIElement=value.Activity,
                 //Events=value.Events.Select(ev=>new Event { EventName=ev }).ToList()
                });
                db.SaveChanges();
                db.TriggerAcitvities.Attach(triggerActivity);

            }
            else
            {

            }
            var gate = db.StateOrthogonalityGates.Find(value.Gate);
            var _state=db.States.Add(new State
            {
                Gate = gate,
                //TriggerAcitvity = triggerActivity,
                IsEnd = value.IsEnd,
                IsStart = value.IsStart,
                IsMultiInstance = value.IsMultiInstance,
                Name = value.Name,
                ParentStateId = value.ParentStateId,
                TaskConfiguration = String.IsNullOrWhiteSpace(value.TaskConfiguration) ? null : new TaskConfiguration { TaskName = value.TaskConfiguration },
                StateType = db.StateTypes.Attach(db.StateTypes.FirstOrDefault(e => e.Name == "Buisness State Machine"))

            });
            db.SaveChanges();
            var pt = db.PermissionTypes.Attach(db.PermissionTypes.FirstOrDefault(e => e.Name == "Edit"));
            var teams=value.Team.Select(tm => new StateRole {  Team= db.Teams.Attach(new Team { Id = tm }) ,
                 State=_state,
                  PermissionType= pt
            }).ToList();
            db.StateRoles.AddRange(teams);
            db.SaveChanges();
        }
    }
}
