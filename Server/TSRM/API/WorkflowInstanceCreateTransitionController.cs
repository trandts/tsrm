﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API
{
    public class WorkflowInstanceCreateTransitionParam
    {
        public int WorkflowInstance { get; set; }
        public string Event { get; set; }
        public int User { get; set; }
        public string Context { get; set; }
        public int StateId { get; set; }
    }
    public class WorkflowInstanceCreateTransitionController : ApiController
    {
        public int Post([FromBody]WorkflowInstanceCreateTransitionParam model)
        {
            return WorkflowEngine.EventWorkflowInstance(
                model.Event, model.WorkflowInstance,
                model.User, model.Context);//, model.StateId
        }
    }
}
