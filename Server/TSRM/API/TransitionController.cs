﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;

namespace TSRM.API
{
    public class TransitionModel
    {
        public int FromState { get; set; }
        public int ToState { get; set; }
        public string Event { get; set; }
        public string TName { get; set; }
    }
    public class TransitionController : ApiController
    {
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public void Post([FromBody]TransitionModel value)
        {
            var db = new TSRMDBContext();
            var from = db.States.Find(value.FromState);
            Event _event = null;// from.TriggerAcitvity?.Events?.FirstOrDefault(ev => ev.EventName == value.Event);
            if (_event!=null)
            {
                db.Events.Attach(_event);
            }
           var _t= db.Transitions.Add(new Transition {
                Event = _event
                , FromState=from
                , Name=value.TName,
                 ToState = db.States.Find(value.ToState)
            });
            db.SaveChanges();
        }

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}