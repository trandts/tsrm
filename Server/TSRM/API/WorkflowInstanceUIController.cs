﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;
using TSRM.Engines;

namespace TSRM.API
{
    public class WorkflowInstanceUI
    {
        public StateInstance Instance { get; set; }
        public WorkflowInstanceUIPart States { get; set; }
        public List<TransitionUI> Histories { get; set; }
        public List<TransitionUI> Comments { get; set; }
        public List<TransitionUI> Tags { get; set; }
        public List<OtherStateTypeInstance> OtherStateTypeInstance { get; set; }
    }
    public class OtherStateTypeInstance
    {
        public int StateId { get; set; }
        public int StateInstanceId { get; set; }
        public string TriggerUIElement { get; set; }
        public string TriggerMethod { get; set; }
        public string Context { get; set; }
        public List<string> Events { get; set; }
        public bool IsAutomated { get;  set; }
    }
    public class TransitionUI
    {
        public string Name { get; set; }
        public string Creator { get; set; }
        public DateTime? Date { get; set; }
    }
    public class WorkflowInstanceUIPart
    {
        public string StateName { get; set;}
        public int StateId { get; set; }
        public int StateInstanceId { get; set; }
        public int USM_ID { get; set; }
        public string TriggerUIElement { get; set; }
        public string TriggerMethod { get; set; }
        public List<string> Events { get; set; }
        public bool IsAutomated { get;  set; }

        public bool IsTask { get;  set; }
        public string TaskName { get; set; }
        public string TaskContext { get; set; }
        public string TaskContextType { get; set; }
        public List<string> AssignedUsers { get; set; }
        public List<string> VisibilityUsers { get; set; }
        public DateTime? LastTransitionOn { get; set; }
        public string LastUser { get; set; }

        public string TaskProject { get; set; }
        public List<ContextTagInfo> ContextTagInfo { get; set; }
    }
    public class ContextTagInfo
    {
        public int current { get; set; }
        public List<Tag> Options { get; set; }
        public string Category { get; set; }
        public int Instance { get; set; }
        public string Context { get; set; }
        public string ContextTagCategoryId { get; set; }
    }
    public class WorkflowInstanceUIController : ApiController
    {
        // GET api/<controller>/5
        public WorkflowInstanceUI Get(int id, int UserId)
        {
            var db = new TSRMDBContext();

            var wi = db.HistoryStateInstances.FirstOrDefault(f => f.CurrentStateInstance.Id == id && f.DisableHistoryId == 0 &&
                        f.CurrentStateInstance.Status == "Active");
            var cs = WorkflowEngine.GetCurrentStatesWithTriggerActivities(id, UserId);
            if (wi == null||cs==null)
            {
                return null;
            }
            var guid = Guid.NewGuid().ToString();
            var States = db.States.Where(e => e.ParentStateId == cs.State.Id && e.StateType.Id != cs.State.StateType.Id).ToList();
            var Other_SIDs=States.Select(e => WorkflowEngine.CreateWorkflowInstance(UserId, guid, e.Id,id,null, e.IsStart)).ToList();
            //
            var _task = db.Tasks.Where(e => e.StateInstanceId == cs.Id &&
                     e.ToDisplay == true && e.IsCompleted == false && e.IsClosed == false && e.IsDeleted == false).FirstOrDefault() ?? new Task();
            var _allTeams = db.StateRoles.Where(e => e.State.Id == cs.State.Id).Select(e => e.Team.Id).ToList();
            var _case_id = cs.State?.Case?.Id ?? 0;
            var filteredcs = new WorkflowInstanceUIPart { StateInstanceId = cs.Id, StateId = cs.State.Id,
                TriggerMethod = cs.State.TriggerAcitvity?.MethodName,//..
                IsAutomated=cs.State.TriggerAcitvity?.ActivityType?.IsAutomated??false,
                StateName=cs.State.Name,
                IsTask=(cs?.State?.TaskConfiguration?.Id??0)>0,
                 //Events = cs.State.TriggerAcitvity?.Events.Select(ev => ev.EventName).ToList(),
                Events = db.Transitions.Where(t => t.FromState.Id == cs.State.Id).Select(t => t.Event.EventName).ToList(),
                TaskName = _task?.Name,
                TaskContext = _task?.CaseContext,
                TaskContextType = _task?.Case,
                TaskProject = "Task" == _task?.Case ? db.Tasks.Find(_task?.CaseContextId)?.Project?.Name : null,
                

                VisibilityUsers = db.CaseTeamMembers.Where(e => e.Case.Id == _case_id && e.Case_ContextId == _task.CaseContextId && !e.IsDeleted && _allTeams.Contains(e.Team.Id)).Select(e => e.User.Username).Distinct().ToList(),
                AssignedUsers = db.CaseTeamMembers.Where(e => e.Case.Id == _case_id && e.Case_ContextId == _task.CaseContextId && !e.IsDeleted && e.Team.Name == "Project Task Assigned To").Select(e => e.User.Username).Distinct().ToList(),
                LastUser = db.User.Find(_task?.CreatedBy ?? 0)?.Username,
                LastTransitionOn = _task?.CreatedOn
            };
            var nestedDiffTypedSMsInstances = db.HistoryStateInstances
                .Where(e => e.CurrentStateInstance.ParentInstanceId == id && e.CurrentStateInstance.State.StateType.Id != cs.State.StateType.Id
                && e.DisableHistoryId == 0 && e.CurrentStateInstance.Context == guid && !e.IsDeleted).Select(e => e.CurrentStateInstance).ToList();
           
           var nestedDiffTypedSMs= nestedDiffTypedSMsInstances
                .Select(e=>new OtherStateTypeInstance { Context=guid, StateId =e.State.Id, StateInstanceId=e.Id,
                    TriggerMethod = e.State.TriggerAcitvity?.MethodName,//..
                    IsAutomated = e.State.TriggerAcitvity?.ActivityType?.IsAutomated ?? false,
                    //TriggerUIElement =e.State.TriggerAcitvity?.UIElement,
                    Events = db.Transitions.Where(t=>t.FromState.Id==e.State.Id).Select(t=>t.Event.EventName).ToList()
                }).ToList();
            db.Configuration.ProxyCreationEnabled = false;
            var si = db.StateInstances.Include(e=>e.State.Case).FirstOrDefault(e=>e.Id==id);
            var caseId = si.State?.Case?.Id??0;
            var hist = db.HistoryStateInstances.Where(e => e.CurrentStateInstance.ParentInstanceId == si.ParentInstanceId &&
             e.CurrentStateInstance.Context == si.Context )
             .Include(e=>e.History.Transition)
             .Include(e => e.History.User).OrderBy(e=>e.ModifiedOn).ToList();
           var _hist= hist.Select(e => e.History).ToList().Select(e=>new TransitionUI {  Date=e.CreatedOn,
               Name =e.Transition?.Name
           , Creator=e.User?.Username}).ToList();
            var b = db.UsmBsmBridge.Where(e => e.BSM.Id == filteredcs.StateId).Include(e=>e.USM).FirstOrDefault();
            filteredcs.TriggerUIElement = b?.USM?.Name;
            filteredcs.USM_ID = b?.USM?.Id??0;
            si.State = null;
            si.Requestor = null;
            var _currentContextStateInstances=db.HistoryStateInstances.Where(s => s.DisableHistoryId == 0 && s.CurrentStateInstance.State.Case.Id == caseId &&
            s.CurrentStateInstance.Context == si.Context).Select(e => e.CurrentStateInstance.Id).ToList();
            var _currentContextTasks = db.Tasks.Where(e => _currentContextStateInstances.Contains(e.StateInstanceId)).Select(e => e.Id).ToList();
            var notes = db.Notes.Where(e => _currentContextTasks.Contains(e.task.Id)).ToList().Select(e=>new TransitionUI {  Creator=e.User.Username, Date=e.CreatedOn, Name=e.Name }).ToList();
            filteredcs.ContextTagInfo = GetContextTagInfo(db, _case_id, si);
            var ContentTags = db.ContentTags.Where(e => e.ContentTagCategory.ContentContext.Context == si.Context && e.ContentTagCategory.ContentContext.ContextType == caseId).Include(e => e.Tag.TagCategory)
                .Include(e => e.ContentTagCategory)
                     .ToList();

            var _tagsInfo= ContentTags.Select(e => new TransitionUI { Creator = e.ByUser.Username, Date = e.CreatedOn, Name = e.Tag.Name + " (" + e.Tag.TagCategory.CategoryName + ")" }).ToList();
            _tagsInfo.AddRange(db.TaskTags.Where(e => _task.Id == e.TaskTagCategory.Task.Id).Include(e => e.Tag.TagCategory).ToList().Select(e => new TransitionUI { Creator = e.ByUser.Username, Date = e.CreatedOn, Name = e.Tag.Name + " (" + e.Tag.TagCategory.CategoryName + ")" }).ToList());
            return new WorkflowInstanceUI
            {
                 OtherStateTypeInstance= nestedDiffTypedSMs,
                States = filteredcs,
                Instance = si,
                Histories = _hist,
                Comments = notes,
                Tags = _tagsInfo
            };
        }

        private List<ContextTagInfo> GetContextTagInfo(TSRMDBContext db,int caseId,StateInstance si)
        {
            var ContentTags = db.ContentTags.Where(e => e.ContentTagCategory.ContentContext.Context == si.Context && e.ContentTagCategory.ContentContext.ContextType == caseId && e.IsDeleted==false).Include(e => e.Tag.TagCategory)
                .Include(e => e.ContentTagCategory)
                     .ToList();

            var list = ContentTags.Where(e => e.Tag.TagCategory.CategoryName != "StateLessCategories").Select(e => e.ContentTagCategory).Distinct().ToList();
   
            var ContextTagInfo = list.Select(e => new ContextTagInfo
            {
                Category = e.TagCategory.CategoryName,
                ContextTagCategoryId = e.Id.ToString(),
                Options = db.Tags.Where(t => t.TagCategory.Id == e.TagCategory.Id).ToList(),
                current = ContentTags.FirstOrDefault(f => f.ContentTagCategory.Id == e.Id)?.Tag?.Id ?? 0
            }).ToList();
            for (int i = 0; i < ContextTagInfo.Count(); i++)
            {
                var ContextTagCategoryId = ContextTagInfo[i].ContextTagCategoryId;
                var a = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 &&
                  e.CurrentStateInstance.Context == ContextTagCategoryId
                  && e.CurrentStateInstance.State.Case.Name == "ContentTagCategory"
                  && e.CurrentStateInstance.State.Name == "Display Task Category Tag").Select(e => e.CurrentStateInstance).FirstOrDefault();
                ContextTagInfo[i].Instance = a.Id;
                ContextTagInfo[i].Context = a.Context;
            }
            return ContextTagInfo;
        }

        /*
// POST api/<controller>
public void Post([FromBody]TriggerAcitvity model)
{
   var db = new TSRMDBContext();
   var AT= db.ActivityTypes.Attach(db.ActivityTypes.FirstOrDefault(f => f.Name == "UI Element"));
   model.ActivityType = AT;
   db.TriggerAcitvities.Add(model);
   db.SaveChanges();
}
*/
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}