﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;
using TSRM.Engines;

namespace TSRM.API
{
    public class WorkflowModel
    {
        public int ParentStateId { get; set; }
        public int ParentStateInstanceId { get; set; }
        public int StateId { get; set; }
        public bool IsChildMultiInstance { get; set; }
        public string UIElement { get; set; }
    }
    public class WorkflowController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return this.Request.CreateResponse(
                    HttpStatusCode.OK,
                    db.States.Where(e=>!e.IsDeleted && e.IsStart && e.ParentStateId==0).ToList()
              );
        }

        // GET api/<controller>/5
        public List<WorkflowModel> Get(int id)
        {
            var s = WorkflowEngine.GetStartStates(id);
            //e.IsChildMultiInstance
            return s.Select(e => new WorkflowModel
            {
                IsChildMultiInstance = e.IsMultiInstance,
                ParentStateId = e.ParentStateId,
                StateId = e.Id,
                //UIElement = e.TriggerAcitvity?.UIElement,
                ParentStateInstanceId = 0
            }).ToList();
        }
      
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}