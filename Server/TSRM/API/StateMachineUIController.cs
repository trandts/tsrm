﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;

namespace TSRM.API
{
    public class StateMachineUINode
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int LogicId { get; set; }
        public string Logic { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public bool IsMultiInstance { get; set; }
        public int ParentStateId { get; set; }
        public string ContextType { get; set; }
        public List<StateMachineUITransitionsNode> Transitions { get; set; }
        public List<string> Events { get; set; }
        public List<StateMachineUINode> ChildStateMachine { get; set; }
        public List<StateMachineUINode> ChildDataStateMachine { get; set; }
        public List<StateMachineUIRoles> Teams { get; set; }
        public void Load( TSRMDBContext db)
        {
            var data = db.States.Where(E => E.ParentStateId == Id && E.StateType.Name== Type && E.IsDeleted==false).ToList().Select(e => new StateMachineUINode
            {
                Id = e.Id,
                Name = e.Name,
                Logic = e.Gate?.Name,
                LogicId=e.Gate?.Id??0,
                Type = e.StateType?.Name,
                IsStart = e.IsStart,
                IsEnd = e.IsEnd,
                ContextType=e.Case?.Name,
                Teams =db.StateRoles.Where(sr=>sr.State.Id==e.Id).ToList().Select(sr=>new StateMachineUIRoles
                {
                      Team=sr.Team.Id,
                      Permission=sr.PermissionType.Id
                }).ToList(),
                ParentStateId=e.ParentStateId,
                IsMultiInstance = e.IsMultiInstance,
                Events = db.Transitions.Where(t => t.FromState.Id == e.Id).Select(te => te.Event.EventName).ToList(),
                Transitions = db.Transitions.Where(t => t.FromState.Id == e.Id).ToList().Select(t =>
                    new StateMachineUITransitionsNode { FromState = t.FromState.Name, FromStateId=t.FromState.Id, ToState = t.ToState.Name, ToStateId = t.ToState.Id, Name = t.Name, Event = t.Event?.EventName }
              ).ToList()
            }).ToList();
            foreach (var item in data)
            {
                item.Load(db);
            }
            ChildStateMachine= data;
            data = db.States.Where(E => E.ParentStateId == Id && E.StateType.ShortName == "DSM" && E.IsDeleted == false).ToList().Select(e => new StateMachineUINode
            {
                Id = e.Id,
                Name = e.Name,
                Logic = e.Gate.Name,
                Type = e.StateType.Name,
                ContextType = e.Case?.Name,
                IsStart =e.IsStart,
                  IsEnd=e.IsEnd,
                  IsMultiInstance= e.IsMultiInstance,
                Events = db.Transitions.Where(t => t.FromState.Id == e.Id).Select(te => te.Event.EventName).ToList(),
                Transitions = db.Transitions.Where(t => t.FromState.Id == e.Id).ToList().Select(t =>
                    new StateMachineUITransitionsNode { FromState = t.FromState.Name, ToState = t.ToState.Name, Name = t.Name, Event = t.Event?.EventName }
              ).ToList()
            }).ToList();
            foreach (var item in data)
            {
                item.Load(db);
            }
            ChildDataStateMachine = data;
        }
    }
    public class StateMachineUIRoles
    {
        public int Team { get; set; }
        public int Permission { get; set; }
    }
        public class StateMachineUITransitionsNode
    {
        public string Name { get; set; }
        public string Event { get; set; }
        public string FromState { get; set; }
        public string ToState { get; set; }
        public int FromStateId { get; set; }
        public int ToStateId { get; set; }
    }
    public class StateMachineUIController : ApiController
    {
        public List<StateMachineUINode> Get()
        {
            var db = new TSRMDBContext();
           var data=db.States.Where(E => E.ParentStateId == 0).ToList().Select(e=>new StateMachineUINode {  Id=e.Id,Name=e.Name, Logic=e.Gate.Name,  Type=e.StateType.Name
               ,
               Events = db.Transitions.Where(t => t.FromState.Id == e.Id).Select(te=>te.Event.EventName).ToList()
            ,
               IsStart = e.IsStart,
               IsEnd = e.IsEnd,
               IsMultiInstance = e.IsMultiInstance,
               ParentStateId = e.ParentStateId,
               ContextType = e.Case?.Name,
               LogicId = e.Gate?.Id ?? 0,
               Teams = db.StateRoles.Where(sr => sr.State.Id == e.Id).ToList().Select(sr => new StateMachineUIRoles
               {
                   Team = sr.Team.Id,
                   Permission = sr.PermissionType.Id
               }).ToList(),

               Transitions =db.Transitions.Where(t=>t.FromState.Id==e.Id).ToList().Select(t=>
            new StateMachineUITransitionsNode {  FromState=t.FromState.Name, ToState=t.ToState.Name, Name=t.Name, Event=t.Event?.EventName}
            ).ToList()}).ToList();
            foreach (var item in data)
            {
                item.Load(db);
            }
            return data;
        }
    }
}
