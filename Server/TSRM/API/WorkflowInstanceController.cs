﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API
{
   
    public class WorkflowInstanceController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return this.Request.CreateResponse(
                    HttpStatusCode.OK,
                    db.HistoryStateInstances.Where(e=> e.CurrentStateInstance.Status=="Active" &&
                    e.DisableHistoryId==0).Select(e=>e.CurrentStateInstance)
                    .Include(e => e.State).ToList()
              );
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int workflowid)
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return this.Request.CreateResponse(
                    HttpStatusCode.OK,
                    db.HistoryStateInstances.Where(e=>
                    e.DisableHistoryId==0).Select(e=>e.CurrentStateInstance)
                    .Include(e => e.State).ToList()
               //     db.WorkflowInstances.Include(b => b.Workflow).Where(e=>
                 //   e.Workflow.Id== workflowid).ToList()
              );
        }
        /*
        // POST api/<controller>
        public void Post([FromBody]TriggerAcitvity model)
        {
            var db = new TSRMDBContext();
            var AT= db.ActivityTypes.Attach(db.ActivityTypes.FirstOrDefault(f => f.Name == "UI Element"));
            model.ActivityType = AT;
            db.TriggerAcitvities.Add(model);
            db.SaveChanges();
        }
        */
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}