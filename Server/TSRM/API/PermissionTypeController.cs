﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;

namespace TSRM.API
{
    public class PermissionTypeController : ApiController
    {
        public List<PermissionType> Get()
        {
            var db = new TSRMDBContext();
            return db.PermissionTypes.ToList();
        }
    }
}
