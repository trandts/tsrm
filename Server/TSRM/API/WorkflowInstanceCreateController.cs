﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API
{
    public class WorkflowInstanceParam
    {
        public int StateId { get; set; }
        public string Event { get; set; }
        public int User { get; set; }
        public string Context { get; set; }
        public int ParentStateInstanceId { get; set; }
        public int ActualStateId { get; set; }
    }
    public class WorkflowInstanceCreateController : ApiController
    {
        public bool Post([FromBody]WorkflowInstanceParam model)
        {
            WorkflowEngine.CreateWorkflowInstance(
                model.User, model.Context, model.StateId,
                model.ParentStateInstanceId, model.Event,true);
           return true;
        }
    }
}
