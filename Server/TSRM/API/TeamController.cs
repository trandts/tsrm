﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API
{
    public class TeamController : ApiController
    {
        //GET api/<controller>
        public List<Team> Get()
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Teams.ToList().Select(e=>new Team { Id=e.Id, Name=e.Name }).ToList();
        }
    }
}
