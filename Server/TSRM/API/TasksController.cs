﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;

namespace TSRM.API
{
    public class ProjectApproved_Task
    {
        public int Id { get; set; }
        public Project Project { get; set; }
        public int StateInstanceId { get; set; }
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
        public string Case { get; set; }
        public string CaseContext { get; set; }
        public bool IsAssigned { get; set; }
        public bool IsCommon { get; set; }
    }
    public class TasksController : ApiController
    {
        public IEnumerable<ProjectApproved_Task> Get(int Id)
        {
            var db = new TSRMDBContext();
            var _tasks = db.Tasks.Where(e => e.IsDeleted==false && e.ToDisplay==true && e.IsClosed==false && e.StateInstanceId!=0).ToList();
            var Assignedteam = db.Teams.Where(e => e.Name == "Project Task Assigned To" && e.IsDeleted == false).Select(e => e.Id).ToList();
            var list = new List<ProjectApproved_Task>();
            foreach (var e in _tasks)
            {
                try
                {
                    var stateId = db.StateInstances.Find(e.StateInstanceId).State.Id;
                    var teams = db.StateRoles.Where(sr => sr.State.Id == stateId)
                             .Select(s => s.Team.Id).ToList();
                    var InTeams = db.CaseTeamMembers.Where(tm => e.IsDeleted == false && teams.Contains(tm.Team.Id) && tm.User.Id == Id && tm.Case.Name == e.Case && tm.Case_ContextId == e.CaseContextId).Select(t => t.Team).ToList();
                    if (InTeams.Count()>0)
                    {
                        list.Add(new ProjectApproved_Task { Case = e.Case, CaseContext = e.CaseContext, IsCompleted = e.IsCompleted, Id = e.Id, Name = e.Name, StateInstanceId = e.StateInstanceId, Project = e.Project
                          ,IsAssigned= InTeams.Any(it=> Assignedteam.Any(at=>at==it.Id))
                          , IsCommon=db.Transitions.Any(t=>t.IsDeleted==false && t.FromState.Id==stateId
                          && t.ToState.IsMultiInstance==true)
                        });
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            return list;
        }
    }
}
