﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API.Setup
{
    public class TriggerAcitvityController : ApiController
    {
        public IEnumerable<TriggerAcitvity> Get()
        {
            var db = new TSRMDBContext();
            db.Configuration.ProxyCreationEnabled = false;
            return db.TriggerAcitvities.Include(e=>e.ActivityType)
                .Where(e => !e.IsDeleted).ToList();
        }
    }
}
