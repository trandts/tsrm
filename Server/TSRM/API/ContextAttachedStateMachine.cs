﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Extentions;
using TSRM.Models;

namespace TSRM.API
{
    public class ContextAttachedStateMachine
    {
        public int NotesManagementId { get; set; }
        public int NotesManagementInstanceId { get; set; }
        public string TaskId { get; set; }
    }
    public class ContextAttachedStateMachineController : ApiController
    {
        public List<ContextAttachedStateMachine> Get(int Id,string ManagementStateName)
        {
            ManagementStateName = ManagementStateName + " Management";
            var db = new TSRMDBContext();
            var SI = db.StateInstances.Find(Id);
            var TaskId = db.HistoryStateInstances.FirstOrDefault(e => e.CurrentStateInstance.Id == Id && e.DisableHistoryId == 0)?.Task?.Id;
            
            if (TaskId == null)
            {
                return null;
            }
            var CaseId = SI?.State?.Case?.Id ?? 0;
            var ContextId = SI?.Context;
            var _ctx=db.ContentContexts.FirstOrDefault(f => f.Context == ContextId && f.ContextType == CaseId);
            if (_ctx==null)
            {
                _ctx= db.ContentContexts.Add(new ContentContext { Context = ContextId, ContextType = CaseId });
                db.SaveChanges();
            }
            var _ctxId = _ctx.Id.ToString();
            if (SI.State.Case.Name != "System")
            {
                while (SI.State.Name != "Organization Management")
                {
                    SI = db.StateInstances.Find(SI.ParentInstanceId);
                }

                var ManagementState = db.States.FirstOrDefault(f => f.Name == ManagementStateName) ?? new State ();
                SI = db.StateInstances.FirstOrDefault(e =>  e.ParentInstanceId == SI.Id && e.State.Id == ManagementState.Id);
                
                var SI_Comment = db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.Context == _ctxId && e.CurrentStateInstance.ParentInstanceId == SI.Id)
                    .Select(e => e.CurrentStateInstance).FirstOrDefault();
                var list = new List<ContextAttachedStateMachine>();
                if (SI_Comment==null)
                {
                   SI_Comment = db.StateInstances.FirstOrDefault(e =>  e.ParentInstanceId == SI.Id && e.State.IsStart == true);
                    var SI_CommentId = (SI_Comment?.Id ?? 0);
                    list.Add(new ContextAttachedStateMachine
                    {
                        NotesManagementId = SI_Comment.State.Id,
                        NotesManagementInstanceId = SI_CommentId,
                        TaskId = _ctxId
                    });
                }
                else
                {
                    list.AddRange(GetRecursiveNodes(SI_Comment.Id, SI_Comment.State.StateType.Id));
                }
                return list.DistinctBy(e=>e.NotesManagementInstanceId).ToList();
            }
            else
            {
                return null;
            }
        }

        private List<ContextAttachedStateMachine> GetRecursiveNodes(int ParentInstanceId,int StateTypeId)
        {
            var list = new List<ContextAttachedStateMachine>();
            var db = new TSRMDBContext();
            if (db.HistoryStateInstances.Any(e => e.CurrentStateInstance.ParentInstanceId == ParentInstanceId && e.DisableHistoryId == 0 && e.CurrentStateInstance.State.StateType.Id == StateTypeId))
            {
                var AllChildInstances = db.HistoryStateInstances.Where(e => e.CurrentStateInstance.ParentInstanceId == ParentInstanceId && e.DisableHistoryId == 0 && e.CurrentStateInstance.State.StateType.Id == StateTypeId).Select(e => e.CurrentStateInstance).ToList();
                list.AddRange(AllChildInstances.Select(e => new ContextAttachedStateMachine { NotesManagementInstanceId=e.Id, NotesManagementId=e.State.Id, TaskId=e.Context }));
                list.AddRange(AllChildInstances.SelectMany(e => GetRecursiveNodes(e.Id, StateTypeId)));
            }
            return list;
        }
    }
}
