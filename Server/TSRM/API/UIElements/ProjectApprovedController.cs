﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class ProjectApproved
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TaskWorkflowId { get; set; }
        public List<ProjectApproved_Task> Tasks { get; set; }
    }
    public class ProjectApproved_Task
    {
        public int Id { get; set; }
        public int WorkflowId { get; set; }
        public string Name { get; set; }
        public int Percentage { get; set; }
    }
    public class ProjectApprovedController : ApiController
    {
        

        // GET api/<controller>/5
        public ProjectApproved Get(int id)
        {
            return null;
            //var db = new TSRMDBContext();
            //var _tasks = db.Tasks.Where(e => e.Project.Id == id && !e.IsDeleted).ToList();
            //var pe = db.ProjectEstimations.FirstOrDefault(f => f.Project.Id == id && !f.IsDeleted && !f.Archived);
            //var wf = db.Workflows.FirstOrDefault(f => f.Name == "Project Task Creation");
            //return new ProjectApproved
            //{
            //    Id = id,
            //    Name = pe?.Project?.Name,
            //    TaskWorkflowId= wf?.Id ?? 0,
            //    Tasks = _tasks.Select(e=>new ProjectApproved_Task { Id=e.Id, Name=e.Name, Percentage=3 }).ToList()
            //};
        }
        // GET api/<controller>/5
        public int Get(string Taskid)
        {
            return 0;
           // var db = new TSRMDBContext();
           //return db.WorkflowInstances.FirstOrDefault(f => f.Context== Taskid && f.Workflow.Name == "Project Task Creation")?.Id??0;
            
        }

        // POST api/<controller>
        public int Post([FromBody]ProjectApproved model)
        {
            var db = new TSRMDBContext();
            
            return 0;
        }

        // PUT api/<controller>/5
        public int Put(int id, [FromBody]ProjectCreation model)
        {
            
            return 0;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}