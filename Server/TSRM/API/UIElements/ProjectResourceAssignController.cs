﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API.UIElements
{
    public class ProjectResourceAssign
    {
        public int ProjectId { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }
        public List<ProjectResourceDetails> Resource { get; set; }
    }
    public class ProjectResourceDetails
    {
        public int User { get; set; }
        public int Team { get; set; }
    }
    public class ProjectResourceAssignController : ApiController
    {

        // GET api/<controller>/5
        public ProjectResourceAssign Get(int id)
        {
            var db = new TSRMDBContext();
            var _R = db.CaseTeamMembers.Where(e =>
                e.Case_ContextId == id &&
                e.Case.Name == "Project" && e.IsDeleted == false).Include(e=>e.User)
                .Include(e=>e.Team).ToList().Select(e => new ProjectResourceDetails
                {
                    Team = e.Team.Id,
                    User = e.User.Id
                }).ToList();

            db.Configuration.ProxyCreationEnabled = false;
            var o = new ProjectResourceAssign {
                Users = db.User.ToList().Select(e=> new User{ Id=e.Id,Username= e.Username}).ToList(),
                Teams = db.Teams.ToList().Select(e => new Team { Id=e.Id,Name=e.Name}).ToList(),
                Resource = _R,
                ProjectId = id
            };

            return o;

        }
        // GET api/<controller>/5
        public void Post(int id, [FromBody]ProjectResourceAssign model)
        {
            model.Resource.RemoveAll(e => e.Team == 0 || e.User == 0);
            var db = new TSRMDBContext();
            foreach (var item in db.CaseTeamMembers.Where(e => e.Case_ContextId == model.ProjectId &&
              e.Case.Name == "Project" && e.IsDeleted == false).Select(e=>e.Id).ToList())
            {
                var a = db.CaseTeamMembers.Find(item);
                a.IsDeleted = true;
                db.SaveChanges();
            }
            var projectCase = db.Cases.Attach(db.Cases.FirstOrDefault(f => f.Name == "Project"));
            db.CaseTeamMembers.AddRange(model.Resource
                .Select(e=>new CaseTeamMember {
                     Case_ContextId=model.ProjectId,
                      Case=projectCase,
                    Team = db.Teams.Find(e.Team),
                    User = db.User.Find(e.User)
                }).ToList());
            db.SaveChanges();
        }

    }
}