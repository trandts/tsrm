﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;
using TSRM.Engines;

namespace TSRM.API.UIElements
{
    public class ReadyToCreateContentTagCategoryController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<TagCategory> Get()
        {
            var db = new TSRMDBContext();
            return db.TagCategories.ToList();
        }

        // GET api/<controller>/5
        public bool Get(int id,int Instance,int User)
        {
            var db = new TSRMDBContext();
            var ctx = db.ContentContexts.Attach(new ContentContext { Id = id });
            var _User = db.User.Attach(new User { Id = User });
            var ContentTagCategories= db.TagCategories.Where(e => !db.ContentTagCategories.Where(r => r.ContentContext.Id == id).Select(c => c.TagCategory.Id).Contains(e.Id)).ToList().Select(e => new ContentTagCategory { TagCategory = db.TagCategories.Attach(e), ContentContext = ctx , ContentTags=db.Tags.Where(tg=>tg.IsDefault==true &&tg.TagCategory .Id==e.Id).ToList().Select(tg=>new ContentTag { ByUser = _User, CreatedBy = User, CreatedOn = DateTime.Now, Tag =db.Tags.Attach( tg)  } ).ToList()}).ToList();
            db.ContentTagCategories.AddRange(ContentTagCategories);
            db.SaveChanges();
            ContentTagCategories.Select(e => WorkflowEngine.EventWorkflowInstance("OnTaskTagCategoryCreated", Instance, User, e.Id.ToString())).ToList();
            return ContentTagCategories.Count() > 0;
        }

        // POST api/<controller>
        public void Post(int id)
        {
            var db = new TSRMDBContext();
            db.SaveChanges();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}