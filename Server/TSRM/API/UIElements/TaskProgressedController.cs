﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class TaskProgressed
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public int EstimatedHours { get; set; }
        public int ActualHours { get; set; }
        public int Completed { get; set; }
    }
    public class TaskProgressedController : ApiController
    {

        // GET api/<controller>/5
        public TaskProgressed Get(int id, int UserId)
        {
            var db = new TSRMDBContext();
            var t = db.Tasks.FirstOrDefault(f => f.Id == id && !f.IsDeleted);
            var tae = db.TaskAssignmentEstimations.OrderByDescending(e => e.Id)
                  .FirstOrDefault(f => f.TaskAssignment.Task.Id == id && f.TaskAssignment.User.Id == UserId
                  && !f.IsDeleted && !f.Archived) ?? new TaskAssignmentEstimation();
            var taW = db.TaskAssignmentWorkItems.OrderByDescending(e => e.Id)
                   .FirstOrDefault(f => f.TaskAssignment.Task.Id == id && f.TaskAssignment.User.Id == UserId
                   && !f.IsDeleted) ?? new TaskAssignmentWorkItem();
            return new TaskProgressed
            {
                Id = id,
                UserId = UserId,
                Name = t.Name,
                EstimatedHours = tae.EstimatedHours,
                ActualHours = tae.ActualHours,
                Completed = taW.Completion
            };

        }
        // GET api/<controller>/5
        public void Post(int id, [FromBody]TaskProgressed model)
        {
            var db = new TSRMDBContext();
            var t = db.Tasks.Attach(db.Tasks.FirstOrDefault(f => f.Id == id && !f.IsDeleted));
            var now = DateTime.Now;
            var ta = db.TaskAssignments.FirstOrDefault(f => f.Task.Id == id
         && f.FromDate < now && f.ToDate == null
            && f.User.Id == model.UserId && !f.IsDeleted);
            var tae = db.TaskAssignmentEstimations.OrderByDescending(e => e.Id)
                  .FirstOrDefault(f => f.TaskAssignment.Task.Id == id && f.TaskAssignment.User.Id == model.UserId
                  && !f.IsDeleted && !f.Archived);
            if (ta != null)
            {
                db.TaskAssignments.Attach(ta);
                if (tae != null)
                {
                    db.TaskAssignmentEstimations.Attach(tae);
                    tae.Archived = true;
                }
            }
            else
            {
                var user = db.User.Attach(db.User.Find(model.UserId));
                ta = new TaskAssignment { User = user, FromDate = DateTime.Now, Task = t };
            }
            db.TaskAssignmentEstimations.Add(new TaskAssignmentEstimation { EstimatedHours = model.EstimatedHours, ActualHours = model.ActualHours, TaskAssignment = ta });
            db.TaskAssignmentWorkItems.Add(new TaskAssignmentWorkItem { Completion = model.Completed, TaskAssignment = ta });
            db.SaveChanges();
        }

    }
}