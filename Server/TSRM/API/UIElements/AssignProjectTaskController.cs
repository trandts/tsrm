﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class AssignProjectTask
    {
        public int TId { get; set; }
        public int UserId { get; set; }
        public List<User> Users { get; set; }
    }
    public class AssignProjectTaskController : ApiController
    {

        // GET api/<controller>/5
        public AssignProjectTask Get(int id)
        {
            var db = new TSRMDBContext();
         return new AssignProjectTask { TId = id,
                UserId = db.CaseTeamMembers.FirstOrDefault(f => f.Case_ContextId == id && f.Team.Name== "Project Task Assigned To" && f.Case.Name == "Task" && !f.IsDeleted)?.User?.Id ?? 0,
                  Users=db.User.ToList()
            }; 

        }
        // GET api/<controller>/5
        public void Post(int id, [FromBody]AssignProjectTask model)
        {
            var db = new TSRMDBContext();
            var oldUser=db.CaseTeamMembers.FirstOrDefault(f => f.Case_ContextId == model.TId && f.Team.Name == "Project Task Assigned To" && f.Case.Name == "Task" && !f.IsDeleted);
            if (oldUser!=null)
            {
                oldUser = db.CaseTeamMembers.Find(oldUser.Id);
                oldUser.IsDeleted = true;
                oldUser.ModifiedBy = id;
                oldUser.ModifiedOn = DateTime.Now;
                db.SaveChanges();
            }
            if (model.UserId!=0)
            {
                var user = db.User.Find(model.UserId);
                var team = db.Teams.Attach(db.Teams.FirstOrDefault(f => f.Name == "Project Task Assigned To"));
                var _case = db.Cases.Attach(db.Cases.FirstOrDefault(f => f.Name == "Task"));
                db.CaseTeamMembers.Add(new CaseTeamMember
                {
                    Case = _case,
                    Team = team,
                    User = user,
                    Case_ContextId = model.TId,
                    CreatedBy = id,
                    CreatedOn = DateTime.Now

                });
                db.SaveChanges();
            }
        }

    }
}