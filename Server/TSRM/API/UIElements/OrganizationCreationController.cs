﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Extentions;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class OrganizationCreationController : ApiController
    {
        public int Post(int Id,Organization model)
        {
            var db = new TSRMDBContext();
            model.CreatedOn = DateTime.Now;
            var org = db.Organizations.Add(model);
            db.SaveChanges();
            var _t_CEO = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "CEO"));
            var _t_ContentReq = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Content Creator"));
            var _t_Leaders = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Leaders"));
            var Case = db.Cases.Attach(db.Cases.FirstOrDefault(e => e.Name == "Organization"));
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = org.Id,
                User = db.User.SafeAttach(db.User.FirstOrDefault(e => e.Username == "ip")),
                Team = _t_CEO,
                CreatedBy = Id,
                CreatedOn = DateTime.Now
            });
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = org.Id,
                User = db.User.Find(Id),
                Team = _t_ContentReq,
                CreatedBy = Id,
                CreatedOn = DateTime.Now
            });
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = org.Id,
                User = db.User.SafeAttach(db.User.FirstOrDefault(e => e.Username == "salman")),
                Team = _t_Leaders,
                CreatedBy = Id,
                CreatedOn = DateTime.Now
            });
            db.SaveChanges();
            return org.Id;
        }
    }
}
