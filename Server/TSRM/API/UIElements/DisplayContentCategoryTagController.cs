﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using TSRM.Signalr;

namespace TSRM.API.UIElements
{
    public class DisplayContentCategoryTag
    {
        public TagCategory Category { get; set; }
        public List<string> SelectedTags { get; set; } = new List<string>();
        public List<Tag> Tags { get; set; }
        public int User { get; set; }
    }
    public class DisplayContentCategoryTagController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            //DisplayTaskCategoryTag
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public DisplayContentCategoryTag Get(int id)
        {
            var db = new TSRMDBContext();
            var f = db.ContentTagCategories.Find(id);
            var model = new DisplayContentCategoryTag
            {
                Category = f.TagCategory,
                SelectedTags = db.ContentTags.Where(t =>
                t.ContentTagCategory.Id == id &&
                t.IsDeleted == false).Select(e => e.Tag.Id.ToString()).ToList(),
                Tags = db.Tags.Where(e => e.TagCategory.Id == f.TagCategory.Id).ToList()
            };
            return model;
        }

        // POST api/<controller>
        public void Post(int id, [FromBody]DisplayContentCategoryTag value)
        {
            var db = new TSRMDBContext();
            foreach (var item in db.ContentTags.Where(t =>
            t.ContentTagCategory.Id == id &&
            t.IsDeleted == false).ToList())
            {
                if (value.SelectedTags.All(t => t != item.Tag.Id.ToString()))
                {
                    db.ContentTags.Attach(item);
                    item.IsDeleted = true;
                }
            }
            db.SaveChanges();
            var user = db.User.Attach(db.User.Find(value.User));
            var f = db.ContentTagCategories.Find(id);
            //
            var _tasks = new List<ContentTag>();
            var _tagCat = db.TagCategories.Attach(f.TagCategory);
            foreach (var item in value.SelectedTags)
            {
                if (!db.ContentTags.Any(t =>
            t.ContentTagCategory.Id == id &&
            t.IsDeleted == false && t.Tag.Id.ToString() == item))
                {

                    int Id = 0;
                    Tag _tag = null;
                    if (int.TryParse(item, out Id))
                    {
                        _tag = db.Tags.Attach(new Tag { Id = Id });
                    }
                    else
                    {
                        _tag = new Tag { Name = item, TagCategory = _tagCat };
                    }
                    _tasks.Add(new ContentTag { Tag = _tag, ByUser = user, ContentTagCategory = f, CreatedBy = user.Id, CreatedOn = DateTime.Now });
                }
            }

            db.ContentTags.AddRange(_tasks);
            db.SaveChanges();
            db = new TSRMDBContext();
            var _taskNames = _tasks.Select(e => db.Tags.Find(e.Tag.Id).Name + " (" + e.ContentTagCategory.TagCategory.CategoryName + ")").ToList();
            var SIs=db.HistoryStateInstances.Where(e => e.DisableHistoryId == 0 && e.CurrentStateInstance.Context == f.ContentContext.Context && e.CurrentStateInstance.State.Case.Id == f.ContentContext.ContextType)
                .Select(e => e.CurrentStateInstance.Id).ToList();
            MainHub.TagAdded(SIs, new List<int> { user.Id }, user.Username, _taskNames);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}