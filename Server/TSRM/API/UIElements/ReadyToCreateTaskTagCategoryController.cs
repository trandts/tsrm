﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;
using System.Data.Entity;

namespace TSRM.API.UIElements
{
    public class ReadyToCreateTaskTagCategoryController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<TagCategory> Get()
        {
            var db = new TSRMDBContext();
            return db.TagCategories.ToList();
        }

        // GET api/<controller>/5
        public IEnumerable<TagCategory> Get(int id)
        {
            var db = new TSRMDBContext();
            return db.TagCategories.Where(e=> !db.TaskTagCategories.Select(c => c.TagCategory.Id).Contains(e.Id)).ToList();
        }

        // POST api/<controller>
        public int Post([FromBody]TaskTagCategory value)
        {
            var db = new TSRMDBContext();
            db.TagCategories.Attach(value.TagCategory);
            db.Tasks.Attach(value.Task);
            db.TaskTagCategories.Add(value);
            db.SaveChanges();
           return value.Id;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}