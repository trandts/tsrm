﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class TaskCreateController : ApiController
    {

        // POST api/<controller>
        public int Post(int id,[FromBody]Task model)
        {
            var db = new TSRMDBContext();
            db.Projects.Attach(model.Project);
            db.Tasks.Add(model);
            db.SaveChanges();
            var _t_CEO = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "CEO"));
            var _t_ContentReq = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Content Creator"));
            var Case = db.Cases.Attach(db.Cases.FirstOrDefault(e => e.Name == "Task"));
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = model.Id,
                User = db.User.Attach(db.User.FirstOrDefault(e => e.Username == "ip")),
                Team = _t_CEO,
                     CreatedBy = id,
                CreatedOn = DateTime.Now
            });
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = model.Id,
                User = db.User.Find(id),
                Team = _t_ContentReq,
                CreatedBy = id,
                CreatedOn = DateTime.Now
            });
            var _t_ProjectManager = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Project Manager"));
            var PM = db.CaseTeamMembers.FirstOrDefault(f => f.Case.Name == "Project" && f.Case_ContextId == model.Project.Id
              && f.Team.Name == "Project Manager")?.User?.Id ?? 0;
            if (PM!=0)
            {

                db.CaseTeamMembers.Add(new CaseTeamMember
                {
                    Case = Case,
                    Case_ContextId = model.Id,
                    User = db.User.Find(PM),
                    Team = _t_ProjectManager,
                    CreatedBy = id,
                    CreatedOn = DateTime.Now
                });
            }
            db.SaveChanges();
            return model.Id;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]ProjectCreation model)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}