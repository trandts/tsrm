﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Extentions;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class AnotherNoteAddNoteInfo
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Username { get; set; }
        public DateTime Time { get; set; }
        public bool HasTask { get; set; }
        public List<int> Tags { get; set; }
    }
    public class AnotherNoteAdd
    {
        public List<AnotherNoteAddNoteInfo> AnotherNoteAddNoteInfo { get; set; }
        public List<Project> Projects { get; set; }
        public List<Tag> Tags { get; set; }
    }
    public class AnotherNoteAddConvert
    {
        public int NoteId { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
    }
    public class AnotherNoteAddTagUpdate
    {
        public int NoteId { get; set; }
        public List<string> Tags { get; set; }
        public int UserId { get; set; }
    }
    public class AnotherNoteAddController : ApiController
    {
        // GET api/<controller>/5
        public AnotherNoteAdd Get(int id)
        {
            var db = new TSRMDBContext();
            var ctx_type=db.Cases.FirstOrDefault(f => f.Name == "Note")?.Id ?? 0;
            var NoteInfo = db.Notes.Where(e => e.task.Id == id).ToList().Select(e => new AnotherNoteAddNoteInfo { Text = e.Name, Username = e.User.Username, Time = e.CreatedOn ?? DateTime.Now, HasTask=e.asTaskId>0, Id=e.Id ,
                Tags=db.ContentTags.Where(t=>t.IsDeleted==false&& t.ContentTagCategory.TagCategory.CategoryName== "StateLessCategories"
                &&t.ContentTagCategory.ContentContext.Context==e.Id.ToString()
                &&t.ContentTagCategory.ContentContext.ContextType== ctx_type).Select(t=>t.Tag.Id).ToList()
            })
                .ToList();
            var PIds = db.HistoryStateInstances.Where(e =>
              e.CurrentStateInstance.State.Name == "Ready To Create Project Task"
              && e.DisableHistoryId == 0
            ).Select(e => e.CurrentStateInstance.Context).ToList().Select(e=> Convert.ToInt32(e)).ToList();
            var _projects=db.Projects.Where(e => e.IsDeleted == false && PIds.Contains(e.Id)).ToList();
            var _Tags = db.Tags.Where(e => e.TagCategory.CategoryName == "StateLessCategories").ToList();
            return new AnotherNoteAdd { AnotherNoteAddNoteInfo=NoteInfo , Projects= _projects, Tags= _Tags };
          
        }

        public void Post([FromBody]AnotherNoteAddConvert model)
        {
            var db = new TSRMDBContext();
            var TaskId = new TaskCreateController().Post(model.UserId, new Task { Project = new Project { Id = model.ProjectId }, Name = db.Notes.Find(model.NoteId)?.Name });
              db = new TSRMDBContext();
            var note = db.Notes.FirstOrDefault(f => f.Id == model.NoteId);
            db.Notes.SafeAttach(note);
            note.asTaskId = TaskId;
            db.SaveChanges();
            var SI=db.HistoryStateInstances.Where(e =>
            e.CurrentStateInstance.State.Name == "Ready To Create Project Task"
            && e.CurrentStateInstance.Context == model.ProjectId.ToString()
            && e.DisableHistoryId == 0
            ).Select(e => e.CurrentStateInstance.Id).FirstOrDefault();
            WorkflowEngine.EventWorkflowInstance("OnTaskCreated", SI, model.UserId, TaskId.ToString());
        }
        public void Post(int id,[FromBody]AnotherNoteAddTagUpdate model)
        {
            var db = new TSRMDBContext();
             var TagCategory=db.TagCategories.SafeAttach(db.TagCategories.FirstOrDefault(f => f.CategoryName == "StateLessCategories"));
            foreach (var item in db.ContentTags.Where(t =>
            t.ContentTagCategory.TagCategory.Id == TagCategory.Id &&
            t.IsDeleted == false).ToList())
            {
                if (model.Tags.All(t => t != item.Tag.Id.ToString()))
                {
                    db.ContentTags.Attach(item);
                    item.IsDeleted = true;
                }
            }
            db.SaveChanges();
            var user = db.User.Attach(db.User.Find(model.UserId));
            var ctxType = db.Cases.FirstOrDefault(f => f.Name == "Note")?.Id ?? 0;
            var ctc=db.ContentTagCategories.FirstOrDefault(f=>f.TagCategory.CategoryName== "StateLessCategories"
            && f.ContentContext.Context==id.ToString() && f.ContentContext.ContextType== ctxType);
            if (ctc==null)
            {
                ctc= db.ContentTagCategories.Add(new ContentTagCategory
                {
                    ContentContext = new ContentContext { Context = id.ToString(), ContextType = ctxType },
                    TagCategory= TagCategory
                });
                db.SaveChanges();
            }
            db.ContentTagCategories.SafeAttach(ctc);
            var _tasks = new List<ContentTag>();
            foreach (var item in model.Tags)
            {
                if (!db.ContentTags.Any(t =>
            t.ContentTagCategory.Id == ctc.Id &&
            t.IsDeleted == false && t.Tag.Id.ToString() == item))
                {

                    int Id = 0;
                    Tag _tag = null;
                    if (int.TryParse(item, out Id))
                    {
                        _tag = db.Tags.SafeAttach(new Tag { Id = Id });
                    }
                    else
                    {
                        _tag = new Tag { Name = item, TagCategory = TagCategory };
                    }
                    _tasks.Add(new ContentTag { Tag = _tag, ByUser = user, ContentTagCategory = ctc, CreatedBy = user.Id, CreatedOn = DateTime.Now });
                }
            }
            db.ContentTags.AddRange(_tasks);
            db.SaveChanges();
        }
    }
}
