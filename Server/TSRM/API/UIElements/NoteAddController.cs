﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;
using TSRM.Signalr;
using TSRM.UtilityClasses;

namespace TSRM.API.UIElements
{
    public class NoteAddController : ApiController
    {

        // GET api/<controller>/5
        public  int Post([FromBody]Note model)
        {

            var db = new TSRMDBContext();
            model.task = db.Tasks.Find(model.task.Id);
            model.User = db.User.Find(model.User.Id);
            model.CreatedBy = model.User.Id;
            model.CreatedOn = DateTime.Now;
            db.Notes.Add(model);
            db.SaveChanges();

            ExecutionPlan.Delay(3500, () => MainHub.CommentAdded(model.task.StateInstanceId, new List<int> { model.User.Id }, model.User?.Username, model.Name));
         
            return model.Id;
        }


    }


}