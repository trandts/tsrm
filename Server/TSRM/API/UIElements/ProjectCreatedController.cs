﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class ProjectCreated
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? EstimatedStartDate { get; set; }
        public DateTime? EstimatedEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
    }
    public class ProjectCreatedController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public ProjectCreated Get(int id)
        {
            var db = new TSRMDBContext();
            var pe = db.ProjectEstimations.FirstOrDefault(f => f.Project.Id == id && !f.IsDeleted && !f.Archived);
            return new ProjectCreated
            {
                Id = id,
                Name = pe.Project.Name,
                EstimatedStartDate = pe.EstimatedStart,
                EstimatedEndDate = pe.EstimatedEnd,
                ActualStartDate = pe.ActualStart,
                ActualEndDate = pe.ActualEnd
            };

        }

        // POST api/<controller>
        public int Post([FromBody]ProjectCreation model)
        {
            return 0;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]ProjectCreation model)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}