﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Engines;
using TSRM.Models;

namespace TSRM.API.UIElements
{
    public class ProjectCreation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? EstimatedStartDate { get; set; }
        public DateTime? EstimatedEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
    }
    public class ProjectCreationController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public ProjectCreation Get(int id)
        {
            var db = new TSRMDBContext();
            var pe = db.ProjectEstimations.FirstOrDefault(f => f.Project.Id == id && !f.IsDeleted && !f.Archived);
            return new ProjectCreation
            {
                Id = id,
                Name = pe.Project.Name,
                EstimatedStartDate = pe.EstimatedStart,
                EstimatedEndDate = pe.EstimatedEnd,
                ActualStartDate = pe.ActualStart,
                ActualEndDate = pe.ActualEnd
            };
        }

        // POST api/<controller>
        public int Post(int Id,[FromBody]ProjectCreation model)
        {
            var db = new TSRMDBContext();
            var pe=db.ProjectEstimations.Add(new ProjectEstimation { ActualStart=model.ActualStartDate,
                ActualEnd = model.ActualEndDate,
                EstimatedStart = model.EstimatedStartDate,
                EstimatedEnd = model.EstimatedEndDate,
                Project =new Project { Name=model.Name } });
            db.SaveChanges();
            var _t_CEO = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "CEO"));
            var _t_ContentReq = db.Teams.Attach(db.Teams.FirstOrDefault(e => e.Name == "Content Creator"));
            var Case = db.Cases.Attach(db.Cases.FirstOrDefault(e => e.Name == "Project"));
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = pe.Project.Id,
                User = db.User.Attach(db.User.FirstOrDefault(e => e.Username == "ip")),
                Team = _t_CEO,
                     CreatedBy = Id,
                CreatedOn = DateTime.Now
            });
            db.CaseTeamMembers.Add(new CaseTeamMember
            {
                Case = Case,
                Case_ContextId = pe.Project.Id,
                User = db.User.Find(Id),
                Team = _t_ContentReq,
                     CreatedBy = Id,
                CreatedOn = DateTime.Now
            });
            db.SaveChanges();
            return pe.Project.Id;
        }

        // PUT api/<controller>/5
        public int Put(int id, [FromBody]ProjectCreation model)
        {
            var db = new TSRMDBContext();
            var p = db.Projects.Attach(db.Projects.Find(id));
            var pe = db.ProjectEstimations.Attach(db.ProjectEstimations.FirstOrDefault(f => f.Project.Id == id && !f.IsDeleted && !f.Archived));
            pe.Archived = true;
            db.ProjectEstimations.Add(new ProjectEstimation
            {
                ActualStart = model.ActualStartDate,
                ActualEnd = model.ActualEndDate,
                EstimatedStart = model.EstimatedStartDate,
                EstimatedEnd = model.EstimatedEndDate,
                Project = p,
                CreatedOn = DateTime.Now
            });
            db.SaveChanges();
            return p.Id;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}