﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSRM.Models;

namespace TSRM.API
{
   
    public class UIElementController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]TriggerAcitvity model)
        {
            var db = new TSRMDBContext();
            var AT= db.ActivityTypes.Attach(db.ActivityTypes.FirstOrDefault(f => f.Name == "UI Element"));
            model.ActivityType = AT;
            db.TriggerAcitvities.Add(model);
            db.SaveChanges();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}