﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSRM.Models.Common
{
    public class CommonModel
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}