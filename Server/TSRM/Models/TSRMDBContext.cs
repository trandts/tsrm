﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TSRM.Models.Common;

namespace TSRM.Models
{
    public class TSRMDBContext : DbContext
    {
        public TSRMDBContext()
                  : base("TSRMDBContext")
        {

        }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    Database.SetInitializer<TSRMDBContext>(null);
        //    base.OnModelCreating(modelBuilder);
        //}
        public DbSet<User> User { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<CaseTeamMember> CaseTeamMembers { get; set; }
        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<TriggerAcitvity> TriggerAcitvities { get; set; }
        public DbSet<NonTriggerActivity> NonTriggerActivities { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<StateRole> StateRoles { get; set; }
        public DbSet<Transition> Transitions { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<Organization> Organizations { get; set; }


        public DbSet<DeliverableType> DeliverableTypes { get; set; }
        public DbSet<TaskAssignmentWorking> TaskAssignmentWorkings { get; set; }
        public DbSet<TaskAssignmentWorkItemNote> TaskAssignmentWorkItemNotes { get; set; }
        public DbSet<TaskAssignmentWorkItem> TaskAssignmentWorkItems { get; set; }
        public DbSet<TaskIteration> TaskIterations { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagCategory> TagCategories { get; set; }
        public DbSet<TaskAssignmentEstimation> TaskAssignmentEstimations { get; set; }
        public DbSet<TaskApprover> TaskApprovers { get; set; }
        public DbSet<TaskAssignment> TaskAssignments { get; set; }
        public DbSet<TaskEstimation> TaskEstimations { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<IterationEstimation> IterationEstimations { get; set; }
        public DbSet<Iteration> Iterations { get; set; }
        public DbSet<ProjectEstimation> ProjectEstimations { get; set; }
        public DbSet<TaskConfiguration> TaskConfigurations { get; set; }
        public DbSet<StateOrthogonalityGate> StateOrthogonalityGates { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<HistoryStateInstance> HistoryStateInstances { get; set; }
        public DbSet<StateInstance> StateInstances { get; set; }
        public DbSet<Note> Notes { get; set; }

        public DbSet<UsmBsmBridge> UsmBsmBridge { get; set; }
        public DbSet<StateType> StateTypes { get; set; }
        public DbSet<AuthenticationSession> AuthenticationSessions { get; set; }


        public DbSet<RealtimeConnection> RealtimeConnections { get; set; }
        public DbSet<RealtimeConnectionData> RealtimeConnectionData { get; set; }

        public DbSet<TaskTagCategory> TaskTagCategories { get; set; }
        public DbSet<TaskTag> TaskTags { get; set; }


        public DbSet<ContentTagCategory> ContentTagCategories { get; set; }
        public DbSet<ContentTag> ContentTags { get; set; }
        public DbSet<ContentContext> ContentContexts { get; set; }
    }
    public class RealtimeConnection : CommonModel
    {
        public int User { get; set; }
        public string ConnectionID { get; set; }
        public virtual List<RealtimeConnectionData> Data { get; set; }
    }
    public class RealtimeConnectionData : CommonModel
    {
        public virtual RealtimeConnection RealtimeConnection { get; set; }
        public int StateInstance { get; set; }
    }
    public class CaseTeamMember : CommonModel
    {
        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
        public int Case_ContextId { get; set; }
        public virtual Case Case { get; set; }
    }
    public class StateInstance : CommonModel
    {
        public virtual State State { get; set; }
        public string Context { get; set; }
        public virtual User Requestor { get; set; }
        public string Status { get; set; }
        public int ParentInstanceId { get; set; }

        public string internalContext_ParentStateInstance { get; set; }
        public string internalContext_ParentStateInstanceContext { get; set; }

    }
    public class HistoryStateInstance : CommonModel
    {

        public virtual History History { get; set; }
        public virtual StateInstance CurrentStateInstance { get; set; }
        public int DisableHistoryId { get; set; }
        public virtual Task Task { get; set; }
    }
    public class History : CommonModel
    {
        public virtual Transition Transition { get; set; }
        public virtual User User { get; set; }
    }
    public class User : CommonModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
    public class Team : CommonModel
    {
        public string Name { get; set; }
        public virtual Team ParentTeam { get; set; }


        public virtual List<Team> ChildTeam { get; set; }
    }
    public class Project : CommonModel
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }

    }
    public class ActivityType : CommonModel
    {
        public string Name { get; set; }
        public bool IsAutomated { get; set; }
    }
    public class TriggerAcitvity : CommonModel
    {
        public virtual ActivityType ActivityType { get; set; }
        public string UIElement { get; set; }
        public string MethodName { get; set; }

        public virtual List<Event> Events { get; set; }//..
    }
    public class NonTriggerActivity : CommonModel
    {
        public virtual ActivityType ActivityType { get; set; }
        public string UIElement { get; set; }
        public string MethodName { get; set; }
    }
    public class Event : CommonModel
    {
       public virtual TriggerAcitvity TriggerAcitvity { get; set; } //..
        public string EventName { get; set; }
    }
    public class PermissionType : CommonModel
    {
        public string Name { get; set; }
    }
    public class TaskConfiguration : CommonModel
    {
        public string TaskName { get; set; }
        public bool IsAutomatic { get; set; }
        public int Delay { get; set; }
    }
    public class StateOrthogonalityGate : CommonModel
    {
        public string Name { get; set; }
    }
    public class StateRole : CommonModel
    {
        public virtual State State { get; set; }
        public virtual Team Team { get; set; }
        public virtual PermissionType PermissionType { get; set; }

    }
    public class ProjectEstimation : CommonModel
    {
        public virtual Project Project { get; set; }
        public DateTime? EstimatedStart { get; set; }
        public DateTime? EstimatedEnd { get; set; }
        public DateTime? ActualStart { get; set; }
        public DateTime? ActualEnd { get; set; }
        public bool Archived { get; set; }
    }
    public class Iteration : CommonModel
    {
        public virtual Project Project { get; set; }
    }
    public class IterationEstimation : CommonModel
    {
        public virtual Iteration Iteration { get; set; }
        public DateTime? EstimatedStart { get; set; }
        public DateTime? EstimatedEnd { get; set; }
        public DateTime? ActualStart { get; set; }
        public DateTime? ActualEnd { get; set; }
        public bool Archived { get; set; }
    }
    public class TaskEstimation : CommonModel
    {
        public virtual Iteration Iteration { get; set; }
        public DateTime? EstimatedStart { get; set; }
        public DateTime? EstimatedEnd { get; set; }
        public bool Archived { get; set; }
    }
    public class TaskAssignment : CommonModel
    {
        public virtual Task Task { get; set; }
        public virtual User User { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool IsApproved { get; set; }
        public virtual User ApprovedUser { get; set; }
        public DateTime? ApprovedTime { get; set; }
    }
    public class TaskApprover : CommonModel
    {
        public virtual Task Task { get; set; }
        public virtual User User { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
    public class TaskAssignmentEstimation : CommonModel
    {
        public virtual TaskAssignment TaskAssignment { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int EstimatedHours { get; set; }
        public int ActualHours { get; set; }
        public bool Archived { get; set; }
    }
    public class TaskTagCategory: CommonModel
    {
        public virtual TagCategory TagCategory { get; set; }
        public virtual Task Task { get; set; }

        public virtual List<TaskTag> TaskTags { get; set; }
    }

    public class ContentTagCategory : CommonModel
    {
        public virtual TagCategory TagCategory { get; set; }
        public virtual ContentContext ContentContext { get; set; }
        public virtual List<ContentTag> ContentTags { get; set; }
    }
    public class ContentContext:CommonModel
    {
        public string Context { get; set; }
        public int ContextType { get; set; }
    }
    public class ContentTag : CommonModel
    {
        public virtual ContentTagCategory ContentTagCategory { get; set; }
        public virtual Tag Tag { get; set; }
        public virtual User ByUser { get; set; }
    }
    public class TagCategory : CommonModel
    {
        public string CategoryName { get; set; }
    }
    public class Tag : CommonModel
    {
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public virtual TagCategory TagCategory { get; set; }
    }
    public class TaskTag : CommonModel
    {
        public virtual TaskTagCategory TaskTagCategory { get; set; }
        public virtual Tag Tag { get; set; }
        public virtual User ByUser { get; set; }
    }
    public class TaskIteration : CommonModel
    {
        public virtual Task Task { get; set; }
        public virtual Iteration Iteration { get; set; }
    }
    public class TaskAssignmentWorkItem : CommonModel
    {
        public virtual TaskAssignment TaskAssignment { get; set; }
        public int Completion { get; set; }
    }
    public class TaskAssignmentWorkItemNote : CommonModel
    {
        public virtual TaskAssignmentWorkItem TaskAssignmentWorkItem { get; set; }
        public string Note { get; set; }
        public string Deliverable { get; set; }
        public virtual DeliverableType DeliverableType { get; set; }
    }
    public class TaskAssignmentWorking : CommonModel
    {
        public TaskAssignment TaskAssignment { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
    public class DeliverableType : CommonModel
    {
        public string Name { get; set; }
    }
    public class Transition : CommonModel
    {
        public string Name { get; set; }
        public virtual State FromState { get; set; }
        public virtual State ToState { get; set; }
        public virtual Event Event { get; set; }
        public virtual NonTriggerActivity OnTransitionActivity { get; set; }
    }
    public class Task : CommonModel
    {
        public string Name { get; set; }
        public virtual Project Project { get; set; }

        public int StateInstanceId { get; set; }
        public bool ToDisplay { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsClosed { get; set; }


        public string Case { get; set; }
        public int CaseContextId { get; set; }
        public string CaseContext { get; set; }
    }
    public class State : CommonModel
    {
        public string Name { get; set; }
        public virtual TriggerAcitvity TriggerAcitvity { get; set; }//..
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public virtual NonTriggerActivity EnterNonTriggerActivity { get; set; }
        public virtual NonTriggerActivity ExitNonTriggerActivity { get; set; }
        public int ParentStateId { get; set; }
        public virtual StateOrthogonalityGate Gate { get; set; }
        public virtual TaskConfiguration TaskConfiguration { get; set; }
        public virtual Case Case { get; set; }
        public bool IsMultiInstance { get; set; }
        public virtual StateType StateType { get; set; }
        public string Region { get; set; }

        public DateTime? ActiveFrom { get; set; }
        public DateTime? ActiveTo { get; set; }
        public bool IsDisabled { get; set; }
    }
    public class UsmBsmBridge : CommonModel
    {
        public virtual State USM { get; set; }
        public virtual State BSM { get; set; }
        public string Url { get; set; }
    }

    public class AuthenticationSession : CommonModel
    {
        public string Session { get; set; }
        public int User { get; set; }
        public string GUID { get; set; }
        public bool IsOpen { get; set; }
    }
    public class StateType : CommonModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
    public class Case : CommonModel
    {
        public string Name { get; set; }
    }
    public class Organization : CommonModel
    {
        public string Name { get; set; }
    }
    public class Note : CommonModel
    {
        public string Name { get; set; }
        public virtual User User { get; set; }
        public virtual Task task { get; set; }

        public int asTaskId { get; set; }
    }
}