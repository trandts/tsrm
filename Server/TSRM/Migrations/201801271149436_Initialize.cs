namespace TSRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsAutomated = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AuthenticationSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Session = c.String(),
                        User = c.Int(nullable: false),
                        GUID = c.String(),
                        IsOpen = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CaseTeamMembers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Case_ContextId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Case_Id = c.Int(),
                        Team_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.Case_Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Case_Id)
                .Index(t => t.Team_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ParentTeam_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.ParentTeam_Id)
                .Index(t => t.ParentTeam_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContentContexts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Context = c.String(),
                        ContextType = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContentTagCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ContentContext_Id = c.Int(),
                        TagCategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentContexts", t => t.ContentContext_Id)
                .ForeignKey("dbo.TagCategories", t => t.TagCategory_Id)
                .Index(t => t.ContentContext_Id)
                .Index(t => t.TagCategory_Id);
            
            CreateTable(
                "dbo.ContentTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ByUser_Id = c.Int(),
                        ContentTagCategory_Id = c.Int(),
                        Tag_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ByUser_Id)
                .ForeignKey("dbo.ContentTagCategories", t => t.ContentTagCategory_Id)
                .ForeignKey("dbo.Tags", t => t.Tag_Id)
                .Index(t => t.ByUser_Id)
                .Index(t => t.ContentTagCategory_Id)
                .Index(t => t.Tag_Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TagCategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TagCategories", t => t.TagCategory_Id)
                .Index(t => t.TagCategory_Id);
            
            CreateTable(
                "dbo.TagCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeliverableTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TriggerAcitvity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TriggerAcitvities", t => t.TriggerAcitvity_Id)
                .Index(t => t.TriggerAcitvity_Id);
            
            CreateTable(
                "dbo.TriggerAcitvities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UIElement = c.String(),
                        MethodName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ActivityType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActivityTypes", t => t.ActivityType_Id)
                .Index(t => t.ActivityType_Id);
            
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Transition_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transitions", t => t.Transition_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Transition_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Transitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Event_Id = c.Int(),
                        FromState_Id = c.Int(),
                        OnTransitionActivity_Id = c.Int(),
                        ToState_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Events", t => t.Event_Id)
                .ForeignKey("dbo.States", t => t.FromState_Id)
                .ForeignKey("dbo.NonTriggerActivities", t => t.OnTransitionActivity_Id)
                .ForeignKey("dbo.States", t => t.ToState_Id)
                .Index(t => t.Event_Id)
                .Index(t => t.FromState_Id)
                .Index(t => t.OnTransitionActivity_Id)
                .Index(t => t.ToState_Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsStart = c.Boolean(nullable: false),
                        IsEnd = c.Boolean(nullable: false),
                        ParentStateId = c.Int(nullable: false),
                        IsMultiInstance = c.Boolean(nullable: false),
                        Region = c.String(),
                        ActiveFrom = c.DateTime(),
                        ActiveTo = c.DateTime(),
                        IsDisabled = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Case_Id = c.Int(),
                        EnterNonTriggerActivity_Id = c.Int(),
                        ExitNonTriggerActivity_Id = c.Int(),
                        Gate_Id = c.Int(),
                        StateType_Id = c.Int(),
                        TaskConfiguration_Id = c.Int(),
                        TriggerAcitvity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cases", t => t.Case_Id)
                .ForeignKey("dbo.NonTriggerActivities", t => t.EnterNonTriggerActivity_Id)
                .ForeignKey("dbo.NonTriggerActivities", t => t.ExitNonTriggerActivity_Id)
                .ForeignKey("dbo.StateOrthogonalityGates", t => t.Gate_Id)
                .ForeignKey("dbo.StateTypes", t => t.StateType_Id)
                .ForeignKey("dbo.TaskConfigurations", t => t.TaskConfiguration_Id)
                .ForeignKey("dbo.TriggerAcitvities", t => t.TriggerAcitvity_Id)
                .Index(t => t.Case_Id)
                .Index(t => t.EnterNonTriggerActivity_Id)
                .Index(t => t.ExitNonTriggerActivity_Id)
                .Index(t => t.Gate_Id)
                .Index(t => t.StateType_Id)
                .Index(t => t.TaskConfiguration_Id)
                .Index(t => t.TriggerAcitvity_Id);
            
            CreateTable(
                "dbo.NonTriggerActivities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UIElement = c.String(),
                        MethodName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ActivityType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActivityTypes", t => t.ActivityType_Id)
                .Index(t => t.ActivityType_Id);
            
            CreateTable(
                "dbo.StateOrthogonalityGates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ShortName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskName = c.String(),
                        IsAutomatic = c.Boolean(nullable: false),
                        Delay = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HistoryStateInstances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisableHistoryId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        CurrentStateInstance_Id = c.Int(),
                        History_Id = c.Int(),
                        Task_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StateInstances", t => t.CurrentStateInstance_Id)
                .ForeignKey("dbo.Histories", t => t.History_Id)
                .ForeignKey("dbo.Tasks", t => t.Task_Id)
                .Index(t => t.CurrentStateInstance_Id)
                .Index(t => t.History_Id)
                .Index(t => t.Task_Id);
            
            CreateTable(
                "dbo.StateInstances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Context = c.String(),
                        Status = c.String(),
                        ParentInstanceId = c.Int(nullable: false),
                        internalContext_ParentStateInstance = c.String(),
                        internalContext_ParentStateInstanceContext = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Requestor_Id = c.Int(),
                        State_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Requestor_Id)
                .ForeignKey("dbo.States", t => t.State_Id)
                .Index(t => t.Requestor_Id)
                .Index(t => t.State_Id);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StateInstanceId = c.Int(nullable: false),
                        ToDisplay = c.Boolean(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        IsClosed = c.Boolean(nullable: false),
                        Case = c.String(),
                        CaseContextId = c.Int(nullable: false),
                        CaseContext = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Project_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .Index(t => t.Project_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IterationEstimations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EstimatedStart = c.DateTime(),
                        EstimatedEnd = c.DateTime(),
                        ActualStart = c.DateTime(),
                        ActualEnd = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Iteration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Iterations", t => t.Iteration_Id)
                .Index(t => t.Iteration_Id);
            
            CreateTable(
                "dbo.Iterations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Project_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .Index(t => t.Project_Id);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        asTaskId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        task_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.task_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.task_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PermissionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectEstimations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EstimatedStart = c.DateTime(),
                        EstimatedEnd = c.DateTime(),
                        ActualStart = c.DateTime(),
                        ActualEnd = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Project_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Project_Id)
                .Index(t => t.Project_Id);
            
            CreateTable(
                "dbo.RealtimeConnectionDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StateInstance = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        RealtimeConnection_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RealtimeConnections", t => t.RealtimeConnection_Id)
                .Index(t => t.RealtimeConnection_Id);
            
            CreateTable(
                "dbo.RealtimeConnections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User = c.Int(nullable: false),
                        ConnectionID = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StateRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        PermissionType_Id = c.Int(),
                        State_Id = c.Int(),
                        Team_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PermissionTypes", t => t.PermissionType_Id)
                .ForeignKey("dbo.States", t => t.State_Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id)
                .Index(t => t.PermissionType_Id)
                .Index(t => t.State_Id)
                .Index(t => t.Team_Id);
            
            CreateTable(
                "dbo.TaskApprovers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(),
                        ToDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Task_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.Task_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Task_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.TaskAssignmentEstimations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(),
                        End = c.DateTime(),
                        EstimatedHours = c.Int(nullable: false),
                        ActualHours = c.Int(nullable: false),
                        Archived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TaskAssignment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskAssignments", t => t.TaskAssignment_Id)
                .Index(t => t.TaskAssignment_Id);
            
            CreateTable(
                "dbo.TaskAssignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(),
                        ToDate = c.DateTime(),
                        IsApproved = c.Boolean(nullable: false),
                        ApprovedTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ApprovedUser_Id = c.Int(),
                        Task_Id = c.Int(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ApprovedUser_Id)
                .ForeignKey("dbo.Tasks", t => t.Task_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.ApprovedUser_Id)
                .Index(t => t.Task_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.TaskAssignmentWorkings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromDate = c.DateTime(),
                        ToDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TaskAssignment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskAssignments", t => t.TaskAssignment_Id)
                .Index(t => t.TaskAssignment_Id);
            
            CreateTable(
                "dbo.TaskAssignmentWorkItemNotes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        Deliverable = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        DeliverableType_Id = c.Int(),
                        TaskAssignmentWorkItem_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeliverableTypes", t => t.DeliverableType_Id)
                .ForeignKey("dbo.TaskAssignmentWorkItems", t => t.TaskAssignmentWorkItem_Id)
                .Index(t => t.DeliverableType_Id)
                .Index(t => t.TaskAssignmentWorkItem_Id);
            
            CreateTable(
                "dbo.TaskAssignmentWorkItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Completion = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TaskAssignment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskAssignments", t => t.TaskAssignment_Id)
                .Index(t => t.TaskAssignment_Id);
            
            CreateTable(
                "dbo.TaskEstimations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EstimatedStart = c.DateTime(),
                        EstimatedEnd = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Iteration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Iterations", t => t.Iteration_Id)
                .Index(t => t.Iteration_Id);
            
            CreateTable(
                "dbo.TaskIterations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        Iteration_Id = c.Int(),
                        Task_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Iterations", t => t.Iteration_Id)
                .ForeignKey("dbo.Tasks", t => t.Task_Id)
                .Index(t => t.Iteration_Id)
                .Index(t => t.Task_Id);
            
            CreateTable(
                "dbo.TaskTagCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        TagCategory_Id = c.Int(),
                        Task_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TagCategories", t => t.TagCategory_Id)
                .ForeignKey("dbo.Tasks", t => t.Task_Id)
                .Index(t => t.TagCategory_Id)
                .Index(t => t.Task_Id);
            
            CreateTable(
                "dbo.TaskTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        ByUser_Id = c.Int(),
                        Tag_Id = c.Int(),
                        TaskTagCategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ByUser_Id)
                .ForeignKey("dbo.Tags", t => t.Tag_Id)
                .ForeignKey("dbo.TaskTagCategories", t => t.TaskTagCategory_Id)
                .Index(t => t.ByUser_Id)
                .Index(t => t.Tag_Id)
                .Index(t => t.TaskTagCategory_Id);
            
            CreateTable(
                "dbo.UsmBsmBridges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(),
                        ModifiedBy = c.Int(),
                        CreatedOn = c.DateTime(),
                        ModifiedOn = c.DateTime(),
                        BSM_Id = c.Int(),
                        USM_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.BSM_Id)
                .ForeignKey("dbo.States", t => t.USM_Id)
                .Index(t => t.BSM_Id)
                .Index(t => t.USM_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsmBsmBridges", "USM_Id", "dbo.States");
            DropForeignKey("dbo.UsmBsmBridges", "BSM_Id", "dbo.States");
            DropForeignKey("dbo.TaskTags", "TaskTagCategory_Id", "dbo.TaskTagCategories");
            DropForeignKey("dbo.TaskTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.TaskTags", "ByUser_Id", "dbo.Users");
            DropForeignKey("dbo.TaskTagCategories", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.TaskTagCategories", "TagCategory_Id", "dbo.TagCategories");
            DropForeignKey("dbo.TaskIterations", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.TaskIterations", "Iteration_Id", "dbo.Iterations");
            DropForeignKey("dbo.TaskEstimations", "Iteration_Id", "dbo.Iterations");
            DropForeignKey("dbo.TaskAssignmentWorkItemNotes", "TaskAssignmentWorkItem_Id", "dbo.TaskAssignmentWorkItems");
            DropForeignKey("dbo.TaskAssignmentWorkItems", "TaskAssignment_Id", "dbo.TaskAssignments");
            DropForeignKey("dbo.TaskAssignmentWorkItemNotes", "DeliverableType_Id", "dbo.DeliverableTypes");
            DropForeignKey("dbo.TaskAssignmentWorkings", "TaskAssignment_Id", "dbo.TaskAssignments");
            DropForeignKey("dbo.TaskAssignmentEstimations", "TaskAssignment_Id", "dbo.TaskAssignments");
            DropForeignKey("dbo.TaskAssignments", "User_Id", "dbo.Users");
            DropForeignKey("dbo.TaskAssignments", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.TaskAssignments", "ApprovedUser_Id", "dbo.Users");
            DropForeignKey("dbo.TaskApprovers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.TaskApprovers", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.StateRoles", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.StateRoles", "State_Id", "dbo.States");
            DropForeignKey("dbo.StateRoles", "PermissionType_Id", "dbo.PermissionTypes");
            DropForeignKey("dbo.RealtimeConnectionDatas", "RealtimeConnection_Id", "dbo.RealtimeConnections");
            DropForeignKey("dbo.ProjectEstimations", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.Notes", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Notes", "task_Id", "dbo.Tasks");
            DropForeignKey("dbo.IterationEstimations", "Iteration_Id", "dbo.Iterations");
            DropForeignKey("dbo.Iterations", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.HistoryStateInstances", "Task_Id", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.HistoryStateInstances", "History_Id", "dbo.Histories");
            DropForeignKey("dbo.HistoryStateInstances", "CurrentStateInstance_Id", "dbo.StateInstances");
            DropForeignKey("dbo.StateInstances", "State_Id", "dbo.States");
            DropForeignKey("dbo.StateInstances", "Requestor_Id", "dbo.Users");
            DropForeignKey("dbo.Histories", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Histories", "Transition_Id", "dbo.Transitions");
            DropForeignKey("dbo.Transitions", "ToState_Id", "dbo.States");
            DropForeignKey("dbo.Transitions", "OnTransitionActivity_Id", "dbo.NonTriggerActivities");
            DropForeignKey("dbo.Transitions", "FromState_Id", "dbo.States");
            DropForeignKey("dbo.States", "TriggerAcitvity_Id", "dbo.TriggerAcitvities");
            DropForeignKey("dbo.States", "TaskConfiguration_Id", "dbo.TaskConfigurations");
            DropForeignKey("dbo.States", "StateType_Id", "dbo.StateTypes");
            DropForeignKey("dbo.States", "Gate_Id", "dbo.StateOrthogonalityGates");
            DropForeignKey("dbo.States", "ExitNonTriggerActivity_Id", "dbo.NonTriggerActivities");
            DropForeignKey("dbo.States", "EnterNonTriggerActivity_Id", "dbo.NonTriggerActivities");
            DropForeignKey("dbo.NonTriggerActivities", "ActivityType_Id", "dbo.ActivityTypes");
            DropForeignKey("dbo.States", "Case_Id", "dbo.Cases");
            DropForeignKey("dbo.Transitions", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.Events", "TriggerAcitvity_Id", "dbo.TriggerAcitvities");
            DropForeignKey("dbo.TriggerAcitvities", "ActivityType_Id", "dbo.ActivityTypes");
            DropForeignKey("dbo.ContentTagCategories", "TagCategory_Id", "dbo.TagCategories");
            DropForeignKey("dbo.ContentTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.Tags", "TagCategory_Id", "dbo.TagCategories");
            DropForeignKey("dbo.ContentTags", "ContentTagCategory_Id", "dbo.ContentTagCategories");
            DropForeignKey("dbo.ContentTags", "ByUser_Id", "dbo.Users");
            DropForeignKey("dbo.ContentTagCategories", "ContentContext_Id", "dbo.ContentContexts");
            DropForeignKey("dbo.CaseTeamMembers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.CaseTeamMembers", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.Teams", "ParentTeam_Id", "dbo.Teams");
            DropForeignKey("dbo.CaseTeamMembers", "Case_Id", "dbo.Cases");
            DropIndex("dbo.UsmBsmBridges", new[] { "USM_Id" });
            DropIndex("dbo.UsmBsmBridges", new[] { "BSM_Id" });
            DropIndex("dbo.TaskTags", new[] { "TaskTagCategory_Id" });
            DropIndex("dbo.TaskTags", new[] { "Tag_Id" });
            DropIndex("dbo.TaskTags", new[] { "ByUser_Id" });
            DropIndex("dbo.TaskTagCategories", new[] { "Task_Id" });
            DropIndex("dbo.TaskTagCategories", new[] { "TagCategory_Id" });
            DropIndex("dbo.TaskIterations", new[] { "Task_Id" });
            DropIndex("dbo.TaskIterations", new[] { "Iteration_Id" });
            DropIndex("dbo.TaskEstimations", new[] { "Iteration_Id" });
            DropIndex("dbo.TaskAssignmentWorkItems", new[] { "TaskAssignment_Id" });
            DropIndex("dbo.TaskAssignmentWorkItemNotes", new[] { "TaskAssignmentWorkItem_Id" });
            DropIndex("dbo.TaskAssignmentWorkItemNotes", new[] { "DeliverableType_Id" });
            DropIndex("dbo.TaskAssignmentWorkings", new[] { "TaskAssignment_Id" });
            DropIndex("dbo.TaskAssignments", new[] { "User_Id" });
            DropIndex("dbo.TaskAssignments", new[] { "Task_Id" });
            DropIndex("dbo.TaskAssignments", new[] { "ApprovedUser_Id" });
            DropIndex("dbo.TaskAssignmentEstimations", new[] { "TaskAssignment_Id" });
            DropIndex("dbo.TaskApprovers", new[] { "User_Id" });
            DropIndex("dbo.TaskApprovers", new[] { "Task_Id" });
            DropIndex("dbo.StateRoles", new[] { "Team_Id" });
            DropIndex("dbo.StateRoles", new[] { "State_Id" });
            DropIndex("dbo.StateRoles", new[] { "PermissionType_Id" });
            DropIndex("dbo.RealtimeConnectionDatas", new[] { "RealtimeConnection_Id" });
            DropIndex("dbo.ProjectEstimations", new[] { "Project_Id" });
            DropIndex("dbo.Notes", new[] { "User_Id" });
            DropIndex("dbo.Notes", new[] { "task_Id" });
            DropIndex("dbo.Iterations", new[] { "Project_Id" });
            DropIndex("dbo.IterationEstimations", new[] { "Iteration_Id" });
            DropIndex("dbo.Tasks", new[] { "Project_Id" });
            DropIndex("dbo.StateInstances", new[] { "State_Id" });
            DropIndex("dbo.StateInstances", new[] { "Requestor_Id" });
            DropIndex("dbo.HistoryStateInstances", new[] { "Task_Id" });
            DropIndex("dbo.HistoryStateInstances", new[] { "History_Id" });
            DropIndex("dbo.HistoryStateInstances", new[] { "CurrentStateInstance_Id" });
            DropIndex("dbo.NonTriggerActivities", new[] { "ActivityType_Id" });
            DropIndex("dbo.States", new[] { "TriggerAcitvity_Id" });
            DropIndex("dbo.States", new[] { "TaskConfiguration_Id" });
            DropIndex("dbo.States", new[] { "StateType_Id" });
            DropIndex("dbo.States", new[] { "Gate_Id" });
            DropIndex("dbo.States", new[] { "ExitNonTriggerActivity_Id" });
            DropIndex("dbo.States", new[] { "EnterNonTriggerActivity_Id" });
            DropIndex("dbo.States", new[] { "Case_Id" });
            DropIndex("dbo.Transitions", new[] { "ToState_Id" });
            DropIndex("dbo.Transitions", new[] { "OnTransitionActivity_Id" });
            DropIndex("dbo.Transitions", new[] { "FromState_Id" });
            DropIndex("dbo.Transitions", new[] { "Event_Id" });
            DropIndex("dbo.Histories", new[] { "User_Id" });
            DropIndex("dbo.Histories", new[] { "Transition_Id" });
            DropIndex("dbo.TriggerAcitvities", new[] { "ActivityType_Id" });
            DropIndex("dbo.Events", new[] { "TriggerAcitvity_Id" });
            DropIndex("dbo.Tags", new[] { "TagCategory_Id" });
            DropIndex("dbo.ContentTags", new[] { "Tag_Id" });
            DropIndex("dbo.ContentTags", new[] { "ContentTagCategory_Id" });
            DropIndex("dbo.ContentTags", new[] { "ByUser_Id" });
            DropIndex("dbo.ContentTagCategories", new[] { "TagCategory_Id" });
            DropIndex("dbo.ContentTagCategories", new[] { "ContentContext_Id" });
            DropIndex("dbo.Teams", new[] { "ParentTeam_Id" });
            DropIndex("dbo.CaseTeamMembers", new[] { "User_Id" });
            DropIndex("dbo.CaseTeamMembers", new[] { "Team_Id" });
            DropIndex("dbo.CaseTeamMembers", new[] { "Case_Id" });
            DropTable("dbo.UsmBsmBridges");
            DropTable("dbo.TaskTags");
            DropTable("dbo.TaskTagCategories");
            DropTable("dbo.TaskIterations");
            DropTable("dbo.TaskEstimations");
            DropTable("dbo.TaskAssignmentWorkItems");
            DropTable("dbo.TaskAssignmentWorkItemNotes");
            DropTable("dbo.TaskAssignmentWorkings");
            DropTable("dbo.TaskAssignments");
            DropTable("dbo.TaskAssignmentEstimations");
            DropTable("dbo.TaskApprovers");
            DropTable("dbo.StateRoles");
            DropTable("dbo.RealtimeConnections");
            DropTable("dbo.RealtimeConnectionDatas");
            DropTable("dbo.ProjectEstimations");
            DropTable("dbo.PermissionTypes");
            DropTable("dbo.Organizations");
            DropTable("dbo.Notes");
            DropTable("dbo.Iterations");
            DropTable("dbo.IterationEstimations");
            DropTable("dbo.Projects");
            DropTable("dbo.Tasks");
            DropTable("dbo.StateInstances");
            DropTable("dbo.HistoryStateInstances");
            DropTable("dbo.TaskConfigurations");
            DropTable("dbo.StateTypes");
            DropTable("dbo.StateOrthogonalityGates");
            DropTable("dbo.NonTriggerActivities");
            DropTable("dbo.States");
            DropTable("dbo.Transitions");
            DropTable("dbo.Histories");
            DropTable("dbo.TriggerAcitvities");
            DropTable("dbo.Events");
            DropTable("dbo.DeliverableTypes");
            DropTable("dbo.TagCategories");
            DropTable("dbo.Tags");
            DropTable("dbo.ContentTags");
            DropTable("dbo.ContentTagCategories");
            DropTable("dbo.ContentContexts");
            DropTable("dbo.Users");
            DropTable("dbo.Teams");
            DropTable("dbo.CaseTeamMembers");
            DropTable("dbo.Cases");
            DropTable("dbo.AuthenticationSessions");
            DropTable("dbo.ActivityTypes");
        }
    }
}
