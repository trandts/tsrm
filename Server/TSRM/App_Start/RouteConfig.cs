﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace TSRM
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            string[] Site = { "", "tsrm/" };
            string[] Routes = { "LoginService", "LoginCheck","GetUSM","GetUserSM", "CurrentUI","LogoutService","SessionCheck" };


            foreach (var item in Site)
            {
                foreach (var routeItem in Routes)
                {
                    routes.MapRoute(
                          name: item + routeItem,
                          url: item + routeItem,
                          defaults: new { controller = routeItem, action = "Index" }
                      );
                }
                
            }
            routes.MapRoute(
             "All routes",
               "{*url}",
            new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
