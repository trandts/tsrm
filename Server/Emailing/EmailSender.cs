﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Web.Mvc;
using System.Web.Http;



namespace Emailing
{
    public class EmailSender
    {
        public Task EmailAllText(List<EmailMessage> BranchesList, Attachment[] EmailAttachments, EmailConfiguration config = null)
        {
            if (config == null)
            {
                throw new Exception("config Invalid");
            }
            return Task.Run(() =>
                        {

                            foreach (EmailMessage EM in BranchesList)
                            {
                                try
                                {
                                    var fromAddress = new MailAddress(config.Email, "Emailing System");
                                    var toAddress = new MailAddress(EM.Receiver, EM.FromName);

                                    var smtp = new SmtpClient()
                                    {
                                        Host = config.Host,//"smtp.gmail.com",
                                        Port = config.Port,//587,
                                        EnableSsl = config.EnableSsl,//true,
                                        DeliveryMethod = SmtpDeliveryMethod.Network,
                                        UseDefaultCredentials = config.UseDefaultCredentials,//false,
                                        Credentials = new NetworkCredential(fromAddress.Address, config.Password)
                                    };

                                    using (var message = new MailMessage(fromAddress, toAddress)
                                    {
                                        Subject = EM.Subject,
                                        Body = EM.Body,


                                    })
                                    {
                                        foreach (Attachment att in EmailAttachments)
                                        {
                                            message.Attachments.Add(att);
                                        }
                                        smtp.Send(message);
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }

                        });
        }
        public Task EmailAllHTML(List<EmailMessage> BranchesList, List<VMMailContent> linkedResources = null, List<VMMailContent> attachments = null, EmailConfiguration config = null)
        {
            if (config == null)
            {
                throw new Exception("config Invalid");
            }
            return Task.Run(() =>
            {
                foreach (EmailMessage EM in BranchesList)
                {
                    try
                    {

                        var message = prepare_html_email(EM.Body, linkedResources, attachments);
                        message.From = new MailAddress(config.Email, "EROB");
                        message.To.Add(new MailAddress(EM.Receiver, EM.FromName));

                        var smtp = new SmtpClient()
                        {
                            Host = config.Host,//"smtp.gmail.com",
                            Port = config.Port,//587,
                            EnableSsl = config.EnableSsl,//true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = config.UseDefaultCredentials,//false,
                            Credentials = new NetworkCredential(message.From.Address, config.Password)
                        };

                        message.Subject = EM.Subject;
                        smtp.Send(message);

                    }
                    catch (Exception ex)
                    {

                    }

                }

            });
        }

        MailMessage prepare_html_email(string body, List<VMMailContent> linkedResources = null, List<VMMailContent> attachments = null)
        {
            MailMessage mail;
            try
            {
                mail = new MailMessage();

                //set the HTML format to true
                mail.IsBodyHtml = true;

                //create Alrternative HTML view
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");


                // add resources e.g. images and css
                if (linkedResources != null)
                    foreach (var i in linkedResources)
                    {
                        //Add Image
                        LinkedResource lr = new LinkedResource(i.path, i.mimetype);
                        lr.ContentId = i.cid;
                        //lr.ContentType = new System.Net.Mime.ContentType(i.mimetype);

                        //Add the Image to the Alternate view
                        htmlView.LinkedResources.Add(lr);
                    }

                // add resources e.g. images and css
                if (attachments != null)
                    foreach (var i in attachments)
                    {
                        //Add Image
                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(i.path);
                        mail.Attachments.Add(attachment);
                    }

                mail.AlternateViews.Add(htmlView);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            return mail;
        }
        // var mail = prepare_html_email(body, linkedResources, attachments);

        //email_send(sendFrom, sendTo, sendCC, sendBCC, subject, mail);


    }
    public class EmailingMarkerController : Controller
    { }
}
