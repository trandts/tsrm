﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
namespace Emailing
{
    public static class MVCHelper
    {
        /**    public static string RenderPartialToString<T>(this Controller controller, string viewName, T model)
            {
                controller.ViewData.Model = model;

                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
         **/
        public static string RenderPartialToString<T>(this Controller controller, string viewName, T model)
        {
            try
            {
                controller.ViewData.Model = model;
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    //viewContext.ClientValidationEnabled = false;
                    //viewContext.UnobtrusiveJavaScriptEnabled = false;
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch 
            {
                throw ;
                //return RenderPartialToString<T>(new EmailingMarkerController(), viewName, model);
            }
            
        }
        //    public static string RenderToString(this PartialViewResult partialView)
        //{
        //    var httpContext = HttpContext.Current;

        //    if (httpContext == null)
        //    {
        //        throw new NotSupportedException("An HTTP context is required to render the partial view to a string");
        //    }

        //    var controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

        //    var controller = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(httpContext.Request.RequestContext, controllerName);

        //    var controllerContext = new ControllerContext(httpContext.Request.RequestContext, controller);

        //    var view = ViewEngines.Engines.FindPartialView(controllerContext, partialView.ViewName).View;

        //    var sb = new StringBuilder();

        //    using (var sw = new StringWriter(sb))
        //    {
        //        using (var tw = new HtmlTextWriter(sw))
        //        {
        //            view.Render(new ViewContext(controllerContext, view, partialView.ViewData, partialView.TempData, tw), tw);
        //        }
        //    }

        //    return sb.ToString();
        //}
    }
}
