﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Emailing
{
    public class EmailConfiguration 
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
        public bool UseDefaultCredentials { get; set; }
    }
}
