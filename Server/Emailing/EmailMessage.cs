﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emailing
{
    public class EmailMessage
    {
        public string Receiver { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public string Body { get; set; }
    }
}
