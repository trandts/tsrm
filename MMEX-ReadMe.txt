MMEX-SANOFI
====================================

This ReadMe file is in base directory of Project.

Important Points For Developers/Designers
------------------------------------------------------------------------
1) Run NPM on same directory level as this readme file.
2) Front-End code can be found in ./src
3) When developing Front-End use following command "npm start", the APP will start on localhost 3000 port.
4) When generating distribution,  use following command "npm run build". The output folder is ./TSSA/Content
5) Once distribution is generated, without any further configuration it is available within Visual Studio Development as well.
6) For Back-End development, open "TSSA.sln" file in Visual Studio and Debug.
7) All MVC (non WebApi) Routes are being handled via Home Controller, which outputs html file from ./TSSA/Content. Therefore all non-WebApi Routes are configured on Front-End.
8) All static files including html,images,Js,Css are being served from ./TSSA/Content



Important points Regarding Configuration
-----------------------------------------------------------------------
1) NPM and Webpack configuration is modified in Package.json and both WebPack.cong to adapt compiling and bundling with respect to current Folder structure.
2) tools/distServer.js is also modified to replace build location.
3) Following Link helped me to setup NPM environment with Visual Studio (for future references) https://turbofuture.com/computers/Getting-Started-with-ReactJS-from-Scratch