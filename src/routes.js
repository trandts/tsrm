import React from 'react';
import { Route} from 'react-router';//, IndexRoute 
/*
import NotFoundPage from './containers/NotFoundPage.js';
import FormPage from './containers/FormPage';
import TablePage from './containers/TablePage';
import TaskMatrix from './containers/TaskMatrix';
import EditMyWork from './containers/EditMyWork';
import SurveyPage from './containers/SurveyPage';
import TestPage from './containers/TestPage';
import Dashboard from './containers/DashboardPage';
import CreateSurvey from './containers/CreateSurvey';
import CreateQuestion from './containers/CreateQuestion';
import SelectProduct from './containers/SelectProduct';
import TaskTimeline from './containers/TaskTimeline';
import DisplayActivity from './containers/DisplayActivity';
import Project from './containers/Project';
import User from './containers/User';
import Tag from './containers/Tag';
import Agenda from './containers/Agenda';
import Trace from './containers/Trace';
import UIElement from './containers/UIElement';
import InstantiateWorkflow from './containers/InstantiateWorkflow';
import OpenWorkflowInstance from './containers/OpenWorkflowInstance';
import WorkflowInstanceDisplay from './containers/WorkflowInstanceDisplay';
import Tasks from './containers/Tasks';
import StateMachine from './containers/StateMachine';
import StateMachineUI from './containers/StateMachineUI';

import App from './containers/App';
import LoginPage from './containers/LoginPage';
*/import Data from './data';

import WorkflowOpen from './components/WorkflowOpen';
import StateMachineUI from './containers/StateMachineUI';

/*
const InternalRoutes = (<div>
  <IndexRoute component={TaskMatrix} />
  <Route path="dashboard" component={Dashboard} />
  <Route path="form" component={FormPage} />
  <Route path="table" component={TablePage} />
  <Route path="test" component={TestPage} />
  <Route path="jobsurvey" component={SurveyPage} />
  <Route path="CreateSurvey" component={CreateSurvey} />
  <Route path="CreateQuestion" component={CreateQuestion} />
  <Route path="SelectProduct" component={SelectProduct} />
  <Route path="Tasks" component={Tasks}/>
  <Route path="Task" component={TaskMatrix} />
  <Route path="UserTimeline" component={EditMyWork} />
  <Route path="TaskTimeline" component={TaskTimeline}/>
  <Route path="Overview" component={DisplayActivity}/>
  <Route path="Agenda" component={Agenda}/>
  <Route path="Project" component={Project}/>
  <Route path="User" component={User}/>
  <Route path="Tag" component={Tag}/>
  <Route path="Trace" component={Trace}/>
  <Route path="UIElement" component={UIElement}/>
  <Route path="InstantiateWorkflow" component={InstantiateWorkflow}/>
  <Route path="OpenWorkflowInstance" component={OpenWorkflowInstance}/>
  <Route path="WorkflowInstanceDisplay/:Id" component={WorkflowInstanceDisplay}/>
  <Route path="StateMachine" component={StateMachine}/>
  <Route path="StateMachineUI" component={StateMachineUI}/>
  <Route path="*" component={NotFoundPage} />
</div>);
*/
export default (
 <div>
    <Route path={Data.AppDir2+"StateMachineUI"} component={StateMachineUI} />
  <Route path="*" component={WorkflowOpen} />
    </div>
);
/*<Route ><Route path="login" component={LoginPage} />
    <Route path={Data.AppDir2+"login"} component={LoginPage} />
    <Route path="test" component={TestPage} />
    <Route path={Data.AppDir3} component={App}>
    {InternalRoutes}
  </Route>
    <Route path="/" component={App}>
    {InternalRoutes}
  </Route></Route>*/