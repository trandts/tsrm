import getMuiTheme from 'material-ui/styles/getMuiTheme';
//import {blue600} from 'material-ui/styles/colors';

const themeDefault = getMuiTheme({
  palette: {
    textColor:"#000000",
    xprimary1Color:'#626a99',
    primary1Color:'#00a652',
  },
  appBar: {
    height: 57,
    // blue
    color: '#00a652',
  },
  drawer: {
    width: 230,
    color: '#00a652',
  },
  raisedButton: {
    primaryColor:'#bca36a',
  },
  textField:{
    
  },
  slider: {
      trackSize: 2,
   //   trackColor: palette.primary3Color,
   trackColor: '#bca36a',

   //   trackColorSelected: palette.accent3Color,
      handleSize: 15  ,
   //   handleSizeDisabled: 8,
      handleSizeActive: 18,
   //   handleColorZero: palette.primary3Color,
   //   handleFillColor: palette.alternateTextColor,
   //   selectionColor: palette.primary1Color,
       selectionColor: '#00a652'
   //   rippleColor: palette.primary1Color
    },
    menu:{
      backgroundColor:"black"
    }
});


export default themeDefault;