import React from 'react';
import PageBase from '../components/PageBase';
import $ from 'jquery';
import StateNode from '../containers/StateNode';
import Data from '../data';
import StateMachineGraphEditor from'../components/StateMachineGraphEditor';
import themeDefault from '../theme-default';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class StateMachineUI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      states: []
    };
    this.Refresh=this.Refresh.bind(this);
  }
  componentDidMount() {
    this.Refresh();
  }
  Refresh()
  {
    $.ajax({
      url: Data.AppDir+'/api/StateMachineUI',
      type: 'GET',
      success: function ($data) { 
        this.setState({ states: $data });
      }.bind(this)
    });
  }
  render() {

    const { states } = this.state;
    return (
      <MuiThemeProvider muiTheme={themeDefault}><PageBase title="State Machine UI"
        navigation="Application / State Machine">
        <div>
        {states.map((e, i) => <StateNode
          key={i}
          state={e}
        />)}
          {states.length>0?<StateMachineGraphEditor states={states} Refresh={this.Refresh}/>:null}
        </div>
      </PageBase></MuiThemeProvider>
    );
  }
}


export default StateMachineUI;
StateMachineUI.propTypes = {
  /**
   * The axis on which the slider will slide.
   */   
};
