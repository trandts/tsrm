import React from 'react';
import PageBase from '../components/PageBase';
import Timeline from 'react-visjs-timeline';
import { render } from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import Slider from 'material-ui/Slider';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import { blue500 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import VirtualizedSelect from 'react-virtualized-select';
import Data from '../data';
import ImageNavigateBefore from 'material-ui/svg-icons/image/navigate-before';
import ImageNavigateNext from 'material-ui/svg-icons/image/navigate-next';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import TimePicker from 'material-ui/TimePicker';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';

const iconStyles = {
  outer: {
    padding: 0,
    height: 15,
    width: 15
  },
  inner: {
    padding: 0,
    height: 15,
    width: 15
  }
};
const styles = {

  columns: {
    id: {
      width: '10%'
    },
    name: {
      width: '20%'
    },
    price: {
      width: '20%'
    },
    category: {
      width: '20%'
    },
    edit: {
      width: '10%'
    }
  },
  saveButton: {
    margin: '10px'
  }
};

class Agenda extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [{
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 1',
        group: 1,
        itemType: 'task',
        id: 1
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 5',
        group: 1,
        itemType: 'task',
        id: 0
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 7, 22),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 2

      }, {
        start: new Date(2017, 8, 15),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 3
      }, {
        start: new Date(2017, 8, 17),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 3',
        group: 1,
        itemType: 'task',
        id: 4
      }, {
        start: new Date(2017, 8, 17),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 5
      }, {
        start: new Date(2017, 7, 22),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 6
      }],
      groups: [{
        id: 1,
        content: 'Project 1',
      }, {
        id: 2,
        content: 'Project 2',
      }],
      notesMissing: null,
      notesMissingLength: 1,
      Dimension: null,
      project: [],
      selectValue: [],
      TimePeriod: "Today",
      defaultDate: new Date()
    };
    this.Next = this.Next.bind(this);
    this.Before = this.Before.bind(this);

  }
  Next() {
    const { defaultDate, TimePeriod } = this.state;
    if (TimePeriod == "Today") {
      defaultDate.setDate(defaultDate.getDate() + 1);
    }
    if (TimePeriod == "This Week") {
      defaultDate.setDate(defaultDate.getDate() + 7);
    }
    if (TimePeriod == "This Month") {
      defaultDate.setMonth(defaultDate.getMonth() + 1);
    }
    this.setState({ defaultDate });
  }
  Before() {
    const { defaultDate, TimePeriod } = this.state;

    if (TimePeriod == "Today") {
      defaultDate.setDate(defaultDate.getDate() - 1);
    }
    if (TimePeriod == "This Week") {
      defaultDate.setDate(defaultDate.getDate() - 7);
    }
    if (TimePeriod == "This Month") {
      defaultDate.setMonth(defaultDate.getMonth() - 1);
    }
    this.setState({ defaultDate });
  }
  render() {
    let value = this.state.TimePeriod;
    let defaultDate = this.state.defaultDate;
    let FromDate = null;
    let ToDate = null;
    if (value == "Today") {
      FromDate = new Date(defaultDate.toJSON());
      ToDate = new Date(defaultDate.toJSON());
    }
    if (value == "This Week") {
      let curr = defaultDate;
      let firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      let lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
      FromDate = firstday;
      ToDate = lastday;
    }
    if (value == "This Month") {
      let date = defaultDate;
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      FromDate = firstDay;
      ToDate = lastDay;
    }

    FromDate.setHours(0, 0, 0, 0);

    ToDate.setHours(23, 59, 59, 999);
    const options = {
      width: '100%',
      height: '300px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'box',
      snap: function (date) {
        let hour = 60 * 60 * 1000;
        return Math.round(date / hour) * hour;
      },
      editable: true,
      template: function (item, element, data) {
        if (typeof item.id != "number") {
          return render(<div onClick={() => this.setState({ current: item })}>
            <select
              value={1}
              onChange={() => item.id = Math.random() * 500}
            >
              <option key={1} value={1}>Task 1</option>
              <option key={2} value={2}>Task 2</option>
            </select> </div>
            , element);
        } else if (item.itemType == 'task') {
          return render(<div onClick={() => this.setState({ current: item })}>
            {data.content}</div>
            , element);
        } else {
          return render(<div onClick={() => this.setState({ current: item })}>
            {data.content + data.start.toDateString()}</div>
            , element);
        }

        //`<div>${data.content}<div>${data.start.toLocaleString()}-${data.end.toLocaleString()}</div></div>`;
      }.bind(this),
      min: FromDate,
      max: ToDate
    };
 
    let onSelect = function ({ items }) {
      let item = this.state.items.filter(e => items.indexOf(e.id) > -1 && e.itemType == "notesMissing")[0] || null;
      if (item) {
        this.setState({ notesMissing: item, notesMissingLength: 1 });
      }
    }.bind(this);
    let _tasks = Data.taskMatrix.projects.filter(p =>
      this.state.project.map(e => e.id).indexOf(p.id) > -1)
      .map(e => e.tasks).reduce((a, b) => a.concat(b), []);
    let _taskItems = this.state.selectValue.map(e => ({
      start: e.startAt,
      //end: e.EndTarget,  // end is optional
      content: e.name,
      group: 1,
      itemType: 'task',
      id: e.id
    })).filter(e => e.start);
    return (
      <PageBase title="Agenda"
        navigation="Application / Agenda">
        <div>
          <div className="row">
            <div className="col-xs-12 col-sm-6" >
              <SelectField
                floatingLabelText="Time Period"
                value={this.state.TimePeriod}
                onChange={(e, i, TimePeriod) => this.setState({ TimePeriod })}
              ><MenuItem value="Today" primaryText="Today" />
                <MenuItem value="This Week" primaryText="This Week" />
                <MenuItem value="This Month" primaryText="This Month" />
              </SelectField>
            </div>
            <div className="col-xs-12 col-sm-6" >
              <SelectField
                floatingLabelText="Resource"
                value={this.state.users}
                onChange={(e, i, users) => this.setState({ users })}
              >{Data.taskMatrix.Users.map(
                u => <MenuItem key={u.id} value={u.id} primaryText={u.name} />
              )}
              </SelectField>
            </div>
          </div>
           <div className="row">
            <div className="col-xs-12 col-sm-6" >
              <SelectField
                floatingLabelText="Copy From"
                value={this.state.CopyFrom}
                onChange={(e, i, CopyFrom) => this.setState({ CopyFrom })}
              ><MenuItem value="Previous Day" primaryText="Previous Day" />
                <MenuItem value="This Week" primaryText="This Week" />
              </SelectField>
            </div>
            <div className="col-xs-12 col-sm-6" >
             <RaisedButton  style={{marginTop:25}}  label="Copy Agenda" primary={true} />
            </div>
          </div>
          <div className="row" >
            <div className="col-xs-12 col-sm-6" >
              <VirtualizedSelect
                options={Data.taskMatrix.projects}
                style={{ marginTop: 25 }}
                onChange={(project) => this.setState({ project })}
                multi={true}
                labelKey="name"
                valueKey="id"
                value={this.state.project}
              />
            </div>

            <div className="col-xs-12 col-sm-6">

              <VirtualizedSelect
                options={_tasks}
                style={{ marginTop: 25 }}
                onChange={(selectValue) => this.setState({ selectValue })}
                multi={true}
                labelKey="name"
                valueKey="id"
                value={this.state.selectValue}
              />
            </div>
          </div>
          <div className="row" >
            <div className="col-xs-12 col-sm-6" >
              <SelectField floatingLabelText="Filter By Project"
                value={this.state.a}
                onChange={(e, i, a) => this.setState({ a })}
              >{Data.taskMatrix.projects
                .map(e => <MenuItem value={e.id} key={e.id} primaryText={e.name} />)}
              </SelectField>
            </div>
            <div className="col-xs-12 col-sm-6" >
              <SelectField floatingLabelText="Filter By Working"
                value={this.state.b}
                onChange={(e, i, b) => this.setState({ b })}
              ><MenuItem value="Working" key={1} primaryText="Working" />
                <MenuItem value="Not Working" key={2} primaryText="Not Working" />
              </SelectField>
            </div>
          </div>
          <Paper >
            <AppBar title={<div style={{ display: 'flex' }}>
              <div style={{ width: '30%' }}>{"Agenda " + this.state.TimePeriod}</div>
              <div style={{ width: '70%', textAlign: 'right', fontSize: 12 }}>{(FromDate.toDateString() == ToDate.toDateString() || ToDate == null ?
                FromDate.toDateString() :
                FromDate.toDateString() + " - " + ToDate.toDateString()
              )}</div></div>}
              iconElementLeft={<div />} />
            <div style={{ display: 'flex' }}>
              <div style={{ width: '50%' }}>
                <IconButton tooltip="Back" style={{ float: 'left' }}
                  onClick={this.Before} >
                  <ImageNavigateBefore style={{ margin: 5 }} />
                </IconButton>
              </div>
              <div style={{ width: '50%' }}>
                <IconButton tooltip="Next" onClick={this.Next} style={{ float: 'right' }}>
                  <ImageNavigateNext style={{ margin: 5 }} />
                </IconButton>
              </div>
            </div>
            <Table
              selectable={false}
            >
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
                enableSelectAll={false}
              >
                <TableRow>
                  <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.name}>Task</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.price}>Expected Time</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.price}>Notes</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {this.state.selectValue.map((item, i) =>
                  <TableRow key={i}>
                    <TableRowColumn style={styles.columns.id}>{i + 1}</TableRowColumn>
                    <TableRowColumn style={styles.columns.name}>{item.name}</TableRowColumn>
                    <TableRowColumn style={styles.columns.price}>
                      <DatePicker hintText={"Task Start Date/Time"} defaultDate={item.startAt || new Date()} />
                      <TimePicker />
                    </TableRowColumn>
                    <TableRowColumn style={styles.columns.price}>
                      <Toggle label="Working" style={{ width: '47%', display: 'inline-block' }} />
                      <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
                        onClick={() => this.setState({ open: true })}
                      >
                        <ActionNoteAdd color={blue500} />
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                )}

              </TableBody>
            </Table>
            <Timeline key={_taskItems.length} options={options}
              items={_taskItems}
              animation={{
                duration: 3000,
                easingFunction: 'easeInQuint',
              }}
              selectHandler={onSelect}
            />
          </Paper>
          <div>

            <RaisedButton label="Save"
              style={{ margin: 5 }}
              primary={true} />
          </div>

          <Dialog title="Add Reply Note" key={1}
            actions={[
              <FlatButton key={0}
                label="Cancel"
                primary={true}
                onClick={() => this.setState({ open: false })}
              />,
              <FlatButton key={1}
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onClick={() => this.setState({ open: false })}
              />,
            ]}
            modal={false}
            open={this.state.open || false}
            contentStyle={{ width: '65%', maxWidth: 'none' }}
          ><div>{Array(this.state.notesMissingLength).fill().map((_, i) => i)
            .map((e, _i) => <div key={_i} style={{ display: 'flex' }}>
              <TextField key={1} floatingLabelText="Notes" multiLine={true} rows={2} />
              <TextField key={2} floatingLabelText="Delieverable" multiLine={true} rows={2} />
              <SelectField floatingLabelText="Delieverable Type"
                value={this.state["notesDelieverableType" + _i]}
                onChange={(e, i, v) => this.setState({ ["notesDelieverableType" + _i]: v })}>
                <MenuItem value={1} key={1} primaryText="Google Sheet" />
                <MenuItem value={2} key={2} primaryText="Code Reference" />
                <MenuItem value={3} key={3} primaryText="Meeting Notes" />
                <MenuItem value={4} key={4} primaryText="Others" />
              </SelectField>
            </div>)}
              <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
                onClick={() => this.setState({ notesMissingLength: this.state.notesMissingLength + 1 })}
              >
                <ActionNoteAdd color={blue500} />
              </IconButton>
              <DatePicker hintText={"Notes Date"} defaultDate={new Date()} />
              <Slider style={{ width: '90%' }}
                min={0}
                max={100}
                defaultValue={50}
              />
            </div>
          </Dialog>
        </div>
      </PageBase>
    );
  }


}

export default Agenda;
