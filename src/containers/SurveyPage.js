import React from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { grey500 } from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import Data from '../data';
import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import { Redirect } from 'react-router';
import FlatButton from 'material-ui/FlatButton';
import ScaledSlider from '../components/ScaledSlider';

class SurveyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { SavedPopup: false, Submitted: false };
    this.SaveFeedBack = this.SaveFeedBack.bind(this);
    this.Redirect = this.Redirect.bind(this);
  }
  SaveFeedBack() {
    this.setState({ SavedPopup: true });
  }
  Redirect() {
    // browserHistory.push('/some/path');
    // this.setState({ SavedPopup: false, Submitted: true });
    this.setState({ SavedPopup: false });
  }
  render() {
    const styles = {
      marginLeft: 20,
      floatingActionButton: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed',
        backgroundColor: '#bca36a'
      },
      editButton: {
        fill: grey500
      },
      columns: {
        id: {
          width: '10%'
        },
        name: {
          width: '50%'
        },
        price: {
          width: '40%',
          fontSize:10
        },
        category: {
          width: '20%'
        },
        edit: {
          width: '10%'
        }
      },
      saveButton: {
        margin: '10px'
      }
    };

    if (this.state.Submitted) {
      return <Redirect to="/somewhere" />;
    }
    /*{true?null:<TableRow key={-2}>
                  <TableRowColumn style={styles.columns.id} />
                  <TableRowColumn style={styles.columns.name}>Average Rating</TableRowColumn>
                  <TableRowColumn style={styles.columns.price}>
                    2.5
                  </TableRowColumn>
                </TableRow>}*/
    return (
      <PageBase title="Survey Page"
        navigation="Application / Survey Page">

        <div>
          <Paper >
            <AppBar title="Job Satisfaction Survey" iconElementLeft={<div />} />
            <Table
              selectable={false}
            >
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
                enableSelectAll={false}
              >
                <TableRow>
                  <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.name}>Question</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.price}>Disagree very much, Disagree moderately, Disagree slightly<br />Agree slightly,Agree moderately, Agree very much</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {Data.DummyData.question.map((item,i) =>
                  <TableRow key={item.id}>
                    <TableRowColumn style={styles.columns.id}>{item.key}</TableRowColumn>
                    <TableRowColumn style={styles.columns.name}>{item.label}</TableRowColumn>
                    <TableRowColumn style={styles.columns.price}>
                      <ScaledSlider min={0} max={5} step={1} value={i+1>5?5:i+1} />
                       
                    </TableRowColumn>
                  </TableRow>
                )}
                
              </TableBody>
            </Table>

            <TextField floatingLabelText="Feedback"
              multiLine={true}
              rows={2}
              fullWidth={true} />

            <RaisedButton label="Save"
              style={styles.saveButton}
              onClick={this.SaveFeedBack}
              primary={true} />
          </Paper>
          <Dialog
            title="Survey Form Submitted"
            actions={<FlatButton
              label="Ok"
              primary={true}
              keyboardFocused={true}
              onTouchTap={this.Redirect}
            />}
            modal={false}
            open={this.state.SavedPopup}
            onRequestClose={this.Redirect}
          >
            Survey has been submitted successfully
        </Dialog>

        </div>
      </PageBase>
    );
  }


}

export default SurveyPage;
