import React from 'react';
import PageBase from '../components/PageBase';
import DynamicTimeline from '../components/DynamicTimeline';
import IconButton from 'material-ui/IconButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import VirtualizedSelect from 'react-virtualized-select';
import Data from '../data';
import ImageNavigateBefore from 'material-ui/svg-icons/image/navigate-before';
import ImageNavigateNext from 'material-ui/svg-icons/image/navigate-next';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import Graph from 'react-graph-vis';


class Trace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [{
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 1',
        group: 1,
        itemType: 'task',
        id: 1
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 5',
        group: 1,
        itemType: 'task',
        id: 0
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 7, 22),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 2

      }, {
        start: new Date(2017, 8, 15),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 3
      }, {
        start: new Date(2017, 8, 17),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 3',
        group: 1,
        itemType: 'task',
        id: 4
      }, {
        start: new Date(2017, 8, 17),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 5
      }, {
        start: new Date(2017, 7, 22),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 6
      }],
      groups: [{
        id: 1,
        content: 'Project 1',
      }, {
        id: 2,
        content: 'Project 2',
      }],
      notesMissing: null,
      notesMissingLength: 1,
      Dimension: null,
      projects: null,
      selectValue: null,
      TimePeriod: "Today",
      defaultDate: new Date(),
      SelectedNodeOptions: null,
      TimelineOptions:null
    };
    this.Next = this.Next.bind(this);
    this.Before = this.Before.bind(this);

  }
  Next() {
    const { defaultDate, TimePeriod } = this.state;
    if (TimePeriod == "Today") {
      defaultDate.setDate(defaultDate.getDate() + 1);
    }
    if (TimePeriod == "This Week") {
      defaultDate.setDate(defaultDate.getDate() + 7);
    }
    if (TimePeriod == "This Month") {
      defaultDate.setMonth(defaultDate.getMonth() + 1);
    }
    this.setState({ defaultDate });
  }
  Before() {
    const { defaultDate, TimePeriod } = this.state;

    if (TimePeriod == "Today") {
      defaultDate.setDate(defaultDate.getDate() - 1);
    }
    if (TimePeriod == "This Week") {
      defaultDate.setDate(defaultDate.getDate() - 7);
    }
    if (TimePeriod == "This Month") {
      defaultDate.setMonth(defaultDate.getMonth() - 1);
    }
    this.setState({ defaultDate });
  }
  render() {
    let value = this.state.TimePeriod;
    let defaultDate = this.state.defaultDate;
    let SelectedNodeOptions = this.state.SelectedNodeOptions;
    let TimelineOptions = this.state.TimelineOptions;
    let FromDate = null;
    let ToDate = null;
    if (value == "Today") {
      FromDate = new Date(defaultDate.toJSON());
      ToDate = new Date(defaultDate.toJSON());
    }
    if (value == "This Week") {
      let curr = defaultDate;
      let firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      let lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
      FromDate = firstday;
      ToDate = lastday;
    }
    if (value == "This Month") {
      let date = defaultDate;
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      FromDate = firstDay;
      ToDate = lastDay;
    }
    if (value == "All Time") {
      let date = defaultDate;
      let firstDay = new Date(date.getFullYear()-30, date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear()+30, date.getMonth() + 1, 0);
      FromDate = firstDay;
      ToDate = lastDay;
    }

    FromDate.setHours(0, 0, 0, 0);

    ToDate.setHours(23, 59, 59, 999);


    let _tasks = Data.taskMatrix.projects.filter(p =>
      [this.state.projects].filter(e => e).map(e => e.id).indexOf(p.id) > -1)
      .map(e => e.tasks.filter(t =>
        (t.startAt < FromDate && FromDate < t.EndTarget) ||
        (t.startAt < ToDate && ToDate < t.EndTarget) ||
        (FromDate < t.startAt && t.EndTarget < ToDate))).reduce((a, b) => a.concat(b), []);
    let _Nodes = SelectedNodeOptions ? SelectedNodeOptions.Nodes : [];
    let _Edges = SelectedNodeOptions ? SelectedNodeOptions.Edges : [];

    let graph = {
      nodes: _Nodes,
      edges: _Edges,
    };

    let options2 = {
      height: '400px',//'100%',
      width: '100%',
      layout: {
        improvedLayout: true,
      },
      edges: {
        color: "#000000"
      }
    };

    let events = {
      selectNode: function ({ nodes }) {
        let Node = this.state.SelectedNodeOptions.Nodes.filter(e => nodes.indexOf(e.id) > -1)[0] || null;
        if (!Node) return;
        let TimelineOptions = {
          obj:Node.obj,
          objType:Node.objType
        };
        this.setState({  TimelineOptions: TimelineOptions });

      }.bind(this)
    };
    let TaskSelected = function (selectValue) {
      if (!selectValue) {
        this.setState({ selectValue, SelectedNodeOptions: null });
      }
      let edges = [];
      let Nodes = [{ id: "Task" + selectValue.id, label: selectValue.name + '\n(Task)', shape: 'box', obj: selectValue, objType: 'Task' }];

      let _projects = Data.taskMatrix.projects.filter(e => e.tasks.filter(t => t.id == selectValue.id).length > 0);
      Nodes = Nodes.concat(_projects.map(p => ({ id: "Project" + p.id, label: p.name + '\n(Project)', shape: 'box', obj: p, objType: 'Project' })));
      edges = edges.concat(_projects.map(p => ({ to: "Project" + p.id, from: "Task" + selectValue.id, label: 'Belongs To', font: { align: 'top' } })));

      let iterations = _projects.map(e => e.iterations).reduce((a, b) => a.concat(b), []).filter(f => f.id == selectValue.iteration);
      Nodes = Nodes.concat(iterations.map(p => ({ id: "Iteration" + p.id, label: p.name + '\n(Iteration)', shape: 'box', obj: p, objType: 'Iteration' })));
      edges = edges.concat(iterations.map(p => ({ to: "Iteration" + p.id, from: "Task" + selectValue.id, label: 'Belongs To', font: { align: 'top' } })));

      Nodes = Nodes.concat(selectValue.Assigned.map((p, _i) => ({ id: "Assigned" + _i, label: p.user, shape: 'box', obj: p, objType: 'Assigned' })));
      edges = edges.concat(selectValue.Assigned.map((p, _i) => ({ to: "Assigned" + _i, from: "Task" + selectValue.id, label: 'Assigned To', font: { align: 'top' } })));

      Nodes = Nodes.concat([ { id: "Notes0", label: "Notes", shape: 'box', obj: {}, objType: 'Notes' }]);
      edges = edges.concat([{ to: "Notes0", from: "Task" + selectValue.id, label: 'Based On', font: { align: 'top' } }]);



      let SelectedNodeOptions = {
        key: new Date().toString(),
        itemType: "Task",
        Id: selectValue,
        Nodes: Nodes,
        Edges: edges
      };
      this.setState({ selectValue, SelectedNodeOptions: SelectedNodeOptions });
    }.bind(this);
    return (
      <PageBase title="Trace"
        navigation="Application / Trace">
        <div>
          <div className="row">
            <div className="col-xs-12 col-sm-4" >
              <SelectField
                floatingLabelText="Time Period"
                value={this.state.TimePeriod}
                onChange={(e, i, TimePeriod) => this.setState({ TimePeriod })}
              ><MenuItem value="Today" primaryText="Today" />
                <MenuItem value="This Week" primaryText="This Week" />
                <MenuItem value="This Month" primaryText="This Month" />
                <MenuItem value="All Time" primaryText="All Time" />
              </SelectField>
            </div>
            <div className="col-xs-12 col-sm-4" >
              <VirtualizedSelect
                options={Data.taskMatrix.projects}
                style={{ margin: 25, width: '95%' }}
                onChange={(projects) => this.setState({ projects })}
                multi={false}
                labelKey="name"
                valueKey="id"
                value={this.state.projects}
              />
            </div>
            <div className="col-xs-12 col-sm-4" >
              <VirtualizedSelect
                options={_tasks}
                style={{ marginTop: 25 }}
                onChange={TaskSelected}
                multi={false}
                labelKey="name"
                valueKey="id"
                value={this.state.selectValue}
              />
            </div>
          </div>
          <Paper >
            <AppBar title={<div style={{ display: 'flex' }}>
              <div style={{ width: '30%' }}>{SelectedNodeOptions ? SelectedNodeOptions.itemType : null}</div>
              <div style={{ width: '70%', textAlign: 'right', fontSize: 12 }}>{(FromDate.toDateString() == ToDate.toDateString() || ToDate == null ?
                FromDate.toDateString() :
                FromDate.toDateString() + " - " + ToDate.toDateString()
              )}</div></div>}
              iconElementLeft={<div />} />
            <div style={{ display: 'flex' }}>
              <div style={{ width: '50%' }}>
                <IconButton tooltip="Back" style={{ float: 'left' }}
                  onClick={this.Before} >
                  <ImageNavigateBefore style={{ margin: 5 }} />
                </IconButton>
              </div>
              <div style={{ width: '50%' }}>
                <IconButton tooltip="Next" onClick={this.Next} style={{ float: 'right' }}>
                  <ImageNavigateNext style={{ margin: 5 }} />
                </IconButton>
              </div>
            </div>
            {SelectedNodeOptions ?
              <Graph key={SelectedNodeOptions.key} graph={graph} options={options2} events={events} /> :
              null}
              {TimelineOptions ?
              <DynamicTimeline options={TimelineOptions} /> :
              null}
          </Paper>
        </div>
      </PageBase>
    );
  }


}

export default Trace;
