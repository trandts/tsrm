import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import PageBase from '../components/PageBase';
import $ from 'jquery';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import { Link } from 'react-router';
import { Tabs, Tab } from 'material-ui/Tabs';
import Data from '../data';


class Tasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Tasks:[],value:"1"};
    }
  componentDidMount() {
     $.ajax({
        url: Data.AppDir+'/api/Tasks/'+$("#User").attr("value") ,
        type: 'GET',
        success: function ($data) {
          this.setState({ Tasks: $data });
        }.bind(this)
      });
  }
    render()
    {
      const {Tasks}=this.state;
      const styles={
        Id:{
          width:'5%'
        },
        TaskName:{
          width:'50%'
        },
        Case:{
          width:'15%'
        },
        CaseName:{
          width:'15%'
        },
        Open:{
          width:'15%'
        }
      };
      return (
        <PageBase title="Tasks"
          navigation="Application / Tasks">
          <div>
          <Tabs value={this.state.value}
          onChange={(value) => this.setState({ value: value })}
        > <Tab label="In Complete My Tasks" value="1" >
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn style={styles.Id}>ID</TableHeaderColumn>
                <TableHeaderColumn style={styles.TaskName}>Task Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Case}>Case</TableHeaderColumn>
                <TableHeaderColumn style={styles.CaseName}>Case Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Open} />
              </TableRow>
            </TableHeader>
            <TableBody>
              {Tasks.filter(e=>!e.IsCompleted && e.IsAssigned).map((e) => (<TableRow key={e.Id}>
                <TableRowColumn style={styles.Id}>{e.Id}</TableRowColumn>
                <TableRowColumn  style={styles.TaskName}>{e.Name}</TableRowColumn>
                <TableRowColumn style={styles.Case}>{e.Case}</TableRowColumn>
                <TableRowColumn style={styles.CaseName}>{e.CaseContext}</TableRowColumn>
                <TableRowColumn style={styles.Open}><Link to={'/WorkflowInstanceDisplay/'+e.StateInstanceId}><RaisedButton label="Open Task" primary={true} />
                  </Link>
                </TableRowColumn>
              </TableRow>))}
            </TableBody>
          </Table>
          </Tab>
          <Tab label="Other In Complete Tasks" value="2" >
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn style={styles.Id}>ID</TableHeaderColumn>
                <TableHeaderColumn style={styles.TaskName}>Task Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Case}>Case</TableHeaderColumn>
                <TableHeaderColumn style={styles.CaseName}>Case Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Open} />
              </TableRow>
            </TableHeader>
            <TableBody>
              {Tasks.filter(e=>!e.IsCompleted && !e.IsCommon && !e.IsAssigned).map((e) => (<TableRow key={e.Id}>
                <TableRowColumn style={styles.Id}>{e.Id}</TableRowColumn>
                <TableRowColumn  style={styles.TaskName}>{e.Name}</TableRowColumn>
                <TableRowColumn style={styles.Case}>{e.Case}</TableRowColumn>
                <TableRowColumn style={styles.CaseName}>{e.CaseContext}</TableRowColumn>
                <TableRowColumn style={styles.Open}><Link to={'/WorkflowInstanceDisplay/'+e.StateInstanceId}><RaisedButton label="Open Task" primary={true} />
                  </Link>
                </TableRowColumn>
              </TableRow>))}
            </TableBody>
          </Table>
          </Tab>
          <Tab label="In Complete Common Tasks" value="3" >
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn style={styles.Id}>ID</TableHeaderColumn>
                <TableHeaderColumn style={styles.TaskName}>Task Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Case}>Case</TableHeaderColumn>
                <TableHeaderColumn style={styles.CaseName}>Case Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.Open} />
              </TableRow>
            </TableHeader>
            <TableBody>
              {Tasks.filter(e=>!e.IsCompleted && e.IsCommon).map((e) => (<TableRow key={e.Id}>
                <TableRowColumn style={styles.Id}>{e.Id}</TableRowColumn>
                <TableRowColumn  style={styles.TaskName}>{e.Name}</TableRowColumn>
                <TableRowColumn style={styles.Case}>{e.Case}</TableRowColumn>
                <TableRowColumn style={styles.CaseName}>{e.CaseContext}</TableRowColumn>
                <TableRowColumn style={styles.Open}><Link to={'/WorkflowInstanceDisplay/'+e.StateInstanceId}><RaisedButton label="Open Task" primary={true} />
                  </Link>
                </TableRowColumn>
              </TableRow>))}
            </TableBody>
          </Table>
          </Tab>
          <Tab label="Completed Tasks" value="4">
          <Table>
            <TableHeader>
              <TableRow>
              <TableHeaderColumn style={styles.Id}>ID</TableHeaderColumn>
              <TableHeaderColumn style={styles.TaskName}>Task Name</TableHeaderColumn>
              <TableHeaderColumn style={styles.Case}>Case</TableHeaderColumn>
              <TableHeaderColumn style={styles.CaseName}>Case Name</TableHeaderColumn>
              <TableHeaderColumn style={styles.Open} />
              </TableRow>
            </TableHeader>
            <TableBody>
              {Tasks.filter(e=>e.IsCompleted).map((e) => (<TableRow key={e.Id}>
                <TableRowColumn style={styles.Id}>{e.Id}</TableRowColumn>
                <TableRowColumn  style={styles.TaskName}>{e.Name}</TableRowColumn>
                <TableRowColumn style={styles.Case}>{e.Case}</TableRowColumn>
                <TableRowColumn style={styles.CaseName}>{e.CaseContext}</TableRowColumn>
                <TableRowColumn style={styles.Open}><Link to={'/WorkflowInstanceDisplay/'+e.StateInstanceId}><RaisedButton label="Open Task" primary={true} />
                  </Link>
                </TableRowColumn>
              </TableRow>))}
            </TableBody>
          </Table>
          </Tab>
          </Tabs>
          </div>
        </PageBase>
      );
    }
}

export default Tasks;
