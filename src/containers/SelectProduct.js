import React from 'react';
//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
//import FlatButton from 'material-ui/FlatButton';
//import Checkbox from 'material-ui/Checkbox';
import { grey500, white } from 'material-ui/styles/colors';

import { Link } from 'react-router';
import PageBase from '../components/PageBase';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Data from '../data';

class SelectProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, index, value) {
    this.setState({ value });
  }
  render() {
    const styles = {
      loginContainer: {
        minWidth: 265,
        maxWidth: 400,
        height: 'auto',
        xposition: 'absolute',
        xtop: '20%',
        xleft: 0,
        xright: 0,
        margin: 'auto'
      },
      paper: {
        padding: 20,
        overflow: 'auto',
      },
      buttonsDiv: {
        textAlign: 'center',
        padding: 10,
        display: 'none'
      },
      flatButton: {
        color: grey500
      },
      checkRemember: {
        style: {
          float: 'left',
          maxWidth: 180,
          paddingTop: 5,
          display: 'none'
        },
        labelStyle: {
          color: grey500
        },
        iconStyle: {
          color: grey500,
          borderColor: grey500,
          fill: grey500
        }
      },
      loginBtn: {
        float: 'right'
      },
      btn: {
        background: '#4f81e9',
        color: white,
        padding: 7,
        borderRadius: 2,
        margin: 2,
        fontSize: 13
      },
      btnFacebook: {
        background: '#4f81e9'
      },
      btnGoogle: {
        background: '#e14441'
      },
      btnSpan: {
        marginLeft: 5
      },
      logoContainer: {
        xmarginTop: 242,
        backgroundColor: "#ffffff",
        padding: 2,
        paddingTop: 5,
        paddingLeft: 5,
        maxHeight: 57,
        display: 'inline',
        float: 'left'
      },
      appImg: {
        padding: 3,
        width: 220,
        maxHeight: 57,
        float: 'right',
        display: 'inline'
      }
    };

    return (
      <PageBase title="Select Product"
        navigation="Application / Select Product">
        <div className="loginPgRight bblue ">
          <div style={styles.loginContainer}>
            <div style={styles.paper}>
              <form>
                <SelectField
                  floatingLabelText="Product"
                  fullWidth={true}
                  onChange={this.handleChange}
                  value={this.state.value}
                >{Data.DummyData.products.map((item,i) =>
                  <MenuItem key={i} value={item.key} primaryText={item.label} />)}
                </SelectField>

                <div>
                  <Link to="/">
                    <RaisedButton label="Survey" primary={true} style={styles.loginBtn} />
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div></PageBase>
    );
  }
}

export default SelectProduct;
