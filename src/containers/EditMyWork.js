import React from 'react';
import PageBase from '../components/PageBase';
import Timeline from 'react-visjs-timeline';
import { render } from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import Slider from 'material-ui/Slider';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import { blue500 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


const iconStyles = {
  outer: {
    padding: 0,
    height: 15,
    width: 15
  },
  inner: {
    padding: 0,
    height: 15,
    width: 15
  }
};

class UserTimeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [{
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 1',
        group: 1,
        itemType: 'task',
        id: 1
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 8, 2),  // end is optional
        content: 'Task 5',
        group: 1,
        itemType: 'task',
        id: 0
      }, {
        start: new Date(2017, 7, 15),
        end: new Date(2017, 7, 22),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 2

      }, {
        start: new Date(2017, 8, 15),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 2',
        group: 2,
        itemType: 'task',
        id: 3
      }, {
        start: new Date(2017, 8, 17),
        end: new Date(2017, 9, 2),  // end is optional
        content: 'Task 3',
        group: 1,
        itemType: 'task',
        id: 4
      }, {
        start: new Date(2017, 8, 17),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 5
      }, {
        start: new Date(2017, 7, 22),
        content: 'Notes Missing ',
        group: 1,
        itemType: 'notesMissing',
        type: 'point',
        id: 6
      }],
      groups: [{
        id: 1,
        content: 'Project 1',
      }, {
        id: 2,
        content: 'Project 2',
      }],
      notesMissing:null,
      notesMissingLength:1
    };
  
  }
  render() {
    const options = {
      width: '100%',
      height: '300px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'range',
      editable:{
        add:true,
        updateTime:true,
        remove:true
      },
      template:function(item, element, data){
          if(typeof item.id!="number")
          {
            return render(<div onClick={() => this.setState({ current: item })}>
              <select
                value={1}
                onChange={()=>item.id=Math.random()*500}
              >
                <option key={1} value={1}>Task 1</option>
                <option key={2} value={2}>Task 2</option>
              </select> </div>
              , element);
          } else if (item.itemType == 'task') {
            return render(<div onClick={() => this.setState({ current: item })}>
              {data.content}</div>
              , element);
          } else {
            return render(<div onClick={() => this.setState({ current: item })}>
              {data.content + data.start.toDateString()}</div>
              , element);
          }
         
          //`<div>${data.content}<div>${data.start.toLocaleString()}-${data.end.toLocaleString()}</div></div>`;
        }.bind(this),
      format: {
        minorLabels: {
          minute: 'h:mma',
          hour: 'ha'
        }
      }
    };








    
     const {items,groups} = this.state;
    let onSelect=function({items})
    {
     let item= this.state.items.filter(e=>items.indexOf(e.id)>-1 && e.itemType=="notesMissing")[0]||null;
      if(item)
      {
         this.setState({notesMissing:item, notesMissingLength:1});
      }
    }.bind(this);
    return (
      <PageBase title="User Timeline"
        navigation="Application / User Timeline">
        
        <div>
          <Timeline options={options} 
          groups={groups}
            items={items}
            animation={{
  duration: 3000,
  easingFunction: 'easeInQuint',
}}
selectHandler ={onSelect}
          />{this.state.notesMissing?
        <div>
         <h2> {this.state.notesMissing.start.toDateString()}</h2>
         <DatePicker hintText="Notes Date"  defaultDate={this.state.notesMissing.start} />
         {Array(this.state.notesMissingLength).fill().map((_, i) => i + 1).map((e,_i) => <div key={_i} style={{display:'flex'}}>
          <TextField key={1} floatingLabelText="Notes" multiLine={true} rows={2} />
          <TextField key={2} floatingLabelText="Delieverable" multiLine={true} rows={2} />
          <SelectField floatingLabelText="Delieverable Type"
            value={this.state["notesDelieverableType" + _i]}
            onChange={(e, i, v) => this.setState({ ["notesDelieverableType" + _i]: v })}>
            <MenuItem value={1} key={1} primaryText="Google Sheet" />
            <MenuItem value={2} key={2} primaryText="Code Reference" />
            <MenuItem value={3} key={3} primaryText="Meeting Notes" />
            <MenuItem value={4} key={4} primaryText="Others" />
          </SelectField>
        </div>)}
         <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
         onClick={()=>this.setState({notesMissingLength:this.state.notesMissingLength+1})}
         >
              <ActionNoteAdd color={blue500} />
            </IconButton>
         <Slider style={{ width: '90%' }}
                min={0}
                max={100}
                defaultValue={50}
              />
        </div>
        :null}
           <RaisedButton label="Save"
                        style={{margin:5}}
                        primary={true}/>
        </div>
      
      </PageBase>
    );
  }


}

export default UserTimeline;
