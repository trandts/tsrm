import React , { PropTypes } from 'react';
import WorkflowOpen from '../components/WorkflowOpen';

class WorkflowInstanceDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      WorkflowInstanceId:parseInt(this.props.params.Id||0)
    };
  }
render()
{

  const {WorkflowInstanceId}=this.state;
  return (
   <div><WorkflowOpen WorkflowInstance={WorkflowInstanceId} />
   </div>
  );
}
}

export default WorkflowInstanceDisplay;
WorkflowInstanceDisplay.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  WorkflowInstance: PropTypes.number,
   params: React.PropTypes.shape({
      Id: React.PropTypes.string.isRequired
    })
};
