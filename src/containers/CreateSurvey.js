import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { grey400 } from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import PageBase from '../components/PageBase';
import ResponseSlider from '../components/dashboard/ResponseSlider';
import Data from '../data';
import Checkbox from 'material-ui/Checkbox';
import Subheader from 'material-ui/Subheader';


const CreateSurvey = () => {

  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },

    toggleThumbOff: {
      backgroundColor: '#ffcccc',
    },
    toggleTrackOff: {
      backgroundColor: '#ff9d9d',
    },
    toggleThumbSwitched: {
      backgroundColor: 'red',
    },
    toggleTrackSwitched: {
      backgroundColor: '#ff9d9d',
    },
    toggleLabelStyle: {
      color: grey400,
      fontWeight: 100
    },


    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    },
    sliderContainer: {
      display: 'none',
      xmaxWidth: 300,
      xmarginTop: 40,
      xmarginBottom: 5
    }
  };


  return (
    <PageBase title="Create Survey"
      navigation="Application / Create Survey">
      <form>

        <TextField
          hintText="Survey"
          floatingLabelText="Survey Name"
          fullWidth={true}
        />
          <Subheader>Users</Subheader>
          {Data.DummyData.directors
            .map((item,i) => <Checkbox
              key={i}
              label={item.label}
              labelPosition="left"
              style={styles.checkbox}
            />)
          }
        <Divider />
          <Subheader>Questions</Subheader>
          {Data.DummyData.question
            .map((item,i) => <Checkbox
              key={i}
              label={item.label}
              labelPosition="left"
              style={styles.checkbox}
            />)
          }
        <Divider />
          <Subheader>Products</Subheader>
          {Data.DummyData.products
            .map((item,i) => <Checkbox
              key={i}
              label={item.label}
              labelPosition="left"
              style={styles.checkbox}
            />)
          }

        <Divider />

        <div style={styles.sliderContainer}>
          <ResponseSlider title="Alignment with brand plan" />
        </div>

        <div style={styles.buttons}>
          <RaisedButton label="Save"
            style={styles.saveButton}
            type="submit"
            primary={true} />
        </div>
      </form>
    </PageBase>
  );
};

export default CreateSurvey;
