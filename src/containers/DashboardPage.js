import React from 'react';
import { cyan600, pink600, orange600, purple600, purple500,purple100,pink100 } from 'material-ui/styles/colors';
//import Assessment from 'material-ui/svg-icons/action/assessment';
//import Face from 'material-ui/svg-icons/action/face';
//import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
//import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import InfoBox from '../components/dashboard/InfoBox';
import MonthlySales from '../components/dashboard/MonthlySales';
//import BrowserUsage from '../components/dashboard/BrowserUsage';
//import RecentlyProducts from '../components/dashboard/RecentlyProducts';
import globalStyles from '../styles';
import Data from '../data';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { CardTitle } from 'material-ui/Card';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import CommunicationContacts from 'material-ui/svg-icons/communication/contacts';
import CommunicationBusiness from 'material-ui/svg-icons/communication/business';
import ActionAccountCircle from 'material-ui/svg-icons/action/account-circle';


const customContentStyle = {
  width: '100%',
  maxWidth: 'none',
};

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullscreen: false
    };
    this.handleClick = this.handleClick.bind(this);
  }


  handleClick() 
  {
    this.setState({ fullscreen: true });
  }
  render() {

    return (
      <div>
        <h3 style={globalStyles.navigation}>Application / Dashboard</h3>
        <CardTitle title="Dashboard" />
        <div className="row">

          <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
            <InfoBox Icon={CommunicationBusiness}
              color={pink600}
              title="Total Departments"
              value="7"
            />
          </div>


          <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
            <InfoBox Icon={PermIdentity}
              color={cyan600}
              title="Total Directors"
              value="9"
            />
          </div>

          <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
            <InfoBox Icon={CommunicationContacts}
              color={purple600}
              title="Total Managers"
              value="14"
            />
          </div>

          <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3 m-b-15 ">
            <InfoBox Icon={ActionAccountCircle}
              color={orange600}
              title="Total Labour"
              value="208"
            />
          </div>
        </div>

        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-md m-b-15" >
            <MonthlySales
              data={Data.dashBoardPage.monthlySales2} displayXAxis={true} displayYAxis={true}
              Height="250"
              title="Job Satisfaction" barColor={purple100} paperColor={purple600} headerColor={purple500} />
          </div>

          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 m-b-15" onClick={this.handleClick.bind(this)}>
            <MonthlySales data={Data.dashBoardPage.monthlySales} barColor={pink100} 
            Height="250"
            displayXAxis={true} displayYAxis={true}
            title="Learning & Growth Opportunities" style={{ pointerEvents: 'none' }} />
          </div>
        </div>
        <Dialog modal={true}
          contentStyle={customContentStyle}
          open={this.state.fullscreen}
          actions={<FlatButton
            label="Close"
            primary={true}
            onTouchTap={() => this.setState({ fullscreen: false })}
          />}
        >
          <MonthlySales data={Data.dashBoardPage.monthlySales} title="Learning & Growth Opportunities"  barColor={pink100} 
            Height={450} displayXAxis={true} displayYAxis={true} />
        </Dialog>
      </div>
    );
  }
}

export default DashboardPage;
