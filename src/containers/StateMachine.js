import React from 'react';
import PageBase from '../components/PageBase';
import $ from 'jquery';
import State from '../containers/State';
import Data from '../data';

class StateMachine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      states: [],
      TriggerAcitvities:[]
    };
  }
  componentDidMount() {
    $.ajax({
      url:Data.AppDir+ '/api/state',
      type: 'GET',
      success: function ($data) {
        this.setState({ states: $data });
      }.bind(this)
    });
    $.ajax({
      url: Data.AppDir+'/api/TriggerAcitvity',
      type: 'GET',
      success: function ($data) {
        this.setState({ TriggerAcitvities: $data });
      }.bind(this)
    });
  }
  render() {

    const { states,TriggerAcitvities } = this.state;
    return (
      <PageBase title="State Machine"
        navigation="Application / State Machine">
        {states.map((e, i) => <State
          key={i}
          state={e}
          states={states}
          TriggerAcitvities={TriggerAcitvities}
        />)}
      </PageBase>
    );
  }
}


export default StateMachine;
StateMachine.propTypes = {
  /**
   * The axis on which the slider will slide.
   */   
};
