import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import {grey400} from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import PageBase from '../components/PageBase';
import ResponseSlider from '../components/dashboard/ResponseSlider';
import Data from '../data';

const Tag = () => {

  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },

    toggleThumbOff: {
      backgroundColor: '#ffcccc',
    },
    toggleTrackOff: {
      backgroundColor: '#ff9d9d',
    },
    toggleThumbSwitched: {
      backgroundColor: 'red',
    },
    toggleTrackSwitched: {
      backgroundColor: '#ff9d9d',
    },
    toggleLabelStyle: {
      color: grey400,
       fontWeight: 100
    },


    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    },
    sliderContainer:{
      display:'none'
    }
  };

  
  return (
    <PageBase title="Tag"
              navigation="Application / Tag">
      <form>

        <TextField
          floatingLabelText="Tag"
          fullWidth={true}
        />
        <SelectField floatingLabelText="Tag Category"
        value={null} >
                {Data.taskMatrix.tagCategories.map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)}
              </SelectField>
        <Divider />

        <div style={styles.sliderContainer}>
          <ResponseSlider title="Alignment with brand plan"/>
        </div>
       
        <div style={styles.buttons}>
          <RaisedButton label="Cancel"/>

          <RaisedButton label="Save"
                        style={styles.saveButton}
                        type="submit"
                        primary={true}/>
        </div>
      </form>
    </PageBase>
  );
};

export default Tag;
