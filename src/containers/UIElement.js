import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { grey400 } from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import PageBase from '../components/PageBase';
import ResponseSlider from '../components/dashboard/ResponseSlider';
import $ from "jquery";
import Data from '../data';

class UIElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 1,
      Name: ""
    };
    this.Save = this.Save.bind(this);
  }
  Save() {
    let data = {
      UIElement: this.state.Name,
      Events: Array.from(Array(this.state.count).keys()).map(e => this.state["Event" + e])
        .filter(e => e).map(e => ({ EventName: e }))
    };
    $.ajax({
      data: data,
      url:  Data.AppDir+'/api/UIElement',
      type: 'POST',
      success: function () {
      }
    });
  }
  render() {

    const styles = {
      toggleDiv: {
        maxWidth: 300,
        marginTop: 40,
        marginBottom: 5
      },
      toggleLabel: {
        color: grey400,
        fontWeight: 100
      },

      toggleThumbOff: {
        backgroundColor: '#ffcccc',
      },
      toggleTrackOff: {
        backgroundColor: '#ff9d9d',
      },
      toggleThumbSwitched: {
        backgroundColor: 'red',
      },
      toggleTrackSwitched: {
        backgroundColor: '#ff9d9d',
      },
      toggleLabelStyle: {
        color: grey400,
        fontWeight: 100
      },


      buttons: {
        marginTop: 30,
        float: 'right'
      },
      saveButton: {
        marginLeft: 5
      },
      sliderContainer: {
        display: 'none',
      }
    };

    return (
      <PageBase title="UI Element"
        navigation="Application / Form">
        <form>

          <TextField key={-1}
            hintText="Name"
            floatingLabelText="Name"
            value={this.state["Name"]}
            onChange={(a, v) => this.setState({ ["Name"]: v })}
            fullWidth={true}
          />
          {Array.from(Array(this.state.count).keys()).map((e, i) => <TextField
            key={i}
            hintText="Event"
            floatingLabelText="Name"
            value={this.state["Event" + i]}
            fullWidth={true}
            onChange={(a, v) => this.setState({ ["Event" + i]: v })}
          />)}

          <Divider />


          <div style={styles.sliderContainer}>
            <ResponseSlider title="Alignment with brand plan" />
          </div>

          <div style={styles.buttons}>

            <RaisedButton label="Add Event"
              style={styles.saveButton}
              onClick={() => this.setState({ count: this.state.count + 1 })}
              primary={true} />
            <RaisedButton label="Save"
              style={styles.saveButton}
              onClick={this.Save}
              primary={true} />
          </div>
        </form>
      </PageBase>
    );
  }
}

export default UIElement;
