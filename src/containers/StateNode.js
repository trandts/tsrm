import React, { PropTypes } from 'react';
import NotificationEventNote from 'material-ui/svg-icons/notification/event-note';
import ImageTransform from 'material-ui/svg-icons/image/transform';
import ActionChangeHistory from 'material-ui/svg-icons/action/change-history';
import ActionSettingsApplications from 'material-ui/svg-icons/action/settings-applications';
import IconButton from 'material-ui/IconButton';

class StateNode extends React.Component {
  constructor(props) {
    super(props);
    this.state = { State:this.props.state ,display:true
      ,displayEvent:false,displayTransition:false,displayChildren:false,displayDSM:false
      ,displayGraph:false};
    this.EventToggle=this.EventToggle.bind(this);
    this.TransitionToggle=this.TransitionToggle.bind(this);
    this.ChildrenToggle=this.ChildrenToggle.bind(this);
    this.DSMToggle=this.DSMToggle.bind(this);
  }
  EventToggle()
  {
    this.setState({displayEvent:!this.state.displayEvent});
  }
  TransitionToggle()
  {
    this.setState({displayTransition:!this.state.displayTransition});
  }
  ChildrenToggle()
  {
    this.setState({displayChildren:!this.state.displayChildren});
  }
  DSMToggle()
  {
    this.setState({displayDSM:!this.state.displayDSM});
  } 
  render() {

    const { Type,Name,Logic,Transitions 
      ,Events,ChildStateMachine,ChildDataStateMachine
    } = this.state.State;
    const {displayEvent,displayTransition,displayChildren,displayDSM}=this.state;

let Event=Events.length>0?(<div style={{border: 'red 1px double',minHeight:46}}><IconButton tooltip="Events" style={{float: 'left' }} 
onClick={this.EventToggle} >
  <NotificationEventNote style={{ margin: 5 }} />
</IconButton>
  {displayEvent?Events.map((ev,j)=><div key={j}>{ev}</div>):null}</div>):null;

const Transition=Transitions.length>0?(<div style={{border: 'green 1px double',minHeight:46}}><IconButton tooltip="Transitions" style={{float: 'left' }} 
onClick={this.TransitionToggle} >
  <ImageTransform style={{ margin: 5 }} />
</IconButton>
  {displayTransition?Transitions.map((ev,j)=><div key={j}>{ev.Event}:{ev.FromState}->{ev.ToState}</div>):null}</div>):null;

const ChildLogo=ChildStateMachine.length>0?(<div><IconButton tooltip="Children" style={{float: 'left' }} 
  onClick={this.ChildrenToggle} >
    <ActionChangeHistory style={{ margin: 5 }} />
  </IconButton></div>):null;

  
const ChildDSM=(<div>{ChildDataStateMachine.length>0?
(<div style={{border: 'orange 1px double',minHeight:46}}>
  DSM<IconButton tooltip="Children" style={{float: 'left' }} 
  onClick={this.DSMToggle} >
    <ActionSettingsApplications style={{ margin: 5 }} />
  </IconButton>
  {displayDSM?ChildDataStateMachine.map((e,i)=><StateNode key={'a'+i} state={e} />):null}
</div>):null}</div>);

    return (
      <div style={{ border: 'black 1px dotted',marginTop:10,marginLeft:10 }}>
        <div ><div style={{minHeight:36}}>{ChildLogo}
        {Name} ({Type}-{Logic})</div>
          {Event}
          {Transition}
          {ChildDSM}
          {displayChildren?ChildStateMachine.map((e,i)=><StateNode key={i} state={e} />):null}
        </div>
      </div>
    );
  }
}

export default StateNode;
StateNode.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  state:PropTypes.object 
};
