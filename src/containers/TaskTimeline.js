import React from 'react';
import PageBase from '../components/PageBase';
import Timeline from 'react-visjs-timeline';
import TaskCommentTimeline from '../components/TaskCommentTimeline';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class TaskTimeline extends React.Component {
  constructor(props) {
    super(props);
     this.state = {
      FromDate: null,
      ToDate: null,
      value:"Day"
    };
    this.handleChange=this.handleChange.bind(this);
  }
  handleChange(event, index, value)
  {
    this.setState({value});
  }
  render() {
    const options = {
      width: '100%',
      height: '300px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'range',
      template: function (item, element, data) {

        return data.style ? `<div>${data.content}</div>` : `<div>${data.content}<div>${data.start.toLocaleString()}-${data.end.toLocaleString()}</div></div>`;
      },
      format: {
        minorLabels: {
          minute: 'h:mma',
          hour: 'ha'
        }
      }
    };
    const { value } = this.state;
    let FromDate = this.state.FromDate;
    let ToDate = this.state.ToDate;
    if (value == "Today") {
      FromDate = new Date();
      ToDate = new Date();
    }
    if (value == "This Week") {
      let curr = new Date;
      let firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      let lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
      FromDate = firstday;
      ToDate = lastday;
    }
    if (value == "This Month") {
      let date = new Date();
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      FromDate = firstDay;
      ToDate = lastDay;
    }

    const items = [{
      start: new Date(2017, 7, 15),
      end: new Date(2017, 8, 2),  // end is optional
      content: 'Faheem',
      group:1
    },{
      start: new Date(2017, 7, 15),
      end: new Date(2017, 7, 22),  // end is optional
      content: 'Noman',
      group:1
      
    },{
      start: new Date(2017, 7, 14),  // end is optional
      content: 'LifeCycle:Started',
      style:"color: #25d495; background-color: #eaffc0;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 7, 16),  // end is optional
      content: 'Priority:Low',
      style:"color: #25d495; background-color: #eaffc0;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 7, 24),  // end is optional
      content: 'Priority:High',
      style:"color: #25d495; background-color: #eaffc0;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 7, 25),  // end is optional
      content: 'Unassigned To Noman',
      style:"color: black; background-color: #e8a76f;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 7, 26),  // end is optional
      content: 'Assigned To Faheem',
      style:"color: black; background-color: #e8a76f;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 7, 29),  // end is optional
      content: 'LifeCycle:Paused',
      style:"color: #25d495; background-color: #eaffc0;",
      group:1,
      type:'box'
    },{
      start: new Date(2017, 8, 5),  // end is optional
      content: 'LifeCycle:Resumed',
      style:"color: #25d495; background-color: #eaffc0;",
      group:1,
      type:'box'
    }, {
      start: new Date(2017, 8, 15),
      end: new Date(2017, 9, 2),  // end is optional
      content: 'Raheel',
      group:2
    }, {
      start: new Date(2017, 8, 17),
      end: new Date(2017, 9, 2),  // end is optional
      content: 'IP',
      group:1
    }].filter(e=>
    (e.start>=FromDate||FromDate==null)
    &&
    (e.end<=ToDate||ToDate==null)
    );
 /*   const groups = [{
      id: 1,
      content: 'Iteration 1',
    }, {
      id: 2,
      content: 'Iteration 2',
    }, {
      id: 3,
      content: 'Iteration 3',
    }, {
      id: 4,
      content: 'Iteration 4',
    }];*/
    return (
      <PageBase title="Task Timeline"
        navigation="NFL / Task 1">
        <div>
          <Timeline options={options}
            items={items}
            animation={{
              duration: 3000,
              easingFunction: 'easeInQuint',
            }}
          />
          <SelectField
          floatingLabelText="Time Period"
          value={value}
          onChange={this.handleChange}
        >
          <MenuItem value="Today" primaryText="Today" />
          <MenuItem value="This Week" primaryText="This Week" />
          <MenuItem value="This Month" primaryText="This Month" />
          <MenuItem value="Custom" primaryText="Custom" />
        </SelectField>
          <div style={{ display: value=="Custom"?'flex':"none" }}>
            <DatePicker hintText="From Date"  style={{ width: '50%' }} value={FromDate} onChange={(e,FromDate)=>this.setState({ FromDate })} />
            <DatePicker hintText="To Date" style={{ width: '50%' }} value={ToDate} onChange={(e,ToDate)=>this.setState({ ToDate })} />
          </div>
          <TaskCommentTimeline FromDate={FromDate} ToDate={ToDate}/>
        </div>
      </PageBase>
    );
  }


}

export default TaskTimeline;
