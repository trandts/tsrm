import React from 'react';
import PageBase from '../components/PageBase';
import Timeline from 'react-visjs-timeline';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Data from '../data';
import ImageNavigateBefore from 'material-ui/svg-icons/image/navigate-before';
import ImageNavigateNext from 'material-ui/svg-icons/image/navigate-next';
import IconButton from 'material-ui/IconButton';
import TaskCommentTimeline from '../components/TaskCommentTimeline';

class DisplayActivity extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      TimePeriod:"Today" ,
      User:null,
      defaultDate:new Date()
    };
    this.Next=this.Next.bind(this);
    this.Before=this.Before.bind(this);
  }
  Next()
  {
    const {defaultDate,TimePeriod} = this.state;
    if(TimePeriod=="Today")
    {
      defaultDate.setDate(defaultDate.getDate()+1);
    }
    if(TimePeriod=="This Week")
    {
      defaultDate.setDate(defaultDate.getDate()+7);
    }
    if(TimePeriod=="This Month")
    {
     defaultDate.setMonth(defaultDate.getMonth()+1);
    }
    this.setState({ defaultDate });
  }
  Before()
  {
    const {defaultDate,TimePeriod} = this.state;
    
    if(TimePeriod=="Today")
    {
      defaultDate.setDate(defaultDate.getDate()-1);
    }
    if(TimePeriod=="This Week")
    {
      defaultDate.setDate(defaultDate.getDate()-7);
    }
    if(TimePeriod=="This Month")
    {
     defaultDate.setMonth(defaultDate.getMonth()-1);
    }
    this.setState({ defaultDate });
  }
  render() {
    let value = this.state.TimePeriod;
    let User = this.state.User;
    let defaultDate=this.state.defaultDate;
    let FromDate = null;
    let ToDate = null;
    if (value == "Today") {
      FromDate = new Date(defaultDate.toJSON());
      ToDate = new Date(defaultDate.toJSON());
    }
    if (value == "This Week") {
      let curr = defaultDate;
      let firstday = new Date(curr.setDate(curr.getDate() - curr.getDay()));
      let lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 6));
      FromDate = firstday;
      ToDate = lastday;
    }
    if (value == "This Month") {
      let date =defaultDate;
      let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      FromDate = firstDay;
      ToDate = lastDay;
    }

    FromDate.setHours(0, 0, 0, 0);

    ToDate.setHours(23, 59, 59, 999);
    const options = {
      width: '100%',
      height: '300px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'range',
      template: function (item, element, data) {
        if(data.end)
        {
          return `<div>
        <div style="font-size:7px">${data.project +"-"+ data.content}</div>
        <div style="font-size:7px">${data.start.toLocaleString()}}</div>
        </div>`;
        }
        const _outerStyle = `style=" background-color: rebeccapurple; color: white; "`;
        const _innerStyle = `style=" font-size: 10px;word-wrap: break-word;"`;//max-width:200px;overflow-x:hidden;text-overflow:ellipsis; 
        return `<div ${_outerStyle}>
        <div style="font-size:7px">${data.project + (data.task ? "-" + data.task : "")}</div>
        <p ${_innerStyle}>${data.content}</p>
        <div style="font-size:7px">${data.start.toLocaleString()}-${(data.end || "").toLocaleString()}</div>
        </div>`;
      },
      format: {
        minorLabels: {
          minute: 'h:mma',
          hour: 'ha'
        }
      },
      min:FromDate,
      max:ToDate
    };
    
    const items = [{
      start: new Date(2017, 9, 20),
      end: new Date(2017, 9, 21),  // end is optional
      content: 'Task 1',
      project:"Project 1",
      group:3
    },{
      start: new Date(2017, 9, 22),
      end: new Date(2017, 9, 24),  // end is optional
       content: 'Task 2',
      project:"Project 1",
      group:1
      
    },{
      start: new Date(2017, 9, 22),
      end: new Date(2017, 9, 25),  // end is optional
      content: 'Task 1',
      group:2,
      project:"Project 1",
    },{
      start: new Date(2017, 9, 26),  // end is optional
       end: new Date(2017, 9, 27), 
      content: 'Task 2',
      group:2,
      project:"Project 2",
    },{
      start: new Date(2017, 9, 28),
      end: new Date(2017, 9, 29),  // end is optional
      content: 'Task 3',
      group:1,
      project:"Project 1",
    },{
      start: new Date(2017, 9, 29),
      content: `Noman:If true, increase the tooltip element's size. Useful for increasing tooltip readability on mobile devices.`,
      group:4,
      project:"Project 2",
      task:"Task 2",
      type:'box'
    },{
      start: new Date(2017, 10, 5),  // end is optional
      content: `Faheem:Callback function fired when the element is focused or blurred by the keyboard.`,
      group:1,
      project:"Project 2",
      task:"Task 1",
      type:'box'
    }];
    
   const groups = Data.taskMatrix.Users.map(e=>({id:e.id, content:e.name}) )
   .filter(e=>e.id==User||User==null);
    return (
      <PageBase title="Overview"
        navigation="Application / Overview">
        <div>
          <div style={{ display: 'flex' }} >
            <SelectField style={{ width: '50%' }}
              floatingLabelText="Time Period"
              value={this.state.TimePeriod}
              onChange={(e, i, TimePeriod) => this.setState({ TimePeriod })}
            >
              <MenuItem value="Today" primaryText="Today" />
              <MenuItem value="This Week" primaryText="This Week" />
              <MenuItem value="This Month" primaryText="This Month" />
            </SelectField>
            <SelectField style={{ width: '50%' }}
              floatingLabelText="User"
              value={this.state.User}
              onChange={(e, i, User) => this.setState({ User })}
            >{Data.taskMatrix.Users
              .map(u =>
                <MenuItem key={u.id} value={u.id} primaryText={u.name} />)
              }
            </SelectField>
          </div>
          <div style={{ display: 'flex' }}>
            <div style={{ width: '50%'}}>
              <IconButton tooltip="Back" style={{float: 'left' }} 
              onClick={this.Before} >
                <ImageNavigateBefore style={{ margin: 5 }} />
              </IconButton>
            </div>
            <div style={{ width: '50%' }}>
              <IconButton tooltip="Next" onClick={this.Next} style={{ float: 'right' }}>
                <ImageNavigateNext style={{ margin: 5 }} />
              </IconButton>
            </div>
          </div>
          <Timeline options={options} key={value + User+ FromDate.toLocaleString()+ ToDate.toLocaleString()}
            items={items}
            animation={{
              duration: 3000,
              easingFunction: 'easeInQuint',
            }}
            groups={groups}
          />
        </div>
           <TaskCommentTimeline FromDate={new Date(2015,1,1)} ToDate={new Date(2018,1,1)} User={User}/>
      </PageBase>
    );
  }


}

export default DisplayActivity;
