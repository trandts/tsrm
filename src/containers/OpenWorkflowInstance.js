import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import {grey400} from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import PageBase from '../components/PageBase';
import WorkflowOpen from '../components/WorkflowOpen';
import ResponseSlider from '../components/dashboard/ResponseSlider';
import $ from "jquery";
import Data from '../data';

class OpenWorkflowInstance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      IsOpened:false,
      Name:"",
      Workflows:[],
      WorkflowInstance:null
    };
  }
  componentDidMount()
  {
    $.ajax({
      url:Data.AppDir+'/api/WorkflowInstance',
      type:'GET',
      success:function ($data) {
        this.setState({ Workflows:$data.filter(e=>e.Status=="Active") });
      }.bind(this)
    });
  }
render()
{

  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },

    toggleThumbOff: {
      backgroundColor: '#ffcccc',
    },
    toggleTrackOff: {
      backgroundColor: '#ff9d9d',
    },
    toggleThumbSwitched: {
      backgroundColor: 'red',
    },
    toggleTrackSwitched: {
      backgroundColor: '#ff9d9d',
    },
    toggleLabelStyle: {
      color: grey400,
       fontWeight: 100
    },


    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    },
    sliderContainer:{
      display:'none',
    }
  };
  const {IsOpened,Workflows,WorkflowInstance}=this.state;
  return (
    <PageBase title="Open Workflow" navigation="Application / Form">
      {!IsOpened?<form>
        <SelectField
          floatingLabelText="Workflow"          
          fullWidth={true} 
          value={WorkflowInstance}
          onChange={(e, i, v) => this.setState({WorkflowInstance:v})}
          >{Workflows.map(e=>
          <MenuItem key={e.Id} value={e.Id} primaryText={e.State.Name} />)}
        </SelectField>
        <Divider/>
        <div style={styles.sliderContainer}>
          <ResponseSlider title="Alignment with brand plan"/>
        </div>
        <div style={styles.buttons}>
          {WorkflowInstance?<RaisedButton label="Open Workflow"
                        style={styles.saveButton}
                        onClick={()=>this.setState({IsOpened:true})}
                        primary={true}/>:null}
        </div>
      </form>:<WorkflowOpen WorkflowInstance={WorkflowInstance}/>
      }
    </PageBase>
  );
}
}

export default OpenWorkflowInstance;
