import React from 'react';
import TaskListMatrix from '../components/TaskListMatrix';
import TaskAssignmentMatrix from '../components/TaskAssignmentMatrix';
import TaskIterationsMatrix from '../components/TaskIterationsMatrix';
import TaskListTimeline from '../components/TaskListTimeline';
import PageBase from '../components/PageBase';
import Data from '../data';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import VirtualizedSelect from 'react-virtualized-select';
import { Tabs, Tab } from 'material-ui/Tabs';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Paper from 'material-ui/Paper';
import { white, purple500, deepOrange300, indigo900, blue300, orange200, pink400, amber900, lightGreen900, deepPurple300 } from 'material-ui/styles/colors';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

class TaskMatrix extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project: Data.taskMatrix.projects,
      sort:[],
      SelectedCell:null
    };
    this.getFlattenedData = this.getFlattenedData.bind(this);
    this.taskAssign = this.taskAssign.bind(this); 
  }
  taskAssign(cell){
    this.setState({ SelectedCell:cell.attached });
  }
  getFlattenedData(data) {

    let flattenData = function (d, parent) {
      if (parent) {
        d.parent = parent;
      }
      let nextParentName = (parent ? (parent + "-") : "") + d.name;
      let children = d.child.map(e => flattenData(e, nextParentName)).reduce((a, b) => a.concat(b), []);
      return [d].concat(children);
    };
    return data.map(e => flattenData(e, null)).reduce((a, b) => a.concat(b), []);
  }

  render() {
    const currentProject = (this.state.project || []).length > 0 ? this.state.project : [];

    const RawData = this.getFlattenedData(currentProject.map(e => e.tasks || []).reduce((a, b) => a.concat(b), []));

    let colorPairs = [
      { main: deepOrange300, background: purple500 },
      { main: blue300, background: indigo900 },
      { main: orange200, background: pink400 },
      { main: white, background: amber900 },
      { main: white, background: lightGreen900 },
      { main: white, background: deepPurple300 }
    ];

    let users = [...new Set(RawData.map(e => e.users).reduce((a, b) => a.concat(b), []))]
      .map((e, ei) => ({ Username: e, avatar: e.split(' ').map(e => e[0] || null).filter(e => e).reduce((a, b) => a + b, '').toUpperCase(), color: colorPairs[ei] || {} }));

    let options = Data.taskMatrix.tags.map(e =>
      ({
        id: e.id,
        displayname: e.name + ` (` + Data.taskMatrix.tagCategories.filter(c => c.id == e.category).map(c => c.name) + `)`
        , name: e.name, subtype: Data.taskMatrix.tagCategories.filter(c => c.id == e.category)[0] || null
      }))
      .concat(users.map(u => ({ id: u.Username, displayname: u.Username + " (Resource)", name: u.Username, subtype: "User" })));


      let options2=Data.taskMatrix.tagCategories;

    return (<PageBase title="Task"
      navigation="Application / Task">
      <Paper>
        <div className="row">
          <div className="col-xs-12 col-sm-6" >
            <VirtualizedSelect
              options={Data.taskMatrix.projects}
              style={{ margin: 25, width: '95%' }}
              onChange={(project) => this.setState({ project })}
              multi={true}
              labelKey="name"
              valueKey="id"
              value={this.state.project}
            />
          </div>

          <div className="col-xs-12 col-sm-6" >

            <VirtualizedSelect
              options={options}
              style={{ marginTop: 25, width: '95%'  }}
              onChange={(selectValue) => this.setState({ selectValue })}
              multi={true}
              labelKey="displayname"
              valueKey="id"
              value={this.state.selectValue}
            />
          </div>
        </div>

        <div className="row">

          <div className="col-xs-12 col-sm-6" >

            <VirtualizedSelect
              options={options2}
              style={{ margin: 25 }}
              onChange={(sort) => this.setState({ sort })}
              multi={true}
              labelKey="name"
              valueKey="id"
              value={this.state.sort}
            />
          </div>
        </div>
        <Tabs value={this.state.value}
          onChange={(value) => this.setState({ value: value })}
        > <Tab label="Task List" value="1" >
            <div>
              <div className="row">
                <div className="col-xs-12 col-sm-6" >
                  <h2 style={styles.headline}>Task List</h2>
                </div><div className="col-xs-12 col-sm-6" style={{ textAlign: 'end' }} >
                  <RaisedButton label="Add Task" style={{ margin: 15 }} secondary={true}
                    onClick={() => this.setState({ AddTaskDialog: true })} />

                  <Dialog
                    title="Add Task"
                    actions={[
                      <FlatButton key={0}
                        label="Cancel"
                        primary={true}
                        onClick={() => this.setState({ AddTaskDialog: false })}
                      />,
                      <FlatButton key={1}
                        label="Submit"
                        primary={true}
                        keyboardFocused={true}
                        onClick={() => this.setState({ AddTaskDialog: false })}
                      />,
                    ]}
                    modal={false}
                    open={this.state.AddTaskDialog || false}
                  ><TextField floatingLabelText="Task" />
                    <DatePicker hintText="Start Date" defaultDate={new Date()} />
                    <DatePicker hintText="End Date" defaultDate={new Date()} />

                    <br />
                    <SelectField floatingLabelText="Project">
                      {currentProject
                        .map((e, i) => <MenuItem key={i} value={e.id} primaryText={e.name} />)}
                    </SelectField>
                    <br />
                  </Dialog>
                </div>
              </div>
              {
                currentProject.map((e, i) =>
                  <div key={i}> <h2 style={styles.headline}>{e.name}</h2>
                    <TaskListMatrix filter={this.state.selectValue}
                      currentProject={[e]}
                      sort={this.state.sort}
                      onTaskEtimation={this.taskAssign} />
                    {((this.state.SelectedCell || {}).Project || {}).id == e.id ?
                      <TaskAssignmentMatrix task={this.state.SelectedCell.Task || null} /> : null}
                  </div>
                )
              }
            </div>
          </Tab>
          <Tab label="Task Iterations" value="3">
            <div>
              <div className="row">
                <div className="col-xs-12 col-sm-6" /><div className="col-xs-12 col-sm-6" style={{ textAlign: 'end' }} >
                  <RaisedButton label="Add Iteration" style={{ margin: 15 }} secondary={true}
                    onClick={() => this.setState({ AddIterationDialog: true })} />
                  <Dialog
                    title="Add Iteration"
                    actions={[
                      <FlatButton key={0}
                        label="Cancel"
                        primary={true}
                        onClick={() => this.setState({ AddTaskDiaAddIterationDialoglog: false })}
                      />,
                      <FlatButton key={1}
                        label="Submit"
                        primary={true}
                        keyboardFocused={true}
                        onClick={() => this.setState({ AddIterationDialog: false })}
                      />,
                    ]}
                    modal={false}
                    open={this.state.AddIterationDialog || false}
                  ><TextField floatingLabelText="Iteration" />
                    <DatePicker  hintText="Start Date" defaultDate={new Date()} /> 
                    <DatePicker  hintText="End Date" defaultDate={new Date()} />
                  </Dialog>
                </div>
              </div>
              {
                currentProject.map((e, i) =>
                  <div key={i}> <h2 style={styles.headline}>{e.name}</h2>
                    <TaskIterationsMatrix filter={this.state.selectValue}
                      currentProject={[e]}
                      sort={this.state.sort} />
                  </div>
                )
              }

            </div>
          </Tab>
          <Tab label="Task Timeline" value="4">
            <div>
              <h2 style={styles.headline}>Task Timeline</h2>
              <TaskListTimeline filter={this.state.selectValue}
                currentProject={currentProject} />
            </div>
          </Tab>
        </Tabs>

      </Paper></PageBase>);
  }
}


export default TaskMatrix;





