import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';


class State extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.state;
  }
  render() {

    const { Name,IsStart,IsEnd,TriggerAcitvity 
      ,ParentStateId
     // ,Gate,TaskConfiguration ,Case,IsChildMultiInstance
    } = this.state;
    return (
      <div style={{ border:'black 1px dotted'}}>
      <div className="row">
        <div className="col-xs-12 col-sm-6" >
          <TextField
            floatingLabelText="Name"
            value={Name}
            fullWidth={true}
            onChange={(a, v) => this.setState({ Name: v })}
          />
        </div>
        <div className="col-xs-12 col-sm-6" >
        <Toggle label="Is Start" toggled={IsStart}  onToggle={()=>this.setState({IsStart:!IsStart})}/>
        <Toggle label="Is End" toggled={IsEnd}  onToggle={()=>this.setState({IsEnd:!IsEnd})}/>
        </div>
        <div className="col-xs-12 col-sm-6" >
        <SelectField
          floatingLabelText="Trigger Acitvity"
          value={(TriggerAcitvity||{}).Id}
          onChange={(e,i,v)=>this.setState({ TriggerAcitvity:{Id:v} })}
        >
          <MenuItem key={0} value={null} primaryText="None" />
          {this.props.TriggerAcitvities.map(s=>
            <MenuItem key={s.Id} value={s.Id} primaryText={(s.MethodName||'')+(s.UIElement||'')} />
          )}
        </SelectField>
        </div>
        <div className="col-xs-12 col-sm-6" >
        <SelectField
          floatingLabelText="Parent State"
          value={ParentStateId}
          onChange={(e,i,v)=>this.setState({ ParentStateId:v})}
        >
          <MenuItem key={0} value={0} primaryText="None" />
          {this.props.states.map(s=>
            <MenuItem key={s.Id} value={s.Id} primaryText={s.Name} />
          )}
        </SelectField>
        </div>
        <div className="col-xs-12 col-sm-6" >
        <RaisedButton label="Save" primary={true}/>
        </div>
        </div>
        </div>
    );
  }
}

export default State;
State.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  state:PropTypes.object ,
  states:PropTypes.array ,
  TriggerAcitvities:PropTypes.array  
};
