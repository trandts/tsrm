import React from 'react';
import {Link} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {grey400} from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import ResponseSlider from '../components/dashboard/ResponseSlider';


const CreateQuestion = () => {

  const styles = {
    toggleDiv: {
      maxWidth: 300,
      marginTop: 40,
      marginBottom: 5
    },
    toggleLabel: {
      color: grey400,
      fontWeight: 100
    },

    toggleThumbOff: {
      backgroundColor: '#ffcccc',
    },
    toggleTrackOff: {
      backgroundColor: '#ff9d9d',
    },
    toggleThumbSwitched: {
      backgroundColor: 'red',
    },
    toggleTrackSwitched: {
      backgroundColor: '#ff9d9d',
    },
    toggleLabelStyle: {
      color: grey400,
       fontWeight: 100
    },


    buttons: {
      marginTop: 30,
      float: 'right'
    },
    saveButton: {
      marginLeft: 5
    },
    sliderContainer:{
      display:'none',
      xmaxWidth: 300,
      xmarginTop: 40,
      xmarginBottom: 5
    }
  };

  
  return (
    <PageBase title="Create Question Page"
              navigation="Application / Create Question Page">
      <form>

        <TextField
          hintText="Name"
          floatingLabelText="Question Name"
          fullWidth={true}
        />

        <div style={styles.sliderContainer}>
          <ResponseSlider title="Alignment with brand plan"/>
        </div>
        
        <div style={styles.buttons}>
          <Link to="/">
            <RaisedButton label="Save"
                        style={styles.saveButton}
                        type="submit"
                        primary={true}/>
          </Link>
        </div>
      </form>
    </PageBase>
  );
};

export default CreateQuestion;
