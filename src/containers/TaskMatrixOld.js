import React from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import {  grey500,green500, red500, orange500, blue500 ,pink500,white, purple500,deepOrange300,indigo900,blue300,orange200,pink400,amber900,lightGreen900,deepPurple300 }from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import Data from '../data';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import CommunicationComment from 'material-ui/svg-icons/communication/comment';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import IconButton from 'material-ui/IconButton';
import ActionLabel from 'material-ui/svg-icons/action/label';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import Avatar from 'material-ui/Avatar';
import IconMenu from 'material-ui/IconMenu';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import ImageEdit from 'material-ui/svg-icons/image/edit';
import Toggle  from 'material-ui/Toggle';

class TaskMatrixOld extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.handleChange = this.handleChange.bind(this);
    this.getFlattenedData = this.getFlattenedData.bind(this);
  }

  handleChange(event, index, IterationsFrom) {
    this.setState({ IterationsFrom });
  }
  getFlattenedData(data) {
    let flattenData = function (d, parent) {
      if (parent) {
        d.parent = parent;
      }

      return [d].concat(d.child.map(e => flattenData(e, (parent ? (parent + "-") : "") + d.name)).reduce((a, b) => a.concat(b), []));
    };
    return data.map(e => flattenData(e, null)).reduce((a, b) => a.concat(b), []);
  }
  render() {
    const styles = {
      floatingActionButton: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed',
        backgroundColor: '#bca36a'
      },
      editButton: {
        fill: grey500
      },
      columns: {
        id: {
          width: '10%'
        },
        iteration: {
          width: '10%'
        },
        Manager: {
          width: '10%'
        },
        edit: {
          width: '10%'
        }
      }
    };
    const iconStyles = {
      outer:{
        padding:0,
        height:15,
        width:15
      },
      inner:{
      padding: 0,
      height: 15,
      width: 15
    }};
    const currentProject = (Data.taskMatrix.projects.filter(e => e.id == this.state.project)[0] || {});

    const Iterations = (currentProject.iterations || []).filter(e => e.id <= this.state.IterationsTo && e.id >= this.state.IterationsFrom);
    const DisplayData = this.getFlattenedData(currentProject.tasks || []).filter(a => Iterations.map(i => i.id).indexOf(a.iteration) > -1 || a.iteration == null);
   
   let colorPairs=[
     {main:deepOrange300,background:purple500},
     {main:blue300,background:indigo900},
     {main:orange200,background:pink400},
     {main:white,background:amber900},
     {main:white,background:lightGreen900},
     {main:white,background:deepPurple300}
     ];

    let users = [...new Set(DisplayData.map(e=>e.users).reduce((a,b)=>a.concat(b),[]))]
    .map((e,ei)=>({Username:e, avatar:e.split(' ').map(e=>e[0]||null).filter(e=>e).reduce((a,b)=>a+b,'').toUpperCase() ,color:colorPairs[ei]||{} }));
    return (
      <PageBase title="Task Matrix"
        navigation="Application / Task Matrix">
        <div>
          <div className="row">
            <div className="col-xs-12 col-sm-6" >
              <SelectField floatingLabelText="Project"
                value={this.state.project}
                onChange={(a, b, project) => this.setState({ project })} >
                {Data.taskMatrix.projects.map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)}
              </SelectField>
            </div>
            
            <div className="col-xs-12 col-sm-6" >{
              this.state.project?<RaisedButton label="Add Iteration" primary={true} style={{margin:20}} 
              onClick={()=>this.setState({ IterationAddDialog:true})} />:null
              }
              
            </div>
            </div>
            <div className="row">
            <div className="col-xs-12 col-sm-6" >
              <SelectField floatingLabelText="Iterations From"
                value={this.state.IterationsFrom}
                onChange={(a, b, IterationsFrom) => this.setState({ IterationsFrom })} >
                {(
                  currentProject.iterations || []).map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)
                }
              </SelectField>
            </div>
            <div className="col-xs-12 col-sm-6" >
              <SelectField floatingLabelText="Iterations To"
                value={this.state.IterationsTo}
                onChange={(a, b, IterationsTo) => this.setState({ IterationsTo })} >
                {(currentProject.iterations || []).filter(e => e.id >= this.state.IterationsFrom).map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)
                }
              </SelectField>
            </div>
          </div>
          <div>

            <Table
              selectable={true}
            >
              <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
                enableSelectAll={false}
              >
                <TableRow>
                  <TableHeaderColumn style={styles.columns.Manager}>Parent Task</TableHeaderColumn>
                  <TableHeaderColumn style={styles.columns.id}>Task</TableHeaderColumn>
                  {Iterations.map((e) => <TableHeaderColumn key={e.id} style={styles.columns.iteration}>{e.name}</TableHeaderColumn>)}
                </TableRow>
              </TableHeader>
              <TableBody
                displayRowCheckbox={false}
                showRowHover={true}
              >
                {DisplayData.map((task, taskIndex) =>
                  <TableRow key={taskIndex}>
                    <TableRowColumn style={styles.columns.Manager}>{task.parent}</TableRowColumn>
                    <TableRowColumn style={styles.columns.id}>
                      {task.name}
                      <div style={{ height: iconStyles.outer.height }}>

                        <IconMenu
                          iconButtonElement={<IconButton tooltipPosition="top-right" tooltip="Add Tag" style={iconStyles.outer} iconStyle={iconStyles.inner}
                          >
                            <ActionLabel color={red500} />
                          </IconButton>}
                          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        >{Data.taskMatrix.tagCategories.map((c) =>
                          <MenuItem key={c.id} primaryText={c.name} rightIcon={<ArrowDropRight />}
                            menuItems={Data.taskMatrix.tags.filter(t => t.category == c.id).map((t) =>
                              <MenuItem key={t.id} primaryText={t.name} />)}
                          />
                        )}
                        </IconMenu>
                        <IconButton tooltipPosition="top-right" tooltip="Task Timeline" style={iconStyles.outer} iconStyle={iconStyles.inner}>
                          <ActionTimeline color={orange500} />
                        </IconButton>
                        <IconMenu
                          iconButtonElement={<IconButton tooltipPosition="top-right" tooltip="Comment" style={iconStyles.outer} iconStyle={iconStyles.inner}>
                            <CommunicationComment color={blue500} />
                          </IconButton>}
                          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        >
                          <div> <TextField floatingLabelText="Comment" />
                            <FlatButton label="Add Comment" primary={true}
                              keyboardFocused={true} />
                          </div>
                          <MenuItem key={3} primaryText="Cancel" />
                        </IconMenu>

                        <IconButton tooltipPosition="top-right" tooltip="Add Child Task" style={iconStyles.outer} iconStyle={iconStyles.inner}
                          onClick={() => this.setState({ TaskAddDialog: true })}
                        >
                          <ActionNoteAdd color={green500} />
                        </IconButton>
                        <IconButton tooltipPosition="top-right" tooltip="Edit Task" style={iconStyles.outer} iconStyle={iconStyles.inner}
                          onClick={() => this.setState({ TaskEditDialog: true })}
                        >
                          <ImageEdit color={pink500} />
                        </IconButton>
                        <IconButton tooltipPosition="top-right" tooltip="Delete" style={iconStyles.outer} iconStyle={iconStyles.inner}
                        >
                          <ActionDelete color={purple500} />
                        </IconButton>
                        
                        <Toggle label="Working"
                          style={{ width: '40%', display: 'inline-block' }}
                        />
                      </div>
                      {task.users.length > 0 ?
                        <div style={{ height: iconStyles.outer.height }}>
                          {task.users.map((u, ai) =>
                            users.filter(allu => allu.Username == u).map(user =>

                              <Avatar key={ai}
                                color={user.color.main}
                                backgroundColor={user.color.background}
                                size={15}
                              >{user.avatar}</Avatar>
                            ))}
                        </div> : null}
                    </TableRowColumn>
                    {Iterations.map((e) =>
                      <TableRowColumn key={e.id} style={styles.columns.iteration}>
                        {e.id == task.iteration ?
                          <SelectField floatingLabelText=""
                            value={this.state["State" + task.id] == undefined ? task.state : this.state["State" + task.id]}
                            onChange={function (id, a, b, State) {
                              let obj = {};
                              obj["State" + id] = State;
                              this.setState(obj);
                            }.bind(this, task.id)} >
                            {Data.taskMatrix.states.map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)}
                          </SelectField> : "(N/A)"}
                      </TableRowColumn>)}
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
        <Dialog
         key={9}
            title="Add Iteration"
            actions={<FlatButton
              label="Ok"
              primary={true}
              keyboardFocused={true}
              onTouchTap={()=>this.setState({IterationAddDialog:false})}
            />}
            modal={false}
            open={this.state.IterationAddDialog||false}
          >
            <TextField floatingLabelText="Iteration Name" />
        </Dialog>
        <Dialog
          key={10}
          title="Add Task"
          actions={<FlatButton
            label="Ok"
            primary={true}
            keyboardFocused={true}
            onTouchTap={() => this.setState({ TaskAddDialog: false })}
          />}
          modal={false}
          open={this.state.TaskAddDialog || false}
        >
          <div className="row">
            <TextField floatingLabelText="Task Name" />
            <SelectField floatingLabelText="Iteration"
              value={null}>
              {(currentProject.iterations || [])
                .map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)}
            </SelectField>
          </div>
        </Dialog>

         <Dialog
         key={11}
            title="Edit Task"
            actions={<FlatButton
              label="Ok"
              primary={true}
              keyboardFocused={true}
              onTouchTap={() => this.setState({ TaskEditDialog: false })}
            />}
            modal={false}
            open={this.state.TaskEditDialog || false}
          ><div className="row">
              <div className="col-xs-12 col-sm-6" >
                <TextField floatingLabelText="Task Name" />
              </div>
              <div className="col-xs-12 col-sm-6" >
                <SelectField floatingLabelText="Iteration"
                  value={null}>
                  {(currentProject.iterations || [])
                    .map(e => <MenuItem key={e.id} value={e.id} primaryText={e.name} />)}
                </SelectField></div>
            </div>
          </Dialog>
        </div>
      </PageBase>
    );
  }
}

export default TaskMatrixOld;
