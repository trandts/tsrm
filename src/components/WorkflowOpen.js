import React, { PropTypes } from 'react';
import $ from "jquery";
import Data from '../data';

import themeDefault from '../theme-default';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {EventEmitter} from '../utilities/EventEmitter';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import Chip from 'material-ui/Chip';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

import Badge from 'material-ui/Badge';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';

import TaskAttachedStateMachine from '../components/TaskAttachedStateMachine';
import ContextAttachedStateMachine from '../components/ContextAttachedStateMachine';

import ProjectCreation from '../uielements/ProjectCreation';
import ProjectCreated from '../uielements/ProjectCreated';
import ProjectWaitingForApproval from '../uielements/ProjectWaitingForApproval';
import ProjectCompleted from '../uielements/ProjectCompleted';
import TaskCreate from '../uielements/TaskCreate';
import TaskCreated from '../uielements/TaskCreated';
import TaskProgressed from '../uielements/TaskProgressed';
import TaskClose from '../uielements/TaskClose';
import ProjectApproved from '../uielements/ProjectApproved';
import OrganizationCreation from '../uielements/OrganizationCreation';
import ProjectUpdate from '../uielements/ProjectUpdate';
import NoteAdd from '../uielements/NoteAdd';
import AnotherNoteAdd from '../uielements/AnotherNoteAdd';
import ProjectResourceAssign from '../uielements/ProjectResourceAssign';
import AssignProjectTask from '../uielements/AssignProjectTask';
import Shell from '../uielements/Shell';
import Header from '../uielements/Header';
import SideMenu from '../uielements/SideMenu';
import Content from '../uielements/Content';
import Login from '../uielements/Login';
import LoggedInPanel from '../uielements/LoggedInPanel';
import Tasks from '../uielements/Tasks';
import SessionManagerStarter from '../uielements/SessionManagerStarter';
import ReadyToCreateTaskTagCategory from '../uielements/ReadyToCreateTaskTagCategory';
import TaskTagInitializer from '../uielements/TaskTagInitializer';
import DisplayTaskCategoryTag from '../uielements/DisplayTaskCategoryTag';
import ReadyToCreateContentTagCategory from '../uielements/ReadyToCreateContentTagCategory';
import ContentTagInitializer from '../uielements/ContentTagInitializer';
import DisplayContentCategoryTag from '../uielements/DisplayContentCategoryTag';


class WorkflowOpen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      EventBus:this.props.EventBus||new EventEmitter(),
      WorkflowInstance: this.props.WorkflowInstance,
      Refferer:this.props.Refferer,
      OtherInfo:this.props.OtherInfo||{},
      WI: {},
      WorkflowInitated: false,
      Histories:[],
      Context: this.props.Context||null,
      SupportingSM:null,
      Functions:Functions,
      key:new Date().toString(),
      IsOpen:false,
      CurrentTag:{},
      Comments:[]
    };
    this.EventCallback = this.EventCallback.bind(this);
    this.DataEventCallback = this.DataEventCallback.bind(this);
    this.WorkflowLoad = this.WorkflowLoad.bind(this);
    this.AutoUSMLoad=this.AutoUSMLoad.bind(this);
    this.CloseTask=this.CloseTask.bind(this);
    this.CommentAdded=this.CommentAdded.bind(this);
    this.TagAdded=this.TagAdded.bind(this);
    this.OpenTask=this.OpenTask.bind(this);
    this.OnTagChanged=this.OnTagChanged.bind(this);

    this.state.EventBus.on("StateMachineReload",function(ev,wi){
      if(this.state.WorkflowInstance==wi)
        {
          this.WorkflowLoad();
        }
      }.bind(this));
  }
  componentDidMount() {

    if(this.state.WorkflowInstance>0)
      {
        this.WorkflowLoad();
        this.state.EventBus.on([this.state.WorkflowInstance,"CommentAdded"],this.CommentAdded);
        this.state.EventBus.on([this.state.WorkflowInstance,"TagAdded"],this.TagAdded);
      }else{
        this.state.EventBus.on("PageReload",this.AutoUSMLoad);
        
        this.AutoUSMLoad();
      }
      
  }
  componentWillUnmount()
  {
    if(this.state.WorkflowInstance>0)
      {
        this.state.EventBus.clean(this.state.WorkflowInstance);
        this.state.EventBus.off([this.state.WorkflowInstance,"CommentAdded"],this.CommentAdded);
        this.state.EventBus.off([this.state.WorkflowInstance,"TagAdded"],this.TagAdded);
      }
  }
  CommentAdded(Ev,StateInstanceId,ByUserName,Comments)
  {
    let WI=this.state.WI;
    WI.Comments=(WI.Comments||[]).concat([{Username:ByUserName,Comment:Comments}]);
    let _C=(this.state.Comments||[]).concat([{Creator:ByUserName,Name:Comments,Date:new Date()}]);
  
    this.setState({WI:WI,Comments:_C});
  }
  TagAdded(Ev,StateInstanceId,ByUserName,Tags)
  {
    let WI=this.state.WI;
    WI.Tags=(WI.Tags||[]).concat(Tags.map(t=>({Creator:ByUserName,Name:t,Date:new Date()})));
    let _Tags=(this.state.Tags||[]).concat(Tags.map(t=>({Creator:ByUserName,Name:t,Date:new Date()})));
    this.setState({WI:WI,Tags:_Tags});
  }
  CloseTask()
  {
    this.setState({ IsOpen:false});
  }
  OpenTask(){
    let {WI,EventBus}=this.state;
    WI.Comments=[];
    EventBus.emit("System_CloseTasks");
     this.setState({IsOpen:true,WI:WI});
  }
  EventCallback(StateId,events, Event,Context) {
    let _locale=null;
    if(Array.isArray(Event))
        {
            _locale=Event[0];
            Event=Event[1];
        }
    events.map(ev=>this.state.EventBus.off([_locale,ev]));
    $.ajax({
      url: Data.AppDir+ '/api/WorkflowInstanceCreateTransition',
      type: 'POST',
      data: { WorkflowInstance: this.state.WorkflowInstance, Event: Event, Context: Context, User: $("#User").attr("value") ,StateId:StateId},
      success: function ($data) {
        this.setState({ WorkflowInitated: $data!=0 ,WorkflowInstance:$data});
        this.WorkflowLoad();
      }.bind(this)
    });
  }
  DataEventCallback(InstanceId,StateId,Context, Event,callBackEvent) {
    
    let _locale=null;
    if(Array.isArray(Event))
        {
            _locale=Event[0];
            Event=Event[1];
        }
      
   let callBackArgz=Array.prototype.slice.call(arguments, 4);
   
   //this.state.SupportingSM.Events.filter(e=>e!=Event).map(ev=>this.state.EventBus.off([_locale,ev]));
    $.ajax({
      url: Data.AppDir+'/api/WorkflowInstanceCreateTransition',
      type: 'POST',
      data: { WorkflowInstance: InstanceId, Event: Event, Context: Context, User: $("#User").attr("value") ,StateId:StateId},
      success: function ($,Data,_callBackEvent,_callBackArgz,__locale,$_data) {
        if($_data!=0)
          {
            $.ajax({
              url: Data.AppDir+'/api/WorkflowInstanceUI/' +$_data + '?UserId='+$("#User").attr("value"),
              type: 'GET',
              success: function (__callBackEvent,__callBackArgz,___locale,$data) {
              
                if ($data == null) {  
                  let wi=this.state.WI;
                  wi.SupportingSM=null;
                  let ssm=this.state.SupportingSM||{};
                  ssm.StateId=0;
                  ssm.StateInstanceId=0;
                  ssm.TriggerUIElement=null;
                  ssm.TriggerMethod=null;
                  ssm.TriggerMethod=null;
                  ssm.Events=[];
                  this.setState({ SupportingSM:ssm ,WI:wi});
                } else {
                  let  _function=(Functions[$data.States.TriggerMethod]||null);  
                  if (_function) {
                    
                    if ($data.States.IsAutomated) {
                      _function.bind(this,
                        this.DataEventCallback.bind(this, $data.States.StateInstanceId, $data.States.StateId, this.state.SupportingSM.Context)
                      ).apply(this, [___locale].concat(__callBackArgz));
                    
                    } else {
                      if (_function ) {
                        _function = _function.bind(this, this.DataEventCallback.bind(this, $data.States.StateInstanceId, $data.States.StateId, this.state.SupportingSM.Context));
                        
                        ($data.States || {}).Events.map(ev => this.state.EventBus.Register([___locale,ev], _function));
                      }
                    }
                  }
                  
                  let wi = this.state.WI;
                  wi.SupportingSM = null;
                  let ssm = this.state.SupportingSM||{};
                  ssm.Context=ssm.Context;
                  ssm.StateId = $data.States.StateId;
                  ssm.StateInstanceId = $data.States.StateInstanceId;
                  ssm.TriggerUIElement = $data.States.TriggerUIElement;
                  ssm.TriggerMethod = $data.States.TriggerMethod;
                  ssm.Events = $data.States.Events;
                  ssm.IsAutomated = $data.States.IsAutomated;
                  this.state.SupportingSM=ssm;
                  wi.Context=ssm.Context;
                  this.state.WI=wi;
                  //this.setState({ SupportingSM: ssm, WI: wi });
                }
              }.bind(this,_callBackEvent,_callBackArgz,__locale)
            });
          }else{
            let wi=this.state.WI;
            wi.SupportingSM=null;
            let ssm=this.state.SupportingSM||{};
            ssm.StateId=0;
            ssm.StateInstanceId=0;
            ssm.TriggerUIElement=null;
            ssm.TriggerMethod=null;
            this.setState({ SupportingSM:ssm });
          }
      }.bind(this,$,Data,callBackEvent,callBackArgz,_locale)
    });
  }
  AutoUSMLoad(data) {
    let $url=Data.AppDir+'/GetUserSM?UserId='+$("#User").attr("value");
    $.ajax({
      url: $url,
      type:data?'POST': 'GET',
      data:data?data:{},
      success: function ($,$data) {
         const {UserId,UserName} =$data.Data;
         const { EventBus }=this.state;
         $("#User").attr("value", $("#User").attr("value")||UserId);
         $("#User").attr("data-Username", $("#User").attr("data-Username")||UserName);
        EventBus.emit("System_SignalRStart",UserId,$data.States);
        this.setState({USMInfo:$data.Data, WorkflowInstance:$data.Data.BSM, key:new Date().toString()});
        
        this.WorkflowLoad();
      }.bind(this,$)
    });
  }

  WorkflowLoad() {
    
    if(this.state.WorkflowInstance>0)
    {
      let $url=Data.AppDir + '/api/WorkflowInstanceUI/' + this.state.WorkflowInstance + '?UserId=' + $("#User").attr("value");
      this.$url=$url;
      $.ajax({
        url: $url,
        type: 'GET',
        success: function ($data) {
          if ($data == null) {
            //alert("Workflow Closed");
          } else {
            this.setState({
              WorkflowInitated: false,
              Context: eval(this.state.Context || $data.Instance.Context || null),
              WI: $data.States,
              Histories: $data.Histories,
              Comments: $data.Comments,
              Tags: $data.Tags,
              SupportingSM: $data.OtherStateTypeInstance[0] || null
            });
            let _function = (this.EventCallback || (() => { })).bind(this, $data.States.StateId, $data.States.Events);
           
            if (_function && $data.States.TriggerUIElement) {
              [...new Set($data.States.Events)].map(ev => this.state.EventBus.Register([$data.States.StateInstanceId,ev], _function));
            }
            let SupportingSM = ($data.OtherStateTypeInstance[0] || {});
            _function = (Functions[SupportingSM.TriggerMethod] || null);
            
            if (SupportingSM.IsAutomated) {
            
             let temp= _function.bind(this, this.DataEventCallback.bind(this, SupportingSM.StateInstanceId, SupportingSM.StateId, SupportingSM.Context));
              temp.apply(this, []);//__callBackArgz
            } else {
              if (_function &&SupportingSM) {
                _function = _function.bind(this, this.DataEventCallback.bind(this, SupportingSM.StateInstanceId, SupportingSM.StateId, SupportingSM.Context));
                let $_events=[...new Set(($data.OtherStateTypeInstance[0] || {}).Events)];
                $_events.map(ev => this.state.EventBus.Register([$data.States.StateInstanceId, ev], _function));
              }
            }
            const {EventBus}=this.state;
            if($data.States.IsTask)
              {
              EventBus.on("System_CloseTasks",this.CloseTask);
              }
            EventBus.emit([$data.States.StateInstanceId,"EventsLoaded"],{ Main:$data.States.Events, Secondary:SupportingSM.Events||[] });
          }
        }.bind(this)
      });
    }else{
      //console.log("could not load state machine");
    }
  }
  OnTagChanged(e,val){
    let {CurrentTag}=this.state;
    $.ajax({
      url:Data.AppDir+'/api/DisplayContentCategoryTag/'+CurrentTag.Context,
      type:'POST',
      data:{ SelectedTags:[val.toString()], User:$("#User").attr("value")},
      success:function($,Data,CurrentTag){
        $.ajax({
          url: Data.AppDir+ '/api/WorkflowInstanceCreateTransition',
          type: 'POST',
          data: { WorkflowInstance: CurrentTag.Instance, Event: 'OnTagUpdated', Context: CurrentTag.Context, User: $("#User").attr("value") ,StateId:0},
          success: function () {
            this.setState({ TagOpen: false });
            this.state.EventBus.emit("PageReload");
          }.bind(this)
        });
      }.bind(this,$,Data,CurrentTag)
    });
    
  }
  render() {
    const { Tags,Comments,WI,USMInfo, WorkflowInitated, Context, WorkflowInstance,Histories,EventBus,key,IsOpen} = this.state;
    let e={ StateId: WI.StateId,TriggerUIElement:WI.TriggerUIElement, Component: components[WI.TriggerUIElement] || null };
    
    if((WI||{}).IsTask && !IsOpen)
    {
      let date= new Date((WI||{}).LastTransitionOn);
      let DateTime=date.toLocaleDateString()==new Date().toLocaleDateString()?"Today "+date.toLocaleTimeString():date.toLocaleDateString();
      let CommentAlert=((WI||{}).Comments||[]).length==0?null:
                          (<Badge badgeContent={((WI||{}).Comments||[]).length}
                          primary={true} ><NotificationsIcon /></Badge>);
      let TagChips=(<div>{(WI||{}).ContextTagInfo.map((e,n)=><Chip key={n}
                          onClick={(ev)=>this.setState({TagOpen:true,anchorEl: ev.currentTarget,CurrentTag:e})}
                          style={{ margin: 4}}
                        >
                          {e.Options.filter(o=>o.Id==e.current).map(o=>o.Name).toString()+' ('+e.Category+')'}
                        </Chip>)}<Popover
          open={this.state.TagOpen||false}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={()=>this.setState({TagOpen:false})}
        >
          <Menu onChange={this.OnTagChanged}>{
            (this.state.CurrentTag.Options||[])
            .map(e=><MenuItem key={e.Id} primaryText={e.Name} value={e.Id} />)}
          </Menu>
          </Popover></div>);
      if ((WI||{}).TaskProject)
        {
          //(WI||{}).LastUser
          return (<div className="row" style={{ border:'1px black groove',fontSize:'12px'}}>
          <div className="col-md-1"  onClick={this.OpenTask}>{(WI||{}).TaskProject} </div>
          <div className="col-md-1">{(WI||{}).TaskContext}</div>
          <div className="col-md-2">{(WI||{}).StateName} </div>
          <div className="col-md-2">Assigned: {(WI||{}).AssignedUsers.toString()} </div>
          <div className="col-md-2">Visibility: {(WI||{}).VisibilityUsers.toString()} </div>
          <div className="col-md-2">{TagChips} </div>
          <div className="col-md-2">{DateTime} {CommentAlert}</div>
          </div>);
        }else{
          return (<div className="row" style={{ border:'1px black groove',fontSize:'12px'}}>
          <div className="col-md-4"  onClick={this.OpenTask}>{(WI||{}).StateName} </div>
          <div className="col-md-2">{(WI||{}).TaskContextType +':'+(WI||{}).TaskContext}</div>
          <div className="col-md-2">Visibility: {(WI||{}).VisibilityUsers.toString()} </div>
          <div className="col-md-2">{TagChips} </div>
          <div className="col-md-2">{DateTime} 
                 {CommentAlert}
           </div>
          </div>);
        }
    }
    let DisplayUIElement=e.Component;
    if(this.PreviousUI && this.PreviousUI!=e.TriggerUIElement)
      { 
        DisplayUIElement=this.PreviousUI;
        setTimeout(()=>this.setState({test:1}),200);
      } 
      
      const nestedUIFunc=(arr)=><div>{(arr||[]).map((csms,_i)=><WorkflowOpen key={_i} WorkflowInstance={csms.BSM} EventBus={EventBus} otherInfo={csms} ><div>{nestedUIFunc(csms.ChildSMs)}</div></WorkflowOpen>)}</div>;
      
      const nestedUIs=nestedUIFunc((USMInfo||{}).ChildSMs||[]);
      const childLength=((USMInfo||{}).ChildSMs||[]).length;
      let UIElementComponents = null;
      let Searcher=null;
      if(USMInfo && $("#User").attr("value")>0)
      {
        Searcher=(<div className="row" ><div className="col-md-offset-4 col-md-4" >
              <div><TextField  floatingLabelText="Search"
            value={this.state.value}
            onChange={(e)=>this.setState({ value: e.target.value })}
          /><RaisedButton label="Search" primary={true} style={{margin:5}}
            onClick={()=>this.AutoUSMLoad({Search:this.state.value})} /></div>
        </div></div>);
      }
      if(!(WI||{}).StateInstanceId)
        {
          return (<div />);
        }else if(!DisplayUIElement && !e.TriggerUIElement)
        {
          if( childLength>0)
            {
              UIElementComponents= (<div>{nestedUIs}
              {this.props.children}</div>);
            }else{
              return this.props.children;
            }
        }else{
          if (DisplayUIElement) {
            UIElementComponents = (<div ><DisplayUIElement key={1} WorkflowInstance={WorkflowInstance}
              Context={Context} EventBus={EventBus}><div>
              <div> {nestedUIs}</div>
              <div>{this.props.children}</div>
               {Searcher}
              </div>
            </DisplayUIElement>{//"Tags"
              ["Notes"].map((e,i)=>
              <TaskAttachedStateMachine key={i+2} stateInstance={WorkflowInstance} EventBus={EventBus} SpecialNode={e} />)
            }{["ContentTags"].map((e,i)=>
              <ContextAttachedStateMachine key={i+2} stateInstance={WorkflowInstance} EventBus={EventBus} SpecialNode={e} />)
              }
            </div>);
          }else if(e.TriggerUIElement){
            UIElementComponents = (<div>Component Not Found:{e.TriggerUIElement}</div>);
          }else{
            UIElementComponents=(<div>Rendering Please Wait</div>);
          }
        }
      this.PreviousUI=e.TriggerUIElement;
      let listEvents=(Comments||[]).map(e=>({Creator:"(Comment)"+e.Creator,Name:e.Name,Date:e.Date})).concat(Histories.filter(e=>e)).concat((Tags||[]).map(e=>({Creator:"(Tag)"+e.Creator,Name:e.Name,Date:e.Date})));
      let _KSrt=function keysrt(key) {
        return function(a,b){
          if(a[key]==null || b[key]==null) return 0;
         if (new Date(a[key]) < new Date(b[key])) return 1;
         if (new Date(a[key]) > new Date(b[key])) return -1;
         return 0;
        };
      };
      
      listEvents=listEvents.sort(_KSrt('Date'));
      const EventList=(WI||{}).IsTask?
                    (<div><h2>Events</h2>
                    {listEvents.map((e,i)=><div key={i} >{e.Creator}:{e.Name}:{(e.Date?new Date(e.Date):'').toLocaleString()}</div>)}
                    </div>) :null;
      
     const heading=(WI||{}).IsTask?<div onClick={this.CloseTask} >{(WI||{}).StateName+' : '+(WI||{}).TaskContext+' ('+new Date((WI||{}).LastTransitionOn).toLocaleString()+')'}</div>:null;
     const styles=(WI||{}).IsTask?{ border:'1px black groove'}:{};
     
    return (<MuiThemeProvider muiTheme={themeDefault}  key={key}>
              <div style={styles}>
                {heading}
                {WorkflowInitated ? null : <div>
              {UIElementComponents}</div>}
              {EventList}
              </div>
        </MuiThemeProvider> );
  }
}


let components = {
  "Shell":Shell,
  "Header":Header,
  "Content":Content,
  "SideMenu":SideMenu,
  "Login":Login,
  "SessionManagerStarter":SessionManagerStarter,
  "Tasks":Tasks,
  "LoggedInPanel":LoggedInPanel,
  "AssignProjectTask":AssignProjectTask,
  "ProjectResourceAssign":ProjectResourceAssign,
  "ProjectCreation": ProjectCreation,
  "ProjectCreated": ProjectCreated,
  "ProjectWaitingForApproval": ProjectWaitingForApproval,
  "ProjectApproved": ProjectApproved,
  "ProjectCompleted": ProjectCompleted,
  "TaskCreated": TaskCreated,
  "TaskProgressed": TaskProgressed,
  "TaskClose": TaskClose,
  "TaskCreate": TaskCreate,
  "OrganizationCreation":OrganizationCreation,
  "ProjectUpdate":ProjectUpdate,
  "NoteAdd":NoteAdd,
  "AnotherNoteAdd":AnotherNoteAdd,
  "TaskTagInitializer":TaskTagInitializer,
  "ReadyToCreateTaskTagCategory":ReadyToCreateTaskTagCategory,
  "DisplayTaskCategoryTag":DisplayTaskCategoryTag,
  "ContentTagInitializer":ContentTagInitializer,
  "ReadyToCreateContentTagCategory":ReadyToCreateContentTagCategory,
  "DisplayContentCategoryTag":DisplayContentCategoryTag,
};
let Functions = {
  "Ajax": function (Transition,UiLocale, data, url, type) {
    $.ajax({
      data: data,
      url: Data.AppDir+url,
      type: type,
      success: function (_Transition,_UiLocale,$data) {
      this.state.EventBus.emit([_UiLocale,"Success"],$data);
      _Transition([_UiLocale,"Success"]);
      }.bind(this,Transition,UiLocale)
      , error: function (_Transition,_UiLocale,err) {
        this.state.EventBus.emit([_UiLocale,"Error"],err);
        _Transition([_UiLocale,"Success"]);
      }.bind(this,Transition,UiLocale)
    });
  },
  "Initialization":function(Transition)
  { 
    let callBackArgz=Array.prototype.slice.call(arguments, 1);
    Transition.apply(this, callBackArgz);
    //Transition(Event,function(data,url,type,NextEvent){this.Base.DSM.TriggerEvent(data,url,type,NextEvent,successCallBack);}.bind(this,data,url,type,NextEvent,successCallBack));
     
  },
  "SimpleDSMEventHandler":function(Transition,Event){
    Transition(Event);
  }
};

WorkflowOpen.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  WorkflowInstance: PropTypes.number,
  Context:PropTypes.string,
  Refferer:PropTypes.string,
  EventBus:PropTypes.object,
  OtherInfo:PropTypes.object,
  children:PropTypes.element
};

export default WorkflowOpen;
