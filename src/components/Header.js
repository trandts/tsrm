import React, {PropTypes} from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Menu from 'material-ui/svg-icons/navigation/menu';
import ViewModule from 'material-ui/svg-icons/action/view-module';
import {white} from 'material-ui/styles/colors';

class Header extends React.Component {

  render() {
    const {styles, handleChangeRequestNavDrawer} = this.props;

    const style = {
      appBar: {
        position: 'fixed',        
        top: 0,
        overflow: 'hidden',
        maxHeight: 57,
        zIndex:1200
      },
      menuButton: {
        marginLeft: 10
      },
      iconsRightContainer: {
        marginLeft: 20
      },
      mainContainer:{
        xwidth:'80%',
        xdisplay:'flex'
      },
      appBarContainer:{
        xfloat:'left'
      },
      logoContainer:{
        xdisplay:'flex',
        xfloat:'left',
        display:'none'
      },
      logoImg:{
        padding:0,
        width: 220,
        maxHeight:57 ,
      }
    };

    return (
        <div style={style.mainContainer}>
            <div style={style.logoContainer}>           
                <img src={require('../images/tssanofi-mmex.png')} style={style.logoImg} alt="" />       
             </div>
            <div style={style.appBarContainer}>
            <AppBar
            zDepth={2}
              style={{...styles, ...style.appBar}}
             title="TC Task Management"
              iconElementLeft={
                  <div>
                  <IconButton style={style.menuButton} onClick={handleChangeRequestNavDrawer}>
                    <Menu color={white} />
                  </IconButton>
                  
                  </div>
              }
              iconElementRight={
                <div style={style.iconsRightContainer}>
                  <IconMenu color={white}
                            iconButtonElement={
                              <IconButton><ViewModule color={white}/></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                  >
                    <MenuItem key={1} primaryText="Feature 1"/>
                    <MenuItem key={2} primaryText="Feature 2"/>
                    <MenuItem key={3} primaryText="Feature 3"/>
                  </IconMenu>
                  <IconMenu color={white}
                            iconButtonElement={
                              <IconButton><MoreVertIcon color={white}/></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                  >
                    <MenuItem primaryText="Sign out"/>
                  </IconMenu>
                </div>
              }
            />
            </div>
          </div>
      );
  }
}

Header.propTypes = {
  styles: PropTypes.object,
  handleChangeRequestNavDrawer: PropTypes.func
};

export default Header;
