import React ,{  PropTypes}from 'react';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import Grid from 'react-virtualized/dist/commonjs/Grid';
import ScrollSync from 'react-virtualized/dist/commonjs/ScrollSync';
import cn from "classnames";
import Data from '../data';
import VirtualizedSelect from 'react-virtualized-select';
import {Link} from 'react-router';

import Toggle from 'material-ui/Toggle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DatePicker from 'material-ui/DatePicker';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ImageEdit from 'material-ui/svg-icons/image/edit';
import ActionUpdate from 'material-ui/svg-icons/action/update';
import ActionDateRange from 'material-ui/svg-icons/action/date-range';
import {  cyan900,tealA400 ,pink500,purple500,deepOrange900 }from 'material-ui/styles/colors';
 const iconStyles = {
      outer:{
        padding:0,
        height:15,
        width:15
      },
      inner:{
      padding: 0,
      height: 15,
      width: 15
    }};

/*
 const LEFT_COLOR_FROM = hexToRgb("#471061");
 const LEFT_COLOR_TO = hexToRgb("#BC3959");
 const TOP_COLOR_FROM = hexToRgb("#000000");
 const TOP_COLOR_TO = hexToRgb("#333333");

const LEFT_COLOR_FROM = hexToRgb("#4CAF50");
const LEFT_COLOR_TO = hexToRgb("#CDDC39");
const TOP_COLOR_FROM = hexToRgb("#000000");
const TOP_COLOR_TO = hexToRgb("#81C784");
*/


const LEFT_COLOR_FROM = hexToRgb("#ffffff");
const LEFT_COLOR_TO = hexToRgb("#ffffff");
const TOP_COLOR_FROM = hexToRgb("#81C784");
const TOP_COLOR_TO = hexToRgb("#81C784");

function getColumns() {
  return ["Task","Estimated Hours","Actual Hours","Completion","Iteration"
  ].concat(Data.taskMatrix.tagCategories.map(e => e.name)).concat(["users", "Approver"]);
}
function getTasks(Projects,columns,sort) {
  if (!Projects) return [];
  let arr=[];
  for(let projectIndex=0;projectIndex<(Projects||[]).length;projectIndex++)
  {
      let Proj=Projects[projectIndex];
      for(let taskIndex=0;taskIndex<Proj.tasks.length;taskIndex++)
      {
        let task=Proj.tasks[taskIndex];
        let obj = {
          attached: {
            Project: Proj,
            Task: task,
            current:null
          },
          value:null
        };
        let rowValues=[];
        for(let columnIndex=0;columnIndex<columns.length;columnIndex++)
        {
          let columnName=columns[columnIndex];
          let cell = Object.assign({}, obj);
          if(columnName=="Iteration")
          {
            cell.value=cell.attached.Task.iteration;
          } else if(columnName=="Completion")
          {
            cell.value=(cell.attached.Task.completion||0)+'%';
          } else if(columnName=="Actual Hours")
          {
            cell.value=(cell.attached.Task.actualHours||0);
          } else if(columnName=="Estimated Hours")
          {
            cell.value=(cell.attached.Task.estimatedhours||0);
          } else
          if(columnName=="Task")
          {
            cell.value=cell.attached.Task.name;
          } else if(columnName=="Project")
          {
            cell.value=cell.attached.Project.name;
          }else if(columnName=="users"||columnName=="Approver")
          {
            cell.value="User";
          }else{
           let catId=Data.taskMatrix.tagCategories.filter(e => e.name==columnName).map(e=>e.id)[0]||0;
           let tag=Data.taskMatrix.tags.filter(e=>(cell.attached.Task.tags||[]).indexOf(e.id)>-1 && e.category==catId)[0];
           cell.value=tag?tag.name:null;
          }
          cell.column=columnName;
          rowValues[rowValues.length]=cell;
        }
        arr[arr.length]=rowValues;
      }
  }
  let _sortColumns = sort.map(e => columns.indexOf(e.name));
  let sortFunction = function (sortColumns, sortColumnIndex, a, b) {
    if(sortColumnIndex==sortColumns.length) return 0;
   let v1=a[sortColumns[sortColumnIndex]].value||null;
    let v2=b[sortColumns[sortColumnIndex]].value||null;
    if (v1 < v2) return -1;
    if (v1 > v2) return 1;
    if(v1==v2 && v1==null) return 0;
    return sortFunction.bind(null,sortColumns,sortColumnIndex+1)(a,b);
  };
  
  arr = _sortColumns.length>0?arr.sort(sortFunction.bind(null, _sortColumns, 0)):arr;

  const sortedArr = arr;
  return sortedArr;
}

class TaskListMatrix extends React.Component {
  constructor(props) {
    super(props);
    let columns=getColumns(this.props.currentProject||[]);
    this.state = {
      columnWidth: 200,
      height: 300,
      overscanColumnCount: 0,
      overscanRowCount: 5,
      rowHeight: 50 ,
      columnsFreezed:1,
      filter:this.props.filter||[],
      currentProject: this.props.currentProject||[],
      Columns: columns,
      Tasks: getTasks(this.props.currentProject||[],columns,this.props.sort),
      Sort:this.props.sort,
      onTaskEtimation:this.props.onTaskEtimation||(()=>{})
    };
    this._renderBodyCell = this._renderBodyCell.bind(this);
    this._renderHeaderCell = this._renderHeaderCell.bind(this);
    this._renderLeftSideCell = this._renderLeftSideCell.bind(this);
    this._renderLeftHeaderCell = this._renderLeftHeaderCell.bind(this);
    this.TaskDateRange = this.TaskDateRange.bind(this);
    
  }
  componentWillReceiveProps(newProps) {
    let columns = getColumns(newProps.currentProject);
    this.setState({
      filter: newProps.filter,
      currentProject: newProps.currentProject
      , Columns: columns,
      Tasks: getTasks(newProps.currentProject, columns, newProps.sort)
      , Sort: newProps.sort,

      TaskTimePopup:null
    });
  }
  TaskDateRange(cell)
  {
     this.setState({ TaskTimePopup:cell.attached.Task});
  }
  _renderBodyCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < this.state.columnsFreezed) {
      return;
    }

    return this._renderLeftSideCell({ columnIndex, key, rowIndex, style });
  }

  _renderHeaderCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < this.state.columnsFreezed) {
      return;
    }

    return this._renderLeftHeaderCell({ columnIndex, key, rowIndex, style });
  }

  _renderLeftSideCell({ columnIndex, key, rowIndex, style }) {
    const rowClass =
      rowIndex % 2 === 0
        ? columnIndex % 2 === 0 ? "evenRow" : "oddRow"
        : columnIndex % 2 !== 0 ? "evenRow" : "oddRow";
    const classNames = cn(rowClass, "cell");
    //{(task[this.state.Columns[columnIndex]]||[]).toLocaleString()}
    //catTags.filter(e => (task.tags || []).indexOf(e.id) > -1).map(e => e.id)[0] || 0
    //<TextField name="taskname" defaultValue={task.name} />
    let cell =this.state.Tasks[rowIndex][columnIndex];
    if(cell.column == "Project"||cell.column == "Iteration"||cell.column == "Estimated Hours"||cell.column == "Actual Hours"||cell.column == "Completion"){
      return  (<div className={classNames} key={key} style={style}>{cell.value}</div>);
    }else  if (cell.column == "Task") {
      let TaskName = cell.attached.Task.id == this.state.currentEdit ? (<div key={1} style={{ width: '100%' }} >
        <input name="taskname" value={cell.value} />
      </div>) : (<div key={1} >{cell.value}</div>);
      return (<div className={classNames} key={key} style={style}>
        {TaskName}
        <div key={2} style={{ width: '100%', height: iconStyles.outer.height }}>
          <Link to="/TaskTimeLine" >
           <IconButton tooltipPosition="top-right" tooltip="Task Timeline" style={iconStyles.outer} iconStyle={iconStyles.inner}>
            <ActionTimeline color={deepOrange900} />
          </IconButton>
          </Link>
          <IconButton tooltipPosition="top-right" tooltip="Estimation" style={iconStyles.outer} iconStyle={iconStyles.inner}
          onClick={()=>this.state.onTaskEtimation(cell)}
          ><ActionUpdate color={cyan900} />
          </IconButton>
          <IconButton tooltipPosition="top-right" tooltip="Start/End" style={iconStyles.outer} iconStyle={iconStyles.inner}
          onClick={()=>this.TaskDateRange(cell)}
          ><ActionDateRange color={tealA400} />
          </IconButton>
          <IconButton tooltipPosition="top-right" tooltip="Edit Task" style={iconStyles.outer} iconStyle={iconStyles.inner}
            onClick={() => this.setState({ currentEdit: this.state.currentEdit == cell.attached.Task.id ? null : cell.attached.Task.id })}
          >
            <ImageEdit color={pink500} />
          </IconButton>
          <IconButton tooltipPosition="top-right" tooltip="Delete" style={iconStyles.outer} iconStyle={iconStyles.inner}
          >
            <ActionDelete color={purple500} />
          </IconButton>
          <Toggle label="Working"
            style={{ width: '47%', display: 'inline-block' }}
          />
        </div>
      </div>);
    } else if (cell.column == "users" ||cell.column == "Approver") {
      return (<div className={classNames} key={key} style={style}>
        <VirtualizedSelect
          options={Data.taskMatrix.Users}
          onChange={(selectValue) => this.setState({ selectValue })}
          multi={true}
          labelKey="name"
          valueKey="id"
          style={{color:'black'}}
          value={cell.value}
        />
      </div>);
    } 
      let cat = Data.taskMatrix.tagCategories.filter(e => e.name ==cell.column ).map(e => e.id);
      let catTags = Data.taskMatrix.tags
        .filter(e => cat.indexOf(e.category) > -1);
      return (<div className={classNames} key={key} style={style}>
        <VirtualizedSelect
          options={catTags}
          onChange={(selectValue) => this.setState({ selectValue })}
          labelKey="name"
          valueKey="id"
          multi={true}
          value={catTags.filter(e => (cell.attached.Task.tags || []).indexOf(e.id) > -1)}
        />
      </div>);
  
  }
  _renderLeftHeaderCell({ columnIndex, key, style }) {
    let ColumnFreezeUnFreeze=function(_columnIndex){
    let  move = function (arr,old_index, new_index) {
        if (new_index >= arr.length) {
          let k = new_index - arr.length;
          while ((k--) + 1) {
          arr.push(undefined);
          }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing purposes
      };
      if(this.state.Columns.indexOf(this.state.Columns[_columnIndex])<this.state.columnsFreezed)
      {
        this.setState({ columnsFreezed:this.state.columnsFreezed-1,Tasks:this.state.Tasks.map(e=>move(e,_columnIndex,this.state.columnsFreezed-1)), Columns:move(this.state.Columns,_columnIndex,this.state.columnsFreezed-1)});
      }else{
        this.setState({ columnsFreezed:this.state.columnsFreezed+1,Tasks:this.state.Tasks.map(e=>move(e,_columnIndex,this.state.columnsFreezed)), Columns:move(this.state.Columns,_columnIndex,this.state.columnsFreezed) });
      }
    }.bind(this,columnIndex);
      
    return (
      <div className="headerCell" key={key} style={style}>
       <div> {`${this.state.Columns[columnIndex]}`}</div>
       <div>{columnIndex<1?null:<Toggle toggled={columnIndex<this.state.columnsFreezed} onToggle={ColumnFreezeUnFreeze} />}</div>
      </div>
    );
  }
  render() {
    const {
      columnWidth,
      height,
      overscanColumnCount,
      overscanRowCount,
      rowHeight,
      columnsFreezed
    } = this.state;
    const rowCount= (this.state.Tasks||[]).length;
    const columnCount=this.state.Columns.length;
    
    let columnsAsKey=this.state.Columns.toString()+this.state.Sort.toString()+columnsFreezed;
    return (<div><ScrollSync>
      {({
            clientHeight,
        clientWidth,
        onScroll,
        scrollHeight,
        scrollLeft,
        scrollTop,
        scrollWidth
          }) => {
        const x = scrollLeft / (scrollWidth - clientWidth);
        const y = scrollTop / (scrollHeight - clientHeight);

        const leftBackgroundColor = mixColors(
          LEFT_COLOR_FROM,
          LEFT_COLOR_TO,
          y
        );
        const leftColor = "#000000";
        const topBackgroundColor = mixColors(
          TOP_COLOR_FROM,
          TOP_COLOR_TO,
          x
        );
        const topColor = "#000000";
        const middleBackgroundColor = mixColors(
          leftBackgroundColor,
          topBackgroundColor,
          0.5
        );
        const middleColor = "#000000";
        return (
          <div className="GridRow">
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                color: leftColor,
                backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`
              }}
            >
              <Grid key={columnsAsKey+0}
               cellRenderer={this._renderLeftHeaderCell}
                className="HeaderGrid"
                width={columnWidth*columnsFreezed}
                height={rowHeight}
                rowHeight={rowHeight}
                columnWidth={columnWidth}
                rowCount={1}
                columnCount={columnsFreezed}
              />
            </div>
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: rowHeight,
                color: leftColor,
                backgroundColor: `rgb(${leftBackgroundColor.r},${leftBackgroundColor.g},${leftBackgroundColor.b})`
              }}
            >
              <Grid  key={columnsAsKey+1}
                overscanColumnCount={overscanColumnCount}
                overscanRowCount={overscanRowCount}
                cellRenderer={this._renderLeftSideCell}
                columnWidth={columnWidth}
                columnCount={columnsFreezed}
                className="LeftSideGrid"
                height={height}
                rowHeight={rowHeight}
                rowCount={rowCount}
                scrollTop={scrollTop}
                width={columnWidth*columnsFreezed}

              />
            </div>
            <div className="GridColumn">
              <AutoSizer disableHeight>
                {({ width }) =>
                  <div>
                    <div
                      style={{
                        backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`,
                        color: topColor,
                        height: rowHeight,
                        width: width
                      }}
                    >
                      <Grid  key={columnsAsKey+2}
                        className="HeaderGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={rowHeight}
                        overscanColumnCount={overscanColumnCount}
                        cellRenderer={this._renderHeaderCell}
                        rowHeight={rowHeight}
                        rowCount={1}
                        scrollLeft={scrollLeft}
                        width={width}
                      />
                    </div>
                    <div
                      style={{
                        backgroundColor: `rgb(${middleBackgroundColor.r},${middleBackgroundColor.g},${middleBackgroundColor.b})`,
                        color: middleColor,
                        height,
                        width
                      }}
                    >
                      <Grid  key={columnsAsKey+3}
                        className="BodyGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={height}
                        onScroll={onScroll}
                        overscanColumnCount={overscanColumnCount}
                        overscanRowCount={overscanRowCount}
                        cellRenderer={this._renderBodyCell}
                        rowHeight={rowHeight}
                        rowCount={rowCount}
                        width={width}
                      />
                    </div>
                  </div>}
              </AutoSizer>
            </div>
          </div>
        );
      }}
    </ScrollSync>
      <Dialog title="Dialog With Actions"
        actions={[
          <FlatButton key={1}
            label="Cancel"
            primary={true}
            onClick={()=>this.setState({TaskTimePopup:null})}
          />,
          <FlatButton key={2}
            label="Submit"
            primary={true}
            keyboardFocused={true}
            onClick={()=>this.setState({TaskTimePopup:null})}
          />,
          ]}
          modal={false}
          open={this.state.TaskTimePopup!=null}
          >
           <DatePicker hintText="Task Start" />
           <DatePicker hintText="Task End"/>
          </Dialog>
    </div>
    );
  }
  

}

TaskListMatrix.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  filter: PropTypes.array,
  /**
   * The default value of the slider.
   */
  currentProject: PropTypes.array,
  sort:PropTypes.array,
  onTaskEtimation:PropTypes.func
};

function hexToRgb(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
    : null;
}

/**
 * Ported from sass implementation in C
 * https://github.com/sass/libsass/blob/0e6b4a2850092356aa3ece07c6b249f0221caced/functions.cpp#L209
 */
function mixColors(color1, color2, amount) {
  const weight1 = amount;
  const weight2 = 1 - amount;

  const r = Math.round(weight1 * color1.r + weight2 * color2.r);
  const g = Math.round(weight1 * color1.g + weight2 * color2.g);
  const b = Math.round(weight1 * color1.b + weight2 * color2.b);

  return { r, g, b };
}

export default TaskListMatrix;
