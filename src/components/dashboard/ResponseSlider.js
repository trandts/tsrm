import React, {PropTypes} from 'react';
//import Paper from 'material-ui/Paper';
//import {white, grey800} from 'material-ui/styles/colors';
import {grey800} from 'material-ui/styles/colors';
import {typography} from 'material-ui/styles';
import Slider from 'material-ui/Slider';

class ResponseSlider extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {firstSlider: 3};

    this.handleFirstSlider = this.handleFirstSlider.bind(this);
  }

  handleFirstSlider (event, value)  {
    //console.log('handleFirstSlider',value);
    this.setState({firstSlider: value});
  }

  render() {
    //const {color, title, value, Icon} = this.props;
    const {color, title} = this.props;
    const styles = {
      content: {
        xpadding: '5px 5px',
        marginLeft: 90,
        height: 60,
        backgroundColor:color
      },
      number: {
        display: 'block',
        fontWeight: typography.fontWeightMedium,
        fontSize: 14,
        color: grey800
      },
      text: {
        fontSize: 16,
        xfontWeight: typography.fontWeightLight,
        xcolor: grey800,
        
        paddingTop: 12,
        display: 'block',
        color: 'white',
        fontWeight: 300,
        textShadow: '1px 1px #444'
      },
      iconSpan: {
        float: 'left',
        height: 60,
        width: 90,
        textAlign: 'center',
        backgroundColor: color
      },
      icon: {
        height: 48,
        width: 48,
        marginTop: 10,
        maxWidth: '100%'

      },
      sliderContainer:{
        margin:10,
        marginTop:30,
        xdisplay:'inline',
        border:'1px solid black',
        width:250
      },
      sliderTitle:{
        xfloat:'left',
        display:'inline'
      },
      sliderRoot: {
        display: 'flex',
        height: 50,
        width:200,
        flexDirection: 'row',
        xjustifyContent: 'space-around',
        
      },
      sliderValue:{
        xfloat:'right',
        xdisplay:'inline'
      }
    };

    return (
      <div style={styles.sliderContainer}>
        <div style={styles.sliderTitle} >
          <span>{title}</span>
          <div style={styles.sliderValue}>  
            <span>{' your rating is '}{this.state.firstSlider}</span>
          </div>
        </div>
         
        <div style={styles.sliderRoot}>
        <Slider min={1}
          max={5}
          step={1}
          value={this.state.firstSlider} 
          onChange={this.handleFirstSlider}
          axis="y" />
        </div>
         
      </div>
    );

  }
}

ResponseSlider.propTypes = {
  //Icon: PropTypes.any, // eslint-disable-line
  color: PropTypes.string,
  title: PropTypes.string,
  //value: PropTypes.string
};

export default ResponseSlider;
