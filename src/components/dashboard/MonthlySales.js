import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import {white, pink800, pink500,pink100} from 'material-ui/styles/colors';
import {BarChart, Bar, ResponsiveContainer, XAxis,YAxis} from 'recharts';
import GlobalStyles from '../../styles';

const MonthlySales = (props) => {
const Height=props.Height||150;
  const styles = {
    paper: {
      backgroundColor: props.paperColor||pink800,
      height: Height
    },
    div: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '95%',
      height: Height-65
    },
    header: {
      color: white,
      backgroundColor: props.headerColor||'#626a99'||pink500,
      padding: 10
    },
    bars:{
      backgroundColor:props.barColor||pink100,
    }
  };

  return (
    <Paper style={props.paper||styles.paper}>
      <div style={{...GlobalStyles.title, ...styles.header}}>{props.title||''}</div>
      <div style={styles.div}>
        <ResponsiveContainer>
          <BarChart data={props.data} >
            <Bar dataKey="uv" fill={styles.bars.backgroundColor}/>
            {props.displayXAxis?<XAxis dataKey="name" stroke="none" tick={{fill: white}}/>:[]}
             {props.displayYAxis? <YAxis tick={{fill: white}} domain={[0, 5]} allowDecimals={true} />:[]}
          </BarChart>
        </ResponsiveContainer>
      </div>
    </Paper>
  );
};

MonthlySales.propTypes = {
  data: PropTypes.array,
  displayXAxis:PropTypes.bool,
  displayYAxis:PropTypes.bool,
  title:PropTypes.string,
  paper:PropTypes.object,
  paperColor:PropTypes.string,
  headerColor:PropTypes.string,
  barColor:PropTypes.string,
  Height:PropTypes.number
};

export default MonthlySales;
