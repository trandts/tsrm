import React,{PropTypes} from 'react';
import {
    Step,
    Stepper,
    StepButton,
    StepContent,
    StepLabel
} from 'material-ui/Stepper';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import CommunicationComment from 'material-ui/svg-icons/communication/comment';
import ActionCached from 'material-ui/svg-icons/action/cached';
import Dialog from 'material-ui/Dialog';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import Slider from 'material-ui/Slider';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import {  blue500,red400,green500 }from 'material-ui/styles/colors';
import Data from '../data';


const iconStyles = {
  outer: {
    padding: 0,
    height: 15,
    width: 15
  },
  inner: {
    padding: 0,
    height: 15,
    width: 15
  }
};


class TaskCommentTimeline extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stepIndex: 0,
            data: [
                {
                    type: "Comment",
                    item: {},
                    date: new Date(2017, 9, 18),
                    heading: "Faheem",
                    text: `For each ad campaign that you create, you can control how much
                you're willing to spend on clicks and conversions, which networks
                and geographical locations you want your ads to show on, and more.`}
                , {
                    type: "Comment",
                    item: {},
                    date: new Date(2017, 9, 17),
                    heading: "Noman",
                    text: `An ad group contains one or more ads which target a shared set of keywords.`}
                ,{
                    type: "Comment",
                    item: {},
                    date: new Date(2017, 9, 19),
                    heading: "Faheem",
                    text: `Try out different ad text to see what brings in the most customers,
                and learn how to enhance your ads using features like ad extensions.
                If you run into any problems with your ads, find out how to tell if
                they're running and how to resolve approval issues.`}
                ,  {
                    date: new Date(2017, 7, 14),  // end is optional
                    type: "Tag",
                    text: 'LifeCycle - Started',
                    heading: 'LifeCycle Started',
                    group: 1,
                    item: {},
                }, {
                    date: new Date(2017, 7, 25),  // end is optional
                    type: "Tag",
                    text: 'Unassigned To Noman',
                    heading: 'Unassigned To Noman',
                    group: 1,
                    item: {},
                },{
                    date: new Date(2017, 7, 25),  // end is optional
                    type: "Tag",
                    text: 'Assigned To Faheem',
                    heading: 'Anassigned To Faheem',
                    group: 1,
                    item: {},
                }, {
                    date: new Date(2017, 7, 16),  // end is optional
                    type: "Tag",
                    text: 'Priority - Low',
                    heading: 'Priority Low',
                    group: 1,
                    item: {},
                },{
                    date: new Date(2017, 7, 24),  // end is optional
                    type: "Tag",
                    text: 'Priority - High',
                    heading: 'Priority High',
                    group: 1,
                    item: {},
                }, {
                    date: new Date(2017, 7, 29),  // end is optional
                    type: "Tag",
                    text: 'LifeCycle - Paused',
                    heading: 'LifeCycle Paused',
                    group: 1,
                    item: {},
                }, {
                    date: new Date(2017, 8, 5),  // end is optional
                    type: "Tag",
                    text: 'LifeCycle - Resumed',
                    heading: 'LifeCycle Resumed',
                    group: 1,
                    item: {},
                }
            ],
            FromDate:this.props.FromDate,
            ToDate:this.props.ToDate,
            notesMissingLength:1,
            open:false,
            taskConvertOpen:false,
            User:this.props.User||null
        };
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.renderStepActions = this.renderStepActions.bind(this);
    }
 componentWillReceiveProps(newProps) {
    this.setState({
      FromDate:newProps.FromDate,
      ToDate:newProps.ToDate,
      User:newProps.User||null
    });
  }
    handleNext() {
        const { stepIndex } = this.state;
        if (stepIndex < this.state.data.length) {
            this.setState({ stepIndex: stepIndex + 1 });
        }
    }
    handlePrev() {
        const { stepIndex } = this.state;
        if (stepIndex > 0) {
            this.setState({ stepIndex: stepIndex - 1 });
        }
    }
    renderStepActions() {
        /*<RaisedButton
                    label="Next"
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    primary={true}
                    onClick={this.handleNext}
                    style={{ marginRight: 12 }}
                />
                {step > 0 && (
                    <FlatButton
                        label="Back"
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        onClick={this.handlePrev}
                    />
                )}*/
        return (
            <div style={{ margin: '12px 0' }}>
                <IconButton tooltipPosition="top-right" tooltip="Reply"
                 onClick={()=>this.setState({open:true})} >
                    <CommunicationComment color={blue500} />
                </IconButton>
                <IconButton tooltipPosition="top-right" tooltip="Convert To Task" 
                onClick={()=>this.setState({taskConvertOpen:true})}>
                    <ActionCached color={green500} />
                </IconButton>
                <IconButton tooltipPosition="top-right" tooltip="Delete Note" 
                onClick={()=>this.setState({taskConvertOpen:true})}>
                    <ActionDelete color={red400} />
                </IconButton>
            </div>
        );
    }
    render() {
        const { stepIndex, FromDate, ToDate, data } = this.state;
        const Username=Data.taskMatrix.Users.filter(u=>u.id==this.state.User).map(u=>u.name)[0]||'';
        const filteredData = data.filter(e =>
            (e.date >= FromDate || FromDate == null)
            &&
            (e.date <= ToDate || ToDate == null)
            &&(this.state.User==null ||Username==e.heading.toLowerCase())
        ).sort((a,b)=>b.date-a.date);
        return (
            <div style={{ maxWidth: '100%', maxHeight: 400, margin: 'auto', overflowY: 'scroll' }}>
                <Stepper key={0}
                    activeStep={stepIndex || 0}
                    linear={false}
                    orientation="vertical"
                >{filteredData.map((e, i) =>
                    <Step key={i}>
                        <StepButton onClick={() => this.setState({ stepIndex: i })}>
                                <StepLabel>
                                    <div style={{ width: '100%', display: 'flex' }}>
                                        <div style={{ width: '80%', textAlign: 'left' }}>
                                            {e.type == "Comment" ?
                                                e.heading + ' : ' + e.text.split(' ').slice(0, 3).join(" ") + '...'
                                                : e.heading}</div>
                                        <div style={{ width: '20%', textAlign: 'right' }}>
                                            {e.date.toLocaleString()}
                                            <div style={{ fontSize: 7 }} >
                                                {e.type}
                                            </div>
                                        </div>
                                    </div>
                                </StepLabel>
                        </StepButton>
                        <StepContent>
                            <p>{e.text}</p>
                            <p style={{ fontSize: 10 }}>{e.date.toLocaleString()}</p>
                            {this.renderStepActions(i)}
                        </StepContent>
                        </Step>
                    )}
                </Stepper>
                <Dialog title="Add Reply Note"  key={1}
        actions={[
          <FlatButton key={0}
            label="Cancel"
            primary={true}
            onClick={() => this.setState({ open: false })}
          />,
          <FlatButton key={1}
            label="Submit"
            primary={true}
            keyboardFocused={true}
            onClick={() => this.setState({ open: false })}
          />,
        ]}
        modal={false}
        open={this.state.open || false}
         contentStyle={{  width: '65%',  maxWidth: 'none'}}
      ><div>{Array(this.state.notesMissingLength).fill().map((_, i) => i)
        .map((e, _i) => <div key={_i} style={{display:'flex'}}>
          <TextField key={1} floatingLabelText="Notes" multiLine={true} rows={2} />
          <TextField key={2} floatingLabelText="Delieverable" multiLine={true} rows={2} />
          <SelectField floatingLabelText="Delieverable Type"
            value={this.state["notesDelieverableType" + _i]}
            onChange={(e, i, v) => this.setState({ ["notesDelieverableType" + _i]: v })}>
            <MenuItem value={1} key={1} primaryText="Google Sheet" />
            <MenuItem value={2} key={2} primaryText="Code Reference" />
            <MenuItem value={3} key={3} primaryText="Meeting Notes" />
            <MenuItem value={4} key={4} primaryText="Others" />
          </SelectField>
        </div>)}
          <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
            onClick={() => this.setState({ notesMissingLength: this.state.notesMissingLength + 1 })}
          >
            <ActionNoteAdd color={blue500} />
          </IconButton>
          <DatePicker hintText={"Notes Date"} defaultDate={new Date()} />
          <Slider style={{ width: '90%' }}
            min={0}
            max={100}
            defaultValue={50}
          />
        </div>
      </Dialog>
                  <Dialog key={2}
                    title="Convert To Task"
                    actions={[<FlatButton key={1} label="Convert To Task" primary={true}
                        keyboardFocused={true}
                        onClick={() => this.setState({ taskConvertOpen: false })}
                    />, <FlatButton key={2} label="Cancel" primary={true}
                        keyboardFocused={true}
                        onClick={() => this.setState({ taskConvertOpen: false })}
                    />]}
                    modal={false}
                    open={this.state.taskConvertOpen} >
                    <div><TextField key="Notes" floatingLabelText="Task Name" multiLine={true} rows={2} />
                        <DatePicker hintText="Start Date" defaultDate={new Date()} />
                        <DatePicker hintText="End Date" defaultDate={new Date()} />
                        <SelectField floatingLabelText="Project"
                            value={this.state.taskConvertProject}
                            onChange={(e, i, taskConvertProject) => this.setState({taskConvertProject})}>
                            <MenuItem value={1} primaryText="NFL" />
                            <MenuItem value={2} primaryText="Sannofi" />
                        </SelectField>
                        <br />
                         <SelectField floatingLabelText="Relation"
                            value={this.state.taskConvertRelation}
                            onChange={(e, i, taskConvertRelation) => this.setState({taskConvertRelation})}>
                            <MenuItem value={1} primaryText="parent-child" />
                            <MenuItem value={2} primaryText="dependent Task" />
                            <MenuItem value={2} primaryText="related Task" />
                        </SelectField>
                    </div>
                </Dialog>
            </div>
        );
    }


}


TaskCommentTimeline.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  FromDate: PropTypes.object,
  /**
   * The default value of the slider.
   */
  ToDate: PropTypes.object,
 // User:PropTypes.oneOfType(PropTypes.number,PropTypes.object)
 User:PropTypes.array
};
export default TaskCommentTimeline;
