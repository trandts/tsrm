import React, { PropTypes } from 'react';
import $ from "jquery";
import WorkflowOpen from '../components/WorkflowOpen';
import Data from '../data';

class TaskAttachedStateMachine extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      EventBus:this.props.EventBus,
      stateInstance: this.props.stateInstance,
      SpecialNode: this.props.SpecialNode,
      WorkflowInitated: false,
      CommentInstance:null
    };
    this.WorkflowLoad = this.WorkflowLoad.bind(this);
  }
  componentDidMount() {
   this.WorkflowLoad();
  }
  componentWillReceiveProps(nextProps)
  {
    this.setState({ stateInstance:nextProps.stateInstance,
      WorkflowInitated: false});
    this.WorkflowLoad();
  }
  WorkflowLoad() {
    const {SpecialNode,stateInstance}=this.state;
    $.ajax({
      url: Data.AppDir+'/api/TaskAttachedStateMachine/' + stateInstance+'?ManagementStateName='+SpecialNode,
      type: 'GET',
      success: function ($data) {
        this.setState({ WorkflowInitated: true, CommentInstance: $data });
      }.bind(this)
    });
  }
  render() {
    const { CommentInstance, WorkflowInitated,EventBus } = this.state;
    let Element=null;
    if(WorkflowInitated && CommentInstance)
      {
      Element= CommentInstance
      .map((e,i)=><WorkflowOpen key={i} WorkflowInstance={e.NotesManagementInstanceId} Context={e.TaskId} EventBus={EventBus} />);
    
      }
    return (<div>{Element}</div>);
  }
}

TaskAttachedStateMachine.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  stateInstance: PropTypes.number,
  EventBus:PropTypes.object,
  SpecialNode:PropTypes.string
};

export default TaskAttachedStateMachine;
