import React,  { PropTypes } from 'react';
import Drawer from 'material-ui/Drawer';
import { typography} from 'material-ui/styles';
//import {white, blue600,orange600} from 'material-ui/styles/colors';
import {white} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import {Link} from 'react-router';
//import Face from 'material-ui/svg-icons/action/face';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import LoginUserBox from '../components/dashboard/LoginUserBox';
import muiThemeable from 'material-ui/styles/muiThemeable';


class LeftDrawer extends React.Component {
  constructor(props) {
    super(props);

  }
  getMenu(menu, index) {
     const styles = {
        color: white,
        fontSize: 14,
        backgroundColor:this.props.muiTheme.palette.primary1Color
      };
    return (menu.menuItems || []).length > 0 ?
      <MenuItem
        key={index}
        style={styles}
        primaryText={menu.text}
        leftIcon={menu.icon}
      
        menuItems={menu.menuItems.map((e, i) => this.getMenu(e, i))}
      /> : <MenuItem
        key={index}
        style={styles}
        primaryText={menu.text}
        leftIcon={menu.icon}
        containerElement={<Link to={menu.link} />}
      />;

  }
  render() {
    let props = this.props;
    let { navDrawerOpen,username,designation } = props;

    const styles = {
      logo: {
        cursor: 'pointer',
        fontSize: 22,
        color: typography.textFullWhite,
        fontWeight: typography.fontWeightLight,
        backgroundColor: '#ffffff',
      },
      logoContainer: {
        backgroundColor: "#ffffff",
        padding: 2,
        paddingTop: 5,
        paddingLeft: 60
      },
      logoImg: {

      },
      appImg: {
        padding: 3,
        width: 220,
        maxHeight: 57,
      },
      menuItem: {
        color: white,
        fontSize: 14
      },
      avatar: {
        div: {
          padding: '15px 0 20px 15px',
          height: 45,
          backgroundColor: '#b1ba1b',
          display: 'none'
        },
        icon: {
          float: 'left',
          display: 'block',
          marginRight: 15,
          boxShadow: '0px 0px 0px 8px rgba(0,0,0,0.2)'
        },
        span: {
          paddingTop: 12,
          display: 'block',
          color: 'white',
          fontWeight: 300,
          textShadow: '1px 1px #444'
        }
      }
    };

    return (
      <Drawer
        docked={true}
        open={navDrawerOpen}>
        <div style={styles.logo}>
          <img src={require('../images/trandts.jpg')} style={styles.appImg} alt="" />
        </div>
        <div>
          <LoginUserBox Icon={PermIdentity}
            color="#b1ba1b"
            title={username}
            value={designation}
          />
        </div>
        <div>
          {props.menus.map((menu, index) => this.getMenu(menu,index))}
        </div>
      </Drawer>
    );
  }
}

LeftDrawer.propTypes = {
  navDrawerOpen: PropTypes.bool,
  menus: PropTypes.array,
  username: PropTypes.string,
  designation: PropTypes.string,
  muiTheme:PropTypes.object
};

export default muiThemeable()(LeftDrawer );
