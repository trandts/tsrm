import React, { PropTypes } from 'react';
import Timeline from 'react-visjs-timeline';
import Data from '../data';
import Toggle from 'material-ui/Toggle';

function getColumns() {
  return ["Project","Task"].concat(Data.taskMatrix.tagCategories.map(e => e.name)).concat(["users", "Approver"]);
}
function getTasks(Projects,columns) {
  if (!Projects) return [];
  let arr=[];
  for(let projectIndex=0;projectIndex<(Projects||[]).length;projectIndex++)
  {
      let Proj=Projects[projectIndex];
      for(let taskIndex=0;taskIndex<Proj.tasks.length;taskIndex++)
      {
        let task=Proj.tasks[taskIndex];
        let obj = {
          attached: {
            Project: Proj,
            Task: task,
            current:null
          },
          value:null
        };
        let rowValues=[];
        for(let columnIndex=0;columnIndex<columns.length;columnIndex++)
        {
          let columnName=columns[columnIndex];
          let cell = Object.assign({}, obj);
          if(columnName=="Task")
          {
            cell.value=cell.attached.Task.name;
          } else if(columnName=="Project")
          {
            cell.value=cell.attached.Project.name;
          }else if(columnName=="users"||columnName=="Approver")
          {
            cell.value="User";
          }else{
             cell.value="Tag";
          }
          cell.column=columnName;
          rowValues[rowValues.length]=cell;
        }
        arr[arr.length]=rowValues;
      }
  }
  return arr;
}

class TaskListTimeline extends React.Component {
  constructor(props) {
    super(props);
    let columns=getColumns(this.props.currentProject);
    this.state = {
      columnWidth: 200,
      height: 300,
      overscanColumnCount: 0,
      overscanRowCount: 5,
      rowHeight: 40,
      filter: this.props.filter || [],
      currentProject: this.props.currentProject || [],

      Columns: columns,
      Tasks: getTasks(this.props.currentProject,columns)
    };

  }
  componentWillReceiveProps(newProps) {
    let columns=getColumns(newProps.currentProject);
    this.setState({
      filter: newProps.filter,
      currentProject: newProps.currentProject
       , Columns: columns,
       Tasks: getTasks(newProps.currentProject,columns)
    });
  }
  render() {
    const TimelineOptions = {
      width: '100%',
      height: '500px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'range',
      template: function (item, element, data) {

        return data.style ? `<div>${data.content}</div>` : `<div>${data.content}<div>${(data.end-data.start)/(1000*60*60)} Hours</div></div>`;
      },
      format: {
        minorLabels: {
          minute: 'h:mma',
          hour: 'ha'
        }
      }
    };
    const items = [{
      start: new Date(2010, 7, 15),
      end: new Date(2010, 8, 2),  // end is optional
      content: 'Faheem',
      group: 1,
      id: 1
    }, {
      start: new Date(2017, 7, 15),
      end: new Date(2017, 7, 22),  // end is optional
      content: 'Noman',
      group: 1,
      id: 2

    }, {
      start: new Date(2017, 7, 14),  // end is optional
      content: 'LifeCycle:Started',
      style: "color: #25d495; background-color: #eaffc0;",
      group: 1,
      type: 'box',
      id: 2
    }, {
      start: new Date(2017, 7, 16),  // end is optional
      content: 'Priority:Low',
      style: "color: #25d495; background-color: #eaffc0;",
      group: 1,
      type: 'box',
      id: 3
    }, {
      start: new Date(2017, 7, 24),  // end is optional
      content: 'Priority:High',
      style: "color: #25d495; background-color: #eaffc0;",
      group: 1,
      type: 'box',
      id: 4
    }, {
      start: new Date(2017, 7, 29),  // end is optional
      content: 'LifeCycle:Paused',
      style: "color: #25d495; background-color: #eaffc0;",
      group: 1,
      type: 'box',
      id: 5
    }, {
      start: new Date(2017, 8, 5),  // end is optional
      content: 'LifeCycle:Resumed',
      style: "color: #25d495; background-color: #eaffc0;",
      group: 1,
      type: 'box',
      id: 6
    }, {
      start: new Date(2017, 8, 15),
      end: new Date(2017, 9, 2),  // end is optional
      content: 'Raheel',
      group: 2,
      id: 7
    }, {
      start: new Date(2017, 8, 17),
      end: new Date(2017, 9, 2),  // end is optional
      content: 'IP',
      group: 3,
      id: 8
    }].filter(() => this.state.Tasks.length > 0);
    let group = (this.state.currentProject || []).map(e => [{
      id: "p" + e.id,
      content: e.name+`\n(Actual/Est:${e.tasks.map(t=>t.actualHours||0).reduce((a,b)=>a+b,0)||0}/${e.tasks.map(t=>t.estimatedhours||0).reduce((a,b)=>a+b,0)||0})`
      , nestedGroups: (e.tasks || []).map(t => t.id)
    }].concat((e.tasks || []).map(t => ({ id: t.id
      , content: t.name+`\n(Actual/Est:${t.actualHours||0}/${t.estimatedhours||0})`
     })))).reduce((a, b) => a.concat(b), []);

let userGroup=Array.from(new Set((this.state.currentProject || []).map(e => [{
      id: "p" + e.id,
      content: e.name
      , nestedGroups: Array.from(new Set((e.tasks || []).map(t => (t.users||[]).map(u=>e.id+'-'+u) ).reduce((a,b)=>a.concat(b),[])))
    }].concat(
    Array.from(new Set((e.tasks || []).map(t => t.users||[] ).reduce((a,b)=>a.concat(b),[])))
    .map(t => ({ id:e.id+"-"+ t, content: t }))
    )).reduce((a,b)=>a.concat(b),[])));
    
    let AllTasks=(this.state.currentProject || []).map(p=>p.tasks).reduce((a,b)=>a.concat(b),[]);
    let AllUsers= Array.from(new Set(AllTasks.map(t=>t.users||[]).reduce((a,b)=>a.concat(b),[]) ));
    let projectUserTasks = (this.state.currentProject || [])
      .map(e => AllUsers.map(u => ({ user: +e.id + '-' + u, tasks: e.tasks.filter(t => (t.users || []).indexOf(u) > -1) })).filter(e => e.tasks.length > 0)).reduce((a, b) => a.concat(b), [])
      .map(e => e.tasks.map(t =>
        ({
          start: new Date(2017, 7, t.id%25),
          end: new Date(2017, 8,  t.id%24),  // end is optional
          content: t.name,
          group: e.user,
          id: t.id+e.user
        })
      )
      ).reduce((a,b)=>a.concat(b),[]);

    return (<div>
      <Toggle label="Display Events"
        onToggle={() => this.setState({ displayEvents: !this.state.displayEvents })}
        toggled={this.state.displayEvents || false} />
        <h2>Task Based</h2>
      <Timeline options={TimelineOptions}
        items={items
          .filter(e => e.type != 'box' || this.state.displayEvents)
        }
        animation={{
          duration: 3000,
          easingFunction: 'easeInQuint',
        }}
        groups={group}
      /> <h2>Resource Based</h2>
      <Timeline options={TimelineOptions} style={{marginTop:45}}
        items={projectUserTasks
          .filter(e => e.type != 'box' || this.state.displayEvents)
        }
        animation={{
          duration: 3000,
          easingFunction: 'easeInQuint',
        }}
        groups={userGroup}
      />
      </div>);
  }
}

TaskListTimeline.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  filter: PropTypes.array,
  /**
   * The default value of the slider.
   */
  currentProject: PropTypes.array
};

export default TaskListTimeline;
