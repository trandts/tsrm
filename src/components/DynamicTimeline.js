import React, { PropTypes } from 'react';
import Timeline from 'react-visjs-timeline';


function getState(options) {
  let items = [];
  if (options) {
    if (options.objType == "Task") {
      items[items.length] = {
        start: options.obj.startAt || new Date(),
        end: options.obj.EndTarget,  // end is optional
        content: options.obj.name,
        id: options.obj.id
      };
      items = items.concat(options.obj.Assigned.map(e => ({
        start: options.obj.startAt || new Date(),
        end: new Date(2017, 8, 29),  // end is optional
        content: e.user,
        id: e.user
      })));
      items = items.concat([{
        start: new Date(2017, 7, 14),  // end is optional
        content: 'LifeCycle:Started',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 7, 16),  // end is optional
        content: 'Priority:Low',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 7, 24),  // end is optional
        content: 'Priority:High',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 7, 25),  // end is optional
        content: 'Unassigned To Noman',
        style: "color: black; background-color: #e8a76f;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 7, 26),  // end is optional
        content: 'Assigned To Faheem',
        style: "color: black; background-color: #e8a76f;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 7, 29),  // end is optional
        content: 'LifeCycle:Paused',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 8, 5),  // end is optional
        content: 'LifeCycle:Resumed',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }]);
    } else if (options.objType == "Assigned") {
     
      items[items.length] = {
        start:  new Date(),
        end: new Date(2017, 8, new Date().getDate()+1, options.obj.actualHours),  // end is optional
        content: options.obj.user,
        id: options.obj.user
      };
       items = items.concat([{
        start: new Date(2017, 8, 28),  // end is optional
        content: 'Notes: Task partially complete 30%',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 8, 29),  // end is optional
        content: 'Notes: Task partially complete 60%',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }, {
        start: new Date(2017, 8, 29,10),  // end is optional
        content: 'Notes: Task completed and uploaded',
        style: "color: #25d495; background-color: #eaffc0;",
        group: 1,
        type: 'box'
      }]);
    }

  }


  return { options: options, items: items, key: new Date().toString() };
}

class DynamicTimeline extends React.Component {
  constructor(props) {
    super(props);
    //   let columns=getColumns(this.props.currentProject);

    this.state = getState(this.props.options);

  }
  componentWillReceiveProps(newProps) {
    // let columns=getColumns(newProps.currentProject);

    this.setState(getState(newProps.options));
  }
  render() {
    const TimelineOptions = {
      width: '100%',
      height: '500px',
      stack: true,
      showMajorLabels: true,
      showCurrentTime: true,
      zoomMin: 1000000,
      type: 'range',
      template: function (item, element, data) {

        return `<div>${data.content}</div>`;
      },
      format: {
        minorLabels: {
          minute: 'h:mma',
          hour: 'ha'
        }
      }
    };
    const { items } = this.state;
    return (<div>{items.length > 0 ? <Timeline key={this.state.key} options={TimelineOptions} style={{ marginTop: 45 }}
      items={items}
      animation={{
        duration: 3000,
        easingFunction: 'easeInQuint',
      }}
      groups={[]}
    /> : null}
    </div>);
  }
}

DynamicTimeline.propTypes = {

  options: PropTypes.object
};

export default DynamicTimeline;
