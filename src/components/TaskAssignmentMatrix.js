import React, { PropTypes } from 'react';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import Grid from 'react-virtualized/dist/commonjs/Grid';
import ScrollSync from 'react-virtualized/dist/commonjs/ScrollSync';
import cn from "classnames";

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import IconButton from 'material-ui/IconButton';
import ActionDoneAll from 'material-ui/svg-icons/action/done-all';
import ActionSpeakerNotes from 'material-ui/svg-icons/action/speaker-notes';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import { blue500, purple500,deepOrange900 } from 'material-ui/styles/colors';
import Slider from 'material-ui/Slider';


const iconStyles = {
  outer: {
    padding: 0,
    height: 15,
    width: 15
  },
  inner: {
    padding: 0,
    height: 15,
    width: 15
  }
};

/*
 const LEFT_COLOR_FROM = hexToRgb("#471061");
 const LEFT_COLOR_TO = hexToRgb("#BC3959");
 const TOP_COLOR_FROM = hexToRgb("#000000");
 const TOP_COLOR_TO = hexToRgb("#333333");

const LEFT_COLOR_FROM = hexToRgb("#4CAF50");
const LEFT_COLOR_TO = hexToRgb("#CDDC39");
const TOP_COLOR_FROM = hexToRgb("#000000");
const TOP_COLOR_TO = hexToRgb("#81C784");
*/


const LEFT_COLOR_FROM = hexToRgb("#ffffff");
const LEFT_COLOR_TO = hexToRgb("#ffffff");
const TOP_COLOR_FROM = hexToRgb("#81C784");
const TOP_COLOR_TO = hexToRgb("#81C784");

function getColumns() {
  return ["User", "Start Date", "End Date", "Estimated Hours", "Actual Hours", "Completion"];
}
function getTasks(task) {
  if (!task) return [];
  let arr = [];
  for (let taskIndex = 0; taskIndex < task.Assigned.length; taskIndex++) {
    let assignedTask = task.Assigned[taskIndex];
    let rowValues = [assignedTask.user,
      null, null,
    assignedTask.estimatedhours, assignedTask.actualHours, assignedTask.completion];
    arr[arr.length] = rowValues;
  }
  return arr;
}
class TaskAssignmentMatrix extends React.Component {
  constructor(props) {
    super(props);
    let columns = getColumns();
    this.state = {
      columnWidth: 200,
      height: 300,
      overscanColumnCount: 0,
      overscanRowCount: 5,
      rowHeight: 80,
      columnsFreezed: 1,
      task: getTasks(this.props.task, columns),
      TaskItem: this.props.task,
      Columns: columns,
      notesMissingLength:1
    };
    this._renderBodyCell = this._renderBodyCell.bind(this);
    this._renderHeaderCell = this._renderHeaderCell.bind(this);
    this._renderLeftSideCell = this._renderLeftSideCell.bind(this);
    this._renderLeftHeaderCell = this._renderLeftHeaderCell.bind(this);

  }
  componentWillReceiveProps(newProps) {
    let columns = getColumns();
    this.setState({
      filter: newProps.filter,
      currentProject: newProps.currentProject
      , Columns: columns,
      task: getTasks(newProps.task, columns),
      TaskItem: newProps.task,
      TaskTimePopup: null,
      notesMissingLength:1,
      IconMenu:null
    });

  }
  _renderBodyCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < this.state.columnsFreezed) {
      return;
    }

    return this._renderLeftSideCell({ columnIndex, key, rowIndex, style });
  }

  _renderHeaderCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < this.state.columnsFreezed) {
      return;
    }

    return this._renderLeftHeaderCell({ columnIndex, key, rowIndex, style });
  }

  _renderLeftSideCell({ columnIndex, key, rowIndex, style }) {

    const rowClass =
      rowIndex % 2 === 0
        ? columnIndex % 2 === 0 ? "evenRow" : "oddRow"
        : columnIndex % 2 !== 0 ? "evenRow" : "oddRow";
    const classNames = cn(rowClass, "cell");
    let cell = this.state.task[rowIndex][columnIndex];
    let column = this.state.Columns[columnIndex];
    //"User", "Start Date", "End Date", "Estimated Hours", "Actual Hours", "Completion"
    if (column == "Estimated Hours" || column == "Actual Hours") {
      return (<div className={classNames} key={key} style={style}>
        <div style={{ width: '85%', overflow: 'hidden', alignItems: 'center' }}>
          <TextField floatingLabelText={column} defaultValue={cell} type="number" />
        </div>
      </div>);
    } else if (column == "Start Date" || column == "End Date") {
      return (<div className={classNames} key={key} style={style}>
        <div style={{ width: '85%', overflowX: 'hidden', alignItems: 'center' }}> <DatePicker hintText={column} /></div>
      </div>);
    } else if (column == "Completion") {
      return (<div className={classNames} key={key} style={style}>
        {cell}%</div>);
    } else if (column == "User") {
      if (!this.state.TaskItem) return <div className={classNames} key={key} style={style} />;
      let isOpen = this.state.IconMenu == this.state.TaskItem.Assigned[rowIndex].user;
      let isOpen2 = this.state.IconMenu2 == this.state.TaskItem.Assigned[rowIndex].user;
      return (<div className={classNames} key={key} style={style}>
        {cell} <div key={2} style={{ width: '100%', height: iconStyles.outer.height }}>
          <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
            onClick={() => this.setState({ IconMenu: this.state.TaskItem.Assigned[rowIndex].user })}
          ><ActionSpeakerNotes color={blue500} />
          </IconButton>
          <IconButton tooltipPosition="top-right" tooltip="Approve" style={iconStyles.outer} iconStyle={iconStyles.inner}>
            <ActionDoneAll color={purple500} />
          </IconButton>
           <IconButton tooltipPosition="top-right" tooltip="Edit Working" style={iconStyles.outer} iconStyle={iconStyles.inner}
            onClick={() => this.setState({ IconMenu2: this.state.TaskItem.Assigned[rowIndex].user })} >
            <ActionTimeline color={deepOrange900} />
          </IconButton>
           <Dialog title="Edit Working"
            actions={[
              <FlatButton key={0}
                label="Cancel"
                primary={true}
                onClick={() => this.setState({ IconMenu2: null })}
              />,
              <FlatButton key={1}
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onClick={() => this.setState({ IconMenu2: null })}
              />,
            ]}
            modal={false}
            open={isOpen2}
            contentStyle={{ width: '65%', maxWidth: 'none' }}
          ><div>{Array(this.state.notesMissingLength).fill().map((_, i) => i)
            .map((e, _i) => <div key={_i} className="row">
              <DatePicker key={1} hintText="Start Date" defaultDate={new Date()}/>
              <DatePicker key={2} hintText="End Date" defaultDate={new Date()} />
            </div>)}
              <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
                onClick={() => this.setState({ notesMissingLength: this.state.notesMissingLength + 1 })}
              >
                <ActionNoteAdd color={blue500} />
              </IconButton>
            </div>
          </Dialog>
          <Dialog title="Add Note"
            actions={[
              <FlatButton key={0}
                label="Cancel"
                primary={true}
                onClick={() => this.setState({ IconMenu: null })}
              />,
              <FlatButton key={1}
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onClick={() => this.setState({ IconMenu: null })}
              />,
            ]}
            modal={false}
            open={isOpen}
            contentStyle={{ width: '65%', maxWidth: 'none' }}
          ><div>{Array(this.state.notesMissingLength).fill().map((_, i) => i)
            .map((e, _i) => <div key={_i} style={{ display: 'flex' }}>
              <TextField key={1} floatingLabelText="Notes" multiLine={true} rows={2} />
              <TextField key={2} floatingLabelText="Delieverable" multiLine={true} rows={2} />
              <SelectField floatingLabelText="Delieverable Type"
                value={this.state["notesDelieverableType" + _i]}
                onChange={(e, i, v) => this.setState({ ["notesDelieverableType" + _i]: v })}>
                <MenuItem value={1} key={1} primaryText="Google Sheet" />
                <MenuItem value={2} key={2} primaryText="Code Reference" />
                <MenuItem value={3} key={3} primaryText="Meeting Notes" />
                <MenuItem value={4} key={4} primaryText="Others" />
              </SelectField>
            </div>)}
              <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
                onClick={() => this.setState({ notesMissingLength: this.state.notesMissingLength + 1 })}
              >
                <ActionNoteAdd color={blue500} />
              </IconButton>
              <DatePicker hintText={"Notes Date"} defaultDate={new Date()} />
              <Slider style={{ width: '90%' }}
                min={0}
                max={100}
                defaultValue={this.state.TaskItem.Assigned[rowIndex].completion}
              />
            </div>
          </Dialog>
        </div>
      </div>);


    }
    // Data.taskMatrix.tagCategories.
    return (
      <div className={classNames} key={key} style={style}>
        {`(${columnIndex},${rowIndex})`}
      </div>
    );
  }


  _renderLeftHeaderCell({ columnIndex, key, style }) {

    return (
      <div className="headerCell" key={key} style={style}>
        {`${this.state.Columns[columnIndex]}`}
      </div>
    );
  }

  render() {
    const {
      columnWidth,
      height,
      overscanColumnCount,
      overscanRowCount,
      rowHeight,
      columnsFreezed

    } = this.state;
    const rowCount = (this.state.task || []).length;
    const columnCount = this.state.Columns.length;

    let datekey = new Date().toJSON();
    let columnsAsKey = datekey + (this.state.TaskItem ? this.state.TaskItem.id : 0).toString()+this.state.IconMenu+this.state.IconMenu2;
 
    return (<div style={{marginTop:20}} >
      <h2>(Assignment) - {this.state.TaskItem.name}</h2>
      <ScrollSync>
      {({
            clientHeight,
        clientWidth,
        onScroll,
        scrollHeight,
        scrollLeft,
        scrollTop,
        scrollWidth
          }) => {
        const x = scrollLeft / (scrollWidth - clientWidth);
        const y = scrollTop / (scrollHeight - clientHeight);

        const leftBackgroundColor = mixColors(
          LEFT_COLOR_FROM,
          LEFT_COLOR_TO,
          y
        );
        const leftColor =  "#000000";//"#ffffff";
        const topBackgroundColor = mixColors(
          TOP_COLOR_FROM,
          TOP_COLOR_TO,
          x
        );
        const topColor=  "#000000";//"#ffffff";
        const middleBackgroundColor = mixColors(
          leftBackgroundColor,
          topBackgroundColor,
          0.5
        );
        const middleColor=  "#000000";//"#ffffff";

        return (
          <div className="GridRow">
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                color: leftColor,
                backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`
              }}
            >
              <Grid key={columnsAsKey + 1}
                cellRenderer={this._renderLeftHeaderCell}
                className="HeaderGrid"
                width={columnWidth * columnsFreezed}
                height={rowHeight}
                rowHeight={rowHeight}
                columnWidth={columnWidth}
                rowCount={1}
                columnCount={columnsFreezed}
              />
            </div>
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: rowHeight,
                color: leftColor,
                backgroundColor: `rgb(${leftBackgroundColor.r},${leftBackgroundColor.g},${leftBackgroundColor.b})`
              }}
            >
              <Grid key={columnsAsKey + 2}
                overscanColumnCount={overscanColumnCount}
                overscanRowCount={overscanRowCount}
                cellRenderer={this._renderLeftSideCell}
                columnWidth={columnWidth}
                columnCount={columnsFreezed}
                className="LeftSideGrid"
                height={height}
                rowHeight={rowHeight}
                rowCount={rowCount}
                scrollTop={scrollTop}
                width={columnWidth * columnsFreezed}

              />
            </div>
            <div className="GridColumn">
              <AutoSizer disableHeight>
                {({ width }) =>
                  <div>
                    <div
                      style={{
                        backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`,
                        color: topColor,
                        height: rowHeight,
                        width: width
                      }}
                    >
                      <Grid key={columnsAsKey + 3}
                        className="HeaderGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={rowHeight}
                        overscanColumnCount={overscanColumnCount}
                        cellRenderer={this._renderHeaderCell}
                        rowHeight={rowHeight}
                        rowCount={1}
                        scrollLeft={scrollLeft}
                        width={width}
                      />
                    </div>
                    <div
                      style={{
                        backgroundColor: `rgb(${middleBackgroundColor.r},${middleBackgroundColor.g},${middleBackgroundColor.b})`,
                        color: middleColor,
                        height,
                        width
                      }}
                    >
                      <Grid key={columnsAsKey + 4}
                        className="BodyGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={height}
                        onScroll={onScroll}
                        overscanColumnCount={overscanColumnCount}
                        overscanRowCount={overscanRowCount}
                        cellRenderer={this._renderBodyCell}
                        rowHeight={rowHeight}
                        rowCount={rowCount}
                        width={width}
                      />
                    </div>
                  </div>}
              </AutoSizer>
            </div>
          </div>
        );
      }}
    </ScrollSync>
    </div>
    );
  }

}

TaskAssignmentMatrix.propTypes = {
  task: PropTypes.object
};

function hexToRgb(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
    : null;
}

/**
 * Ported from sass implementation in C
 * https://github.com/sass/libsass/blob/0e6b4a2850092356aa3ece07c6b249f0221caced/functions.cpp#L209
 */
function mixColors(color1, color2, amount) {
  const weight1 = amount;
  const weight2 = 1 - amount;

  const r = Math.round(weight1 * color1.r + weight2 * color2.r);
  const g = Math.round(weight1 * color1.g + weight2 * color2.g);
  const b = Math.round(weight1 * color1.b + weight2 * color2.b);

  return { r, g, b };
}

export default TaskAssignmentMatrix;
