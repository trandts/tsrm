import React, {PropTypes} from 'react';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Chip from 'material-ui/Chip';

class ChipArray extends React.Component {

    constructor(props) {
        super(props);
        this.state = { Selected: props.Selected||[], Data: props.Data||[], Title: props.title };
        this.styles = {
            chip: {
                margin: 4,
            },
            wrapper: {
                display: 'flex',
                flexWrap: 'wrap',
            },
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleRequestDelete = this.handleRequestDelete.bind(this);
    }

    handleRequestDelete(key)
    {

        this.Selected = this.state.Selected;
        const chipToDelete = this.Selected.map((chip) => chip.key).indexOf(key);
        this.Selected.splice(chipToDelete, 1);
        this.setState({ Selected: this.Selected });
    }
    handleChange(event, index)
    {
        let $currentDisplayed = this.state.Data.filter(item => this.state.Selected.filter(e => e.key == item.key).length == 0);
        let $Selected = this.state.Selected;
        $Selected = $Selected.concat([$currentDisplayed[index]]);
        this.setState({ Selected: $Selected });
    }
    renderChip(data) {
        return (
            <Chip
                key={data.key}
                onRequestDelete={() => this.handleRequestDelete(data.key)}
                style={this.styles.chip}
            >
                {data.label}
            </Chip>
        );
    }

    render() {
        return (
            <div style={this.styles.wrapper}>
                <SelectField
                    floatingLabelText={this.state.Title}
                    fullWidth={true}
                    onChange={this.handleChange}
                >{
                        this.state.Data.filter(item => this.state.Selected.filter(e => e.key == item.key).length == 0)
                            .map((item,i) =>
                                <MenuItem key={i} 
                                value={item.key} primaryText={item.label} />
                            )
                    }
                </SelectField>
                {this.state.Selected.map(this.renderChip, this)}
            </div>
        );
    }
}


ChipArray.propTypes = {
  Selected: PropTypes.array,
  Data:PropTypes.array,
  title:PropTypes.string
};

export default ChipArray;
