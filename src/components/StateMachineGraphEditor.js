import React, { PropTypes } from 'react';
import IconButton from 'material-ui/IconButton';
import Data from '../data';
import NavigationArrowUpward from 'material-ui/svg-icons/navigation/arrow-upward';
import NavigationArrowDownward from 'material-ui/svg-icons/navigation/arrow-downward';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import Graph from 'react-graph-vis';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import $ from 'jquery';
import VirtualizedSelect from 'react-virtualized-select';
import { CardTitle } from 'material-ui/Card';
import 'vis/dist/vis-network.min.css';

class StateMachineGraphEditor extends React.Component {
  constructor(props) {
    super(props);
    const states = this.props.states;
    const defaultState = { Id: null, Logic: "AND", Events: [], Transitions: [], ChildStateMachine: states };
    const GetFlatStates = (_states) => _states.map((a) => [a].concat(GetFlatStates(a.ChildStateMachine))).reduce((a, b) => a.concat(b), []);
    const defaultStateInfo = { Activity: '', Events: [], Name: '', ActivityId: 0, ParentStateId: null, IsEnd: false, IsStart: false, Gate: 0, TaskConfiguration: '', Team: [], IsMultiInstance: false };
    this.state = {
      original: states,
      current: defaultState,
      selected: null,
      DisplayState: false,
      DisplayEdge: false,
      DisplayConfiguration:false,
      StateInfo: defaultStateInfo,
      defaultStateInfo: defaultStateInfo,
      TransitionInfo: {},
      LastUpdated:new Date(),
      currentStateConfig:"Current Level",
      SelectData: {
        AllStates: GetFlatStates(states),
        AllGates: [],
        AllTeams: [],
        AllPermissionTypes: []
      },
      Refresh: this.props.Refresh || (() => { })
    };
    this.GoIn = this.GoIn.bind(this);
    this.GoOut = this.GoOut.bind(this);
    this.AddState = this.AddState.bind(this);
    this.AddTransition = this.AddTransition.bind(this);
    this.Change = this.Change.bind(this);
    this.ChangeEvent = this.ChangeEvent.bind(this);
    this.StateClone = this.StateClone.bind(this);
  }
  componentDidMount() {
    $.ajax({
      type: 'GET', url: Data.AppDir + '/api/StateOrthogonalityLogicGate',
      success: function (gates) {
        let selectData = this.state.SelectData;
        selectData.AllGates = gates;
        this.setState({ SelectData: selectData });
      }.bind(this)
    });
    $.ajax({
      type: 'GET',
      url: Data.AppDir + '/api/Team',
      success: function (Teams) {
        let selectData = this.state.SelectData;
        selectData.AllTeams = Teams;
        this.setState({ SelectData: selectData });
      }.bind(this)
    });
    $.ajax({
      type: 'GET',
      url: Data.AppDir + '/api/PermissionType',
      success: function (PermissionType) {
        let selectData = this.state.SelectData;
        selectData.AllPermissionTypes = PermissionType;
        this.setState({ SelectData: selectData });
      }.bind(this)
    });
  }
  componentWillReceiveProps(nextProps)
  {
    const states = nextProps.states;
    const defaultState = { Id: null, Logic: "AND", Events: [], Transitions: [], ChildStateMachine: states };
    const GetFlatStates = (_states) => _states.map((a) => [a].concat(GetFlatStates(a.ChildStateMachine))).reduce((a, b) => a.concat(b), []);
    const defaultStateInfo = { Activity: '', Events: [], Name: '', ActivityId: 0, ParentStateId: null, IsEnd: false, IsStart: false, Gate: 0, TaskConfiguration: '', Team: [], IsMultiInstance: false };
    const flattenedState=GetFlatStates(states);
    this.setState({
      original: states,
      current: flattenedState.filter(e=>e.Id==(this.state.current||{}).Id||0)[0]||defaultState,
      defaultStateInfo: defaultStateInfo,
      LastUpdated:new Date(),
      SelectData: {
        AllStates: flattenedState,
        AllGates: this.state.SelectData.AllGates,
        AllTeams: this.state.SelectData.AllTeams
      }
    });
  }
  Change(obj, prp, value) {
    let _obj = this.state[obj];
    _obj[prp] = value || '';
    this.setState({ [obj]: _obj });
  }
  ChangeEvent(i, val) {
    let SI = this.state.StateInfo;
    SI.Events[i] = val;
    this.setState({ StateInfo: SI });
  }
  GoIn() {
    let nextObj = (this.state.current.ChildStateMachine.filter(e => e.Id == this.state.selected)[0] || {});
    if ((nextObj.ChildStateMachine || []).length > 0) {
      this.setState({ current: nextObj, selected: null });
    }
  }
  GoOut() {
    let Set = function (o, Id) {
      if (o.ChildStateMachine.filter(s => s.Id == Id).length == 0) {
        o.ChildStateMachine.map(e => Set(e, Id));
      } else {
        this.setState({ current: o });
      }
    }.bind(this);
    this.state.original.map(e => Set(e, this.state.current.Id));
  }
  AddState() {
    $.ajax({
      type: 'POST',
      url: Data.AppDir + '/api/State',
      data: this.state.StateInfo,
      success: function () {
        this.state.Refresh();
        this.setState({ DisplayState: false ,StateInfo:this.state.defaultStateInfo});
      }.bind(this)
    });
  }
  AddTransition()
  {
    let TI=JSON.parse(JSON.stringify(this.state.TransitionInfo));
    TI.FromState=this.state.selected;
    $.ajax({
      type: 'POST',
      url: Data.AppDir + '/api/Transition',
      data: TI,
      success: function () {
        this.state.Refresh();
        this.setState({ DisplayTransition: false,TransitionInfo:{} });
      }.bind(this)
    });
  }
  StateClone(){
    const{selected}=this.state;
    if(selected)
      {
        $.ajax({
          type: 'POST',
          url: Data.AppDir + '/api/CloneState',
          data: { User: ($('#User').attr('value')||0) ,SelectedState:selected },
          success: this.state.Refresh
        });
      }else{
        alert('No State Selected');
      }
  }
  render() {
    const { current, selected, DisplayState, DisplayTransition,DisplayConfiguration,DisplaySchemaExecutor,DisplayGateConfiguration,DisplayTeamConfiguration, DisplayStateConfiguration,DisplayPermissionConfiguration,SelectData 
    ,currentStateConfig} = this.state;
    const { Name, ParentStateId, IsEnd, IsStart, Gate, TaskConfiguration, Activity, Team, Events, IsMultiInstance } = this.state.StateInfo;
    const { TName,Event,ToState} = this.state.TransitionInfo;

    const buttonStyle = { margin: 5 };

    const OnAddEvent = function () {
      let si = this.state.StateInfo;
      si.Events[si.Events.length] = null;
      this.setState({ StateInfo: si });
    }.bind(this);

    const StateDialog = (<Dialog title="State"
      actions={[<RaisedButton  key={0} label="Add State"
        onClick={this.AddState}
        primary={true} />]}
      modal={false}
      open={DisplayState||false}
      onRequestClose={() => this.setState({ DisplayState: false })}
      autoScrollBodyContent={true}
    >
      <div className="row">
        <div className="col-md-6">
          <TextField
            floatingLabelText="Name"
            value={Name || ''}
            onChange={(e, v) => this.Change("StateInfo", "Name", v)}
          /></div>
        <div className="col-md-6">
          <TextField
            floatingLabelText="Task Name"
            value={TaskConfiguration || ''}
            onChange={(e, v) => this.Change("StateInfo", "TaskConfiguration", v)}
          />
        </div>
        <div className="col-md-6">

          <SelectField floatingLabelText="Parent State"
            value={ParentStateId}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }}
            disabled={true}>
            {SelectData.AllStates.map(e => <MenuItem key={e.Id} value={e.Id} primaryText={e.Name} />)}
          </SelectField>
        </div>
        <div className="col-md-6">
          <SelectField floatingLabelText="Gate"
            value={Gate}
            onChange={(e, k, v) => this.Change("StateInfo", "Gate", v)}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }}>
            {SelectData.AllGates.map(e => <MenuItem key={e.Id} value={e.Id} primaryText={e.Name + ' (' + (e.Name == 'XOR' ? 'Sequential' : 'Parallel') + ')'} />)}
          </SelectField>
        </div>
        <div className="col-md-6">
          <Checkbox
            label="Is Start"
            style={{ marginTop: 25 }}
            checked={IsStart}
            onCheck={(e, value) => this.Change("StateInfo", "IsStart", !value)}
          />
        </div>
        <div className="col-md-6">
          <Checkbox
            label="Is End"
            style={{ marginTop: 25 }}
            checked={IsEnd}
            onCheck={(e, value) => this.Change("StateInfo", "IsEnd", !value)}
          />
        </div>
        <div className="col-md-6">
          <Checkbox
            label="Is MultiInstance"
            style={{ marginTop: 25 }}
            checked={IsMultiInstance}
            onCheck={(e, value) => this.Change("StateInfo", "IsMultiInstance", !value)}
          />
        </div>
        <div className="col-md-6">
          <VirtualizedSelect
            options={SelectData.AllTeams}
            onChange={(selectValue) => this.Change("StateInfo", "Team", selectValue)}
            multi={true}
            labelKey="Name"
            valueKey="Id"
            style={{ color: 'black' }}
            value={Team}
          />
        </div>
        <div className="col-md-6">
          <TextField
            floatingLabelText="UI Name"
            value={Activity || ''}
            onChange={(e, v) => this.Change("StateInfo", "Activity", v)}
          />
        </div>
        <div className="col-md-offset-4 col-md-6">
          <RaisedButton label="Add Events"
            onClick={OnAddEvent}
            primary={true} />

          {Events.map((ev, i) => <TextField key={i}
            floatingLabelText={"Event Name " + (i + 1)}
            value={ev || ''}
            onChange={(e, v) => this.ChangeEvent.bind(this, i)(v)}
          />)}
        </div>
      </div>
    </Dialog>);


    const TransitionDialog = (<Dialog title="Transition"
      actions={[<RaisedButton key={0} label="Add Transition"
        onClick={this.AddTransition}
        primary={true} />]}
      modal={false}
      open={DisplayTransition && selected||false}
      onRequestClose={() => this.setState({ DisplayTransition: false })}
      autoScrollBodyContent={true}
    >
      <div className="row">
        <div className="col-md-6">
          <TextField
            floatingLabelText="Name"
            value={TName || ''}
            onChange={(e, v) => this.Change("TransitionInfo", "TName", v)}
          /></div>
        <div className="col-md-6">
          <SelectField floatingLabelText="On Event"
            value={Event}
            onChange={(e, k, v) => this.Change("TransitionInfo", "Event", v)}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }}>
            {current.ChildStateMachine.filter(e=>e.Id==selected).map(sm=>sm.Events.map(e => <MenuItem key={e} value={e} primaryText={e} />))[0]}
          </SelectField>
        </div>
        <div className="col-md-6">

          <SelectField floatingLabelText="From State"
            value={selected}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }}
            disabled={true}>
            {SelectData.AllStates.map(e => <MenuItem key={e.Id} value={e.Id} primaryText={e.Name} />)}
          </SelectField>
        </div>
        
        <div className="col-md-6">
          <SelectField floatingLabelText="To State"
            value={ToState}
            onChange={(e, k, v) => this.Change("TransitionInfo", "ToState", v)}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }}>
            {current.ChildStateMachine.
            map(e => <MenuItem key={e.Id} value={e.Id} primaryText={e.Name} />)}
          </SelectField>
        </div>
      </div>
    </Dialog>);
const transformData=(_c,_i)=>_c?{
  Id:_c.Id,
  Name:_c.Name,
  Logic:_c.LogicId,
  ParentStateId:_c.ParentStateId,
  IsStart:_c.IsStart,
  IsEnd:_c.IsEnd,
  Transitions:_c.Transitions,
  ChildStateMachine:(_c.ChildStateMachine||[]).filter(()=>_i>0 ||_i< 0).map(e=>transformData(e,_i-1)),
  _Teams:_c.Teams,
  _Events:_c.Events,  
  _IsMultiInstance:_c.IsMultiInstance,
  _Case:null,
  _ChildDataStateMachine:[]
}:null;
let currentStateData=null;
switch(currentStateConfig)
{
  case'Current Level':
  currentStateData=transformData(current.Id?current.ChildStateMachine.filter(e=>e.Id== selected)[0]||current:current.ChildStateMachine[0],2);
  break;
  case'All Levels':
  currentStateData=transformData(this.state.original[0]||null,-1);
  break;
  case'All Lower Levels':
  currentStateData=transformData(current.Id?current.ChildStateMachine.filter(e=>e.Id== selected)[0]||current:current.ChildStateMachine[0],-1);
  
  break;
}


const ConfigurationGate=(<div className="col-md-12">{DisplayGateConfiguration?<code><pre>
{JSON.stringify(SelectData.AllGates.map(e=>({Id:e.Id,Name:e.Name})), null, 2)}
  </pre></code>:null}</div>);
const ConfigurationTeam=(<div className="col-md-12">{DisplayTeamConfiguration?<code><pre>
    {JSON.stringify(SelectData.AllTeams.map(e=>({Id:e.Id,Name:e.Name})), null, 2)}
      </pre></code>:null}</div>);
const ConfigurationState=(<div className="col-md-12">{DisplayStateConfiguration?<code><pre>
        {JSON.stringify(currentStateData, null, 2)}
          </pre></code>:null}</div>);
 const ConfigurationPermission=(<div className="col-md-12">{DisplayPermissionConfiguration?<code><pre>
            {JSON.stringify(SelectData.AllPermissionTypes.map(e=>({Id:e.Id,Name:e.Name})), null, 2)}
              </pre></code>:null}</div>);
 const SchemaExecutorDialog=(<Dialog title="Display Schema Executor"
  actions={[]}
  modal={false}
  open={DisplaySchemaExecutor||false}
  onRequestClose={() => this.setState({ DisplaySchemaExecutor: false })}
  autoScrollBodyContent={true}
 >
 <div className="row">
   <div className="col-md-12">
   123
   </div>
 </div>
 </Dialog>);//DisplaySchemaExecutor
const ConfigurationDialog = (<Dialog title="Configuration"
actions={[]}
modal={false}
open={DisplayConfiguration}
onRequestClose={() => this.setState({ DisplayConfiguration: false })}
autoScrollBodyContent={true}
>
<div className="row">
  <div className="col-md-12">
  <RaisedButton label="Logic" style={buttonStyle}
  onClick={()=>this.setState({ DisplayGateConfiguration:!DisplayGateConfiguration})}
  primary={true} />
  <RaisedButton label="Teams" style={buttonStyle}
  onClick={()=>this.setState({ DisplayTeamConfiguration:!DisplayTeamConfiguration})}
  primary={true} />
  <RaisedButton label="Permission" style={buttonStyle}
  onClick={()=>this.setState({ DisplayPermissionConfiguration:!DisplayPermissionConfiguration})}
  primary={true} />
  <RaisedButton label="State" style={buttonStyle}
  onClick={()=>this.setState({ DisplayStateConfiguration:!DisplayStateConfiguration})}
  primary={true} />
  <SelectField value={currentStateConfig} floatingLabelText="State"
            onChange={(e, k, v) => this.setState({currentStateConfig:v})}
            style={{ color: 'rgba(0, 0, 0, 0.5)' }} >
  <MenuItem key={0} value={"Current Level"} primaryText={"Current Level"} />
  <MenuItem key={1} value={"All Levels"} primaryText={"All Levels"} />
  <MenuItem key={2} value={"All Lower Levels"} primaryText={"All Lower Levels"} />
  </SelectField>
  </div>
  {ConfigurationGate}
  {ConfigurationTeam}
  {ConfigurationState}
  {ConfigurationPermission}
</div>
</Dialog>);
/* let _Nodes = current.ChildStateMachine.map(e => ({
      id: e.Id, label: e.Name, title: 'abc'//e.ContextType
      , color: { background: (e.IsStart ? "pink" : "") + (e.IsEnd ? "orange" : ""),
      border: e.ChildStateMachine.length > 0 ?"green":'#2B7CE9',
      highlight:{ background: (e.IsStart ? "pink" : "") + (e.IsEnd ? "orange" : ""), }
     }
     ,borderWidth :e.ChildStateMachine.length > 0 ?3:1
     // , shape: e.ChildStateMachine.length > 0 ? 'database' : 'ellipse'
      , font: e.ChildStateMachine.length > 0 ? { size: 10 } : { size: 14 }
      , shadow: e.IsMultiInstance ? { enabled: true, x: 10, y: 10 } : false
    }));
    let _Edges = current.ChildStateMachine.map(c => c.Transitions.map(e => ({ from: e.FromStateId, to: e.ToStateId, label: e.Event }))).reduce((a, b) => a.concat(b), []);
    let options2 = {
      height: '400px',//'100%',
      width: '100%',
      //physics: false,
      layout: {
        improvedLayout: true,
      },
      edges: {
        color: "#000000",
        length: 200,
        smooth: {
          enabled: true,
          type: "dynamic",
          roundness: 0.5
        }
      },
       interaction:{ hover:true},
      manipulation: {
				enabled: false//use this in future
			}
    };
*/
    let _startNodeColor="pink";
    let _endNodeColor="orange";
    let _specialNodeColor="Lime";
    let _NodeStateMachineHighlightColor="green";
    let _Nodes =current.ChildStateMachine.map(e => ({
      id: e.Id, label: e.Name, title: e.ContextType
      , color: { background: e.ContextType?(e.IsStart ?_startNodeColor : "") + (e.IsEnd ?_endNodeColor : ""):_specialNodeColor,
      border: e.ChildStateMachine.length > 0 ?_NodeStateMachineHighlightColor:'#2B7CE9',
      highlight:{ background: e.ContextType? (e.IsStart ? _startNodeColor : "") + (e.IsEnd ? _endNodeColor: ""):_specialNodeColor, }
     }
     ,borderWidth :e.ChildStateMachine.length > 0 ?3:1
     // , shape: e.ChildStateMachine.length > 0 ? 'database' : 'ellipse'
      , font: e.ChildStateMachine.length > 0 ? { size: 10 } : { size: 14 }
      , shadow: e.IsMultiInstance ? { enabled: true, x: 10, y: 10 } : false
    }));
    let _Edges = current.ChildStateMachine.map(c => c.Transitions.map(e => ({ from: e.FromStateId, to: e.ToStateId, label: e.Event }))).reduce((a, b) => a.concat(b), []);

    let graph = {
      nodes: _Nodes,
      edges: _Edges,
    };

    let options2 = {
      height: '400px',//'100%',
      width: '100%',
      //physics: false,
      layout: {
        improvedLayout: true,
      },
      edges: {
        color: "#000000",
        length: 200,
        smooth: {
          enabled: true,
          type: "dynamic",
          roundness: 0.5
        }
      },
       interaction:{ hover:true},
      manipulation: {
				enabled: false//use this in future
			}
    };

    let events = {
      selectNode: function ({ nodes }) {
        if (nodes.length > 0) {
          this.setState({ selected: nodes[0] });
        }
      }.bind(this),
      deselectNode: function () {
        this.setState({ selected: null });
      }.bind(this),
      doubleClick: function ({ nodes }) {
        if (nodes.length > 0) {
          this.setState({ selected: nodes[0] });
          this.GoIn();
        }
      }.bind(this)
    };
    let OnStatePopup = function () {
      let _obj = JSON.parse(JSON.stringify(this.state.defaultStateInfo));
      _obj.ParentStateId = this.state.selected || this.state.current.Id || this.state.current.ChildStateMachine[0].Id;
      //Array.apply(null, {length: 15}).map(Number.call, Number).map(e=>"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(Math.floor(Math.random() * 52))).reduce((a,b)=>a+b,"");
      this.setState({ DisplayState: true, StateInfo: _obj });
    }.bind(this);
    //const BreadCrum = "Application / Trace";
    const BreadCrum=(Id)=>Id?BreadCrum(SelectData.AllStates.filter(e=>e.Id==Id)[0].ParentStateId)+"/"+SelectData.AllStates.filter(e=>e.Id==Id)[0].Name:"";
    return (
      <div>
        <Paper >
        <CardTitle title="" subtitle={BreadCrum(this.state.selected || this.state.current.Id || this.state.current.ChildStateMachine[0].Id)} />
         <AppBar title={current.Name + " (" + (current.Logic == 'XOR' ? "Sequential" : "Parallel") + ")"} iconElementLeft={<div />} />
          <div className="row">
            <div className="col-md-12 col-sm-4" >
              <RaisedButton label="Add State" style={buttonStyle}
                onClick={OnStatePopup}
                primary={true} />
              <RaisedButton label="Add Transition" style={buttonStyle}
                onClick={() => this.setState({ DisplayTransition: true })}
                primary={true} />
                <RaisedButton label="Display Configuration" style={buttonStyle}
                onClick={() => this.setState({ DisplayConfiguration: true })}
                primary={true} />
                <RaisedButton label="Schema Executor" style={buttonStyle}
                onClick={() => this.setState({ DisplaySchemaExecutor: true })}
                primary={true} />
                <RaisedButton label="Clone State" style={buttonStyle}
                onClick={this.StateClone}
                primary={true} />
            </div>
          </div>
          <div style={{ display: 'flex', minHeight: 48 }}>
            <div style={{ width: '50%' }}>
              {current.Id ? <IconButton tooltip="Back" style={{ float: 'left' }}
                onClick={this.GoOut} >
                <NavigationArrowUpward style={{ margin: 5 }} />
              </IconButton> : null}
            </div>
            <div style={{ width: '50%' }}>
              {selected && current.ChildStateMachine.filter(e => e.Id == selected).map(e => e.ChildStateMachine.length > 0)[0] ? <IconButton tooltip="Next" onClick={this.GoIn} style={{ float: 'right' }}>
                <NavigationArrowDownward style={{ margin: 5 }} />
              </IconButton> : null}
            </div>
          </div>
          <Graph graph={graph} key={this.state.LastUpdated.toString()} options={options2} events={events} />
        </Paper>
        {StateDialog}
        {TransitionDialog}
        {ConfigurationDialog}
        {SchemaExecutorDialog}
      </div>
    );
  }


}

export default StateMachineGraphEditor;

StateMachineGraphEditor.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  onNodeSelect: PropTypes.func,
  onNodeDeSelect: PropTypes.func,
  Refresh: PropTypes.func,
  states: PropTypes.array,
};
