import React, { PropTypes } from 'react';
import $ from "jquery";
import ProjectCreation from '../uielements/ProjectCreation';
import ProjectCreated from '../uielements/ProjectCreated';
import ProjectWaitingForApproval from '../uielements/ProjectWaitingForApproval';
import ProjectCompleted from '../uielements/ProjectCompleted';
import TaskCreate from '../uielements/TaskCreate';
import TaskCreated from '../uielements/TaskCreated';
import TaskProgressed from '../uielements/TaskProgressed';
import TaskClose from '../uielements/TaskClose';
import ProjectApproved from '../uielements/ProjectApproved';
import OrganizationCreation from '../uielements/OrganizationCreation';
import ProjectUpdate from '../uielements/ProjectUpdate';
import NoteAdd from '../uielements/NoteAdd';
import AnotherNoteAdd from '../uielements/AnotherNoteAdd';
import ProjectResourceAssign from '../uielements/ProjectResourceAssign';
import AssignProjectTask from '../uielements/AssignProjectTask';
let components = {
  "AssignProjectTask":AssignProjectTask,
  "ProjectResourceAssign":ProjectResourceAssign,
  "ProjectCreation": ProjectCreation,
  "ProjectCreated": ProjectCreated,
  "ProjectWaitingForApproval": ProjectWaitingForApproval,
  "ProjectApproved": ProjectApproved,
  "ProjectCompleted": ProjectCompleted,
  "TaskCreated": TaskCreated,
  "TaskProgressed": TaskProgressed,
  "TaskClose": TaskClose,
  "TaskCreate": TaskCreate,
  "OrganizationCreation":OrganizationCreation,
  "ProjectUpdate":ProjectUpdate,
  "NoteAdd":NoteAdd,
  "AnotherNoteAdd":AnotherNoteAdd
};

class WorkflowInitiate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Workflow: this.props.Workflow,
      Instance: this.props.Instance,
      WfInfo: [],
      WorkflowInitated: false
    };
    this.EventCallback = this.EventCallback.bind(this);
  }
  componentDidMount() {
    if (this.state.Workflow > 0) {
      $.ajax({
        url: '/api/Workflow/' + this.state.Workflow,
        type: 'GET',
        success: function ($data) {
          this.setState({ WfInfo: $data });
        }.bind(this)
      });
    }
  }
  EventCallback(StateId,ParentStateInstanceId,Context, Event) {
    $.ajax({
      url: '/api/WorkflowInstanceCreate',
      type: 'POST',
      data: { ParentStateInstanceId: ParentStateInstanceId, Event: Event, Context: Context, User: $("#User").attr("value") ,StateId:this.state.Workflow
      ,ActualStateId:StateId},
      success: function ($data) {
        this.setState({ WorkflowInitated: $data });
      }.bind(this)
    });
  }
  render() {
    if (this.state.Workflow == 0) return null;
    const { WfInfo, WorkflowInitated ,Instance} = this.state;
    let UIElementComponents = WfInfo.filter(e=>!e.IsChildMultiInstance)
    .map(e => ({ StateId: e.StateId,
       Component: components[e.UIElement] || null,
       ParentStateInstanceId:e.ParentStateInstanceId||Instance||0,
       ParentStateId:e.ParentStateId
       }))
      .map((e, i) => e.Component ?
        <e.Component key={i} Workflow={this.props.Workflow}
          Context={this.props.Context || null} EventCallback={this.EventCallback.bind(this, e.StateId,
          e.ParentStateInstanceId)} /> :
        <div>Component Not Found:{e.UIElement}</div>
      );

    return (<div>{WorkflowInitated ? null : <div>
      {UIElementComponents}</div>}</div>);
  }
}

WorkflowInitiate.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
  Workflow: PropTypes.number,
  Instance:PropTypes.number
};

export default WorkflowInitiate;
