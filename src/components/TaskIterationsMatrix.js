import React, { PropTypes } from 'react';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import Grid from 'react-virtualized/dist/commonjs/Grid';
import ScrollSync from 'react-virtualized/dist/commonjs/ScrollSync';
import cn from "classnames";
import Data from '../data';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import IconButton from 'material-ui/IconButton';
import Slider from 'material-ui/Slider';
import CommunicationComment from 'material-ui/svg-icons/communication/comment';
import ActionNoteAdd from 'material-ui/svg-icons/action/note-add';
import {  blue500  }from 'material-ui/styles/colors';

/*
const LEFT_COLOR_FROM = hexToRgb("#471061");
const LEFT_COLOR_TO = hexToRgb("#BC3959");
const TOP_COLOR_FROM = hexToRgb("#000000");
const TOP_COLOR_TO = hexToRgb("#333333");

const LEFT_COLOR_FROM = hexToRgb("#4DB6AC");
const LEFT_COLOR_TO = hexToRgb("#CDDC39");
const TOP_COLOR_FROM = hexToRgb("#263238");
const TOP_COLOR_TO = hexToRgb("#81C784");
*/
const LEFT_COLOR_FROM = hexToRgb("#ffffff");
const LEFT_COLOR_TO = hexToRgb("#e1f4c3");
const TOP_COLOR_FROM = hexToRgb("#e1f4c3");
const TOP_COLOR_TO = hexToRgb("#81C784");


const iconStyles = {
  outer: {
    padding: 0,
    height: 15,
    width: 15
  },
  inner: {
    padding: 0,
    height: 15,
    width: 15
  }
};

function getColumns(Project) {
  return Array.from(new Set(["Task"].concat(
    ((Project.map(p=>p.iterations||[])))
    .reduce((a,b)=>a.concat(b),[]).map(e=>e.name)
    )));
}
function getTasks(Projects,columns) {
  if (!Projects) return [];
  let arr=[];
  for(let projectIndex=0;projectIndex<(Projects||[]).length;projectIndex++)
  {
      let Proj=Projects[projectIndex];
      for(let taskIndex=0;taskIndex<Proj.tasks.length;taskIndex++)
      {
        let task=Proj.tasks[taskIndex];
        let obj = {
          attached: {
            Project: Proj,
            Task: task,
            current:null
          },
          value:null
        };
        let rowValues=[];
        for(let columnIndex=0;columnIndex<columns.length;columnIndex++)
        {
          let columnName=columns[columnIndex];
          let cell = Object.assign({}, obj);
          if(columnName=="Task")
          {
            cell.value=cell.attached.Task.name;
          } else if(columnName=="Project")
          {
            cell.value=cell.attached.Project.name;
          }else if(columnName=="users"||columnName=="Approver")
          {
            cell.value="User";
          }else{
           let catId=Data.taskMatrix.tagCategories.filter(e => e.name==columnName).map(e=>e.id)[0]||0;
           let tag=Data.taskMatrix.tags.filter(e=>(cell.attached.Task.tags||[]).indexOf(e.id)>-1 && e.category==catId)[0];
           cell.value=tag?tag.name:null;
          }
          cell.column=columnName;
          rowValues[rowValues.length]=cell;
        }
        arr[arr.length]=rowValues;
      }
  }
  const sortedArr = arr;
  return sortedArr;
}


class TaskIterationsMatrix extends React.Component {
  constructor(props) {
    super(props);
    let columns=getColumns(this.props.currentProject||[]);
    this.state = {
      columnWidth: 175,
      height: 300,
      overscanColumnCount: 0,
      overscanRowCount: 5,
      rowHeight: 55,
      filter: this.props.filter || [],
         currentProject: this.props.currentProject||[],

       Columns: columns,
      Tasks: getTasks(this.props.currentProject||[],columns,this.props.sort),
       Sort:this.props.sort,
       notesMissingLength:1
    };
    this._renderBodyCell = this._renderBodyCell.bind(this);
    this._renderHeaderCell = this._renderHeaderCell.bind(this);
    this._renderLeftSideCell = this._renderLeftSideCell.bind(this);
    this._renderLeftHeaderCell = this._renderLeftHeaderCell.bind(this);

  }
  componentWillReceiveProps(newProps) {
    
   let columns=getColumns(newProps.currentProject);
    this.setState({
      filter: newProps.filter,
      currentProject: newProps.currentProject
       , Columns: columns,
         Tasks: getTasks(newProps.currentProject, columns, newProps.sort)
      , Sort: newProps.sort
    });
  }
  _renderBodyCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < 1) {
      return;
    }

    return this._renderLeftSideCell({ columnIndex, key, rowIndex, style });
  }

  _renderHeaderCell({ columnIndex, key, rowIndex, style }) {
    if (columnIndex < 1) {
      return;
    }

    return this._renderLeftHeaderCell({ columnIndex, key, rowIndex, style });
  }

  _renderLeftHeaderCell({ columnIndex, key, style }) {
     return (<div className="headerCell" key={key} style={style}>
       <div> {`${this.state.Columns[columnIndex]}`}</div>
       <div><IconButton tooltipPosition="top-right" tooltip="Add Note" style={{padding:0,height:24,width:24}}
         onClick={() => this.setState({ AddNoteDialog: true })}  >
          <CommunicationComment color={blue500} />
        </IconButton></div>
      </div>);
  }

  _renderLeftSideCell({ columnIndex, key, rowIndex, style }) {
    const rowClass =
      rowIndex % 2 === 0
        ? columnIndex % 2 === 0 ? "evenRow" : "oddRow"
        : columnIndex % 2 !== 0 ? "evenRow" : "oddRow";
    const classNames = cn(rowClass, "cell");
    //{(task[this.state.Columns[columnIndex]]||[]).toLocaleString()}
    //catTags.filter(e => (task.tags || []).indexOf(e.id) > -1).map(e => e.id)[0] || 0
   /* <VirtualizedSelect
      options={iterations}
      onChange={(selectValue) => this.setState({ selectValue })}
      labelKey="name"
      valueKey="id"
      multi={true}
      value={iteration} />*/
    let cell =this.state.Tasks[rowIndex][columnIndex];
    
    if (cell.column == "Task"||cell.column == "Project") {
      return (<div className={classNames} key={key} style={style}>
        {cell.value}
      </div>);
    }
      
      return (<div className={classNames} key={key} style={style}>
      <Checkbox label="" style={{width:'75%'}} />
      </div>);
    
    // Data.taskMatrix.tagCategories.
    
  }
  render() {

    const {
      columnWidth,
      height,
      overscanColumnCount,
      overscanRowCount,
      rowHeight,
    } = this.state;

    const rowCount = (this.state.Tasks || []).length;
    const columnCount = this.state.Columns.length;
    return (<div><ScrollSync>
      {({
        onScroll,
        scrollLeft,
        scrollTop,
          }) => {
        const x = 0;//scrollLeft / (scrollWidth - clientWidth);
        const y = 0;//scrollTop / (scrollHeight - clientHeight);

        const leftBackgroundColor = mixColors(
          LEFT_COLOR_FROM,
          LEFT_COLOR_TO,
          y
        );
        //        const leftColor = "#ffffff";
        const leftColor = "#000000";
        const topBackgroundColor = mixColors(
          TOP_COLOR_FROM,
          TOP_COLOR_TO,
          x
        );
        // const topColor = "#ffffff";
        const topColor = "#000000";
        const middleBackgroundColor = mixColors(
          leftBackgroundColor,
          topBackgroundColor,
          0.5
        );
        // const middleColor = "#ffffff";
        const middleColor = "#000000";
        let columnsAsKey = this.state.Columns.toString();
        return (
          <div className="GridRow">
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                color: leftColor,
                backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`
              }}
            >
              <Grid key={columnsAsKey}
                cellRenderer={this._renderLeftHeaderCell}
                className="HeaderGrid"
                width={columnWidth * 1}
                height={rowHeight}
                rowHeight={rowHeight}
                columnWidth={columnWidth}
                rowCount={1}
                columnCount={1}
              />
            </div>
            <div
              className="LeftSideGridContainer"
              style={{
                position: "absolute",
                left: 0,
                top: rowHeight,
                color: leftColor,
                backgroundColor: `rgb(${leftBackgroundColor.r},${leftBackgroundColor.g},${leftBackgroundColor.b})`
              }}
            >
              <Grid
                overscanColumnCount={overscanColumnCount}
                overscanRowCount={overscanRowCount}
                cellRenderer={this._renderLeftSideCell}
                columnWidth={columnWidth}
                columnCount={1}
                className="LeftSideGrid"
                height={height}
                rowHeight={rowHeight}
                rowCount={rowCount}
                scrollTop={scrollTop}
                width={columnWidth * 1}
              />
            </div>
            <div className="GridColumn">
              <AutoSizer disableHeight>
                {({ width }) =>
                  <div>
                    <div
                      style={{
                        backgroundColor: `rgb(${topBackgroundColor.r},${topBackgroundColor.g},${topBackgroundColor.b})`,
                        color: topColor,
                        height: rowHeight,
                        width: width
                      }}
                    >
                      <Grid key={columnsAsKey + 2}
                        className="HeaderGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={rowHeight}
                        overscanColumnCount={overscanColumnCount}
                        cellRenderer={this._renderHeaderCell}
                        rowHeight={rowHeight}
                        rowCount={1}
                        scrollLeft={scrollLeft}
                        width={width}
                      />
                    </div>
                    <div
                      style={{
                        backgroundColor: `rgb(${middleBackgroundColor.r},${middleBackgroundColor.g},${middleBackgroundColor.b})`,
                        color: middleColor,
                        height,
                        width
                      }}
                    >
                      <Grid key={columnsAsKey + 3}
                        className="BodyGrid"
                        columnWidth={columnWidth}
                        columnCount={columnCount}
                        height={height}
                        onScroll={onScroll}
                        overscanColumnCount={overscanColumnCount}
                        overscanRowCount={overscanRowCount}
                        cellRenderer={this._renderBodyCell}
                        rowHeight={rowHeight}
                        rowCount={rowCount}
                        width={width}
                      />
                    </div>
                  </div>}
              </AutoSizer>
            </div>
          </div>
        );
      }}
    </ScrollSync>
      <Dialog title="Add Note"
        actions={[
          <FlatButton key={0}
            label="Cancel"
            primary={true}
            onClick={() => this.setState({ AddNoteDialog: false })}
          />,
          <FlatButton key={1}
            label="Submit"
            primary={true}
            keyboardFocused={true}
            onClick={() => this.setState({ AddNoteDialog: false })}
          />,
        ]}
        modal={false}
        open={this.state.AddNoteDialog || false}
         contentStyle={{  width: '65%',  maxWidth: 'none'}}
      ><div>{Array(this.state.notesMissingLength).fill().map((_, i) => i)
        .map((e, _i) => <div key={_i} style={{display:'flex'}}>
          <TextField key={1} floatingLabelText="Notes" multiLine={true} rows={2} />
          <TextField key={2} floatingLabelText="Delieverable" multiLine={true} rows={2} />
          <SelectField floatingLabelText="Delieverable Type"
            value={this.state["notesDelieverableType" + _i]}
            onChange={(e, i, v) => this.setState({ ["notesDelieverableType" + _i]: v })}>
            <MenuItem value={1} key={1} primaryText="Google Sheet" />
            <MenuItem value={2} key={2} primaryText="Code Reference" />
            <MenuItem value={3} key={3} primaryText="Meeting Notes" />
            <MenuItem value={4} key={4} primaryText="Others" />
          </SelectField>
        </div>)}
          <IconButton tooltipPosition="top-right" tooltip="Notes" style={iconStyles.outer} iconStyle={iconStyles.inner}
            onClick={() => this.setState({ notesMissingLength: this.state.notesMissingLength + 1 })}
          >
            <ActionNoteAdd color={blue500} />
          </IconButton>
          <DatePicker hintText={"Notes Date"} defaultDate={new Date()} />
          <Slider style={{ width: '90%' }}
            min={0}
            max={100}
            defaultValue={50}
          />
        </div>
      </Dialog>
    </div>);
  }
}



TaskIterationsMatrix.propTypes = {
  /**
   * The axis on which the slider will slide.
   */
  filter: PropTypes.array,
  /**
   * The default value of the slider.
   */
  currentProject: PropTypes.array,
  sort:PropTypes.array,
};

function hexToRgb(hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
    : null;
}

/**
 * Ported from sass implementation in C
 * https://github.com/sass/libsass/blob/0e6b4a2850092356aa3ece07c6b249f0221caced/functions.cpp#L209
 */
function mixColors(color1, color2, amount) {
  const weight1 = amount;
  const weight2 = 1 - amount;

  const r = Math.round(weight1 * color1.r + weight2 * color2.r);
  const g = Math.round(weight1 * color1.g + weight2 * color2.g);
  const b = Math.round(weight1 * color1.b + weight2 * color2.b);

  return { r, g, b };
}


export default TaskIterationsMatrix;
