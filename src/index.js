/* eslint-disable import/default */

import React from 'react';
import { render } from 'react-dom';
import {Router, browserHistory } from 'react-router';
//import { Router, hashHistory } from 'react-router';
import Routes from './routes';
import injectTapEventPlugin from 'react-tap-event-plugin';
require('./favicon.ico');
import './styles.scss';
import 'font-awesome/css/font-awesome.css';
import 'flexboxgrid/css/flexboxgrid.css';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
//import 'react-virtualized/styles.css';

injectTapEventPlugin();
render(
    <Router routes={Routes} history={browserHistory} />, document.getElementById('app')
);
