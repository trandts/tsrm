import React, { PropTypes } from 'react';
import withWidth from 'material-ui/utils/withWidth';

class Shell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          WorkflowInstance: this.props.WorkflowInstance,
          EventBus:this.props.EventBus
        };
        this.state.EventBus.on("ShellReload",()=>this.state.EventBus.emit("StateMachineReload",this.state.WorkflowInstance));
    }

    render() {
        return (
        <div>
        {this.props.children} 
        </div>);
    }
}
Shell.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object,
    children:PropTypes.element
};

export default withWidth()(Shell);



