import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';

class ProjectCreated extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Name: '',
            EstimatedStartDate: null,
            EstimatedEndDate: null,
            ActualStartDate: null,
            ActualEndDate: null,
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.LoadData=this.LoadData.bind(this);
    }
    componentDidMount() {
        const {EventBus,Context,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.LoadData(d));
        let url='/api/ProjectCreated/'+Context;
      //  EventBus.Register([WorkflowInstance,"EventsLoaded"],function(){
          //  const {EventBus,Context,WorkflowInstance}=this.state;
         //   EventBus.off([WorkflowInstance,"EventsLoaded"]);
            EventBus.emit([WorkflowInstance,"OnInitializeData"],null,url,'GET');
       // }.bind(this));
         
    }
    componentWillUnmount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        EventBus.off([WorkflowInstance,"EventsLoaded"]);
        EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    LoadData($data){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        $data.EstimatedStartDate=$data.EstimatedStartDate?new Date($data.EstimatedStartDate):null;
        $data.EstimatedEndDate=$data.EstimatedEndDate?new Date($data.EstimatedEndDate):null;
        $data.ActualStartDate=$data.ActualStartDate?new Date($data.ActualStartDate):null;
        $data.ActualEndDate=$data.ActualEndDate?new Date($data.ActualEndDate):null;
        this.setState($data);
    }
    render() {
        const { WorkflowInstance,Context, Name, EstimatedStartDate, EstimatedEndDate, ActualStartDate, ActualEndDate,EventBus } = this.state;
        const style={ margin:15};
        return (<div>
            <TextField floatingLabelText="Name" disabled={true} value={Name} />
            <DatePicker hintText="Estimated Start Date" disabled={true} value={EstimatedStartDate} />
            <DatePicker hintText="Estimated End Date" disabled={true} value={EstimatedEndDate}/>
            <DatePicker hintText="Actual Start Date" disabled={true} value={ActualStartDate}/>
            <DatePicker hintText="Actual End Date" disabled={true} value={ActualEndDate}  />
            <RaisedButton label="Request More Info" style={style} primary={true} onClick={()=>EventBus.emit([WorkflowInstance,"OnRequestMoreInfo"],Context)} />
            <RaisedButton label="Approved" style={style} primary={true} onClick={()=>EventBus.emit([WorkflowInstance,"OnApproved"],Context)} />
            <RaisedButton label="Rejected" style={style} primary={true} onClick={()=>EventBus.emit([WorkflowInstance,"OnRejected"],Context)} />
        </div>);
    }
}
ProjectCreated.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
   
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ProjectCreated;
