import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Header from '../components/Header';
import LeftDrawer from '../components/LeftDrawer';
import withWidth, {LARGE, SMALL} from 'material-ui/utils/withWidth';
import ThemeDefault from '../theme-default';
import Data from '../data';
import $ from 'jquery';
//import { Redirect } from 'react-router'

class Shell extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      navDrawerOpen: true,
      username:null,
      designation:null
    };
  }
  componentDidMount(){
    $.ajax({
      url:Data.AppDir+'/LoginCheck',
      type:'GET',
      success:function($,Data,data){
        
        let id=((data||{}).Id||0);
        $("#User").attr("value",id);
        if(id!=0)
          {
            this.setState({ username:data.Username,designation:""});
          }else{
            window.location.pathname=Data.AppDir+'/Login';
          }
      }.bind(this,$,Data)
    });    
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.width !== nextProps.width) {
        this.setState({navDrawerOpen: nextProps.width === LARGE});
    }
  }

  handleChangeRequestNavDrawer() {
    this.setState({
      navDrawerOpen: !this.state.navDrawerOpen
    });
  }

  render() {
    let { navDrawerOpen ,username,designation} = this.state;
    const paddingLeftDrawerOpen = 236;
         if (!username) {
           return (<div>User Not Authenticated</div>);
        //  debugger;
       //  return (<Redirect to="/Login"/>);
         }
    const styles = {
      header: {
        paddingLeft: navDrawerOpen ? paddingLeftDrawerOpen : 0
      },
      container: {
        margin: '80px 20px 20px 15px',
        paddingLeft: navDrawerOpen && this.props.width !== SMALL ? paddingLeftDrawerOpen : 0
      }
    };

    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <div>
          <Header styles={styles.header}
                  handleChangeRequestNavDrawer={this.handleChangeRequestNavDrawer.bind(this)}/>

            <LeftDrawer navDrawerOpen={navDrawerOpen}
                        menus={Data.menus}
                        username={username}
                        designation={designation}/>
            <div style={styles.container}>
              {this.props.children}
            </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Shell.propTypes = {
  children: PropTypes.element,
  width: PropTypes.number
};

export default withWidth()(Shell);
