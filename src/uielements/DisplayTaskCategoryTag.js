import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField  from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import $ from "jquery";
import VirtualizedSelect from 'react-virtualized-select';
import { Creatable } from 'react-select';

class DisplayTaskCategoryTag extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            WorkflowInstance:this.props.WorkflowInstance,
            EventBus: this.props.EventBus,
            Categories:{
                Tags:[],
                Category:{},
                SelectedTags:[],
                User:null
            },
            SelectedTags:[]
        };

        this.Save = this.Save.bind(this);
        this.OnChangeFunc=this.OnChangeFunc.bind(this);
        this.OnChangeCreatableFunc=this.OnChangeCreatableFunc.bind(this);
        
    }
    componentDidMount(){

        const {EventBus,WorkflowInstance,Context}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],function($,e,d){
            d.User=$("#User").attr("value")||0;
            this.setState({Categories:d});
        }.bind(this,$));
        let url='/api/DisplayTaskCategoryTag/'+Context;
        setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);

    }
    Save() {
        const {EventBus,Context,Categories,WorkflowInstance}=this.state;

                let url='/api/DisplayTaskCategoryTag/'+Context;
                EventBus.Register([WorkflowInstance, "Success"],()=>EventBus.emit([WorkflowInstance,"OnTagUpdated"],Context));
                EventBus.emit([WorkflowInstance, "OnInitializeData"],Categories,url,'POST');
    }
    OnChangeFunc(val){
        this.setState(({Categories}) => ({
            Categories: {  ...Categories, SelectedTags: val, }
            }) );
    }
    OnChangeCreatableFunc(val){
        this.setState(({Categories}) => ({
            Categories: {  ...Categories, SelectedTags: val.map(v=>v.Id), }
            }) );
    }
    render() {
        const { Categories} = this.state;
        let IsStatelessCat=Categories.Category.CategoryName=='StateLessCategories';
       
        let CategorySelector=IsStatelessCat?
        (<VirtualizedSelect
            options={Categories.Tags}
            selectComponent={Creatable}
            onChange={this.OnChangeCreatableFunc}
            multi={true}
            labelKey="Name"
            valueKey="Id"
            style={{color:'black'}}
            value={Categories.SelectedTags.map(e=>isNaN(e)?e:parseInt(e))}
          />)
        :(<SelectField
        floatingLabelText={Categories.Category.CategoryName}
        value={parseInt(Categories.SelectedTags[0]||null)}
        onChange={(e,k,v)=>this.OnChangeFunc([v])}
        >{Categories.Tags
        .map(d=><MenuItem value={d.Id} primaryText={d.Name} key={d.Id} />)
        }
    </SelectField>);
        return (<div className="row">
            <div className="col-md-6">
            {CategorySelector}
            </div><div className="col-md-6">
            <RaisedButton label={IsStatelessCat?"Update Tag":"Update "+Categories.Category.CategoryName} style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
                </div>
        </div>);
    }
}
DisplayTaskCategoryTag.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default DisplayTaskCategoryTag;
