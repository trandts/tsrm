import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Subheader from 'material-ui/Subheader';

class TaskClose extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };

    }
    render() {
        const { Context,EventBus,WorkflowInstance} = this.state;
        return (<div>
            <Subheader>Task Close</Subheader>
            <RaisedButton label="Close" style={{ margin: 15 }} primary={true}
                onClick={()=>EventBus.emit([WorkflowInstance, "OnTaskClose"] ,Context)} />
        </div>);
    }
}
TaskClose.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default TaskClose;