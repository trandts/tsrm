import React, { PropTypes } from 'react';

class SessionManagerStarter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            HasNotRun:true,
            WorkflowInstance: this.props.WorkflowInstance,
        };
        
        this.InitializeData=this.InitializeData.bind(this);
        
    }
    componentDidMount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],function(event,context)
        {
            EventBus.off([WorkflowInstance,"Success"]);
            if(context.CreateNew && this.state.HasNotRun)
            {
                this.setState({ HasNotRun:false});
                EventBus.emit([WorkflowInstance,"OnStartSessionManagement"],context.AuthSession);
                setTimeout(()=>EventBus.emit("PageReload"),1000);
            }
        }.bind(this));
        EventBus.Register([WorkflowInstance,"EventsLoaded"],this.InitializeData);
    }
    componentWillUnmount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        EventBus.off([WorkflowInstance,"OnStartSessionManagement"]);
        EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    InitializeData()
    {
        const {EventBus,WorkflowInstance}=this.state;
        let url='/SessionCheck';
        EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET');
    }
    render() {
        return (<div />);
    }
}
SessionManagerStarter.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default SessionManagerStarter;