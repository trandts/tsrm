import React, { PropTypes } from 'react';
import $ from "jquery";

class ReadyToCreateContentTagCategory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            WorkflowInstance:this.props.WorkflowInstance,
            EventBus: this.props.EventBus,
            Categories:[]
        };

    }
    componentDidMount(){
        const {EventBus,WorkflowInstance,Context}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],function(e,d){
                if(d)
                {
                    this.state.EventBus.emit("PageReload");
                }
        }.bind(this));
        let url='/api/ReadyToCreateContentTagCategory/'+Context+'?Instance='+WorkflowInstance+'&User='+$("#User").attr("value");
        setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);
    }
    render() {
        return null;
    }
}
ReadyToCreateContentTagCategory.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ReadyToCreateContentTagCategory;
