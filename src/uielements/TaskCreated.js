import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

class TaskCreated extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
            
        };

    }
    render() {
        const { Context,EventBus,WorkflowInstance} = this.state;
        return (<div>
            <RaisedButton label="Start Working" style={{ margin: 15 }} primary={true}
                onClick={()=>EventBus.emit([WorkflowInstance,"OnTaskStartWorking"],Context)} />
                <RaisedButton label="Change Task Assignment" style={{ margin: 15 }} primary={true}
                onClick={()=>EventBus.emit([WorkflowInstance,"StartChangingTaskAssignment"],Context)} />
            <RaisedButton label="Cancel Task" style={{ margin: 15 }} primary={true}
                onClick={()=>EventBus.emit([WorkflowInstance,"OnTaskCancel"],Context)} />
        </div>);
    }
}
TaskCreated.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default TaskCreated;