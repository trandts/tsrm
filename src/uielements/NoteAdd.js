import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import $ from "jquery";

class NoteAdd extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            WorkflowInstance:this.props.WorkflowInstance,
            EventBus: this.props.EventBus
        };

        this.Save = this.Save.bind(this);
    }
    componentDidMount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],function()
        {   
            EventBus.off([WorkflowInstance,"Success"]);
            EventBus.emit([WorkflowInstance,"OnNoteAdd"],this.state.Context);
            EventBus.emit("PageReload");
        }.bind(this));
    }
    componentWillUnmount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    Save() {
        const {EventBus,Context,Name,WorkflowInstance}=this.state;
        let $data={task:{Id:Context},Name:Name,User:{Id:$("#User").attr("value")}};
        let url='/api/NoteAdd';
        EventBus.emit([WorkflowInstance,"OnInitializeData"],$data,url,'POST');
    }
    render() {
        const { Name } = this.state;
        return (<div>
            <TextField floatingLabelText="Name" value={Name} onChange={(e, v) => this.setState({ Name: v || '' })} />
            <RaisedButton label="Add Comment" style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
        </div>);
    }
}
NoteAdd.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default NoteAdd;
