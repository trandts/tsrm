import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField  from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import $ from "jquery";

class ProjectResourceAssign extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            Data:{

                Resource:[]
            },
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };

        this.Save = this.Save.bind(this);
        this.AddAnother = this.AddAnother.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.LoadData=this.LoadData.bind(this);
    }
    componentDidMount() {
        const {EventBus,Context,WorkflowInstance}=this.state;
        if(Context)
            {
              let url='/api/ProjectResourceAssign/'+Context;
              
              EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.LoadData(d));
              
             setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);
              //EventBus.emit(["ProjectResourceAssign","OnInitializeData"],{},url,'GET',null,this.LoadData);
            }
    }
    LoadData($data){
        this.setState({Data:$data});
    }
    Save() {
        const {EventBus,Data,Context,WorkflowInstance}=this.state;
        let url='/api/ProjectResourceAssign/'+$("#User").attr("value");
        
        EventBus.Register([WorkflowInstance,"Success"],()=>EventBus.emit([WorkflowInstance,"OnProjectResourceUpdate"],Context));
        EventBus.emit([WorkflowInstance,"OnInitializeData"],Data,url,'POST');
          

    }
    AddAnother() {
        const {Data}=this.state;
        let res=Data.Resource;
        res[res.length]={Team:0,User:0};
        Data.Resource=res;
        this.setState({Data:Data});

    }
    handleChange(i,Prop,Value){
        const {Data}=this.state;
        let res=Data.Resource;
        res[i][Prop]=Value;
        Data.Resource=res;
        this.setState({Data:Data});
    }
    render() {
        const { Data } = this.state;
       let Element=Data.Resource.map((e,i)=><div key={i}>
<SelectField
          floatingLabelText="Team"
          value={this.state.Data.Resource[i].Team}
          onChange={(e,k,v)=>this.handleChange(i,"Team",v)}
        >{Data.Teams.map(d=><MenuItem value={d.Id} primaryText={d.Name} key={d.Id} />)
        }
        </SelectField>
        <SelectField
          floatingLabelText="User"
          value={this.state.Data.Resource[i].User}
          onChange={(e,k,v)=>this.handleChange(i,"User",v)}
        >{Data.Users.map(d=><MenuItem value={d.Id} primaryText={d.Username} key={d.Id} />)
        }
        </SelectField>
       </div>);
        return (<div>{Element} <RaisedButton label="Add Another" style={{ margin: 15 }} primary={true}
        onClick={this.AddAnother} />
            <RaisedButton label="Update" style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
        </div>);
    }
}
ProjectResourceAssign.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ProjectResourceAssign;
