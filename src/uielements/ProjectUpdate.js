import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';

class ProjectUpdate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Name: '',
            EstimatedStartDate: null,
            EstimatedEndDate: null,
            ActualStartDate: null,
            ActualEndDate: null,
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            WorkflowInstance: this.props.WorkflowInstance,
        };
        this.Save = this.Save.bind(this);
        this.LoadData=this.LoadData.bind(this);
    }
    componentDidMount() {
        const {EventBus,Context,WorkflowInstance}=this.state;
        if(Context)
            {
              let url='/api/ProjectCreated/'+Context;
              
              EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.LoadData(d));
            
              EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET');
            }
    }
    componentWillUnmount(){
       const {EventBus,WorkflowInstance}=this.state;
       EventBus.off([WorkflowInstance,"Success"]);
       EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    LoadData($data){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        $data.EstimatedStartDate=$data.EstimatedStartDate?new Date($data.EstimatedStartDate):null;
        $data.EstimatedEndDate=$data.EstimatedEndDate?new Date($data.EstimatedEndDate):null;
        $data.ActualStartDate=$data.ActualStartDate?new Date($data.ActualStartDate):null;
        $data.ActualEndDate=$data.ActualEndDate?new Date($data.ActualEndDate):null;
        this.setState($data);
    }
    Save() {
        const {EventBus,Context,WorkflowInstance}=this.state;
        if (Context) {  
        let url='/api/ProjectCreation/'+Context;
        EventBus.Register([WorkflowInstance,"Success"],()=>EventBus.emit([WorkflowInstance,"OnProjectUpdate"],Context));
        EventBus.emit([WorkflowInstance,"OnInitializeData"],JSON.parse(JSON.stringify(this.state)),url,'PUT');
        
        
        } 

    }
    render() {
        const {  Name, EstimatedStartDate, EstimatedEndDate, ActualStartDate, ActualEndDate } = this.state;

        return (<div>
            <TextField floatingLabelText="Name" value={Name} onChange={(e, v) => this.setState({ Name: v || '' })} />
            <DatePicker hintText="Estimated Start Date" value={EstimatedStartDate} onChange={(e, v) => this.setState({ EstimatedStartDate: v })} />
            <DatePicker hintText="Estimated End Date" value={EstimatedEndDate} onChange={(e, v) => this.setState({ EstimatedEndDate: v })} />
            <DatePicker hintText="Actual Start Date" value={ActualStartDate} onChange={(e, v) => this.setState({ ActualStartDate: v })} />
            <DatePicker hintText="Actual End Date" value={ActualEndDate} onChange={(e, v) => this.setState({ ActualEndDate: v })} />
            <RaisedButton label="Update Project" primary={true} onClick={this.Save} />
        </div>);
    }
}
ProjectUpdate.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ProjectUpdate;
