import React, {PropTypes} from 'react';
//import LoginPage from '../containers/LoginPage';
import $ from "jquery";
import Data from '../data';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import {grey500, white} from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            key: new Date().toString(),
            Username: '',
            Password:'',
            IsChecked:false,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.Login = this.Login.bind(this);
        this.Authenticated = this.Authenticated.bind(this);
    }
    componentDidMount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],this.Authenticated);
    }
    Authenticated(event, data) {
        const {EventBus,WorkflowInstance}=this.state;
        let id = ((data || {}).Id || 0);
        $("#User").attr("value", id);
        if (id == 0) {
            alert("Authentication Failed");
        } else {
            EventBus.emit([WorkflowInstance,"OnAuthenticated"], data.AuthSession);
            
            EventBus.emit("PageReload");
           // EventBus.emit("Redirect", Data.AppDir + '/Tasks');
        }
    }
  Login(){
      const { EventBus,WorkflowInstance }= this.state;
      let url=Data.AppDir+'/LoginService';
      EventBus.emit([WorkflowInstance,"OnInitializeData"],JSON.parse(JSON.stringify(this.state)),url,'POST');
  }
  render()
  {
      const { Username,Password }=this.state;
      const styles = {
        loginContainer: {
          minWidth: 320,
          maxWidth: 400,
          height: 'auto',
          margin: 'auto'
        },
        paper: {
          padding: 20,
          overflow: 'auto',
          border:'2px solid #626a99'
        },
        buttonsDiv: {
          textAlign: 'center',
          padding: 10,
          display: 'none'
        },
        flatButton: {
          color: grey500
        },
        checkRemember: {
          style: {
            float: 'left',
            maxWidth: 180,
            paddingTop: 5,
            display:'none'
          },
          labelStyle: {
            color: grey500
          },
          iconStyle: {
            color: grey500,
            borderColor: grey500,
            fill: grey500
          }
        },
        loginBtn: {
          float: 'right'
        },
        btn: {
          background: '#4f81e9',
          color: white,
          padding: 7,
          borderRadius: 2,
          margin: 2,
          fontSize: 13
        },
        btnFacebook: {
          background: '#4f81e9'
        },
        btnGoogle: {
          background: '#e14441'
        },
        btnSpan: {
          marginLeft: 5
        },
        logoContainer:{
          xmarginTop:242,
          backgroundColor:"#ffffff",
          padding:2,
          paddingTop:5,
          paddingLeft:5,
          maxHeight:57,
          display:'inline',
          float:'left'
        },
        appImg:{
          padding:3,
          width: 220,
          maxHeight:57 ,
          float:'right',
          display:'inline'
        }
      };
   return (
      <div>
        <div className="loginPgMain"  style={{backgroundImage:'url('+require('../images/marketing01.jpg')+')'}}>

          <div className="loginPgLeft loginPgLeftBg bred"  style={{backgroundImage:'url('+require('../images/material_bg.png')+')'}} />

          <div className="loginPgRight bblue ">


            <div style={styles.loginContainer}>

              <Paper style={styles.paper}>

                <form>
                 
                  <TextField hintText="Username" 
                    value={Username}
                    floatingLabelText="Username" fullWidth={true} 
                    onChange={(e,value)=>this.setState({Username:value})}/>
                  <TextField
                    hintText="Password"
                    floatingLabelText="Password"
                    fullWidth={true}
                    value={Password}
                    type="password"
                    onChange={(e,value)=>this.setState({Password:value})}/>

                  <div>
                    <Checkbox
                      label="Remember me"
                      style={styles.checkRemember.style}
                      labelStyle={styles.checkRemember.labelStyle}
                      iconStyle={styles.checkRemember.iconStyle}/>
                    <RaisedButton label="Login" primary={true} style={styles.loginBtn}
                      onClick={this.Login}/>
                  </div>
                </form>
              </Paper>

            </div>


            
          </div>

        </div>

      </div>
  );
  }

}

Login.propTypes = {
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default Login;