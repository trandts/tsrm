import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

class ProjectCompleted extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
       
    }
    render() {
        const {EventBus,Context ,WorkflowInstance} = this.state;
        return (<div>
            <RaisedButton label="Close Project" style={{margin:15}} primary={true}
             onClick={() =>EventBus.emit( [WorkflowInstance,"OnProjectClosed"],Context)} />
        </div>);
    }
}
ProjectCompleted.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};
export default ProjectCompleted;
