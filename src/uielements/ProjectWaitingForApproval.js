import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Subheader from 'material-ui/Subheader';

class ProjectWaitingForApproval extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Name: '',
            EstimatedStartDate: null,
            EstimatedEndDate: null,
            ActualStartDate: null,
            ActualEndDate: null,
            Context: this.props.Context,
            Base:this.props.Base
        };
        this.LoadData=this.LoadData.bind(this);
    }
    componentDidMount() {
        const {Base,Context}=this.state;
        let url='/api/ProjectCreated/'+Context;
        Base.DSM.TriggerEvent("OnInitializeData",{},url,'GET',null,this.LoadData);
    }
    LoadData($data){
        $data.EstimatedStartDate=$data.EstimatedStartDate?new Date($data.EstimatedStartDate):null;
        $data.EstimatedEndDate=$data.EstimatedEndDate?new Date($data.EstimatedEndDate):null;
        $data.ActualStartDate=$data.ActualStartDate?new Date($data.ActualStartDate):null;
        $data.ActualEndDate=$data.ActualEndDate?new Date($data.ActualEndDate):null;
        this.setState($data);
    }
    render() {
        const { Context, Name, EstimatedStartDate, EstimatedEndDate, ActualStartDate, ActualEndDate,Base } = this.state;
        const style={ margin:15};

        return (<div>
            <Subheader>Project Created</Subheader>
            <TextField floatingLabelText="Name" disabled={true} value={Name} />
            <DatePicker hintText="Estimated Start Date" disabled={true} value={EstimatedStartDate} />
            <DatePicker hintText="Estimated End Date" disabled={true} value={EstimatedEndDate}/>
            <DatePicker hintText="Actual Start Date" disabled={true} value={ActualStartDate}/>
            <DatePicker hintText="Actual End Date" disabled={true} value={ActualEndDate}  />
            <RaisedButton label="Approve" style={style} primary={true} onClick={()=>Base.DSM.TriggerEvent(Context, "OnApproved")} />
            <RaisedButton label="Reject" style={style} primary={true} onClick={()=>Base.DSM.TriggerEvent(Context,"OnRejected")} />
        </div>);
    }
}
ProjectWaitingForApproval.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    EventCallback: PropTypes.func,
    Workflow: PropTypes.number,
    Base:PropTypes.object
};

export default ProjectWaitingForApproval;
