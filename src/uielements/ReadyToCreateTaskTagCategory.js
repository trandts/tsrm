import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField  from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class ReadyToCreateTaskTagCategory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            WorkflowInstance:this.props.WorkflowInstance,
            EventBus: this.props.EventBus,
            Categories:[]
        };

        this.Save = this.Save.bind(this);
    }
    componentDidMount(){
        const {EventBus,WorkflowInstance,Context}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.setState({Categories:d}));
        let url='/api/ReadyToCreateTaskTagCategory/'+Context;
        setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);
    }
    Save() {
        const {EventBus,Context,Category,WorkflowInstance}=this.state;

        let url='/api/ReadyToCreateTaskTagCategory/';
        let _data={TagCategory:{ Id:Category },Task:{ Id:Context } };
        EventBus.Register([WorkflowInstance, "Success"],(e,d)=>EventBus.emit([WorkflowInstance,"OnTaskTagCategoryCreated"],d));
        EventBus.emit([WorkflowInstance, "OnInitializeData"],_data,url,'POST');

    }
    render() {
        const { Categories} = this.state;
        if(Categories.length==0) return null;
        return (<div className="row">
            <div className="col-md-6">
            <SelectField
            floatingLabelText="Category"
            value={this.state.Category}
            onChange={(e,k,Category)=>this.setState({Category})}
            >{Categories
            .map(d=><MenuItem value={d.Id} primaryText={d.CategoryName} key={d.Id} />)
            }
            </SelectField>
            </div><div className="col-md-6">
            <RaisedButton label="Add Tag Category" style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
                </div>
        </div>);
    }
}
ReadyToCreateTaskTagCategory.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ReadyToCreateTaskTagCategory;
