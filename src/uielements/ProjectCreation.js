import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import $ from "jquery";

class ProjectCreation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            Name: '',
            EstimatedStartDate: null,
            EstimatedEndDate: null,
            ActualStartDate: null,
            ActualEndDate: null,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.Save = this.Save.bind(this);
    }

    componentDidMount()
    {
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],
        function(e,d)
        {  
            EventBus.off([WorkflowInstance,"Success"]);
            EventBus.emit([WorkflowInstance,"OnProjectCreated"],d);
        });
    }
    componentWillUnmount(){
       const {EventBus,WorkflowInstance}=this.state;
       EventBus.off([WorkflowInstance,"Success"]);
       EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    Save() {
        const {EventBus,WorkflowInstance}=this.state;
        let $data=JSON.parse(JSON.stringify(this.state));
        let url='/api/ProjectCreation/'+$("#User").attr("value");
        EventBus.emit([WorkflowInstance,"OnInitializeData"],$data,url,'POST');
    }
    render() {
        const {  Name, EstimatedStartDate, EstimatedEndDate, ActualStartDate, ActualEndDate } = this.state;

        return (<div>
            <TextField floatingLabelText="Name" value={Name} onChange={(e, v) => this.setState({ Name: v || '' })} />
            <DatePicker hintText="Estimated Start Date" value={EstimatedStartDate} onChange={(e, v) => this.setState({ EstimatedStartDate: v })} />
            <DatePicker hintText="Estimated End Date" value={EstimatedEndDate} onChange={(e, v) => this.setState({ EstimatedEndDate: v })} />
            <DatePicker hintText="Actual Start Date" value={ActualStartDate} onChange={(e, v) => this.setState({ ActualStartDate: v })} />
            <DatePicker hintText="Actual End Date" value={ActualEndDate} onChange={(e, v) => this.setState({ ActualEndDate: v })} />
            <RaisedButton label="Create Project" primary={true} onClick={this.Save} />
        </div>);
    }
}
ProjectCreation.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ProjectCreation;
