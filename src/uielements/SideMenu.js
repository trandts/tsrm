import React, {PropTypes} from 'react';
import LeftDrawer from '../components/LeftDrawer';
import $ from 'jquery';

class SideMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            navDrawerOpen: true,
            designation:null,
            EventBus: this.props.EventBus
        };
        this.SideMenuToggled=this.SideMenuToggled.bind(this);
        this.state.EventBus.on("SideMenuToggled",this.SideMenuToggled);
    }
    SideMenuToggled(e,navDrawerOpen)
    {
        this.setState({ navDrawerOpen:navDrawerOpen});
    }
  render() {
   
    let { navDrawerOpen ,designation} = this.state;
    let username=$("#User").attr("data-Username");
    //const paddingLeftDrawerOpen = Data.MenuWidth;
    //Data.menus
    
    const IsAuthenticated=($("#User").attr("value")||0)!=0;
    if(!IsAuthenticated)
    {
     return <div />;
    }
    return (
        <LeftDrawer navDrawerOpen={navDrawerOpen}
                        menus={[]}
                        username={username}
                        designation={designation}/>
      );
  }
}

SideMenu.propTypes = {
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default SideMenu;
