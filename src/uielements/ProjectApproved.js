import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';

class ProjectApproved extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            Dialog: false,
            OpenTask: null,
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.ProjectCompleted = this.ProjectCompleted.bind(this);

    }
    ProjectCompleted() {
        const { Context,EventBus ,WorkflowInstance} = this.state;
        EventBus.emit([WorkflowInstance,"OnProjectCompleted"] ,Context);
    }
    render() {
        return (<div>
            <AppBar title=""/>
              <RaisedButton label="Project Completed"  primary={true} onClick={this.ProjectCompleted} />
          

        </div>);
    }
}
ProjectApproved.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ProjectApproved;
