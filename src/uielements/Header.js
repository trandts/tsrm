import React, {PropTypes} from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import Menu from 'material-ui/svg-icons/navigation/menu';
import ViewModule from 'material-ui/svg-icons/action/view-module';
import {white} from 'material-ui/styles/colors';
import Data from '../data';
import $ from 'jquery';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            navDrawerOpen: true,
            username:null,
            designation:null,
            EventBus: this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.handleChangeRequestNavDrawer=this.handleChangeRequestNavDrawer.bind(this);
    }
    handleChangeRequestNavDrawer() {
        const {EventBus}=this.state;
        EventBus.emit("SideMenuToggled",!this.state.navDrawerOpen);
        this.setState({
          navDrawerOpen: !this.state.navDrawerOpen
        });
      }
  render() {
    let { navDrawerOpen } = this.state;
    const paddingLeftDrawerOpen =   Data.MenuWidth;
    const styles = {
          paddingLeft: navDrawerOpen ? paddingLeftDrawerOpen : 0
      };
      const IsAuthenticated=($("#User").attr("value")||0)!=0;
          const style = {
            appBar: {
              position: 'fixed',        
              top: 0,
              overflow: 'hidden',
              maxHeight: 57,
              zIndex:1200
            },
            menuButton: {
              marginLeft: 10
            },
            iconsRightContainer: {
              marginLeft: 20
            },
            mainContainer:{
            },
            appBarContainer:{
            },
            logoContainer:{
              display:'none'
            },
            logoImg:{
              padding:0,
              width: 220,
              maxHeight:57 ,
            }
          };
    return (<div style={style.mainContainer}>
        {IsAuthenticated?<div style={style.appBarContainer}>
        <AppBar
        zDepth={2}
          style={{...styles, ...style.appBar}}
         title="TC Task Management"
          iconElementLeft={
              <div>
              <IconButton style={style.menuButton} onClick={this.handleChangeRequestNavDrawer}>
                <Menu color={white} />
              </IconButton>
              
              </div>
          }
          iconElementRight={
            <div style={style.iconsRightContainer}>
            
              <IconMenu color={white}
                        iconButtonElement={
                          <IconButton><ViewModule color={white}/></IconButton>
                        }
                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              >
        {this.props.children}
              </IconMenu>
            </div>
          }
        />
        </div>:this.props.children}
      </div>
      );
  }
}

Header.propTypes = {
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object,
    children:PropTypes.element
};

export default Header;
