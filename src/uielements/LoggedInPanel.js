import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Data from '../data';

class LoggedInPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.LoggedOut=this.LoggedOut.bind(this);
    }
    componentDidMount(){

        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],
        function()
        {  
            const {EventBus,Context,WorkflowInstance}=this.state;
            EventBus.off([WorkflowInstance,"Success"]);
            EventBus.emit([WorkflowInstance,"OnLoggedOut"],Context);
            EventBus.emit("PageReload");
        }.bind(this));
    }
    componentWillUnmount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    LoggedOut(){
       const { EventBus,WorkflowInstance} = this.state;
        let url=Data.AppDir+'/LogoutService';
        EventBus.emit([WorkflowInstance,"OnInitializeData"],null,url,'GET');
    }
    render() {
        return (<div>Logged In
            <RaisedButton label="LogOut" style={{ margin: 15 }} primary={true}
                onClick={this.LoggedOut} />
        </div>);
    }
}
LoggedInPanel.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default LoggedInPanel;