import React, { PropTypes } from 'react';

class ContentTagInitializer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            WorkflowInstance:this.props.WorkflowInstance,
            EventBus: this.props.EventBus,
            Categories:[]
        };

    }
    componentDidMount(){
        const {EventBus,WorkflowInstance,Context}=this.state;
        EventBus.Register([WorkflowInstance,"EventsLoaded"],()=>EventBus.emit([WorkflowInstance,"OnTaskTagInitialize"],Context));
        
    }
    render() {
        return (<div />);
    }
}
ContentTagInitializer.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default ContentTagInitializer;
