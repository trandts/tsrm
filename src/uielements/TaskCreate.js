import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import $ from "jquery";

class TaskCreate extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };

        this.Save = this.Save.bind(this);
    }
    Save() {
        const {WorkflowInstance,EventBus,Context,Name}=this.state;
    let url='/api/TaskCreate/'+$("#User").attr("value");
    EventBus.Register([WorkflowInstance,"Success"],(e,d)=>EventBus.emit([WorkflowInstance,"OnTaskCreated"],d));
    EventBus.emit([WorkflowInstance,"OnInitializeData"],{Project:{Id:Context},Name:Name},url,'POST');

    }
    render() {
        const { Name } = this.state;
        return (<div>
            <TextField floatingLabelText="Name" value={Name} onChange={(e, v) => this.setState({ Name: v || '' })} />
            <RaisedButton label="Create Task" style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
        </div>);
    }
}
TaskCreate.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
   
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default TaskCreate;
