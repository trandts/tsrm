import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Subheader from 'material-ui/Subheader';
import TextField from 'material-ui/TextField';
import $ from 'jquery';
import Slider from 'material-ui/Slider';

class TaskProgressed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            Name: '',
            EstimatedHours: 0,
            ActualHours: 0,
            Completed: 0,
            UserId: $("#User").attr("value"),
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.TaskProgressed = this.TaskProgressed.bind(this);
    }

    componentDidMount() {
        const { Context, UserId ,EventBus,WorkflowInstance } = this.state;
        let url='/api/TaskProgressed/' + Context + '?UserId=' + UserId;
        EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.setState(d));
        setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);
    }
    TaskProgressed() {
          const { Context ,EventBus,WorkflowInstance} = this.state;
          let url='/api/TaskProgressed/' + Context ;
          
        EventBus.Register([WorkflowInstance,"Success"],()=>EventBus.emit([WorkflowInstance, "OnTaskProgressChanged"],Context));
        EventBus.emit([WorkflowInstance,"OnInitializeData"],JSON.parse(JSON.stringify(this.state)),url,'POST');
       
    }
    render() {
        const { Name, EstimatedHours, ActualHours, Completed, Context,EventBus ,WorkflowInstance} = this.state;
        return (<div>
            <Subheader>{Name}</Subheader>
            <TextField floatingLabelText="EstimatedHours" value={EstimatedHours} type="number" onChange={(e, v) => this.setState({ EstimatedHours: v || '' })} />
            <TextField floatingLabelText="ActualHours" value={ActualHours} type="number" onChange={(e, v) => this.setState({ ActualHours: v || '' })} />
            <div>{Completed}%</div>
            <Slider min={0} max={100} value={Completed} onChange={(e, v) => this.setState({ Completed: v })} step={1} />
            <RaisedButton label="Update Progress" style={{ margin: 15 }} primary={true}
                onClick={this.TaskProgressed} />
            <RaisedButton label="Cancel Task" style={{ margin: 15 }} primary={true}
                onClick={() => EventBus.emit([WorkflowInstance, "OnTaskCancel"],Context)} />
            <RaisedButton label="Task Completed" style={{ margin: 15 }} primary={true}
                onClick={() => EventBus.emit([WorkflowInstance, "OnTaskCompleted"],Context)} />
            <RaisedButton label="Change Task Assignment" style={{ margin: 15 }} primary={true}
                onClick={()=>EventBus.emit([WorkflowInstance, "StartChangingTaskAssignmentWhileWorking"],Context)} />
        </div>);
    }
}
TaskProgressed.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default TaskProgressed;