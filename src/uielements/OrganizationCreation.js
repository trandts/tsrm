import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import $ from "jquery";
import TextField from 'material-ui/TextField';

class OrganizationCreation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            Name: '',
            WorkflowInstance:this.props.WorkflowInstance
        };
        this.Save = this.Save.bind(this);
    }
    componentDidMount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.Register([WorkflowInstance,"Success"],function(event,context)
        {
            EventBus.emit([WorkflowInstance,"OnOrganizationCreated"],context);
        });
    }
    componentWillUnmount(){
        const {EventBus,WorkflowInstance}=this.state;
        EventBus.off([WorkflowInstance,"Success"]);
        EventBus.emit([WorkflowInstance,"OnInitializeCancelled"]);
    }
    Save(){ 
        const {EventBus,WorkflowInstance}=this.state;
        let url='/api/OrganizationCreation/'+$("#User").attr("value");
        EventBus.emit([WorkflowInstance,"OnInitializeData"],{ Name: this.state.Name},url,'POST');
    }
    render() {
        const { Name } = this.state;
        
        return (<div>
              <TextField
              floatingLabelText="Name"
              value={Name||''}
              fullWidth={true}
              onChange={(a, v) => this.setState({ Name: v })}
            />
            <RaisedButton label="Save" primary={true} onClick={this.Save} />
        </div>);
    }
}
OrganizationCreation.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default OrganizationCreation;
