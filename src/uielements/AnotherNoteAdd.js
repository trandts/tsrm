import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import $ from "jquery";
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ImageTransform from 'material-ui/svg-icons/image/transform';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import VirtualizedSelect from 'react-virtualized-select';
import { Creatable } from 'react-select';

class AnotherNoteAdd extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            Comments: [],
            EventBus: this.props.EventBus,
            WorkflowInstance: this.props.WorkflowInstance
        };

        this.Save = this.Save.bind(this);
        this.ConvertToTask = this.ConvertToTask.bind(this);
        this.LoadData = this.LoadData.bind(this);
        this.OnChangeCreatableFunc = this.OnChangeCreatableFunc.bind(this);
    }
    componentDidMount() {
        const { EventBus, Context, WorkflowInstance } = this.state;
        if (Context) {
            let url = '/api/AnotherNoteAdd/' + Context;
            EventBus.Register([WorkflowInstance, "Success"], (e, d) => this.LoadData(d));
            setTimeout(() => EventBus.emit([WorkflowInstance, "OnInitializeData"], {}, url, 'GET'), 1000);
        }
    }
    LoadData($data) {
        this.setState({
            Comments: $data.AnotherNoteAddNoteInfo
            , Projects: $data.Projects,
            Tags: $data.Tags
        });
    }
    Save() {
        const { EventBus, Context, Name, WorkflowInstance } = this.state;
        let url = '/api/NoteAdd';
        EventBus.Register([WorkflowInstance, "Success"], () => EventBus.emit([WorkflowInstance, "OnAnotherNoteAdd"], Context));
        EventBus.emit([WorkflowInstance, "OnInitializeData"], { task: { Id: Context }, Name: Name, User: { Id: $("#User").attr("value") } }, url, 'POST');

    }

    ConvertToTask(ev, value) {
        const { EventBus, WorkflowInstance, NoteId } = this.state;
        let url = '/api/AnotherNoteAdd';
        EventBus.Register([WorkflowInstance, "Success"], () => EventBus.emit([WorkflowInstance, "OnNoteConvertedToTask"], NoteId));
        EventBus.emit([WorkflowInstance, "OnInitializeParallelData"], { NoteId: NoteId, ProjectId: value, UserId: $("#User").attr("value") }, url, 'POST');
    }

    OnChangeCreatableFunc(obj, val) {
        const { Comments, EventBus, WorkflowInstance } = this.state;
        let url = '/api/AnotherNoteAdd/' + obj.Id;
        EventBus.Register([WorkflowInstance, "Success"], () => EventBus.emit([WorkflowInstance, "OnAddUpdateNoteTag"], obj.Id));
        EventBus.emit([WorkflowInstance, "OnInitializeParallelData2"], { NoteId: obj.Id, Tags: val.map(v => v.Id), UserId: $("#User").attr("value") }, url, 'POST');
        for (let i = 0; i < Comments.length; i++) {
            if (obj.Id == Comments[i].Id) {
                Comments[i].Tags = val;
            }
        }
        this.setState({ Comments: Comments });
    }
    render() {
        const { Name, Comments, Tags, Projects, EventBus, Context, WorkflowInstance } = this.state;
        return (<div> <Table selectable={false}>
            <TableHeader displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                    <TableHeaderColumn>Username</TableHeaderColumn>
                    <TableHeaderColumn>Comment</TableHeaderColumn>
                    <TableHeaderColumn>Time</TableHeaderColumn>
                    <TableHeaderColumn>Convert To Task</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody
                displayRowCheckbox={false}>
                {Comments.map((e, i) => <TableRow key={i}>
                    <TableRowColumn>{e.Username}</TableRowColumn>
                    <TableRowColumn>{e.Text}</TableRowColumn>
                    <TableRowColumn>{new Date(e.Time).toLocaleString()}</TableRowColumn>
                    <TableRowColumn><div className="row">
                        <div className="col-md-4">{e.HasTask ? null : <FloatingActionButton mini={true}
                            style={{ marginRight: 20 }}
                            onClick={(ev) => this.setState({ open: true, anchorEl: ev.currentTarget, NoteId: e.Id })}>
                            <ImageTransform />
                        </FloatingActionButton>}
                        </div> <div className="col-md-8">
                            <VirtualizedSelect
                                options={Tags}
                                selectComponent={Creatable}
                                onChange={this.OnChangeCreatableFunc.bind(this, e)}
                                multi={true}
                                labelKey="Name"
                                valueKey="Id"
                                menuContainerStyle={{ 'zIndex': 999999 }}
                                style={{ color: 'black' }}
                                value={e.Tags.map(e => isNaN(e) ? e : parseInt(e))}
                            /></div>
                    </div>
                    </TableRowColumn>
                </TableRow>)}
            </TableBody>
        </Table>
            <Popover
                open={this.state.open}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                onRequestClose={() => this.setState({ open: false })}
            >
                <Menu onChange={this.ConvertToTask}>
                    {(Projects || []).map(p => <MenuItem key={p.Id} value={p.Id} primaryText={p.Name} />)}
                </Menu>
            </Popover>
            <TextField floatingLabelText="Name" value={Name} onChange={(e, v) => this.setState({ Name: v || '' })} />
            <RaisedButton label="Add Another Comment" style={{ margin: 15 }} primary={true}
                onClick={this.Save} />
            <RaisedButton label="Lock All Further Notes" style={{ margin: 15 }} primary={true}
                onClick={() => EventBus.emit([WorkflowInstance, "OnNoteClosed"], Context)} />
        </div>);
    }
}
AnotherNoteAdd.propTypes = {
    /**
     * The axis on which the slider will slide.
     */

    Context: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
    WorkflowInstance: PropTypes.number,
    EventBus: PropTypes.object
};

export default AnotherNoteAdd;
