import React, {PropTypes} from 'react';
import Data from '../data';
import {Tabs, Tab} from 'material-ui/Tabs';
import $ from "jquery";
 
class Content extends React.Component {
  constructor(props) {
        super(props);

        this.state = {
            Context: this.props.Context,
            EventBus: this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance,
            key: new Date().toString(),
            Refferer:'/',
            navDrawerOpen: true
        };
        this.SideMenuToggled=this.SideMenuToggled.bind(this);
        this.state.EventBus.on("SideMenuToggled",this.SideMenuToggled);

    }
        SideMenuToggled(e,navDrawerOpen)
        {
            this.setState({ navDrawerOpen:navDrawerOpen});
        }
  render() {
   const{navDrawerOpen}=this.state;
   const paddingLeftDrawerOpen =   Data.MenuWidth;
  const styles= {
    margin: '80px 20px 20px 15px',
    paddingLeft: navDrawerOpen  ? paddingLeftDrawerOpen : 0
  };
  const Tasks=this.props.children.props.children[1].props.children.props.children.props.children;
  const TabElement=(<Tabs>
                        <Tab label="Project Tasks" >
                            <div className="col-md-12">
                                {Tasks.filter(e => e.props.otherInfo.TaskProject)}
                            </div>
                        </Tab>
                        <Tab label="Tasks" >
                            <div className="col-md-12">
                                {Tasks.filter(e => !e.props.otherInfo.TaskProject)}
                            </div>
                        </Tab>
                    </Tabs>);
  const DisplayElement= $("#User").attr("value")>0?TabElement:this.props.children;
    return (
        <div style={styles}>
            {DisplayElement}
        </div>
      );
  }
}

Content.propTypes = {
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object,
    children:PropTypes.element
};

export default Content;
