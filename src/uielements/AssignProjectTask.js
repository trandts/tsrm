import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField  from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import $ from "jquery";

class AssignProjectTask extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ProjectContent: null,
            Context: this.props.Context,
            Data:{
                Id:this.props.Context||0,
                UserId:0,
                Users:[]
            },
            EventBus:this.props.EventBus,
            WorkflowInstance:this.props.WorkflowInstance
        };

        this.Save = this.Save.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.LoadData=this.LoadData.bind(this);
        this.GoBack = this.GoBack.bind(this);
    }
    componentDidMount(){
        const {EventBus,Context,WorkflowInstance}=this.state;
        if(Context)
            {
              let url='/api/AssignProjectTask/'+Context;
              EventBus.Register([WorkflowInstance,"Success"],(e,d)=>this.LoadData(d));
             setTimeout(()=>EventBus.emit([WorkflowInstance,"OnInitializeData"],{},url,'GET'),1000);
            }
    }
    LoadData($data){
        this.setState({Data:$data});
    }
    Save() { 
        const {EventBus,Data,Context,WorkflowInstance}=this.state;
    let url= '/api/AssignProjectTask/'+$("#User").attr("value");
    EventBus.Register([WorkflowInstance,"Success"],()=>EventBus.emit([WorkflowInstance,"OnTaskAssignmentChange"],Context));
    EventBus.emit([WorkflowInstance,"OnInitializeData"],Data,url,'POST');

    }
    handleChange(v){ 
        const { Data } = this.state;
        Data.UserId=v;
        this.setState({ Data:Data });
    }
    GoBack()
    {
        const { Context,EventBus,WorkflowInstance } = this.state; 
        EventBus.emit([WorkflowInstance,"OnCancel"],Context);
    }
    render() {
        const { Data} = this.state; 
        return (<div>
            <SelectField
           floatingLabelText="Assigned User"
           value={Data.UserId}
           onChange={(e,i,v)=>this.handleChange(v)}
            >{Data.Users.map(d=><MenuItem value={d.Id} primaryText={d.Username} key={d.Id} />)}
            </SelectField>
            <RaisedButton label="Update Assigned User" style={{ margin: 15 }} primary={true} 
              onClick={this.Save} />
            <RaisedButton label="Go Back" style={{ margin: 15 }} primary={true} 
              onClick={this.GoBack} />
        </div>);
    }
}
AssignProjectTask.propTypes = {
    /**
     * The axis on which the slider will slide.
     */
    Context: PropTypes.oneOfType([ PropTypes.string, PropTypes.number , PropTypes.object ]),
    WorkflowInstance: PropTypes.number,
    EventBus:PropTypes.object
};

export default AssignProjectTask;
