function EventEmitter() {
    this._listeners = {};
    this.last={locale:null,type:null,Date:new Date()};
    this.SignalStarted=false;
    this.on("System_SignalRStart",function(ev,UserId,States){
        let connection = global.$.connection;
        let oldQs=(connection.hub.qs||{User:null,StateInstances:null});
        if(this.SignalStarted && (oldQs.User!=UserId || JSON.stringify(oldQs.StateInstances)!=JSON.stringify(States)))
        {
            connection.hub.stop();
            this.SignalStarted = false;
        }
        connection.hub.qs = { User: UserId, StateInstances: States };
        if (!this.SignalStarted) {
            this.SignalStarted = true;
            connection.mainHub.client.PageReload = function () {
                this.emit("PageReload");
            }.bind(this);
            connection.mainHub.client.CommentAdded= (StateInstanceId,ByUserName,Comments) =>this.emit([StateInstanceId,"CommentAdded"],StateInstanceId,ByUserName,Comments);
            connection.mainHub.client.TagAdded= (StateInstanceId,ByUserName,Tags) =>this.emit([StateInstanceId,"TagAdded"],StateInstanceId,ByUserName,Tags);
            connection.hub.start().done();
        }

    }.bind(this));
    
}

EventEmitter.prototype.on = function _on(type, listener) {
    let _locale=null;
    if(Array.isArray(type))
        {
            _locale=type[0];
            type=type[1];
        }
    if (!Array.isArray(this._listeners[type])) {
        this._listeners[type] = [];
    }
    
        this._listeners[type].push({
            locale:_locale,
            listener:listener});
    

    return this;
};
EventEmitter.prototype.Register = function _once(type, listener) {
    let self = this;

    function __once() {
        let args = [];
        for (let i = 0; i < arguments.length; i += 1) {
            args[i] = arguments[i];
        }
        self.off(type, __once);
       try {
             listener.apply(self, args);
            }catch(c){
              // console.log("listner not found",type)
            }
    }

    __once.listener = listener;
    return this.on(type, __once);
};

EventEmitter.prototype.off = function _off(type, listener,anotherloop) {
   
    let _locale=null;
    if(Array.isArray(type))
        {
            _locale=type[0];
            type=type[1];
        }
    if (!Array.isArray(this._listeners[type])) {
        return this;
    }
    if (typeof listener === 'undefined') {
        this._listeners[type] = (this._listeners[type]||[]).filter(e=>e.locale!=_locale &&_locale!=null);
        return this;
    }
    let index = this._listeners[type].map(e=>(e.locale==_locale||_locale==null)?e.listener:'`').indexOf(listener);

    if (index === -1) {
        for (let i = 0; i < this._listeners[type].length; i += 1) {
            if (this._listeners[type][i].listener === listener) {
                index = i;
                break;
            }
        }
    }
    
    if(index>-1)
    {
        this._listeners[type].splice(index, 1);
    }
    if(typeof anotherloop === 'undefined' ||anotherloop)
    {
        this.off([_locale,type],listener,false);
    }
    return this;
};

EventEmitter.prototype.clean = function _clean(_locale) {
    Object.keys(this._listeners).map(ev=>this.off([_locale,ev]));
     
     return this;
 };


EventEmitter.prototype.emit = function _emit(type) {
    let _locale=null;
    if(Array.isArray(type))
        {
            _locale=type[0];
            type=type[1];
        }
        if(this.last.locale==_locale && this.last.type==type && (new Date()-this.last.Date)<1000)
            {
                //debugger;
                return this;
            }
            
    this.last={locale:_locale,type:type,Date:new Date()};

    if (!Array.isArray(this._listeners[type])) {
        return this;
    }
    let args = [];
    for (let i = 1; i < arguments.length; i += 1) {
        args[i - 1] = arguments[i];
    }
    args=arguments;
    const _listner=this._listeners[type].filter(e=>e.locale==_locale||e.locale==null);// [...new Set(this._listeners[type].filter(e=>e.locale==_locale||e.locale==null).map(e=>e.listener))];
    if(_listner.length>1 && type.indexOf('System_')!=0){
       //console.log(_listner,type);
    }
    _listner.forEach(function __emit(listenerObj) {
        
        listenerObj.listener.apply(this, args);
    }, this);

    return this;
};

module.exports.EventEmitter = EventEmitter;