import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Web from 'material-ui/svg-icons/av/web';
import { cyan600, pink600, purple600, white } from 'material-ui/styles/colors';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

//const ExtraRoute='tsrm';
const ExtraRoute=process.env.NODE_ENV==='development'?'':'tsrm';
const AppDir=ExtraRoute?('/'+ExtraRoute):ExtraRoute;
const data = {
  AppDir:AppDir,
  AppDir2:ExtraRoute+'/',
  AppDir3:ExtraRoute?ExtraRoute:'/',
  MenuWidth:236,
  menus: [
    {
      text: 'Task', icon: <Assessment color={white} />, link: AppDir+'/Task'
      , menuItems: []
    }, {
      text: 'User Timeline', icon: <Assessment color={white} />, link:  AppDir+'/UserTimeline'
      , menuItems: []
    }, {
      text: 'Task TimeLine', icon: <Web color={white} />, link:  AppDir+'/TaskTimeLine'
      , menuItems: []
    }, {
      text: 'Overview', icon: <Web color={white} />, link:  AppDir+'/Overview'
      , menuItems: []
    }, {
      text: 'Agenda', icon: <Web color={white} />, link:  AppDir+'/Agenda'
      , menuItems: []
    }, {
      text: 'Trace', icon: <Web color={white} />, link:  AppDir+'/Trace'
      , menuItems: []
    }, {
      text: 'Setup', icon: <Web color={white} />, link: null
      , menuItems: [{
        text: 'Project', icon: <Web color={white} />, link:  AppDir+'/Project'
        , menuItems: []
      }, {
        text: 'Add User', icon: <Web color={white} />, link:  AppDir+'/User'
        , menuItems: []
      }, {
        text: 'Tag', icon: <Web color={white} />, link:  AppDir+'/Tag'
        , menuItems: []
      }]
    }, {
      text: 'Workflow Module', icon: <Web color={white} />, link: null
      , menuItems: [{
        text: 'UI Element', icon: <Web color={white} />, link:  AppDir+'/UIElement'
        , menuItems: []
      }, {
         text: 'Instantiate Workflow', icon: <Web color={white} />, link:  AppDir+'/InstantiateWorkflow'
        , menuItems: []
      }, {
         text: 'Open Workflow Instance', icon: <Web color={white} />, link:  AppDir+'/OpenWorkflowInstance'
        , menuItems: []
      }, {
        text: 'Task', icon: <Web color={white} />, link:  AppDir+'/Tasks'
       , menuItems: []
     }, {
      text: 'StateMachine', icon: <Web color={white} />, link:  AppDir+'/StateMachine'
     , menuItems: []
   }, {
    text: 'StateMachineUI', icon: <Web color={white} />, link: AppDir+'/StateMachineUI'
   , menuItems: []
 }, {
    text: 'Login', icon: <Web color={white} />, link:  AppDir+'/Login'
   , menuItems: []
 }]
    }
  ],
  taskMatrix: {
    Users: [
      { id: 1, name: "ip" },
      { id: 2, name: "faheem" },
      { id: 3, name: "raheel" },
      { id: 4, name: "noman" },
      { id: 5, name: "salman" }
    ],
    states: [
      { id: null, name: "(N/A)" },
      { id: 1, name: "TBD" },
      { id: 2, name: "Done" }
    ],
    tagCategories: [
      { id: 1, name: "State" },
      { id: 2, name: "LifeCycle" },
      { id: 3, name: "Priority" },
      { id: 4, name: "Complexity" }
    ],
    tags: [
      { id: 1, name: "Active", category: 1 },
      { id: 2, name: "InActive", category: 1 },
      { id: 3, name: "Started", category: 2 },
      { id: 4, name: "Completed", category: 2 },
      { id: 5, name: "Closed", category: 2 },
      { id: 6, name: "Paused", category: 2 },
      { id: 7, name: "Resumed", category: 2 },
      { id: 8, name: "Low", category: 3 },
      { id: 9, name: "Medium", category: 3 },
      { id: 10, name: "High", category: 3 },
      { id: 11, name: "Low", category: 4 },
      { id: 12, name: "Medium", category: 4 },
      { id: 13, name: "High", category: 4 }
    ],
    projects: [
      {
        id: 1, name: "NFL",

        iterations: [
          { id: 1, name: "v 0.1" },
          { id: 2, name: "v 0.2" },
          { id: 3, name: "v 0.3" },
          { id: 4, name: "v 1.0" }],
        tasks: [
          {
            id: 1, name: "Mock Up Management", iteration: 1, startAt: new Date(2017,8,26), EndTarget: new Date(2017,8,28), estimatedhours: 5, actualHours: 3, completion: 50, tags: [1, 3], Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["faheem", "salman"], child: [
              { id: 12, name: "App", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: [1, 3], Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 13, name: "App.Client", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 14, name: "App.Title", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["salman", "ip"], child: [] },
              { id: 15, name: "App.Url", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 16, name: "App.Users", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 17, name: "App.EffectiveFrom", iteration: [1, 3], state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 18, name: "App.EffectiveTo", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["raheel", "salman"], child: [] },
              { id: 19, name: "App.Client.Logo", iteration: [1, 3], state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 20, name: "App.Logo", iteration: [1, 3], state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["faheem", "salman", "noman"], child: [] }
            ]
          },
          {
            id: 2, name: "Basic Layout", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [
              { id: 21, name: "Button", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 22, name: "Dialog", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 23, name: "Table View", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 24, name: "Combo", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 25, name: "Page", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 26, name: "Cascading Combo", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 27, name: "Tabs", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 28, name: "AppBar", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 29, name: "Entity Form", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 30, name: "TextBox", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 31, name: "Application", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 32, name: "Page", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 33, name: "Master Layout", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 34, name: "Variable", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 35, name: "FontIcon", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 36, name: "For Each", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 37, name: "For", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 38, name: "if", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] }]
          },
          { id: 3, name: "Data", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 4, name: "Basic Data Binding", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 5, name: "Components", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 6, name: "Theme", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], tags: [1, 4], child: [] },
          { id: 7, name: "Navigation", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          {
            id: 8, name: "Mockup Tools Access", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [
              { id: 39, name: "rte.trandts.com/ide/login", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 40, name: "rte.trandts.com/Tracker/notes/MyTasks", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 41, name: "Structure: Org/Property/Instance/App/Module/Feature", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 42, name: "Structure: Trandts/Web/DefaultInstance/Demo/Designer", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 43, name: "Structure: Trandts/Web/DefaultInstance/Demo/Presenter", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 44, name: "Demo Designer/Publisher (for Presenter)", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 45, name: "rte.trandts.com/Demo/Designer/Ide", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 46, name: "Demo Designer/Generator (Standalone)", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] }]
          },
          {
            id: 9, name: "Mockup Demo", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [
              { id: 47, name: "Demo Presenter", iteration: 2, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 48, name: "rte.trandts.com/Demo/Presenter/<client>/<project>", iteration: 2, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 49, name: "Client Landing Page", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 50, name: "Users/Login", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 51, name: "Image Uploading", iteration: 3, startAt: new Date(2017, 8, 19), EndTarget: new Date(2017, 8, 21), estimatedhours: 4, actualHours: 2, completion: 80, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] }]
          },
          {
            id: 10, name: "Management Of Demos", iteration: 4, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [
              { id: 52, name: "Updation", iteration: 4, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 53, name: "Copy Pages and Demos", iteration: 4, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] }
            ]
          },
          {
            id: 11, name: "Audit", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [
              { id: 54, name: "Demo/Login/IP", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
              { id: 54, name: "Analytics", iteration: null, startAt: new Date(2017, 8, 26), EndTarget: new Date(2017, 8, 28), estimatedhours: 10, actualHours: 0, completion: 0, state: null, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] }
            ]
          }
        ]
      },
      {
        id: 2, name: "Sannofi",

        iterations: [
          { id: 1, name: "v 0.1" },
          { id: 2, name: "v 0.2" },],
        tasks: [
          { id: 111, name: "Sannofi Management", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, tags: [1, 3], Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["faheem", "salman"], child: [] },
          { id: 112, name: "Sannofi T1", iteration: 1, startAt: new Date(), EndTarget: new Date(new Date().setTime(new Date().getTime() + (2*60*60*1000))), estimatedhours: 5, actualHours: 3, completion: 50, state: [1, 3], Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 113, name: "Sannofi T2", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 114, name: "Sannofi T3", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["salman", "ip"], child: [] },
          { id: 115, name: "Sannofi T4", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 116, name: "Sannofi T5", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 117, name: "Sannofi T6", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 118, name: "Sannofi T7", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["raheel", "salman"], child: [] },
          { id: 119, name: "Sannofi T8", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 1, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: [], child: [] },
          { id: 120, name: "Sannofi T9", iiteration: [1, 3], startAt: new Date(2017, 8, 20), EndTarget: new Date(2017, 8, 22), estimatedhours: 7, actualHours: 2, completion: 20, state: 2, Assigned: [{ user: "faheem", estimatedhours: 5, actualHours: 3, completion: 50 }, { user: "salman", estimatedhours: 6, actualHours: 2, completion: 30 }], users: ["faheem", "salman", "noman"], child: [] }
        ]
      }
    ]
  },
  tablePage: {
    items: [
      { id: 1, Product: 'Selsun Blue', Manager: 'Nadeem A Khan' },
      { id: 2, Product: 'Enterogermina', Manager: 'Ahsen Zeeshan' },
      { id: 3, Product: 'NO SPA', Manager: 'Rumman Khan' },
      { id: 4, Product: 'FLAGYL', Manager: 'Owais Qureshi' },
      { id: 5, Product: 'Amaryl', Manager: 'Raheel Jamal' },
      { id: 6, Product: 'Lantus', Manager: 'Danish Tariq' },
      { id: 7, Product: 'Plavix', Manager: 'Zaeem Amin' },
      { id: 8, Product: 'Taxotere', Manager: 'Hassam Ahmed' },
      { id: 9, Product: 'Tritace and Aprovel', Manager: 'Anas Godil' },
      { id: 10, Product: 'Tarivid', Manager: 'Samar Chhotani' },
      { id: 11, Product: 'Claforan', Manager: 'Rustam Irani' },
      { id: 12, Product: 'Aventriax', Manager: 'Muzammil Malik' },
      { id: 13, Product: '', Manager: 'Salina' },
    ]
  },
  surveyPage: {
    items: [
      { id: 1, question: 'Alignment with brand plan' },
      { id: 2, question: 'Quality and effectiveness of scientific information to target audience' },
      { id: 3, question: 'Strength of value proposition (Product USP / differentiation vs competitor)' },
      { id: 4, question: 'Overall Impression' }
      /* { id: 1, question: 'Effectiveness of Sales' },
       { id: 2, question: 'Preparedness' },
       { id: 3, question: 'Value Proposition' },
       { id: 4, question: 'Overall Impression' },
       { id: 5, question: 'Product 5' },
       { id: 6, question: 'Product 6' },*/
    ]
  },
  dashBoardPage: {
    recentProducts: [
      { id: 1, title: 'Selsun Blue', text: 'Ghufran Akhtar Score:5' },
      { id: 2, title: 'Lantus', text: 'Atif Khan Score:5' },
      { id: 3, title: 'Claforan', text: 'Amir Waheed Score:5' },
      { id: 4, title: 'Taxotere', text: 'Ahmad Iqbal Score:5' }
    ],
    monthlySales: [
      { name: '2015', uv: 60 },
      { name: '2016', uv: 76 },
      { name: '2017', uv: 88 }
    ],
    monthlySales2: [
      { name: 'Highly Satisfied', uv: 100 },
      { name: 'Satisfied', uv: 70 },
      { name: 'Moderate', uv: 50 },
      { name: 'Not Satisfied', uv: 60 },
    ],
    newOrders: [
      { pv: 2400 },
      { pv: 1398 },
      { pv: 9800 },
      { pv: 3908 },
      { pv: 4800 },
      { pv: 3490 },
      { pv: 4300 }
    ],
    browserUsage: [
      { name: 'Selsun Blue', value: 800, color: cyan600, icon: <ExpandMore /> },
      { name: 'Plavix', value: 300, color: pink600, icon: <ChevronRight /> },
      { name: 'Amaryl', value: 300, color: purple600, icon: <ExpandLess /> }
    ]
  },
  DummyData: {
    products: [
      { key: 1, label: "Selsun Blue" },
      { key: 2, label: "Enterogermina" },
      { key: 3, label: "NO SPA" },
      { key: 4, label: "FLAGYL" },
      { key: 5, label: "Amaryl" },
      { key: 6, label: "Lantus" },
      { key: 7, label: "Plavix" },
      { key: 8, label: "Taxotere" },
      { key: 9, label: "Tritace and Aprovel" },
      { key: 10, label: "Tarivid" },
      { key: 11, label: "Claforan" },
      { key: 12, label: "Aventriax" },
    ]
    , question: [
      { key: 1, label: 'I feel I am being paid a fair amount for the work I do.' },
      { key: 2, label: 'There is really too little chance for promotion on my job.' },
      { key: 3, label: 'My supervisor is quite competent in doing his/her job.' },
      { key: 4, label: 'I am not satisfied with the benefits I receive.' },
      { key: 5, label: 'When I do a good job, I receive the recognition for it that I should receive.' },
      { key: 6, label: 'Many of our rules and procedures make doing a good job difficult.' },
      { key: 7, label: 'I like the people I work with.' },
      { key: 8, label: 'I sometimes feel my job is meaningless.' },
      { key: 9, label: 'Communications seem good within this organization.' },
      { key: 10, label: 'Raises are too few and far between.' },
      { key: 11, label: 'Those who do well on the job stand a fair chance of being promoted.' },
      { key: 12, label: 'My supervisor is unfair to me.' },
      { key: 13, label: 'The benefits we receive are as good as most other organizations offer.' },
      { key: 14, label: 'My supervisor is unfair to me.' },
      { key: 15, label: 'I do not feel that the work I do is appreciated.' },
      { key: 16, label: 'My efforts to do a good job are seldom blocked by red tape.' },
      { key: 17, label: 'I find I have to work harder at my job because of the incompetence of people I work with.' },
      { key: 18, label: 'I like doing the things I do at work.' },
      { key: 19, label: 'The goals of this organization are not clear to me.' }
    ]
    , directors: [
      { key: 0, label: 'Laila Khan' },
      { key: 1, label: 'Zubair Rizvi' },
      { key: 2, label: 'Amir Waheed' },
      { key: 3, label: 'Ghufran Akhtar' },
      { key: 3, label: 'Amjad' },
      { key: 3, label: 'Tariq Khan' },
      { key: 3, label: 'Shakeel Mapara' },
      { key: 3, label: 'Salman Shamim' },
      { key: 3, label: 'Atif Khan' },
      { key: 3, label: 'Ahmad Iqbal' },
      { key: 3, label: 'Alia Aziz' },
      { key: 3, label: 'Asim Jamal' },
      { key: 3, label: 'Saifullah' },
      { key: 3, label: 'Hira Rizvi' },
      { key: 3, label: 'Irfan Siddiqui' },
      { key: 3, label: 'Asif Ali' },
      { key: 3, label: 'Nadim' },
    ]
  }
};

export default data;
