import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../webpack.config.dev';
import url from 'url';
import proxy from 'proxy-middleware';

const bundler = webpack(config);
let arr=["/api","/LoginService","/LoginCheck","/tsrm","/GetUSM","/CurrentUI","/GetUserSM","/LogoutService","/SessionCheck","/signalr","/scripts","/bundles"];
let arrProxy=[];
for (let i=0;i<arr.length;i++)
  {
    let _arr=arr[i];
    let  proxyOptions = url.parse('http://localhost:60980'+_arr);
    proxyOptions.route = _arr;
    arrProxy[arrProxy.length]=proxy(proxyOptions);
  }

browserSync({
  port: 3000,
  ui: {
    port: 3001
  },
  server: {
    baseDir: 'src',

    middleware: arrProxy.concat([
      historyApiFallback(),
      webpackDevMiddleware(bundler, {
        publicPath: config.output.publicPath,

        noInfo: false,
        quiet: false,
        stats: {
          assets: false,
          colors: true,
          version: false,
          hash: false,
          timings: false,
          chunks: false,
          chunkModules: false
        },
      }),

      webpackHotMiddleware(bundler)
    ])
  },

  files: [
    'src/*.html'
  ]
});
